using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations; 
namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    public partial class ExamCenterModel
    {
        public int Id { get; set; }

        public int StateId { get; set; }

        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        [StringLength(150)]
        public string Address { get; set; }

        public bool IsActive { get; set; }

        public bool IsApproved { get; set; }

        [StringLength(50)]
        public string ContactPersonName { get; set; }

        [StringLength(50)]
        public string ContactPersonPhone { get; set; } 
    }
    public class ExamCenterItem : ExamCenterModel
    {
        public DateTime DateCreated { get; set; }
        public string StateName { get; set; } 
    }
    public class ExamCenterFilter : ExamCenterItem
    {
        public DateTime? DateCreatedFrom { get; set; }
        public DateTime? DateCreatedTo { get; set; }

        public static ExamCenterFilter Deserialize(string whereCondition)
        {
            ExamCenterFilter filter = null;
            if (!String.IsNullOrEmpty(whereCondition))
            {
                filter = JsonConvert.DeserializeObject<ExamCenterFilter>(whereCondition);
            }
            return filter;
        }
    }
}
