using Newtonsoft.Json;
using System; 
using System.ComponentModel.DataAnnotations.Schema;
namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    
    public  class ExamDietTimeTableModel
    {
        public int Id { get; set; }

        public int ExamId { get; set; }

        public TimeSpan StartTime { get; set; }

        public TimeSpan EndTime { get; set; }

        public int ExamDietId { get; set; }

        public int FacultyId { get; set; }

        public int ExamTypeId { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }
        
    }
    public class ExamDietTimeTableItem : ExamDietTimeTableModel
    {
        public DateTime DateCreated { get; set; }
        public string ExamName { get; set; }
        public string ExamDietName { get; set; }
        public string FacultyName { get; set; }
        public string ExamTypeName { get; set; }
    }
    public class ExamDietTimeTableFilter : ExamDietTimeTableItem
    {
        public static ExamDietTimeTableFilter Deserialize(string whereCondition)
        {
            ExamDietTimeTableFilter filter = null;
            if (!String.IsNullOrEmpty(whereCondition))
            {
                filter = JsonConvert.DeserializeObject<ExamDietTimeTableFilter>(whereCondition);
            }
            return filter;
        }
    }
}
