﻿using System;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models
{
  
    public class MemoThreadModel
    {
        public int Id { get; set; }

        public int RecipientId { get; set; }
       
        public DateTime? DateForwarded { get; set; }

        public string Comment { get; set; }

        public DateTime DateCreated { get; set; }

        public string AttachedFile { get; set; }
        public int MemoId { get; set; }

    }

    public partial class MemoThreadItem : MemoThreadModel
    {
        public string RecipientName { get; set; }
        public string ForwardedBy { get; set; }
        public bool IsForwarded { get; set; }
    }

    public class MemoThreadDetail : MemoThreadItem
    {

    }
    public partial class MemoThreadFilter : MemoThreadItem
    {

        public static MemoThreadFilter Deserilize(string whereCondition)
        {
            MemoThreadFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<MemoThreadFilter>(whereCondition);
            }
            return filter;
        }
    }
}
