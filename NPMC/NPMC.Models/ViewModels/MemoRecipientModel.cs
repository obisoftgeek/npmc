﻿using System;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models
{
  
    public class MemoRecipientModel
    {
        public int Id { get; set; }

        public int MemoId { get; set; }
        public int RecipientId { get; set; }
        public int ReadStatus { get; set; }
        public bool IsForwarded { get; set; }
        public int? ForwardedBy { get; set; }
        public DateTime? DateForwarded { get; set; }

    }

    public partial class MemoRecipientItem : MemoRecipientModel
    {
        public string Name { get; set; }
        public string ProfilePhoto { get; set; }
        public string ForwardedByName { get; set; }
    }

    public class MemoMemoRecipientDetail : MemoRecipientItem
    {

    }
    public partial class MemoRecipientFilter : MemoRecipientItem
    {
      
        public int UserId { get; set; }
        public int FolderType { get; set; }
        public static MemoRecipientFilter Deserilize(string whereCondition)
        {
            MemoRecipientFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<MemoRecipientFilter>(whereCondition);
            }
            return filter;
        }
    }
}
