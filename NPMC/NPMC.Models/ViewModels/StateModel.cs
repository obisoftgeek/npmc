using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
    public class StateModel
    {        
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }
    }

    public class StateItem : StateModel
    {
        public DateTime CreatedDate { get; set; }
    }

    public class StateFilter : StateItem
    {
        public static StateFilter Deserilize(string whereCondition)
        {
            StateFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<StateFilter>(whereCondition);
            }
            return filter;
        }

        public DateTime? CreatedDateFrom { get; set; }
        public DateTime? CreatedDateTo { get; set; }
    }
}
