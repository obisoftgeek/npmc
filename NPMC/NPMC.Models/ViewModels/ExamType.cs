using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    
    public partial class ExamTypeModel
    {
         
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public bool IsActive { get; set; } 

        public int ExamId { get; set; }
        
    }
    public class ExamTypeItem : ExamTypeModel
    {
        public DateTime CreatedDate { get; set; }
        public string ExamName { get; set; }
    }
    public class ExamTypeFilter : ExamTypeItem
    {
        public static ExamTypeFilter Deserialize(string whereCondition)
        {
            ExamTypeFilter filter = null;
            if (!String.IsNullOrEmpty(whereCondition))
            {
                filter = JsonConvert.DeserializeObject<ExamTypeFilter>(whereCondition);
            }
            return filter;
        }
    }
}
