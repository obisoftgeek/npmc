using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
   

   
    public  class WorkshopModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        [Required]
        [StringLength(50)]
        public string Venue { get; set; }

       
        public DateTime StartDate { get; set; }

       
        public DateTime EndDate { get; set; }

        public int PaymentFeeId { get; set; }


        public int CreatedBy { get; set; }

        public bool IsApproved { get; set; }

        public int? ApprovedBy { get; set; }

        
    }

    public class WorkshopItem :WorkshopModel
    {
        public DateTime CreatedDate { get; set; }
        public decimal AmountPayable { get; set; }
    }

    public class WorkshopFilter : WorkshopItem
    {
        public static WorkshopFilter Deserilize(string whereCondition)
        {
            WorkshopFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<WorkshopFilter>(whereCondition);
            }
            return filter;
        }

        public DateTime? CreatedDateFrom { get; set; }
        public DateTime? CreatedDateTo { get; set; }
    }
}
