﻿using System;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
  
    public class UnitModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public int DepartmentId { get; set; }
    }

    public partial class UnitItem : UnitModel
    {
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }

    public class UnitDetail : UnitItem
    {

    }
    public partial class UnitFilter : UnitItem
    {

        public static UnitFilter Deserilize(string whereCondition)
        {
            UnitFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<UnitFilter>(whereCondition);
            }
            return filter;
        }
    }
}
 