using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
    [Table("AuditTrail")]
    public partial class AuditTrailModel
    {
        public long Id { get; set; }

        [StringLength(250)]
        public string UserIP { get; set; }

        public string Details { get; set; }

        public DateTime Timestamp { get; set; }

        public int? AuditActionId { get; set; }

        [StringLength(250)]
        public string OtherRefId { get; set; }

        public bool IsMobile { get; set; }

        [StringLength(250)]
        public string BrowserName { get; set; }

        public long UserProfileId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class AuditTrailItem : AuditTrailModel
    {
        public string AuditSection { get; set; }
        public string AuditAction { get; set; }
        public string UserName { get; set; }
        public int? AuditSectionId { get; set; }
    }

    public class AuditTrailFilter : AuditTrailItem
    {
        public DateTime? DateCreatedFrom { get; set; }
        public DateTime? DateCreatedTo { get; set; }

        public static AuditTrailFilter Deserilize(string whereCondition)
        {
            AuditTrailFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<AuditTrailFilter>(whereCondition);
            }
            return filter;
        }
    }
    public class AuditTrailReport:AuditTrailFilter
    {
    
    }
}
