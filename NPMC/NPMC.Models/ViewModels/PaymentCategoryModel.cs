using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
    public class PaymentCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsApproved { get; set; }
        public int CreatedBy { get; set; }
    }
    public class PaymentCategoryItem : PaymentCategoryModel
    {
        public DateTime DateCreated { get; set; } 
    }
    public class PaymentCategoryFilter : PaymentCategoryItem
    {
        public static PaymentCategoryFilter Deserilize(string whereCondition)
        {
            PaymentCategoryFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<PaymentCategoryFilter>(whereCondition);
            }
            return filter;
        }
        public DateTime? DateCreatedFrom { get; set; }
        public DateTime? DateCreatedTo { get; set; }
    }
}
