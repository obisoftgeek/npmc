using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
    public  class PaymentFeeModel
    {
        public int Id { get; set; }

        public int PaymentCategoryId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public bool IsActive { get; set; }

        public decimal Amount { get; set; }

        public int AccountId { get; set; }

        public int? DueDay { get; set; }

        public int? DueMonth { get; set; }

        public int CreatedBy { get; set; }

        public bool IsApproved { get; set; }

        public int? AprovedBy { get; set; }
    }
    public class PaymentFeeItem : PaymentFeeModel
    {
        public DateTime DateCreated { get; set; }
        public string FormattedAmount { get; set; }
    }
    public class PaymentFeeFilter : PaymentFeeItem
    {
        public static PaymentFeeFilter Deserilize(string whereCondition)
        {
            PaymentFeeFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<PaymentFeeFilter>(whereCondition);
            }
            return filter;
        }
        public DateTime? DateCreatedFrom { get; set; }
        public DateTime? DateCreatedTo { get; set; }
    }


}
