﻿using System;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
  
    public class CourseModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public int ExamId { get; set; }
        public bool IsActive { get; set; }
    }

    public partial class CourseItem : CourseModel
    {
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }

    public class CourseDetail : CourseItem
    {

    }
    public partial class CourseFilter : CourseItem
    {

        public static CourseFilter Deserilize(string whereCondition)
        {
            CourseFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<CourseFilter>(whereCondition);
            }
            return filter;
        }

        
    }
}
 
