using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 
namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
   
    public partial class ExamStageModel
    {
        public int Id { get; set; }

        public int ExamId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; } 

        public bool IsActive { get; set; }

        public bool IsApproved { get; set; }
        public List<int> ExamCenterIds { get; set; }
    }
    public class ExamStageItem : ExamStageModel
    {
        public DateTime DateCreated { get; set; }
        public string ExamName { get; set; }
        public List<ExamStageCenterItem> Centers { get; set; }
    }
    public class ExamStageFilter : ExamStageItem
    {
        public static ExamStageFilter Deserialize(string whereCondition)
        {
            ExamStageFilter filter = null;
            if (!String.IsNullOrEmpty(whereCondition))
            {
                filter = JsonConvert.DeserializeObject<ExamStageFilter>(whereCondition);
            }
            return filter;
        }
    }
    public class ExamStageCenterModel
    {
        public int Id { get; set; }

        public int ExamStageId { get; set; }

        public int ExamCenterId { get; set; }
    }
    public class ExamStageCenterItem: ExamStageCenterModel
    {
        public string ExamCenterName { get; set; }
        public string CenterAddress { get; set; }
        public string CenterState { get; set; }
    }
}
