﻿using System;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
  
    public class ExamRequirementModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public int ExamId { get; set; }
        public bool IsActive { get; set; }
    }

    public partial class ExamRequirementItem : ExamRequirementModel
    {
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }

    public class ExamRequirementDetail : ExamRequirementItem
    {

    }
    public partial class ExamRequirementFilter : ExamRequirementItem
    {

        public static ExamRequirementFilter Deserilize(string whereCondition)
        {
            ExamRequirementFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<ExamRequirementFilter>(whereCondition);
            }
            return filter;
        }

        
    }
}
 
