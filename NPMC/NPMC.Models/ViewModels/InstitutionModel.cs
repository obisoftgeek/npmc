using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
    public class InstitutionModel
    {       
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Code { get; set; }

        public int ContactPersonId { get; set; }

        public int StateId { get; set; }

        [StringLength(50)]
        public string Address { get; set; }

        [StringLength(50)]
        public string Website { get; set; }

        public int? AccreditationStatusId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LastAccreditationDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? NextDueDate { get; set; }

        public DateTime DateCreated { get; set; }

        public int? YearEstablished { get; set; }

        public int CreatedBy { get; set; }        
    }

    public class InstitutionItem : InstitutionModel
    {
        public DateTime CreatedDate { get; set; }
    }

    public class InstitutionFilter : InstitutionItem
    {
        public static InstitutionFilter Deserilize(string whereCondition)
        {
            InstitutionFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<InstitutionFilter>(whereCondition);
            }
            return filter;
        }

        public DateTime? CreatedDateFrom { get; set; }
        public DateTime? CreatedDateTo { get; set; }
    }
}
