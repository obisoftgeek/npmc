using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
     
    public  class SubSpecialtyModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }
        [Required]

        public int FacultyId { get; set; }
        [Required]
        public int PaymentFeeId { get; set; }

        public bool IsActive { get; set; }

        public bool IsApproved { get; set; }

        public int? ApprovedBy { get; set; }
        public int PaymentCategoryId { get; set; }
         
    }
    public class SubSpecialtyItem : SubSpecialtyModel
    {
        public DateTime DateCreated { get; set; }
        public decimal AmountPayable { get; set; }
        public string FacultyName { get; set; }
    }
    public class SubSpecialtyFilter : SubSpecialtyItem
    {
        public static SubSpecialtyFilter Deserilize(string whereCondition)
        {
            SubSpecialtyFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<SubSpecialtyFilter>(whereCondition);
            }
            return filter;
        }

        public DateTime? DateCreatedFrom { get; set; }
        public DateTime? DateCreatedTo { get; set; }
    }
}
