﻿using System;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
  
    public class DepartmentModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

    }

    public partial class DepartmentItem : DepartmentModel
    {
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }

    public class DepartmentDetail : DepartmentItem
    {

    }
    public partial class DepartmentFilter : DepartmentItem
    {

        public static DepartmentFilter Deserilize(string whereCondition)
        {
            DepartmentFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<DepartmentFilter>(whereCondition);
            }
            return filter;
        }
    }
}
 
