﻿namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
    public class TextAndValue
    {
        public string Name { get; set; }
        public int Value { get; set; }
        
    }
}
