﻿using System;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
  
    public class UserProfileModel : AspNetUserModel
    {
        public int AspNetUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OtherNames { get; set; }
        public string FullName { get; set; }
        public string FirstAndLastName { get; set; }
        public string Gender { get; set; }
        public bool IsActive { get; set; }
        public bool MustChangePassword { get; set; }
        public int DepartmentId { get; set; }
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public string DepartmentName { get; set; }
    }

    public partial class UserProfileItem : UserProfileModel
    {
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }

    public class UserProfileDetail : UserProfileItem
    {

    }
    public partial class UserProfileFilter : UserProfileItem
    {

        public static UserProfileFilter Deserilize(string whereCondition)
        {
            UserProfileFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<UserProfileFilter>(whereCondition);
            }
            return filter;
        }
    }
}
