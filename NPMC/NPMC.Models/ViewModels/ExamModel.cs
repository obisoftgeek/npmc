﻿using System;
using Newtonsoft.Json;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using System.Collections.Generic;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
  
    public class ExamModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
        
        public bool IsActive { get; set; }
        public List<ExamStageModel> StageModels { get; set; }
        public List<ExamTypeModel> TypeModels { get; set; }
    }

    public partial class ExamItem : ExamModel
    {
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public List<ExamStageItem> Stages { get; set; }
    }
     
    public partial class ExamFilter : ExamItem
    {

        public static ExamFilter Deserilize(string whereCondition)
        {
            ExamFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<ExamFilter>(whereCondition);
            }
            return filter;
        }

        
    }
}
 
