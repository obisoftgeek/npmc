using System.ComponentModel.DataAnnotations;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
    public  class AuditActionModel
    {
        public AuditActionModel(){}

        public int Id { get; set; }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }
        public int AuditSectionId { get; set; }
    }
}
