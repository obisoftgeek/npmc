using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
    [Table("AuditSection")]
    public partial class AuditSectionModel
    {

        public int Id { get; set; }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        public virtual ICollection<AuditActionModel> AuditActions { get; set; }
    }
}
