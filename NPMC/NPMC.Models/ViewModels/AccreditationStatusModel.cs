using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
  
    public class AccreditationStatusModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public int NumberOfYear { get; set; }
    }

    public class AccreditationStatusItem : AccreditationStatusModel
    {
        public DateTime DateCreated { get; set; }
    }

    public class AccreditationStatusFilter : AccreditationStatusItem
    {

        public static AccreditationStatusFilter Deserilize(string whereCondition)
        {
            AccreditationStatusFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<AccreditationStatusFilter>(whereCondition);
            }
            return filter;
        }

        public DateTime? DateCreatedFrom { get; set; }
        public DateTime? DateCreatedTo { get; set; }
    }
}
