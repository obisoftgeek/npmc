using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    
     
    public class ResultUploadModel
    {
        public int Id { get; set; }

        public int ExamDietId { get; set; }

        public int ExamId { get; set; }

        [Required]
        [StringLength(150)]
        public string Upload { get; set; }

        [Required]
        [StringLength(150)]
        public string ScannedCopy { get; set; }

        public DateTime DateUploaded { get; set; }

        public int? UploadedBy { get; set; } 
    }
    public class ResultUploadItem : ResultUploadModel
    {
        public DateTime CreatedDate { get; set; }
        public string ExamName { get; set; }
    }
    public class ResultUploadFilter : ResultUploadItem
    {
        public static ResultUploadFilter Deserialize(string whereCondition)
        {
            ResultUploadFilter filter = null;
            if (!String.IsNullOrEmpty(whereCondition))
            {
                filter = JsonConvert.DeserializeObject<ResultUploadFilter>(whereCondition);
            }
            return filter;
        }
    }
}
