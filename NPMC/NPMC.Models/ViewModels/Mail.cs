using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace SilverEdgeProjects.NPMC.Models.ViewModels
{

    public class MailModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(150)]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        public string Attachment { get; set; }

        public int StatusId { get; set; }

        public int RecepientId { get; set; }
        public string Sender { get; set; }
        public string SenderAddress { get; set; }

    }
    public class MailItem:MailModel
    {
        public DateTime DateCreated { get; set; }
        public string StatusName { get; set; }
        public string RecepientName { get; set; }
        public string UnitName { get; set; }
        public List<AttachmentModel> AttachmentModel { get; set; }
        public bool HasAttachment { get; set; }
    }
    public class MailFilter:MailItem
    {
        public DateTime? DateCreatedFrom { get; set; }
        public DateTime? DateCreatedTo { get; set; }

        public static MailFilter Deserilize(string whereCondition)
        {
            MailFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<MailFilter>(whereCondition);
            }
            return filter;
        }
    }
    public class AttachmentModel
    {
        public string FileTypeName { get; set; }
        public string  FileName { get; set; }
        public int FileTypeId { get; set; }

    }
}
