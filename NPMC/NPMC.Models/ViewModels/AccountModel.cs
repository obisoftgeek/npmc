using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
    
    public class AccountModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Number { get; set; }
        public bool IsActive { get; set; }
        public bool IsApproved { get; set; }
    }
    public  class AccountItem : AccountModel
    {
        public DateTime DateCreated { get; set; } 
    }
    public  class AccountFilter : AccountItem
    {
        public static AccountFilter Deserilize(string whereCondition)
        {
            AccountFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<AccountFilter>(whereCondition);
            }
            return filter;
        }

        public DateTime? DateCreatedFrom { get; set; }
        public DateTime? DateCreatedTo { get; set; }
        
    }
}
