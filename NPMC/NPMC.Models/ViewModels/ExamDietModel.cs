﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
  
    public class ExamDietModel
    {
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime DateCreated { get; set; }

        public decimal DefermentPercentage { get; set; }

        public bool IsActive { get; set; }

        public bool IsApproved { get; set; }

        public int StatusId { get; set; }
        public List<ExamDietPaymentModel> Payments { get; set; }
    }

    public partial class ExamDietItem : ExamDietModel
    {
    }

    
    public partial class ExamDietFilter : ExamDietItem
    {

        public static ExamDietFilter Deserilize(string whereCondition)
        {
            ExamDietFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<ExamDietFilter>(whereCondition);
            }
            return filter;
        }       
    }
    public class ExamDietPaymentModel
    {
        public int Id { get; set; }

        public int DietId { get; set; }

        public int PaymentFeeId { get; set; }

        public int ExamId { get; set; }

        public decimal Amount { get; set; }
    }
}
 
