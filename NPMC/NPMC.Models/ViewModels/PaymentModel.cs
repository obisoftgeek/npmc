﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
    public class PaymentModel
    {
        public long Id { get; set; }
        public System.DateTime TransactionDate { get; set; }
        public System.Guid TransactionId { get; set; }
        public string ResponseTransactionId { get; set; }
        public decimal Amount { get; set; }
        public long UserProfileId { get; set; }
    }

    public class PaymentItem : PaymentModel
    {

    }

    public class PaymentFilter : PaymentItem
    {
        public static PaymentFilter Deserilize(string whereCondition)
        {
            PaymentFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<PaymentFilter>(whereCondition);
            }
            return filter;
        }

        public DateTime? TransactionDateFrom { get; set; }
        public DateTime? TransactionDateTo { get; set; }
        public DateTime? PaymentDateFrom { get; set; }
        public DateTime? PaymentDateTo { get; set; }
        public bool HasPaid { get; set; }
    }

    public class PaymentBreakDown
    {
        public int PaymentId { get; set; }
        public int Quantity { get; set; }
        public decimal UnitCost { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public decimal Total
        {
            get { return UnitCost * Quantity; }
        }
    }
    public class PaymentDetail
    {

        public PaymentDetail()
        {
            Payments = new List<PaymentBreakDown>();
        }

        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string InvoiceId { get; set; }
        public decimal Total { get; set; }
        public List<PaymentBreakDown> Payments { get; set; }
        public string ReferenceId { get; set; }
    }

     

    public class PaymentNotificationModel
    {
        public string Payee { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public decimal Total { get; set; }
        public string InvoiceId { get; set; }
        public string Item { get; set; }
        public DateTime PaymentDate { get; set; }
        public string ReferenceId { get; set; }
    }
     
}
