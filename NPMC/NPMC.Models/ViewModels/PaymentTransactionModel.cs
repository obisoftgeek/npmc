using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
    public class PaymentTransactionModel
    {
        public int Id { get; set; }

        public int UserProfileId { get; set; }
        public string Narration { get; set; }

        public DateTime? PaymentDate { get; set; }

        public int? PaymentType { get; set; }

        public decimal Amount { get; set; }

        public decimal? AmountPaid { get; set; }

        public string ReferenceId { get; set; }

        public bool HasPaid { get; set; }

        public int AccountId { get; set; } 
    }
    public class PaymentTransactionItem : PaymentTransactionModel
    {
        public DateTime DateCreated { get; set; }
        public string AccountName { get; set; }
        public string Payee { get; set; }
    }

    public class PaymentTransactionFilter : PaymentTransactionItem
    {
        public static PaymentTransactionFilter Deserialize(string whereCondition)
        {
            var filter = new PaymentTransactionFilter();
            if (!String.IsNullOrEmpty(whereCondition))
            {
                filter = JsonConvert.DeserializeObject<PaymentTransactionFilter>(whereCondition);
            }
            return filter;
        }

        public DateTime? PaymentDateFrom { get; set; }
        public DateTime? PaymentDateTo { get; set; }
    }
}
