 ﻿using System;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
  
    public class AspNetUserModel
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        public string PasswordHash { get; set; }

        public string SecurityStamp { get; set; }

        public string PhoneNumber { get; set; }

        public bool PhoneNumberConfirmed { get; set; }

        public bool TwoFactorEnabled { get; set; }

        public DateTime? LockoutEndDateUtc { get; set; }

        public bool LockoutEnabled { get; set; }

        public int AccessFailedCount { get; set; }

        public string UserName { get; set; }

    }

    public partial class AspNetUserItem : AspNetUserModel
    {
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }

    public class AspNetUserDetail : AspNetUserItem
    {

    }
    public partial class AspNetUserFilter : AspNetUserItem
    {

        public static AspNetUserFilter Deserilize(string whereCondition)
        {
            AspNetUserFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<AspNetUserFilter>(whereCondition);
            }
            return filter;
        }
    }
}
 