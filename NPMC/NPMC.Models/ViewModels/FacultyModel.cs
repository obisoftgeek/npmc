using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
    
    public class FacultyModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Code { get; set; }

        public int PaymentFeeId { get; set; }
         
        public bool IsActive { get; set; }

        public int CreatedBy { get; set; }

        public bool IsApproved { get; set; }

        public int? ApprovedBy { get; set; } 
        public int PaymentCategoryId { get; set; } 
    }
    public  class FacultyItem : FacultyModel
    {
        public DateTime DateCreated { get; set; }
        public decimal AmountPayable { get; set; }
        public int SubSpecialtyCount { get; set; }
    }
    public  class FacultyFilter : FacultyItem
    {
        public static FacultyFilter Deserilize(string whereCondition)
        {
            FacultyFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<FacultyFilter>(whereCondition);
            }
            return filter;
        }

        public DateTime? DateCreatedFrom { get; set; }
        public DateTime? DateCreatedTo { get; set; }
    }
}
