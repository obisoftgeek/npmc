﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SilverEdgeProjects.NPMC.Models.ViewModels
{
    public class SelectedRecipientModel
    {
        public int Id { get; set; }

        public int MemoId { get; set; }
       
    }
    public class MemoModel
    {
        public int Id { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public int MemoStatusId { get; set; }
        public int FolderType { get; set; }
        public int MemoBoxType { get; set; }
        public string AttachedFile { get; set; }
        public DateTime DateCreated { get; set; }
        public int SenderId { get; set; }
        public List<SelectedRecipientModel> SelectedRecipients { get; set; }
    }
    public class ForwardMemoModel
    {
       
        public int MemoId { get; set; }
        public string Comment { get; set; }
        public List<SelectedRecipientModel> SelectedRecipients { get; set; }

    }
    public partial class MemoItem : MemoModel
    {
        public int TotalRecipients { get; set; }
        public string SenderName { get; set; }
        public string SenderUnitName { get; set; }
        public string SenderDesignationName { get; set; }
        public string MemoStatusName { get; set; }     
        public string SenderProfilePhoto { get; set; }
        public string SenderFirstName { get; set; }
        public string SenderLastName { get; set; }
        public string SenderDepartmentName { get; set; }
        public bool HasBeenRead { get; set; }

    }

    public class MemoDetail : MemoItem
    {
        public List<MemoRecipientItem> MemoRecipients { get; set; }
        public List<MemoThreadItem> MemoThreads { get; set; }
    }
    public partial class MemoFilter : MemoItem
    {
        
        public int UserId { get; set; }
        public static MemoFilter Deserilize(string whereCondition)
        {
            MemoFilter filter = null;
            if (whereCondition != null)
            {
                filter = JsonConvert.DeserializeObject<MemoFilter>(whereCondition);
            }
            return filter;
        }
    }
}
