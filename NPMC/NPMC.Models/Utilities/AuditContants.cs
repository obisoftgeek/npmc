﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilverEdgeProjects.NPMC.Models.Utilities
{
    public class AuditConstants
    {
        public const string UserManagementSection = "User Management";
        public const string ApprovalProcessSection = "Approval Proccess";   
        public const string EmailSection = "Email Section";
        public const string SmsSection = "Sms Section"; 
        public const string PaymentSection = "Payment Section";
        public static string OtherUserActivitiesSection = "Other User Activities Section";
        public static string SetupSection = "Setup Section";
        public const string ActivatingUserAccountAction = "Activating User Account";
        public const string  DeactivatingUserAccountAction = "Deactivating User Account";
        public const string CreationOfNewUserAccountAction = "Creation of New User Account";
        public const string EditingExistingUserAccountAction = "Editing Existing User Account";
        public const string LoginToTheSystemAction = "Login to The System";
        public const string ApprovalProcessAction = "Approval Process"; 
        public const string DeletingOfUserAccountAction = "Deleting User Account";  
        public const string EmailAction = "Email Action";
        public const string SmsAction = "Email Action"; 
        public const string PaymentAction = "Payment Action";
        public static string OtherUserActivitiesAction = "Other User Activities Action"; 
        public static string SetupAction = "Setup Action";
        public static string LogoutFromTheSystemAction = "Logout From The System";

        public static string DocumentManagementSection = "Document Management Section";
        public static string DocumentManagementAction = "Document Management Action";

    }

    public class StringUtility
    {
        public static char NairaSymbol
        {
            get { return (char) 8358; }
        }
    }    
}
