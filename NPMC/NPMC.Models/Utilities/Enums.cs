﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilverEdgeProjects.NPMC.Models
{
    public enum UserUnit
    {
        Ict = 1,
        Transport = 2,
        DrAcademics = 3,
        Exam = 4,
        Library = 5,
        Museum = 6,
        Biomedical = 7,
        Tam = 8,
        Registrar = 9,
        Finance = 13,
        Candidate = 14,
        Fellow = 17,
        Institition = 19,
        Sgba = 20,
        President = 21

    }
    public enum MemoStatusEnum
    {
        Draft = 1,
        InProgress = 2,
        Rejected = 3,
        Closed = 4,
        Approved = 5
    }
    public enum MemoFolderType
    {
        MemoBox = 1,
        Draft = 2    
    }
    public enum MemoBoxType
    {
        InBox = 1,
        SentBox = 2
    }
    public enum UserProfileTypeEnum
    {
        StaffUser = 1,
        Candidate = 2
       
    }
    public enum MailReceiptStatus
    {
        Dispatched=1,
        New=2,
        Received=3
    }

    public class MailReceiptStatusConstant
    {
        public static Dictionary<int, string> MailReceiptStatusNamesFromIdDictionary = new Dictionary<int, string>()
        {
            {1,"Dispatched"},{2, "New"}, {3, "Received"}
        };
    }

    public enum MailAttachmentFileType
    {
        Image=1,
        Pdf=2,
        Doc=3,
        Xls =4
    }
    public class MailAttachmentFileTypeConstant
    {
        public static Dictionary<int, string> MailAttachmentFileTypeNamesFromIdDictionary = new Dictionary<int, string>()
        {
            {1,"Image"},{2, "Pdf"}, {3, "Doc"},{4,"Excel"}
        };
    }
}
