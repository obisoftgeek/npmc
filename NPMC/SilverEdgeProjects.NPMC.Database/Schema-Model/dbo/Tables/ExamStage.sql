CREATE TABLE [dbo].[ExamStage]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ExamId] [int] NOT NULL,
[Name] [nvarchar] (100) NOT NULL,
[DateCreated] [datetime] NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[ExamStage] ADD CONSTRAINT [PK_ExamStage] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[ExamStage] ADD CONSTRAINT [FK_ExamStage_Exam] FOREIGN KEY ([ExamId]) REFERENCES [dbo].[Exam] ([Id])
GO
