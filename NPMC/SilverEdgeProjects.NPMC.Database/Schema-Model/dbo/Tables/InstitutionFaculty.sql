CREATE TABLE [dbo].[InstitutionFaculty]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[InstitutionId] [int] NOT NULL,
[FacultyId] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[InstitutionFaculty] ADD CONSTRAINT [PK_InstitutionFaculty] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[InstitutionFaculty] ADD CONSTRAINT [FK_InstitutionFaculty_Faculty] FOREIGN KEY ([FacultyId]) REFERENCES [dbo].[Faculty] ([Id])
GO
ALTER TABLE [dbo].[InstitutionFaculty] ADD CONSTRAINT [FK_InstitutionFaculty_Institution] FOREIGN KEY ([InstitutionId]) REFERENCES [dbo].[Institution] ([Id])
GO
