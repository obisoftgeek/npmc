CREATE TABLE [dbo].[PaymentCategory]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (80) NOT NULL,
[Description] [nvarchar] (150) NULL,
[DateCreated] [datetime] NOT NULL,
[CreatedBy] [int] NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[PaymentCategory] ADD CONSTRAINT [PK_PaymentCategory] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[PaymentCategory] ADD CONSTRAINT [FK_PaymentCategory_UserProfile] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
