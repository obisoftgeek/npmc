CREATE TABLE [dbo].[AuditSection]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (250) NOT NULL
)
GO
ALTER TABLE [dbo].[AuditSection] ADD CONSTRAINT [PK_AuditSections] PRIMARY KEY CLUSTERED  ([Id])
GO
