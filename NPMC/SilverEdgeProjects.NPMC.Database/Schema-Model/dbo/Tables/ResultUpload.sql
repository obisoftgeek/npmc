CREATE TABLE [dbo].[ResultUpload]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ExamDietId] [int] NOT NULL,
[ExamId] [int] NOT NULL,
[Upload] [nvarchar] (150) NOT NULL,
[ScannedCopy] [nvarchar] (150) NOT NULL,
[DateUploaded] [datetime] NOT NULL,
[UploadedBy] [int] NULL
)
GO
ALTER TABLE [dbo].[ResultUpload] ADD CONSTRAINT [PK_ResultUpload] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[ResultUpload] ADD CONSTRAINT [FK_ResultUpload_Exam] FOREIGN KEY ([ExamId]) REFERENCES [dbo].[Exam] ([Id])
GO
ALTER TABLE [dbo].[ResultUpload] ADD CONSTRAINT [FK_ResultUpload_ExamDiet] FOREIGN KEY ([ExamDietId]) REFERENCES [dbo].[ExamDiet] ([Id])
GO
