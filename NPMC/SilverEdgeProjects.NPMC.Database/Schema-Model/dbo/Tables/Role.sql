CREATE TABLE [dbo].[Role]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[UnitId] [int] NOT NULL,
[CratedDate] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[Role] ADD CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[Role] ADD CONSTRAINT [FK_Role_Unit] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[Unit] ([Id])
GO
