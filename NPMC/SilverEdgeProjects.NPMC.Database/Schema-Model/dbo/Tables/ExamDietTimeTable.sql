CREATE TABLE [dbo].[ExamDietTimeTable]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ExamId] [int] NOT NULL,
[StartTime] [time] NOT NULL,
[EndTime] [time] NOT NULL,
[ExamDietId] [int] NOT NULL,
[FacultyId] [int] NOT NULL,
[ExamTypeId] [int] NOT NULL,
[Date] [date] NOT NULL
)
GO
ALTER TABLE [dbo].[ExamDietTimeTable] ADD CONSTRAINT [PK_ExamDietTimeTable] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[ExamDietTimeTable] ADD CONSTRAINT [FK_ExamDietTimeTable_Exam] FOREIGN KEY ([ExamId]) REFERENCES [dbo].[Exam] ([Id])
GO
ALTER TABLE [dbo].[ExamDietTimeTable] ADD CONSTRAINT [FK_ExamDietTimeTable_ExamDiet] FOREIGN KEY ([ExamDietId]) REFERENCES [dbo].[ExamDiet] ([Id])
GO
ALTER TABLE [dbo].[ExamDietTimeTable] ADD CONSTRAINT [FK_ExamDietTimeTable_ExamType] FOREIGN KEY ([ExamTypeId]) REFERENCES [dbo].[ExamType] ([Id])
GO
ALTER TABLE [dbo].[ExamDietTimeTable] ADD CONSTRAINT [FK_ExamDietTimeTable_Faculty] FOREIGN KEY ([FacultyId]) REFERENCES [dbo].[Faculty] ([Id])
GO
