CREATE TABLE [dbo].[AspNetRoles]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) NULL,
[Name] [nvarchar] (256) NOT NULL
)
GO
ALTER TABLE [dbo].[AspNetRoles] ADD CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED  ([Id])
GO
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles] ([Name])
GO
