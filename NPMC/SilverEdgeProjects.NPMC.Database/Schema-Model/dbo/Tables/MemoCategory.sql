CREATE TABLE [dbo].[MemoCategory]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL
)
GO
ALTER TABLE [dbo].[MemoCategory] ADD CONSTRAINT [PK_MemoType] PRIMARY KEY CLUSTERED  ([Id])
GO
