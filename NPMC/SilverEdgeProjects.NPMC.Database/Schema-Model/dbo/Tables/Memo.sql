CREATE TABLE [dbo].[Memo]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Subject] [nvarchar] (50) NOT NULL,
[Body] [nvarchar] (max) NOT NULL,
[DateCreated] [datetime] NOT NULL,
[SenderId] [int] NOT NULL,
[MemoStatusId] [int] NOT NULL,
[FolderType] [int] NOT NULL,
[AttachedFile] [nvarchar] (128) NULL
)
GO
ALTER TABLE [dbo].[Memo] ADD CONSTRAINT [PK_Memo] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[Memo] ADD CONSTRAINT [FK_Memo_MemoCategory] FOREIGN KEY ([MemoStatusId]) REFERENCES [dbo].[MemoStatus] ([Id])
GO
ALTER TABLE [dbo].[Memo] ADD CONSTRAINT [FK_Memo_UserProfile] FOREIGN KEY ([SenderId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
