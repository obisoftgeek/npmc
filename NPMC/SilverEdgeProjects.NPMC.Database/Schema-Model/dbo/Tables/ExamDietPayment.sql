CREATE TABLE [dbo].[ExamDietPayment]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[DietId] [int] NOT NULL,
[PaymentFeeId] [int] NOT NULL,
[ExamId] [int] NOT NULL,
[Amount] [decimal] (18, 2) NOT NULL
)
GO
ALTER TABLE [dbo].[ExamDietPayment] ADD CONSTRAINT [PK_DietPayment] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[ExamDietPayment] ADD CONSTRAINT [FK_DietPayment_Diet] FOREIGN KEY ([DietId]) REFERENCES [dbo].[ExamDiet] ([Id])
GO
ALTER TABLE [dbo].[ExamDietPayment] ADD CONSTRAINT [FK_DietPayment_PaymentFee] FOREIGN KEY ([PaymentFeeId]) REFERENCES [dbo].[PaymentFee] ([Id])
GO
ALTER TABLE [dbo].[ExamDietPayment] ADD CONSTRAINT [FK_ExamDietPayment_Exam] FOREIGN KEY ([ExamId]) REFERENCES [dbo].[Exam] ([Id])
GO
