CREATE TABLE [dbo].[Unit]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[DepartmentId] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[Unit] ADD CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[Unit] ADD CONSTRAINT [FK_Unit_Department] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[Department] ([Id])
GO
