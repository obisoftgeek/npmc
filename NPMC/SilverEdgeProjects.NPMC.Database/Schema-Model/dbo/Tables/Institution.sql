CREATE TABLE [dbo].[Institution]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Code] [nvarchar] (50) NOT NULL,
[ContactPersonId] [int] NOT NULL,
[StateId] [int] NOT NULL,
[Address] [nvarchar] (50) NULL,
[Website] [nvarchar] (50) NULL,
[AccreditationStatusId] [int] NULL,
[LastAccreditationDate] [date] NULL,
[NextDueDate] [date] NULL,
[DateCreated] [datetime] NOT NULL,
[YearEstablished] [int] NULL,
[CreatedBy] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[Institution] ADD CONSTRAINT [PK_Institution] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[Institution] ADD CONSTRAINT [FK_Institution_AccreditationStatus] FOREIGN KEY ([AccreditationStatusId]) REFERENCES [dbo].[AccreditationStatus] ([Id])
GO
ALTER TABLE [dbo].[Institution] ADD CONSTRAINT [FK_Institution_State] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
GO
ALTER TABLE [dbo].[Institution] ADD CONSTRAINT [FK_Institution_UserProfile] FOREIGN KEY ([ContactPersonId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
ALTER TABLE [dbo].[Institution] ADD CONSTRAINT [FK_Institution_UserProfile1] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
