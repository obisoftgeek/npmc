CREATE TABLE [dbo].[ExamCenter]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[StateId] [int] NOT NULL,
[Name] [nvarchar] (150) NOT NULL,
[Address] [nvarchar] (150) NULL,
[DateCreated] [datetime] NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL,
[ContactPersonName] [nvarchar] (50) NULL,
[ContactPersonPhone] [nvarchar] (50) NULL
)
GO
ALTER TABLE [dbo].[ExamCenter] ADD CONSTRAINT [PK_ExamCenter] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[ExamCenter] ADD CONSTRAINT [FK_ExamCenter_State] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
GO
