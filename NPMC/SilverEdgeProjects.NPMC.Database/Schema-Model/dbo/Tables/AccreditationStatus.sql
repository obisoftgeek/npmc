CREATE TABLE [dbo].[AccreditationStatus]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[NumberOfYear] [int] NOT NULL,
[DateCreated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[AccreditationStatus] ADD CONSTRAINT [PK_AccreditationApprovalStatus] PRIMARY KEY CLUSTERED  ([Id])
GO
