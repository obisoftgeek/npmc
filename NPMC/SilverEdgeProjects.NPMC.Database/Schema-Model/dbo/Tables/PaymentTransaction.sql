CREATE TABLE [dbo].[PaymentTransaction]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UserProfileId] [int] NOT NULL,
[PaymentDate] [datetime] NOT NULL,
[PaymentType] [int] NULL,
[Amount] [decimal] (18, 2) NOT NULL,
[AmountPaid] [decimal] (18, 2) NULL,
[ReferenceId] [nvarchar] (50) NOT NULL,
[HasPaid] [bit] NOT NULL,
[AccountId] [int] NOT NULL,
[Narration] [nvarchar] (150) NOT NULL,
[InvoiceId] [nvarchar] (50) NOT NULL
)
GO
ALTER TABLE [dbo].[PaymentTransaction] ADD CONSTRAINT [PK_AFASPayment] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[PaymentTransaction] ADD CONSTRAINT [FK_PaymentTransaction_Account] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[PaymentTransaction] ADD CONSTRAINT [FK_PaymentTransaction_UserProfile] FOREIGN KEY ([UserProfileId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
