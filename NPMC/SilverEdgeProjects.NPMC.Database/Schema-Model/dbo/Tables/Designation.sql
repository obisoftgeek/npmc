CREATE TABLE [dbo].[Designation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[DateCreated] [datetime] NOT NULL,
[CreatedBy] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[Designation] ADD CONSTRAINT [PK_Designation] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[Designation] ADD CONSTRAINT [FK_Designation_UserProfile] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
