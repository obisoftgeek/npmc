CREATE TABLE [dbo].[UserRole]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UserProfileId] [int] NOT NULL,
[RoleId] [int] NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL,
[AssignedBy] [int] NOT NULL,
[AssignedDate] [datetime] NOT NULL,
[ApprovedDate] [datetime] NULL,
[ApprovedBy] [int] NULL
)
GO
ALTER TABLE [dbo].[UserRole] ADD CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[UserRole] ADD CONSTRAINT [FK_UserRole_Role] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[UserRole] ADD CONSTRAINT [FK_UserRole_UserProfile] FOREIGN KEY ([UserProfileId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
