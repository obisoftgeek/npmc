CREATE TABLE [dbo].[MemoRecipient]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[MemoId] [int] NOT NULL,
[RecipientId] [int] NOT NULL,
[ReadStatus] [int] NOT NULL,
[IsForwarded] [bit] NOT NULL,
[ForwardedBy] [int] NULL,
[DateForwarded] [datetime] NULL
)
GO
ALTER TABLE [dbo].[MemoRecipient] ADD CONSTRAINT [PK_MemoComment] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[MemoRecipient] ADD CONSTRAINT [FK_MemoRecipient_Memo] FOREIGN KEY ([MemoId]) REFERENCES [dbo].[Memo] ([Id]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MemoRecipient] ADD CONSTRAINT [FK_MemoRecipient_MemoRecipient] FOREIGN KEY ([ForwardedBy]) REFERENCES [dbo].[MemoRecipient] ([Id])
GO
ALTER TABLE [dbo].[MemoRecipient] ADD CONSTRAINT [FK_MemoRecipient_UserProfile] FOREIGN KEY ([RecipientId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
