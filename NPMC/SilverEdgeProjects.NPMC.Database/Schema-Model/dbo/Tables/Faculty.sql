CREATE TABLE [dbo].[Faculty]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Code] [nvarchar] (50) NOT NULL,
[PaymentFeeId] [int] NOT NULL,
[DateCreated] [datetime] NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL,
[ApprovedBy] [int] NULL
)
GO
ALTER TABLE [dbo].[Faculty] ADD CONSTRAINT [PK_Faculty] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[Faculty] ADD CONSTRAINT [FK_Faculty_PaymentFee] FOREIGN KEY ([PaymentFeeId]) REFERENCES [dbo].[PaymentFee] ([Id])
GO
ALTER TABLE [dbo].[Faculty] ADD CONSTRAINT [FK_Faculty_UserProfile1] FOREIGN KEY ([ApprovedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
