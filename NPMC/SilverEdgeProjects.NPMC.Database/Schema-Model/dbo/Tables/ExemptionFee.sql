CREATE TABLE [dbo].[ExemptionFee]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ExamId] [int] NOT NULL,
[PaymentFeeId] [int] NOT NULL,
[Description] [nvarchar] (50) NULL,
[DateCreated] [datetime] NOT NULL,
[CreatedBy] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ExemptionFee] ADD CONSTRAINT [PK_ExemptionFee] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[ExemptionFee] ADD CONSTRAINT [FK_ExemptionFee_Exam] FOREIGN KEY ([ExamId]) REFERENCES [dbo].[Exam] ([Id])
GO
ALTER TABLE [dbo].[ExemptionFee] ADD CONSTRAINT [FK_ExemptionFee_PaymentFee] FOREIGN KEY ([PaymentFeeId]) REFERENCES [dbo].[PaymentFee] ([Id])
GO
ALTER TABLE [dbo].[ExemptionFee] ADD CONSTRAINT [FK_ExemptionFee_UserProfile] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
