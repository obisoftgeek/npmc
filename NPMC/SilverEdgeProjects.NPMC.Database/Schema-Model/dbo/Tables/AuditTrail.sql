CREATE TABLE [dbo].[AuditTrail]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[UserIP] [nvarchar] (250) NULL,
[Details] [nvarchar] (max) NULL,
[Timestamp] [datetime] NOT NULL,
[AuditActionId] [int] NULL,
[IsMobile] [bit] NOT NULL,
[BrowserName] [nvarchar] (250) NULL,
[UserProfileId] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[AuditTrail] ADD CONSTRAINT [PK_AuditTrails] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[AuditTrail] ADD CONSTRAINT [FK_AuditTrail_AuditAction] FOREIGN KEY ([AuditActionId]) REFERENCES [dbo].[AuditAction] ([Id])
GO
ALTER TABLE [dbo].[AuditTrail] ADD CONSTRAINT [FK_AuditTrail_UserProfile] FOREIGN KEY ([UserProfileId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
