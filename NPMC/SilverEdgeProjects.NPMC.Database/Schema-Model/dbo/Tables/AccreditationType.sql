CREATE TABLE [dbo].[AccreditationType]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL
)
GO
ALTER TABLE [dbo].[AccreditationType] ADD CONSTRAINT [PK_AccreditationType] PRIMARY KEY CLUSTERED  ([Id])
GO
