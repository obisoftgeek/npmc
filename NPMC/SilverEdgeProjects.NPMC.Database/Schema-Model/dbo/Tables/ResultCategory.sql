CREATE TABLE [dbo].[ResultCategory]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL
)
GO
ALTER TABLE [dbo].[ResultCategory] ADD CONSTRAINT [PK_PassCategory] PRIMARY KEY CLUSTERED  ([Id])
GO
