CREATE TABLE [dbo].[Workshop]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[Venue] [nvarchar] (50) NOT NULL,
[StartDate] [date] NOT NULL,
[EndDate] [date] NOT NULL,
[PaymentFeeId] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[CreatedBy] [int] NOT NULL,
[IsApproved] [bit] NOT NULL,
[ApprovedBy] [int] NULL
)
GO
ALTER TABLE [dbo].[Workshop] ADD CONSTRAINT [PK_Workshop] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[Workshop] ADD CONSTRAINT [FK_Workshop_PaymentFee] FOREIGN KEY ([PaymentFeeId]) REFERENCES [dbo].[PaymentFee] ([Id])
GO
ALTER TABLE [dbo].[Workshop] ADD CONSTRAINT [FK_Workshop_UserProfile] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
ALTER TABLE [dbo].[Workshop] ADD CONSTRAINT [FK_Workshop_UserProfile1] FOREIGN KEY ([ApprovedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
