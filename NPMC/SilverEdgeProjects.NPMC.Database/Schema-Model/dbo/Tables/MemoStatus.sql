CREATE TABLE [dbo].[MemoStatus]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL
)
GO
ALTER TABLE [dbo].[MemoStatus] ADD CONSTRAINT [PK_MemoStatus] PRIMARY KEY CLUSTERED  ([Id])
GO
