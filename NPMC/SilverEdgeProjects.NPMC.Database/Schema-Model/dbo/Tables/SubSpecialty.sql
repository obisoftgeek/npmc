CREATE TABLE [dbo].[SubSpecialty]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (100) NOT NULL,
[FacultyId] [int] NOT NULL,
[PaymentFeeId] [int] NOT NULL,
[DateCreated] [datetime] NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL,
[ApprovedBy] [int] NULL,
[Description] [nvarchar] (max) NULL
)
GO
ALTER TABLE [dbo].[SubSpecialty] ADD CONSTRAINT [PK_SubSpecialty] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[SubSpecialty] ADD CONSTRAINT [FK_SubSpecialty_Faculty] FOREIGN KEY ([FacultyId]) REFERENCES [dbo].[Faculty] ([Id])
GO
ALTER TABLE [dbo].[SubSpecialty] ADD CONSTRAINT [FK_SubSpecialty_PaymentFee] FOREIGN KEY ([PaymentFeeId]) REFERENCES [dbo].[PaymentFee] ([Id])
GO
ALTER TABLE [dbo].[SubSpecialty] ADD CONSTRAINT [FK_SubSpecialty_UserProfile] FOREIGN KEY ([ApprovedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
