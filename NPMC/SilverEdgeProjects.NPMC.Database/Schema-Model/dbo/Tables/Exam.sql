CREATE TABLE [dbo].[Exam]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (150) NULL,
[IsActive] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[Exam] ADD CONSTRAINT [PK_Exams] PRIMARY KEY CLUSTERED  ([Id])
GO
