CREATE TABLE [dbo].[ExamType]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[IsActive] [bit] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ExamId] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ExamType] ADD CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[ExamType] ADD CONSTRAINT [FK_ExamType_Exam] FOREIGN KEY ([ExamId]) REFERENCES [dbo].[Exam] ([Id])
GO
