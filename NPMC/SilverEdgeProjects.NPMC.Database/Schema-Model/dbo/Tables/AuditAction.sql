CREATE TABLE [dbo].[AuditAction]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (250) NOT NULL,
[AuditSectionId] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[AuditAction] ADD CONSTRAINT [PK_AuditActions] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[AuditAction] ADD CONSTRAINT [FK_AuditAction_AuditSection] FOREIGN KEY ([AuditSectionId]) REFERENCES [dbo].[AuditSection] ([Id])
GO
