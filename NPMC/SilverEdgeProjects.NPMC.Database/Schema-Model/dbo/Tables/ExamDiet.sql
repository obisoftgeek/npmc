CREATE TABLE [dbo].[ExamDiet]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (100) NOT NULL,
[Description] [nvarchar] (100) NULL,
[StartDate] [date] NOT NULL,
[EndDate] [date] NOT NULL,
[DateCreated] [datetime] NOT NULL,
[DefermentPercentage] [decimal] (18, 2) NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL CONSTRAINT [DF_Diet_IsApproved] DEFAULT ((0)),
[StatusId] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ExamDiet] ADD CONSTRAINT [PK_Diet] PRIMARY KEY CLUSTERED  ([Id])
GO
