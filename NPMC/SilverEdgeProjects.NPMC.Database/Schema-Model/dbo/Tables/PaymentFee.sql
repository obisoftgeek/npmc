CREATE TABLE [dbo].[PaymentFee]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[PaymentCategoryId] [int] NOT NULL,
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[DateCreated] [datetime] NOT NULL,
[IsActive] [bit] NOT NULL,
[Amount] [decimal] (18, 2) NOT NULL,
[AccountId] [int] NOT NULL,
[DueDay] [int] NULL,
[DueMonth] [int] NULL,
[IsApproved] [bit] NOT NULL,
[AprovedBy] [int] NULL
)
GO
ALTER TABLE [dbo].[PaymentFee] ADD CONSTRAINT [PK_PaymentFee] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[PaymentFee] ADD CONSTRAINT [FK_PaymentFee_Account] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[PaymentFee] ADD CONSTRAINT [FK_PaymentFee_PaymentCategory] FOREIGN KEY ([PaymentCategoryId]) REFERENCES [dbo].[PaymentCategory] ([Id])
GO
ALTER TABLE [dbo].[PaymentFee] ADD CONSTRAINT [FK_PaymentFee_UserProfile1] FOREIGN KEY ([AprovedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
