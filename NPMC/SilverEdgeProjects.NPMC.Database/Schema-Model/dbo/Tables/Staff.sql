CREATE TABLE [dbo].[Staff]
(
[UserProfileId] [int] NOT NULL,
[StaffId] [nvarchar] (50) NULL,
[DesignationId] [int] NULL,
[Title] [nvarchar] (50) NULL
)
GO
ALTER TABLE [dbo].[Staff] ADD CONSTRAINT [PK_Staff] PRIMARY KEY CLUSTERED  ([UserProfileId])
GO
ALTER TABLE [dbo].[Staff] ADD CONSTRAINT [FK_Staff_Designation] FOREIGN KEY ([DesignationId]) REFERENCES [dbo].[Designation] ([Id])
GO
ALTER TABLE [dbo].[Staff] ADD CONSTRAINT [FK_Staff_UserProfile] FOREIGN KEY ([UserProfileId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId]) ON DELETE CASCADE
GO
