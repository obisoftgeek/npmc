CREATE TABLE [dbo].[UserProfile]
(
[AspNetUserId] [int] NOT NULL,
[LastName] [nvarchar] (50) NULL,
[FirstName] [nvarchar] (50) NULL,
[OtherNames] [nvarchar] (50) NULL,
[Gender] [nvarchar] (50) NULL,
[UnitId] [int] NOT NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_UserProfile_IsActvie] DEFAULT ((1)),
[MustChangePassword] [bit] NOT NULL CONSTRAINT [DF_UserProfile_MustChangePassword] DEFAULT ((1)),
[IsHeadOfDept] [bit] NOT NULL CONSTRAINT [DF_UserProfile_IsHeadOfDept] DEFAULT ((0)),
[IsHeadOfUnit] [bit] NOT NULL CONSTRAINT [DF_UserProfile_IsHeadOfUnit] DEFAULT ((0)),
[ProfilePhoto] [nvarchar] (50) NULL
)
GO
ALTER TABLE [dbo].[UserProfile] ADD CONSTRAINT [PK_UserProfile] PRIMARY KEY CLUSTERED  ([AspNetUserId])
GO
ALTER TABLE [dbo].[UserProfile] ADD CONSTRAINT [FK_UserProfile_AspNetUsers] FOREIGN KEY ([AspNetUserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[UserProfile] ADD CONSTRAINT [FK_UserProfile_Unit] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[Unit] ([Id])
GO
