CREATE TABLE [dbo].[Department]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_Department_IsActive] DEFAULT ((1)),
[DateCreated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[Department] ADD CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED  ([Id])
GO
