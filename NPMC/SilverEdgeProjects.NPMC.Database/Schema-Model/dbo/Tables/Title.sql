CREATE TABLE [dbo].[Title]
(
[Id] [int] NOT NULL,
[Name] [nvarchar] (50) NULL
)
GO
ALTER TABLE [dbo].[Title] ADD CONSTRAINT [PK_Title] PRIMARY KEY CLUSTERED  ([Id])
GO
