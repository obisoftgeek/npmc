CREATE TABLE [dbo].[Mail]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (150) NOT NULL,
[Description] [nvarchar] (max) NOT NULL,
[Attachment] [nvarchar] (max) NULL,
[DateCreated] [datetime] NOT NULL,
[StatusId] [int] NOT NULL,
[RecepientId] [int] NOT NULL,
[Sender] [nvarchar] (150) NOT NULL,
[SenderAddress] [nvarchar] (150) NULL
)
GO
ALTER TABLE [dbo].[Mail] ADD CONSTRAINT [PK_Mail] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[Mail] ADD CONSTRAINT [FK_Mail_UserProfile] FOREIGN KEY ([RecepientId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
