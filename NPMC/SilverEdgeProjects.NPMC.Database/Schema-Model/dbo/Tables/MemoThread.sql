CREATE TABLE [dbo].[MemoThread]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[MemoId] [int] NOT NULL,
[RecipientId] [int] NOT NULL,
[Comment] [nvarchar] (max) NOT NULL,
[DateCreated] [datetime] NOT NULL,
[AttachedFile] [nvarchar] (128) NULL
)
GO
ALTER TABLE [dbo].[MemoThread] ADD CONSTRAINT [PK_MemoThread] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[MemoThread] ADD CONSTRAINT [FK_MemoThread_Memo] FOREIGN KEY ([MemoId]) REFERENCES [dbo].[Memo] ([Id])
GO
ALTER TABLE [dbo].[MemoThread] ADD CONSTRAINT [FK_MemoThread_MemoRecipient] FOREIGN KEY ([RecipientId]) REFERENCES [dbo].[MemoRecipient] ([Id]) ON DELETE CASCADE
GO
