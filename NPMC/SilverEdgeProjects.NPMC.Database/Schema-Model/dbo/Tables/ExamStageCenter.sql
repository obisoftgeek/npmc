CREATE TABLE [dbo].[ExamStageCenter]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ExamStageId] [int] NOT NULL,
[ExamCenterId] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ExamStageCenter] ADD CONSTRAINT [PK_ExamStageCenter] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[ExamStageCenter] ADD CONSTRAINT [FK_ExamStageCenter_ExamCenter] FOREIGN KEY ([ExamCenterId]) REFERENCES [dbo].[ExamCenter] ([Id])
GO
ALTER TABLE [dbo].[ExamStageCenter] ADD CONSTRAINT [FK_ExamStageCenter_ExamStage] FOREIGN KEY ([ExamStageId]) REFERENCES [dbo].[ExamStage] ([Id]) ON DELETE CASCADE
GO
