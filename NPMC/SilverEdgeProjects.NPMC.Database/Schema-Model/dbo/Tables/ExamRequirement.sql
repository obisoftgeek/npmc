CREATE TABLE [dbo].[ExamRequirement]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[DateCreated] [datetime] NOT NULL,
[ExamId] [int] NULL,
[IsActive] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[ExamRequirement] ADD CONSTRAINT [PK_ExamRequirement] PRIMARY KEY CLUSTERED  ([Id])
GO
ALTER TABLE [dbo].[ExamRequirement] ADD CONSTRAINT [FK_ExamRequirement_Exam] FOREIGN KEY ([ExamId]) REFERENCES [dbo].[Exam] ([Id])
GO
