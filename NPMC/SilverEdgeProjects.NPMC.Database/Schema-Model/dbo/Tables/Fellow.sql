CREATE TABLE [dbo].[Fellow]
(
[UserProfileId] [int] NOT NULL,
[Title] [nvarchar] (50) NULL
)
GO
ALTER TABLE [dbo].[Fellow] ADD CONSTRAINT [FK_Fellow_UserProfile] FOREIGN KEY ([UserProfileId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
