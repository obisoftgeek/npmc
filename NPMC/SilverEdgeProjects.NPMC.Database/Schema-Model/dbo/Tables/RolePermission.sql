CREATE TABLE [dbo].[RolePermission]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[RoleId] [int] NOT NULL,
[ControllerId] [int] NOT NULL,
[CanView] [bit] NULL,
[CanCreate] [bit] NULL,
[CanModify] [bit] NULL,
[CanApprove] [bit] NULL,
[CreatedDate] [datetime] NULL,
[CreatedBy] [int] NULL,
[IsApproved] [bit] NOT NULL,
[ApprovedBy] [int] NOT NULL,
[ApprovedDate] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[RolePermission] ADD CONSTRAINT [PK_RolePermission] PRIMARY KEY CLUSTERED  ([Id])
GO
