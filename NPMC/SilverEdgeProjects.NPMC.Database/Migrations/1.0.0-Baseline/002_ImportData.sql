﻿-- <Migration ID="53c616f2-02f2-46d7-832e-a45df7a13b6c" />
GO

SET DATEFORMAT YMD;


GO
IF (SELECT COUNT(*)
    FROM   [dbo].[AspNetRoles]) = 0
    BEGIN
        PRINT (N'Add 1 row to [dbo].[AspNetRoles]');
        SET IDENTITY_INSERT [dbo].[AspNetRoles] ON;
        INSERT  INTO [dbo].[AspNetRoles] ([Id], [Description], [Name])
        VALUES                          (1, NULL, N'Admin');
        SET IDENTITY_INSERT [dbo].[AspNetRoles] OFF;
    END


GO
IF (SELECT COUNT(*)
    FROM   [dbo].[AspNetUsers]) = 0
    BEGIN
        PRINT (N'Add 6 rows to [dbo].[AspNetUsers]');
        SET IDENTITY_INSERT [dbo].[AspNetUsers] ON;
        INSERT  INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName])
        VALUES                          (1, N'admin@NPMCDemoDb.com', 1, N'ALu/Mk5Pk9eiS2f9rw2uVhxEoZx49oHZnc1wbVC3dUKJJY2uG40/CE/5zzKBB246iA==', N'47c77290-ccb0-4067-bd5d-e1d9e37fc703', N'081751255329', 0, 0, NULL, 0, 0, N'admin@NPMCDemoDb.com');
        INSERT  INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName])
        VALUES                          (2, N'austin@silveredgeprojects.com', 1, N'AL6Ifx4N3+Qf5VkbWEKVLJ+VA+IUMQrU+8x2syyNidAKju0EJYul7Fn6zYv7NTUROg==', N'06e12026-d53f-4e42-b698-04c0fe440333', N'254863369', 0, 0, NULL, 1, 0, N'austin@silveredgeprojects.com');
        INSERT  INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName])
        VALUES                          (3, N'femi@gmail.com', 1, N'AEnbg2842fi0Hh7ejUoK73wKENQhgWfwkAVneDVH1UiW2R/fXuO7ig+8WEq11vsoAA==', N'04dd2957-2ab0-4bf7-8060-45a0c8cbd9e1', N'08142578965', 0, 0, NULL, 1, 0, N'femi@gmail.com');
        INSERT  INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName])
        VALUES                          (4, N'tam@NPMCDemoDb.com', 1, N'AIZtQEevVU2/D5K3dO1MgVlU2B3/y/mtro2V0mZw+k8QJfnWSpxn7fHA/tK9RCIfaA==', N'108301bb-afbc-43ae-bcfd-d02ea866d007', N'08062445707', 0, 0, NULL, 1, 0, N'tam@NPMCDemoDb.com');
        INSERT  INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName])
        VALUES                          (5, N'favorsurpises@gmail.com', 1, N'AMj6pBM5uf8aic+a0IJ5v313nGcUe6tumPv2wJoYwRCxH4o7aJkF6tZgUQybpEhUFQ==', N'1127ab41-99ce-4f03-a73f-8cbac1ba5c50', N'0803232323232', 0, 0, NULL, 1, 0, N'favorsurpises@gmail.com');
        INSERT  INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName])
        VALUES                          (6, N'registrar@NPMCDemoDb.com', 1, N'AD3P2rzXU1PWpk9Nwq5hZK4cyJcjuvnvvYVy3633o9ET/FrjrL2AW04DmmdLRPlOrg==', N'c3636d2a-ff9b-4a79-ad9e-c4061766e996', N'2323232323232', 0, 0, NULL, 1, 0, N'registrar@NPMCDemoDb.com');
        SET IDENTITY_INSERT [dbo].[AspNetUsers] OFF;
    END


GO
IF (SELECT COUNT(*)
    FROM   [dbo].[Department]) = 0
    BEGIN
        PRINT (N'Add 6 rows to [dbo].[Department]');
        SET IDENTITY_INSERT [dbo].[Department] ON;
        INSERT  INTO [dbo].[Department] ([Id], [Name], [Description], [IsActive], [DateCreated])
        VALUES                         (2, N'Works And Planning', NULL, 1, '1900-04-23 00:00:00.000');
        INSERT  INTO [dbo].[Department] ([Id], [Name], [Description], [IsActive], [DateCreated])
        VALUES                         (4, N'Academic', NULL, 1, '1900-04-23 00:00:00.000');
        INSERT  INTO [dbo].[Department] ([Id], [Name], [Description], [IsActive], [DateCreated])
        VALUES                         (5, N'SGBA', NULL, 1, '1900-04-23 00:00:00.000');
        INSERT  INTO [dbo].[Department] ([Id], [Name], [Description], [IsActive], [DateCreated])
        VALUES                         (6, N'Registry', NULL, 1, '1900-04-23 00:00:00.000');
        INSERT  INTO [dbo].[Department] ([Id], [Name], [Description], [IsActive], [DateCreated])
        VALUES                         (7, N'Presidents', NULL, 1, '1900-04-23 00:00:00.000');
        INSERT  INTO [dbo].[Department] ([Id], [Name], [Description], [IsActive], [DateCreated])
        VALUES                         (9, N'Others', NULL, 0, '1900-04-23 00:00:00.000');
        SET IDENTITY_INSERT [dbo].[Department] OFF;
    END


GO
IF (SELECT COUNT(*)
    FROM   [dbo].[AspNetUserRoles]) = 0
    BEGIN
        PRINT (N'Add 1 row to [dbo].[AspNetUserRoles]');
        INSERT  INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId])
        VALUES                              (1, 1);
    END


GO
IF (SELECT COUNT(*)
    FROM   [dbo].[Unit]) = 0
    BEGIN
        PRINT (N'Add 14 rows to [dbo].[Unit]');
        SET IDENTITY_INSERT [dbo].[Unit] ON;
        INSERT  INTO [dbo].[Unit] ([Id], [Name], [Description], [DepartmentId])
        VALUES                   (1, N'ICT', NULL, 6);
        INSERT  INTO [dbo].[Unit] ([Id], [Name], [Description], [DepartmentId])
        VALUES                   (2, N'Transport', NULL, 2);
        INSERT  INTO [dbo].[Unit] ([Id], [Name], [Description], [DepartmentId])
        VALUES                   (3, N'DR Academics', NULL, 4);
        INSERT  INTO [dbo].[Unit] ([Id], [Name], [Description], [DepartmentId])
        VALUES                   (4, N'Exam', NULL, 4);
        INSERT  INTO [dbo].[Unit] ([Id], [Name], [Description], [DepartmentId])
        VALUES                   (5, N'Library', NULL, 4);
        INSERT  INTO [dbo].[Unit] ([Id], [Name], [Description], [DepartmentId])
        VALUES                   (6, N'Museum', NULL, 4);
        INSERT  INTO [dbo].[Unit] ([Id], [Name], [Description], [DepartmentId])
        VALUES                   (7, N'Biomedical', NULL, 4);
        INSERT  INTO [dbo].[Unit] ([Id], [Name], [Description], [DepartmentId])
        VALUES                   (8, N'TAM', NULL, 4);
        INSERT  INTO [dbo].[Unit] ([Id], [Name], [Description], [DepartmentId])
        VALUES                   (9, N'Registrar', NULL, 6);
        INSERT  INTO [dbo].[Unit] ([Id], [Name], [Description], [DepartmentId])
        VALUES                   (13, N'Finance', NULL, 6);
        INSERT  INTO [dbo].[Unit] ([Id], [Name], [Description], [DepartmentId])
        VALUES                   (14, N'Candidate', NULL, 9);
        INSERT  INTO [dbo].[Unit] ([Id], [Name], [Description], [DepartmentId])
        VALUES                   (15, N'Associate', NULL, 9);
        INSERT  INTO [dbo].[Unit] ([Id], [Name], [Description], [DepartmentId])
        VALUES                   (17, N'Fellow', NULL, 9);
        INSERT  INTO [dbo].[Unit] ([Id], [Name], [Description], [DepartmentId])
        VALUES                   (19, N'Institution', NULL, 9);
        SET IDENTITY_INSERT [dbo].[Unit] OFF;
    END


GO
IF (SELECT COUNT(*)
    FROM   [dbo].[UserProfile]) = 0
    BEGIN
        PRINT (N'Add 6 rows to [dbo].[UserProfile]');
        INSERT  INTO [dbo].[UserProfile] ([AspNetUserId], [LastName], [FirstName], [OtherNames], [Gender], [UnitId], [IsActive], [MustChangePassword], [IsHeadOfDept], [IsHeadOfUnit], [ProfilePhoto])
        VALUES                          (1, N'Akpan', N'Bassey', N'', N'M', 1, 1, 0, 0, 0, NULL);
        INSERT  INTO [dbo].[UserProfile] ([AspNetUserId], [LastName], [FirstName], [OtherNames], [Gender], [UnitId], [IsActive], [MustChangePassword], [IsHeadOfDept], [IsHeadOfUnit], [ProfilePhoto])
        VALUES                          (2, N'Adebayo', N'Moses', N'Peter', N'M', 13, 1, 0, 0, 0, NULL);
        INSERT  INTO [dbo].[UserProfile] ([AspNetUserId], [LastName], [FirstName], [OtherNames], [Gender], [UnitId], [IsActive], [MustChangePassword], [IsHeadOfDept], [IsHeadOfUnit], [ProfilePhoto])
        VALUES                          (3, N'Adekunle', N'Femi', N'Moses', N'M', 9, 1, 0, 0, 0, NULL);
        INSERT  INTO [dbo].[UserProfile] ([AspNetUserId], [LastName], [FirstName], [OtherNames], [Gender], [UnitId], [IsActive], [MustChangePassword], [IsHeadOfDept], [IsHeadOfUnit], [ProfilePhoto])
        VALUES                          (4, N'Ireti', N'Matthew', N'Ayo', N'M', 8, 1, 0, 0, 0, NULL);
        INSERT  INTO [dbo].[UserProfile] ([AspNetUserId], [LastName], [FirstName], [OtherNames], [Gender], [UnitId], [IsActive], [MustChangePassword], [IsHeadOfDept], [IsHeadOfUnit], [ProfilePhoto])
        VALUES                          (5, N'Ojedokun', N'Aderinmola', N'O', N'F', 13, 1, 0, 0, 0, NULL);
        INSERT  INTO [dbo].[UserProfile] ([AspNetUserId], [LastName], [FirstName], [OtherNames], [Gender], [UnitId], [IsActive], [MustChangePassword], [IsHeadOfDept], [IsHeadOfUnit], [ProfilePhoto])
        VALUES                          (6, N'Achibiri', N'Odemwinge', N'Sunday', N'M', 9, 1, 0, 0, 0, NULL);
    END


GO