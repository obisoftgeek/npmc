﻿-- <Migration ID="30a23524-7f3c-4526-8e0b-58c20da57539" />
GO

PRINT N'Creating [dbo].[UserProfile]'
GO
CREATE TABLE [dbo].[UserProfile]
(
[AspNetUserId] [int] NOT NULL,
[LastName] [nvarchar] (50) NULL,
[FirstName] [nvarchar] (50) NULL,
[OtherNames] [nvarchar] (50) NULL,
[Gender] [nvarchar] (50) NULL,
[UnitId] [int] NOT NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_UserProfile_IsActvie] DEFAULT ((1)),
[MustChangePassword] [bit] NOT NULL CONSTRAINT [DF_UserProfile_MustChangePassword] DEFAULT ((1)),
[IsHeadOfDept] [bit] NOT NULL CONSTRAINT [DF_UserProfile_IsHeadOfDept] DEFAULT ((0)),
[IsHeadOfUnit] [bit] NOT NULL CONSTRAINT [DF_UserProfile_IsHeadOfUnit] DEFAULT ((0)),
[ProfilePhoto] [nvarchar] (50) NULL
)
GO
PRINT N'Creating primary key [PK_UserProfile] on [dbo].[UserProfile]'
GO
ALTER TABLE [dbo].[UserProfile] ADD CONSTRAINT [PK_UserProfile] PRIMARY KEY CLUSTERED  ([AspNetUserId])
GO
PRINT N'Creating [dbo].[Department]'
GO
CREATE TABLE [dbo].[Department]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_Department_IsActive] DEFAULT ((1)),
[DateCreated] [datetime] NOT NULL
)
GO
PRINT N'Creating primary key [PK_Department] on [dbo].[Department]'
GO
ALTER TABLE [dbo].[Department] ADD CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[ExamDiet]'
GO
CREATE TABLE [dbo].[ExamDiet]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (100) NOT NULL,
[Description] [nvarchar] (100) NULL,
[StartDate] [date] NOT NULL,
[EndDate] [date] NOT NULL,
[DateCreated] [datetime] NOT NULL,
[DefermentPercentage] [decimal] (18, 2) NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL CONSTRAINT [DF_Diet_IsApproved] DEFAULT ((0)),
[StatusId] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_Diet] on [dbo].[ExamDiet]'
GO
ALTER TABLE [dbo].[ExamDiet] ADD CONSTRAINT [PK_Diet] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[AuditSection]'
GO
CREATE TABLE [dbo].[AuditSection]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (250) NOT NULL
)
GO
PRINT N'Creating primary key [PK_AuditSections] on [dbo].[AuditSection]'
GO
ALTER TABLE [dbo].[AuditSection] ADD CONSTRAINT [PK_AuditSections] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[AuditAction]'
GO
CREATE TABLE [dbo].[AuditAction]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (250) NOT NULL,
[AuditSectionId] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_AuditActions] on [dbo].[AuditAction]'
GO
ALTER TABLE [dbo].[AuditAction] ADD CONSTRAINT [PK_AuditActions] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[AuditTrail]'
GO
CREATE TABLE [dbo].[AuditTrail]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[UserIP] [nvarchar] (250) NULL,
[Details] [nvarchar] (max) NULL,
[Timestamp] [datetime] NOT NULL,
[AuditActionId] [int] NULL,
[IsMobile] [bit] NOT NULL,
[BrowserName] [nvarchar] (250) NULL,
[UserProfileId] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_AuditTrails] on [dbo].[AuditTrail]'
GO
ALTER TABLE [dbo].[AuditTrail] ADD CONSTRAINT [PK_AuditTrails] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[AspNetUsers]'
GO
CREATE TABLE [dbo].[AspNetUsers]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Email] [nvarchar] (256) NULL,
[EmailConfirmed] [bit] NOT NULL,
[PasswordHash] [nvarchar] (max) NULL,
[SecurityStamp] [nvarchar] (max) NULL,
[PhoneNumber] [nvarchar] (max) NULL,
[PhoneNumberConfirmed] [bit] NOT NULL,
[TwoFactorEnabled] [bit] NOT NULL,
[LockoutEndDateUtc] [datetime] NULL,
[LockoutEnabled] [bit] NOT NULL,
[AccessFailedCount] [int] NOT NULL,
[UserName] [nvarchar] (256) NOT NULL
)
GO
PRINT N'Creating primary key [PK_dbo.AspNetUsers] on [dbo].[AspNetUsers]'
GO
ALTER TABLE [dbo].[AspNetUsers] ADD CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating index [UserNameIndex] on [dbo].[AspNetUsers]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers] ([UserName])
GO
PRINT N'Creating [dbo].[AspNetUserClaims]'
GO
CREATE TABLE [dbo].[AspNetUserClaims]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UserId] [int] NOT NULL,
[ClaimType] [nvarchar] (max) NULL,
[ClaimValue] [nvarchar] (max) NULL
)
GO
PRINT N'Creating primary key [PK_dbo.AspNetUserClaims] on [dbo].[AspNetUserClaims]'
GO
ALTER TABLE [dbo].[AspNetUserClaims] ADD CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating index [IX_UserId] on [dbo].[AspNetUserClaims]'
GO
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims] ([UserId])
GO
PRINT N'Creating [dbo].[AspNetUserLogins]'
GO
CREATE TABLE [dbo].[AspNetUserLogins]
(
[LoginProvider] [nvarchar] (128) NOT NULL,
[ProviderKey] [nvarchar] (128) NOT NULL,
[UserId] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_dbo.AspNetUserLogins] on [dbo].[AspNetUserLogins]'
GO
ALTER TABLE [dbo].[AspNetUserLogins] ADD CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED  ([LoginProvider], [ProviderKey], [UserId])
GO
PRINT N'Creating index [IX_UserId] on [dbo].[AspNetUserLogins]'
GO
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins] ([UserId])
GO
PRINT N'Creating [dbo].[AspNetRoles]'
GO
CREATE TABLE [dbo].[AspNetRoles]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) NULL,
[Name] [nvarchar] (256) NOT NULL
)
GO
PRINT N'Creating primary key [PK_dbo.AspNetRoles] on [dbo].[AspNetRoles]'
GO
ALTER TABLE [dbo].[AspNetRoles] ADD CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating index [RoleNameIndex] on [dbo].[AspNetRoles]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles] ([Name])
GO
PRINT N'Creating [dbo].[AspNetUserRoles]'
GO
CREATE TABLE [dbo].[AspNetUserRoles]
(
[UserId] [int] NOT NULL,
[RoleId] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_dbo.AspNetUserRoles] on [dbo].[AspNetUserRoles]'
GO
ALTER TABLE [dbo].[AspNetUserRoles] ADD CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED  ([UserId], [RoleId])
GO
PRINT N'Creating index [IX_RoleId] on [dbo].[AspNetUserRoles]'
GO
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles] ([RoleId])
GO
PRINT N'Creating index [IX_UserId] on [dbo].[AspNetUserRoles]'
GO
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles] ([UserId])
GO
PRINT N'Creating [dbo].[Designation]'
GO
CREATE TABLE [dbo].[Designation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[DateCreated] [datetime] NOT NULL,
[CreatedBy] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_Designation] on [dbo].[Designation]'
GO
ALTER TABLE [dbo].[Designation] ADD CONSTRAINT [PK_Designation] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[ExamDietPayment]'
GO
CREATE TABLE [dbo].[ExamDietPayment]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[DietId] [int] NOT NULL,
[PaymentFeeId] [int] NOT NULL,
[ExamId] [int] NOT NULL,
[Amount] [decimal] (18, 2) NOT NULL
)
GO
PRINT N'Creating primary key [PK_DietPayment] on [dbo].[ExamDietPayment]'
GO
ALTER TABLE [dbo].[ExamDietPayment] ADD CONSTRAINT [PK_DietPayment] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[PaymentFee]'
GO
CREATE TABLE [dbo].[PaymentFee]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[PaymentCategoryId] [int] NOT NULL,
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[DateCreated] [datetime] NOT NULL,
[IsActive] [bit] NOT NULL,
[Amount] [decimal] (18, 2) NOT NULL,
[AccountId] [int] NOT NULL,
[DueDay] [int] NULL,
[DueMonth] [int] NULL,
[IsApproved] [bit] NOT NULL,
[AprovedBy] [int] NULL
)
GO
PRINT N'Creating primary key [PK_PaymentFee] on [dbo].[PaymentFee]'
GO
ALTER TABLE [dbo].[PaymentFee] ADD CONSTRAINT [PK_PaymentFee] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[State]'
GO
CREATE TABLE [dbo].[State]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL
)
GO
PRINT N'Creating primary key [PK_State] on [dbo].[State]'
GO
ALTER TABLE [dbo].[State] ADD CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[ExamCenter]'
GO
CREATE TABLE [dbo].[ExamCenter]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[StateId] [int] NOT NULL,
[Name] [nvarchar] (150) NOT NULL,
[Address] [nvarchar] (150) NULL,
[DateCreated] [datetime] NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL,
[ContactPersonName] [nvarchar] (50) NULL,
[ContactPersonPhone] [nvarchar] (50) NULL
)
GO
PRINT N'Creating primary key [PK_ExamCenter] on [dbo].[ExamCenter]'
GO
ALTER TABLE [dbo].[ExamCenter] ADD CONSTRAINT [PK_ExamCenter] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[Exam]'
GO
CREATE TABLE [dbo].[Exam]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (150) NULL,
[IsActive] [bit] NOT NULL
)
GO
PRINT N'Creating primary key [PK_Exams] on [dbo].[Exam]'
GO
ALTER TABLE [dbo].[Exam] ADD CONSTRAINT [PK_Exams] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[ExamDietTimeTable]'
GO
CREATE TABLE [dbo].[ExamDietTimeTable]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ExamId] [int] NOT NULL,
[StartTime] [time] NOT NULL,
[EndTime] [time] NOT NULL,
[ExamDietId] [int] NOT NULL,
[FacultyId] [int] NOT NULL,
[ExamTypeId] [int] NOT NULL,
[Date] [date] NOT NULL
)
GO
PRINT N'Creating primary key [PK_ExamDietTimeTable] on [dbo].[ExamDietTimeTable]'
GO
ALTER TABLE [dbo].[ExamDietTimeTable] ADD CONSTRAINT [PK_ExamDietTimeTable] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[ExamType]'
GO
CREATE TABLE [dbo].[ExamType]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[IsActive] [bit] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ExamId] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_Course] on [dbo].[ExamType]'
GO
ALTER TABLE [dbo].[ExamType] ADD CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[Faculty]'
GO
CREATE TABLE [dbo].[Faculty]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Code] [nvarchar] (50) NOT NULL,
[PaymentFeeId] [int] NOT NULL,
[DateCreated] [datetime] NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL,
[ApprovedBy] [int] NULL
)
GO
PRINT N'Creating primary key [PK_Faculty] on [dbo].[Faculty]'
GO
ALTER TABLE [dbo].[Faculty] ADD CONSTRAINT [PK_Faculty] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[ExamRequirement]'
GO
CREATE TABLE [dbo].[ExamRequirement]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[DateCreated] [datetime] NOT NULL,
[ExamId] [int] NULL,
[IsActive] [bit] NOT NULL
)
GO
PRINT N'Creating primary key [PK_ExamRequirement] on [dbo].[ExamRequirement]'
GO
ALTER TABLE [dbo].[ExamRequirement] ADD CONSTRAINT [PK_ExamRequirement] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[ExamStage]'
GO
CREATE TABLE [dbo].[ExamStage]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ExamId] [int] NOT NULL,
[Name] [nvarchar] (100) NOT NULL,
[DateCreated] [datetime] NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL
)
GO
PRINT N'Creating primary key [PK_ExamStage] on [dbo].[ExamStage]'
GO
ALTER TABLE [dbo].[ExamStage] ADD CONSTRAINT [PK_ExamStage] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[ExamStageCenter]'
GO
CREATE TABLE [dbo].[ExamStageCenter]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ExamStageId] [int] NOT NULL,
[ExamCenterId] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_ExamStageCenter] on [dbo].[ExamStageCenter]'
GO
ALTER TABLE [dbo].[ExamStageCenter] ADD CONSTRAINT [PK_ExamStageCenter] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[ExemptionFee]'
GO
CREATE TABLE [dbo].[ExemptionFee]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ExamId] [int] NOT NULL,
[PaymentFeeId] [int] NOT NULL,
[Description] [nvarchar] (50) NULL,
[DateCreated] [datetime] NOT NULL,
[CreatedBy] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_ExemptionFee] on [dbo].[ExemptionFee]'
GO
ALTER TABLE [dbo].[ExemptionFee] ADD CONSTRAINT [PK_ExemptionFee] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[Fellow]'
GO
CREATE TABLE [dbo].[Fellow]
(
[UserProfileId] [int] NOT NULL,
[Title] [nvarchar] (50) NULL
)
GO
PRINT N'Creating [dbo].[AccreditationStatus]'
GO
CREATE TABLE [dbo].[AccreditationStatus]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[NumberOfYear] [int] NOT NULL,
[DateCreated] [datetime] NOT NULL
)
GO
PRINT N'Creating primary key [PK_AccreditationApprovalStatus] on [dbo].[AccreditationStatus]'
GO
ALTER TABLE [dbo].[AccreditationStatus] ADD CONSTRAINT [PK_AccreditationApprovalStatus] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[Institution]'
GO
CREATE TABLE [dbo].[Institution]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Code] [nvarchar] (50) NOT NULL,
[ContactPersonId] [int] NOT NULL,
[StateId] [int] NOT NULL,
[Address] [nvarchar] (50) NULL,
[Website] [nvarchar] (50) NULL,
[AccreditationStatusId] [int] NULL,
[LastAccreditationDate] [date] NULL,
[NextDueDate] [date] NULL,
[DateCreated] [datetime] NOT NULL,
[YearEstablished] [int] NULL,
[CreatedBy] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_Institution] on [dbo].[Institution]'
GO
ALTER TABLE [dbo].[Institution] ADD CONSTRAINT [PK_Institution] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[InstitutionFaculty]'
GO
CREATE TABLE [dbo].[InstitutionFaculty]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[InstitutionId] [int] NOT NULL,
[FacultyId] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_InstitutionFaculty] on [dbo].[InstitutionFaculty]'
GO
ALTER TABLE [dbo].[InstitutionFaculty] ADD CONSTRAINT [PK_InstitutionFaculty] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[Mail]'
GO
CREATE TABLE [dbo].[Mail]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (150) NOT NULL,
[Description] [nvarchar] (max) NOT NULL,
[Attachment] [nvarchar] (max) NULL,
[DateCreated] [datetime] NOT NULL,
[StatusId] [int] NOT NULL,
[RecepientId] [int] NOT NULL,
[Sender] [nvarchar] (150) NOT NULL,
[SenderAddress] [nvarchar] (150) NULL
)
GO
PRINT N'Creating primary key [PK_Mail] on [dbo].[Mail]'
GO
ALTER TABLE [dbo].[Mail] ADD CONSTRAINT [PK_Mail] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[MemoStatus]'
GO
CREATE TABLE [dbo].[MemoStatus]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL
)
GO
PRINT N'Creating primary key [PK_MemoStatus] on [dbo].[MemoStatus]'
GO
ALTER TABLE [dbo].[MemoStatus] ADD CONSTRAINT [PK_MemoStatus] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[Memo]'
GO
CREATE TABLE [dbo].[Memo]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Subject] [nvarchar] (50) NOT NULL,
[Body] [nvarchar] (max) NOT NULL,
[DateCreated] [datetime] NOT NULL,
[SenderId] [int] NOT NULL,
[MemoStatusId] [int] NOT NULL,
[FolderType] [int] NOT NULL,
[AttachedFile] [nvarchar] (128) NULL
)
GO
PRINT N'Creating primary key [PK_Memo] on [dbo].[Memo]'
GO
ALTER TABLE [dbo].[Memo] ADD CONSTRAINT [PK_Memo] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[MemoRecipient]'
GO
CREATE TABLE [dbo].[MemoRecipient]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[MemoId] [int] NOT NULL,
[RecipientId] [int] NOT NULL,
[ReadStatus] [int] NOT NULL,
[IsForwarded] [bit] NOT NULL,
[ForwardedBy] [int] NULL,
[DateForwarded] [datetime] NULL
)
GO
PRINT N'Creating primary key [PK_MemoComment] on [dbo].[MemoRecipient]'
GO
ALTER TABLE [dbo].[MemoRecipient] ADD CONSTRAINT [PK_MemoComment] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[MemoThread]'
GO
CREATE TABLE [dbo].[MemoThread]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[MemoId] [int] NOT NULL,
[RecipientId] [int] NOT NULL,
[Comment] [nvarchar] (max) NOT NULL,
[DateCreated] [datetime] NOT NULL,
[AttachedFile] [nvarchar] (128) NULL
)
GO
PRINT N'Creating primary key [PK_MemoThread] on [dbo].[MemoThread]'
GO
ALTER TABLE [dbo].[MemoThread] ADD CONSTRAINT [PK_MemoThread] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[PaymentCategory]'
GO
CREATE TABLE [dbo].[PaymentCategory]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (80) NOT NULL,
[Description] [nvarchar] (150) NULL,
[DateCreated] [datetime] NOT NULL,
[CreatedBy] [int] NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL
)
GO
PRINT N'Creating primary key [PK_PaymentCategory] on [dbo].[PaymentCategory]'
GO
ALTER TABLE [dbo].[PaymentCategory] ADD CONSTRAINT [PK_PaymentCategory] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[Account]'
GO
CREATE TABLE [dbo].[Account]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Number] [nvarchar] (50) NOT NULL,
[DateCreated] [datetime] NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL
)
GO
PRINT N'Creating primary key [PK_Account] on [dbo].[Account]'
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[PaymentTransaction]'
GO
CREATE TABLE [dbo].[PaymentTransaction]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UserProfileId] [int] NOT NULL,
[PaymentDate] [datetime] NOT NULL,
[PaymentType] [int] NULL,
[Amount] [decimal] (18, 2) NOT NULL,
[AmountPaid] [decimal] (18, 2) NULL,
[ReferenceId] [nvarchar] (50) NOT NULL,
[HasPaid] [bit] NOT NULL,
[AccountId] [int] NOT NULL,
[Narration] [nvarchar] (150) NOT NULL,
[InvoiceId] [nvarchar] (50) NOT NULL
)
GO
PRINT N'Creating primary key [PK_AFASPayment] on [dbo].[PaymentTransaction]'
GO
ALTER TABLE [dbo].[PaymentTransaction] ADD CONSTRAINT [PK_AFASPayment] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[ResultUpload]'
GO
CREATE TABLE [dbo].[ResultUpload]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ExamDietId] [int] NOT NULL,
[ExamId] [int] NOT NULL,
[Upload] [nvarchar] (150) NOT NULL,
[ScannedCopy] [nvarchar] (150) NOT NULL,
[DateUploaded] [datetime] NOT NULL,
[UploadedBy] [int] NULL
)
GO
PRINT N'Creating primary key [PK_ResultUpload] on [dbo].[ResultUpload]'
GO
ALTER TABLE [dbo].[ResultUpload] ADD CONSTRAINT [PK_ResultUpload] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[Unit]'
GO
CREATE TABLE [dbo].[Unit]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[DepartmentId] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_Unit] on [dbo].[Unit]'
GO
ALTER TABLE [dbo].[Unit] ADD CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[Role]'
GO
CREATE TABLE [dbo].[Role]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[UnitId] [int] NOT NULL,
[CratedDate] [datetime] NOT NULL
)
GO
PRINT N'Creating primary key [PK_Role] on [dbo].[Role]'
GO
ALTER TABLE [dbo].[Role] ADD CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[Staff]'
GO
CREATE TABLE [dbo].[Staff]
(
[UserProfileId] [int] NOT NULL,
[StaffId] [nvarchar] (50) NULL,
[DesignationId] [int] NULL,
[Title] [nvarchar] (50) NULL
)
GO
PRINT N'Creating primary key [PK_Staff] on [dbo].[Staff]'
GO
ALTER TABLE [dbo].[Staff] ADD CONSTRAINT [PK_Staff] PRIMARY KEY CLUSTERED  ([UserProfileId])
GO
PRINT N'Creating [dbo].[SubSpecialty]'
GO
CREATE TABLE [dbo].[SubSpecialty]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (100) NOT NULL,
[FacultyId] [int] NOT NULL,
[PaymentFeeId] [int] NOT NULL,
[DateCreated] [datetime] NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL,
[ApprovedBy] [int] NULL,
[Description] [nvarchar] (max) NULL
)
GO
PRINT N'Creating primary key [PK_SubSpecialty] on [dbo].[SubSpecialty]'
GO
ALTER TABLE [dbo].[SubSpecialty] ADD CONSTRAINT [PK_SubSpecialty] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[UserRole]'
GO
CREATE TABLE [dbo].[UserRole]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UserProfileId] [int] NOT NULL,
[RoleId] [int] NOT NULL,
[IsActive] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL,
[AssignedBy] [int] NOT NULL,
[AssignedDate] [datetime] NOT NULL,
[ApprovedDate] [datetime] NULL,
[ApprovedBy] [int] NULL
)
GO
PRINT N'Creating primary key [PK_UserRole] on [dbo].[UserRole]'
GO
ALTER TABLE [dbo].[UserRole] ADD CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[Workshop]'
GO
CREATE TABLE [dbo].[Workshop]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL,
[Venue] [nvarchar] (50) NOT NULL,
[StartDate] [date] NOT NULL,
[EndDate] [date] NOT NULL,
[PaymentFeeId] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[CreatedBy] [int] NOT NULL,
[IsApproved] [bit] NOT NULL,
[ApprovedBy] [int] NULL
)
GO
PRINT N'Creating primary key [PK_Workshop] on [dbo].[Workshop]'
GO
ALTER TABLE [dbo].[Workshop] ADD CONSTRAINT [PK_Workshop] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[AccreditationType]'
GO
CREATE TABLE [dbo].[AccreditationType]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL
)
GO
PRINT N'Creating primary key [PK_AccreditationType] on [dbo].[AccreditationType]'
GO
ALTER TABLE [dbo].[AccreditationType] ADD CONSTRAINT [PK_AccreditationType] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[MemoCategory]'
GO
CREATE TABLE [dbo].[MemoCategory]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL
)
GO
PRINT N'Creating primary key [PK_MemoType] on [dbo].[MemoCategory]'
GO
ALTER TABLE [dbo].[MemoCategory] ADD CONSTRAINT [PK_MemoType] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[ResultCategory]'
GO
CREATE TABLE [dbo].[ResultCategory]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) NOT NULL,
[Description] [nvarchar] (50) NULL
)
GO
PRINT N'Creating primary key [PK_PassCategory] on [dbo].[ResultCategory]'
GO
ALTER TABLE [dbo].[ResultCategory] ADD CONSTRAINT [PK_PassCategory] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[RolePermission]'
GO
CREATE TABLE [dbo].[RolePermission]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[RoleId] [int] NOT NULL,
[ControllerId] [int] NOT NULL,
[CanView] [bit] NULL,
[CanCreate] [bit] NULL,
[CanModify] [bit] NULL,
[CanApprove] [bit] NULL,
[CreatedDate] [datetime] NULL,
[CreatedBy] [int] NULL,
[IsApproved] [bit] NOT NULL,
[ApprovedBy] [int] NOT NULL,
[ApprovedDate] [datetime] NOT NULL
)
GO
PRINT N'Creating primary key [PK_RolePermission] on [dbo].[RolePermission]'
GO
ALTER TABLE [dbo].[RolePermission] ADD CONSTRAINT [PK_RolePermission] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating [dbo].[Title]'
GO
CREATE TABLE [dbo].[Title]
(
[Id] [int] NOT NULL,
[Name] [nvarchar] (50) NULL
)
GO
PRINT N'Creating primary key [PK_Title] on [dbo].[Title]'
GO
ALTER TABLE [dbo].[Title] ADD CONSTRAINT [PK_Title] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Adding foreign keys to [dbo].[PaymentFee]'
GO
ALTER TABLE [dbo].[PaymentFee] ADD CONSTRAINT [FK_PaymentFee_Account] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[PaymentFee] ADD CONSTRAINT [FK_PaymentFee_PaymentCategory] FOREIGN KEY ([PaymentCategoryId]) REFERENCES [dbo].[PaymentCategory] ([Id])
GO
ALTER TABLE [dbo].[PaymentFee] ADD CONSTRAINT [FK_PaymentFee_UserProfile1] FOREIGN KEY ([AprovedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[PaymentTransaction]'
GO
ALTER TABLE [dbo].[PaymentTransaction] ADD CONSTRAINT [FK_PaymentTransaction_Account] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[PaymentTransaction] ADD CONSTRAINT [FK_PaymentTransaction_UserProfile] FOREIGN KEY ([UserProfileId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[Institution]'
GO
ALTER TABLE [dbo].[Institution] ADD CONSTRAINT [FK_Institution_AccreditationStatus] FOREIGN KEY ([AccreditationStatusId]) REFERENCES [dbo].[AccreditationStatus] ([Id])
GO
ALTER TABLE [dbo].[Institution] ADD CONSTRAINT [FK_Institution_UserProfile] FOREIGN KEY ([ContactPersonId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
ALTER TABLE [dbo].[Institution] ADD CONSTRAINT [FK_Institution_State] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
GO
ALTER TABLE [dbo].[Institution] ADD CONSTRAINT [FK_Institution_UserProfile1] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[AspNetUserRoles]'
GO
ALTER TABLE [dbo].[AspNetUserRoles] ADD CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[AspNetRoles] ([Id]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] ADD CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
GO
PRINT N'Adding foreign keys to [dbo].[AspNetUserClaims]'
GO
ALTER TABLE [dbo].[AspNetUserClaims] ADD CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
GO
PRINT N'Adding foreign keys to [dbo].[AspNetUserLogins]'
GO
ALTER TABLE [dbo].[AspNetUserLogins] ADD CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
GO
PRINT N'Adding foreign keys to [dbo].[UserProfile]'
GO
ALTER TABLE [dbo].[UserProfile] ADD CONSTRAINT [FK_UserProfile_AspNetUsers] FOREIGN KEY ([AspNetUserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[UserProfile] ADD CONSTRAINT [FK_UserProfile_Unit] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[Unit] ([Id])
GO
PRINT N'Adding foreign keys to [dbo].[AuditTrail]'
GO
ALTER TABLE [dbo].[AuditTrail] ADD CONSTRAINT [FK_AuditTrail_AuditAction] FOREIGN KEY ([AuditActionId]) REFERENCES [dbo].[AuditAction] ([Id])
GO
ALTER TABLE [dbo].[AuditTrail] ADD CONSTRAINT [FK_AuditTrail_UserProfile] FOREIGN KEY ([UserProfileId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[AuditAction]'
GO
ALTER TABLE [dbo].[AuditAction] ADD CONSTRAINT [FK_AuditAction_AuditSection] FOREIGN KEY ([AuditSectionId]) REFERENCES [dbo].[AuditSection] ([Id])
GO
PRINT N'Adding foreign keys to [dbo].[Unit]'
GO
ALTER TABLE [dbo].[Unit] ADD CONSTRAINT [FK_Unit_Department] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[Department] ([Id])
GO
PRINT N'Adding foreign keys to [dbo].[Staff]'
GO
ALTER TABLE [dbo].[Staff] ADD CONSTRAINT [FK_Staff_Designation] FOREIGN KEY ([DesignationId]) REFERENCES [dbo].[Designation] ([Id])
GO
ALTER TABLE [dbo].[Staff] ADD CONSTRAINT [FK_Staff_UserProfile] FOREIGN KEY ([UserProfileId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId]) ON DELETE CASCADE
GO
PRINT N'Adding foreign keys to [dbo].[Designation]'
GO
ALTER TABLE [dbo].[Designation] ADD CONSTRAINT [FK_Designation_UserProfile] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[ExamStageCenter]'
GO
ALTER TABLE [dbo].[ExamStageCenter] ADD CONSTRAINT [FK_ExamStageCenter_ExamCenter] FOREIGN KEY ([ExamCenterId]) REFERENCES [dbo].[ExamCenter] ([Id])
GO
ALTER TABLE [dbo].[ExamStageCenter] ADD CONSTRAINT [FK_ExamStageCenter_ExamStage] FOREIGN KEY ([ExamStageId]) REFERENCES [dbo].[ExamStage] ([Id]) ON DELETE CASCADE
GO
PRINT N'Adding foreign keys to [dbo].[ExamCenter]'
GO
ALTER TABLE [dbo].[ExamCenter] ADD CONSTRAINT [FK_ExamCenter_State] FOREIGN KEY ([StateId]) REFERENCES [dbo].[State] ([Id])
GO
PRINT N'Adding foreign keys to [dbo].[ExamDietPayment]'
GO
ALTER TABLE [dbo].[ExamDietPayment] ADD CONSTRAINT [FK_DietPayment_Diet] FOREIGN KEY ([DietId]) REFERENCES [dbo].[ExamDiet] ([Id])
GO
ALTER TABLE [dbo].[ExamDietPayment] ADD CONSTRAINT [FK_DietPayment_PaymentFee] FOREIGN KEY ([PaymentFeeId]) REFERENCES [dbo].[PaymentFee] ([Id])
GO
ALTER TABLE [dbo].[ExamDietPayment] ADD CONSTRAINT [FK_ExamDietPayment_Exam] FOREIGN KEY ([ExamId]) REFERENCES [dbo].[Exam] ([Id])
GO
PRINT N'Adding foreign keys to [dbo].[ExamDietTimeTable]'
GO
ALTER TABLE [dbo].[ExamDietTimeTable] ADD CONSTRAINT [FK_ExamDietTimeTable_Exam] FOREIGN KEY ([ExamId]) REFERENCES [dbo].[Exam] ([Id])
GO
ALTER TABLE [dbo].[ExamDietTimeTable] ADD CONSTRAINT [FK_ExamDietTimeTable_ExamDiet] FOREIGN KEY ([ExamDietId]) REFERENCES [dbo].[ExamDiet] ([Id])
GO
ALTER TABLE [dbo].[ExamDietTimeTable] ADD CONSTRAINT [FK_ExamDietTimeTable_Faculty] FOREIGN KEY ([FacultyId]) REFERENCES [dbo].[Faculty] ([Id])
GO
ALTER TABLE [dbo].[ExamDietTimeTable] ADD CONSTRAINT [FK_ExamDietTimeTable_ExamType] FOREIGN KEY ([ExamTypeId]) REFERENCES [dbo].[ExamType] ([Id])
GO
PRINT N'Adding foreign keys to [dbo].[ResultUpload]'
GO
ALTER TABLE [dbo].[ResultUpload] ADD CONSTRAINT [FK_ResultUpload_ExamDiet] FOREIGN KEY ([ExamDietId]) REFERENCES [dbo].[ExamDiet] ([Id])
GO
ALTER TABLE [dbo].[ResultUpload] ADD CONSTRAINT [FK_ResultUpload_Exam] FOREIGN KEY ([ExamId]) REFERENCES [dbo].[Exam] ([Id])
GO
PRINT N'Adding foreign keys to [dbo].[ExamRequirement]'
GO
ALTER TABLE [dbo].[ExamRequirement] ADD CONSTRAINT [FK_ExamRequirement_Exam] FOREIGN KEY ([ExamId]) REFERENCES [dbo].[Exam] ([Id])
GO
PRINT N'Adding foreign keys to [dbo].[ExamStage]'
GO
ALTER TABLE [dbo].[ExamStage] ADD CONSTRAINT [FK_ExamStage_Exam] FOREIGN KEY ([ExamId]) REFERENCES [dbo].[Exam] ([Id])
GO
PRINT N'Adding foreign keys to [dbo].[ExamType]'
GO
ALTER TABLE [dbo].[ExamType] ADD CONSTRAINT [FK_ExamType_Exam] FOREIGN KEY ([ExamId]) REFERENCES [dbo].[Exam] ([Id])
GO
PRINT N'Adding foreign keys to [dbo].[ExemptionFee]'
GO
ALTER TABLE [dbo].[ExemptionFee] ADD CONSTRAINT [FK_ExemptionFee_Exam] FOREIGN KEY ([ExamId]) REFERENCES [dbo].[Exam] ([Id])
GO
ALTER TABLE [dbo].[ExemptionFee] ADD CONSTRAINT [FK_ExemptionFee_PaymentFee] FOREIGN KEY ([PaymentFeeId]) REFERENCES [dbo].[PaymentFee] ([Id])
GO
ALTER TABLE [dbo].[ExemptionFee] ADD CONSTRAINT [FK_ExemptionFee_UserProfile] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[InstitutionFaculty]'
GO
ALTER TABLE [dbo].[InstitutionFaculty] ADD CONSTRAINT [FK_InstitutionFaculty_Faculty] FOREIGN KEY ([FacultyId]) REFERENCES [dbo].[Faculty] ([Id])
GO
ALTER TABLE [dbo].[InstitutionFaculty] ADD CONSTRAINT [FK_InstitutionFaculty_Institution] FOREIGN KEY ([InstitutionId]) REFERENCES [dbo].[Institution] ([Id])
GO
PRINT N'Adding foreign keys to [dbo].[SubSpecialty]'
GO
ALTER TABLE [dbo].[SubSpecialty] ADD CONSTRAINT [FK_SubSpecialty_Faculty] FOREIGN KEY ([FacultyId]) REFERENCES [dbo].[Faculty] ([Id])
GO
ALTER TABLE [dbo].[SubSpecialty] ADD CONSTRAINT [FK_SubSpecialty_PaymentFee] FOREIGN KEY ([PaymentFeeId]) REFERENCES [dbo].[PaymentFee] ([Id])
GO
ALTER TABLE [dbo].[SubSpecialty] ADD CONSTRAINT [FK_SubSpecialty_UserProfile] FOREIGN KEY ([ApprovedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[Faculty]'
GO
ALTER TABLE [dbo].[Faculty] ADD CONSTRAINT [FK_Faculty_PaymentFee] FOREIGN KEY ([PaymentFeeId]) REFERENCES [dbo].[PaymentFee] ([Id])
GO
ALTER TABLE [dbo].[Faculty] ADD CONSTRAINT [FK_Faculty_UserProfile1] FOREIGN KEY ([ApprovedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[Fellow]'
GO
ALTER TABLE [dbo].[Fellow] ADD CONSTRAINT [FK_Fellow_UserProfile] FOREIGN KEY ([UserProfileId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[Mail]'
GO
ALTER TABLE [dbo].[Mail] ADD CONSTRAINT [FK_Mail_UserProfile] FOREIGN KEY ([RecepientId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[MemoRecipient]'
GO
ALTER TABLE [dbo].[MemoRecipient] ADD CONSTRAINT [FK_MemoRecipient_MemoRecipient] FOREIGN KEY ([ForwardedBy]) REFERENCES [dbo].[MemoRecipient] ([Id])
GO
ALTER TABLE [dbo].[MemoRecipient] ADD CONSTRAINT [FK_MemoRecipient_Memo] FOREIGN KEY ([MemoId]) REFERENCES [dbo].[Memo] ([Id]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MemoRecipient] ADD CONSTRAINT [FK_MemoRecipient_UserProfile] FOREIGN KEY ([RecipientId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[MemoThread]'
GO
ALTER TABLE [dbo].[MemoThread] ADD CONSTRAINT [FK_MemoThread_MemoRecipient] FOREIGN KEY ([RecipientId]) REFERENCES [dbo].[MemoRecipient] ([Id]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MemoThread] ADD CONSTRAINT [FK_MemoThread_Memo] FOREIGN KEY ([MemoId]) REFERENCES [dbo].[Memo] ([Id])
GO
PRINT N'Adding foreign keys to [dbo].[Memo]'
GO
ALTER TABLE [dbo].[Memo] ADD CONSTRAINT [FK_Memo_MemoCategory] FOREIGN KEY ([MemoStatusId]) REFERENCES [dbo].[MemoStatus] ([Id])
GO
ALTER TABLE [dbo].[Memo] ADD CONSTRAINT [FK_Memo_UserProfile] FOREIGN KEY ([SenderId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[PaymentCategory]'
GO
ALTER TABLE [dbo].[PaymentCategory] ADD CONSTRAINT [FK_PaymentCategory_UserProfile] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[Workshop]'
GO
ALTER TABLE [dbo].[Workshop] ADD CONSTRAINT [FK_Workshop_PaymentFee] FOREIGN KEY ([PaymentFeeId]) REFERENCES [dbo].[PaymentFee] ([Id])
GO
ALTER TABLE [dbo].[Workshop] ADD CONSTRAINT [FK_Workshop_UserProfile] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
ALTER TABLE [dbo].[Workshop] ADD CONSTRAINT [FK_Workshop_UserProfile1] FOREIGN KEY ([ApprovedBy]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[UserRole]'
GO
ALTER TABLE [dbo].[UserRole] ADD CONSTRAINT [FK_UserRole_Role] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[UserRole] ADD CONSTRAINT [FK_UserRole_UserProfile] FOREIGN KEY ([UserProfileId]) REFERENCES [dbo].[UserProfile] ([AspNetUserId])
GO
PRINT N'Adding foreign keys to [dbo].[Role]'
GO
ALTER TABLE [dbo].[Role] ADD CONSTRAINT [FK_Role_Unit] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[Unit] ([Id])
GO
