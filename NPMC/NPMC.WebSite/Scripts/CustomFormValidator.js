﻿

$(document).ready(function () {
    $('.validate-form').each(function () {
        var id = $(this).prop('id');
        initializeFormValidator(id);
    });
});

function initializeFormValidator(formId) {
    $('#' + formId + ' input, #' + formId + ' textarea, #' + formId + ' select').each(function () {
        //alert($(this).prop('type') + '}' + $(this).attr('ng-model'));
        var type = $(this).prop('type'); if (type.indexOf('select') >= 0) type = 'select';
        if (type.indexOf('select') >= 0)
            $(this).change(function () { _controlValid(this); });
        else
            $(this).keyup(function () { _controlValid(this); });
    }).change();
    // $(this).change(function () { _isControlValid(id) ? $(this).removeClass(ERR_CLASS) : $(this).addClass(ERR_CLASS); }); 
    clearFormValidation(formId);
}

function clearFormValues(formId) {
    $('#' + formId + ' input, #' + formId + ' textarea, #' + formId + ' select').each(function() {
        $(this).prop('value', '');
      
    });
}

function isFormValid(formId) {
    clearFormValidation(formId);
    var nInValid = 0;
    $('#' + formId + ' input, #' + formId + ' textarea, #' + formId + ' select').each(function () {
        var isValid = _controlValid(this);
        if (isValid == false) {
            nInValid += 1;
            $(this).parents('.form-group').addClass('has-error');
        }
        if (nInValid == 1) {
            toastr.warning(_formError, 'Warning');
            $(this).focus();
        }
    });
    return (nInValid == 0);
}

var _formError;
function _controlValid(control) {
    _formError = "";
    //date, email, number, range, tel, url, file, password,password2,password3, text, name
    //validation-type="number" error-message="Enter Valid Phone Number" min="" max="" 
    var type = $(control).prop('type'); if (type.indexOf('select') >= 0) type = 'select';
    var errorMessage = $(control).attr('error-message');
    if (errorMessage == 'undefined' || errorMessage == null || errorMessage == '') errorMessage = 'fill in the compulsory field(s)...';
    _formError = errorMessage;

    var isRequired = $(control).prop('required');
    var min = $(control).prop('min'); if (min == 'undefined' || min == '' || min == null || isNaN(min)) min = 0;
    var max = $(control).prop('max'); if (max == 'undefined' || max == '' || max == null || isNaN(max)) max = 0;

    var ngModel = $(control).attr('ng-model');
    var valu = $(control).prop('value'); if (valu.indexOf('undefined:undefined') > 0 || valu == null) valu = '';
    valu = $.trim(valu);

    var validate = $(control).attr('validation-type'); if (validate == 'undefined' || validate == null) validate = 'text';
    if (type == 'date' || type == 'email' || type == 'tel' || type == 'url') validate = type;
    validate = $.trim(validate);

    var isValid = false;
    if (valu.length == 0 && !isRequired) isValid = true;
    else {
        if (validate == 'range') {
            var amt = 0; try { amt = parseFloat(valu); } catch (e) { }
            if (isNaN(amt)) isValid = false; else if (amt < min || amt > max) isValid = false; else isValid = true;
        }
        else if (min > 0 && valu.length < min) isValid = false;
        else if (validate == 'name') isValid = isNameValid(valu);
        else if (validate == 'email') isValid = isEmailValid(valu);
        else if (validate == 'number' && isNaN(valu)) { $(control).prop('value', ''); }
        else if (validate == 'url') isValid = isURLValid(valu);
        else if (validate == 'phone' || validate == 'tel') isValid = isPhoneValid(valu);
        else if (validate == 'date') isValid = isDateValid(valu);
        else if (validate == 'password') isValid = isPwdValid(valu);
        else if (validate == 'password2') isValid = isPwd2Valid(valu);
        else if (validate == 'password3') isValid = isPwd3Valid(valu);
        else if (valu.length < 1) isValid = false;
        else { isValid = true; }
    }
    try { isValid ? $(control).removeClass(ERR_CLASS) : $(control).addClass(ERR_CLASS); } catch (e) { }

    //$('#divUserView').append("TypeOf:{" + type + "} validate:{" + validate + "} Error:{" + errorMessage + "} Required:{" + isRequired +
    //  "} Min:{" + min + "} Max:{" + max + "} Value:{" + valu + "}, isValid:{" + isValid + "},  <br>");
    return isValid;
}

function clearFormValidation(formId) {
    $('#' + formId + ' input, #' + formId + ' textarea, #' + formId + ' select').each(function () {
        try {
           // $(this).removeClass(ERR_CLASS);
            $(this).parents('.form-group').removeClass('has-error');
        } catch (e) { }
    });
    //$('#' + formId + ' textarea').each(function () { try { $(this).removeClass(ERR_CLASS); } catch (e) { } });
    //$('#' + formId + ' select').each(function () {  try { $(this).removeClass(ERR_CLASS); } catch (e) { } });
}


function isNameValid(name) { return /^([a-zA-Z\s\.'-]+){3,}$/.test(name); }
function isEmailValid(email) { return /^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/.test(email); }
function isURLValid(url) {
    if (url.indexOf('http:') < 0) url = "http://" + url;
    return /^(http(?:s)?\:\/\/[a-zA-Z0-9\-]+(?:\.[a-zA-Z0-9\-]+)*\.[a-zA-Z]{2,6}(?:\/?|(?:\/[\w\-]+)*)(?:\/?|\/\w+\.[a-zA-Z]{2,4}(?:\?[\w]+\=[\w\-]+)?)?(?:\&[\w]+\=[\w\-]+)*)$/.test(url);
}

function isDateValid2(dtValue) {
    return /^(0[1-9]|[12][0-9]|3[01])[- //.](0[1-9]|1[012])[- //.](19|20)\d\d$/.test(dtValue) ? true : false; //dd-mm-yyyy
}  //(dd-mm-yyyy)
function isDateValid(dtValue) {
    return /^((19|20)[0-9][0-9])[- //.](0[1-9]|1[012])[- //.](0[1-9]|[12][0-9]|3[01])/.test(dtValue) ? true : false; //dd-mm-yyyy
}  //(yyyy-mm-dd)

function isPhoneValid(phone) { return /^(0\d{10})?(,(\s)?0\d{10})*$/.test(phone); }
function isPwdValid(pwd) { return /^(?=.{6,})(?=.*[a-zA-Z])(?=.*[\d\W]).*$/.test(pwd); }
function isPwd2Valid(pwd) { return /^(?=.{8,})(?=.*[a-zA-Z])(?=.*[\d])(?=.*[\W]).*$/.test(pwd); }
function isPwd3Valid(pwd) { return /^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?=.*[\W]).*$/.test(pwd); }


