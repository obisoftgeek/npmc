﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using System.Web.Http;
using System.Web.Mvc;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;

namespace SilverEdgeProjects.NPMC.WebSite
{
    public class AutofacConfig
    {
        public static void ConfigureDIContainer()
        {
            //Create autofac contailner builder
            var builder = new ContainerBuilder();

            // Register dependencies in controllers
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // Register dependencies in  webApi controllers
            builder.RegisterApiControllers(typeof(MvcApplication).Assembly);

            // Register dependencies in filter attributes
            builder.RegisterFilterProvider();

            RegisterEntityFramworkInstance(builder);

            builder.RegisterAssemblyTypes(typeof(UserProfileService).Assembly)
               .Where(t => t.Name.EndsWith("Service")).AsImplementedInterfaces().InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(UserProfileRepository).Assembly)
             .Where(t => t.Name.EndsWith("Repository")).AsImplementedInterfaces().InstancePerLifetimeScope();

            //Build autofac container
            var container = builder.Build();

            // Set MVC DI resolver to use our Autofac container
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
        private static void RegisterEntityFramworkInstance(ContainerBuilder builder)
        {
            builder.RegisterType<NPMCEntities>().As<IDataContextAsync>().InstancePerRequest();
            builder.RegisterType<EntityFrameorkUnitOfWork>().As<IUnitOfWorkAsync>().InstancePerRequest();
            builder.RegisterType<EntityFrameorkUnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterGeneric(typeof(Repository<>))
                .As(typeof(IRepositoryAsync<>)).InstancePerRequest();
            builder.RegisterGeneric(typeof(Repository<>))
               .As(typeof(IRepository<>)).InstancePerRequest();
        }
    }
}