﻿using System;
using System.Configuration;


namespace SilverEdgeProjects.NPMC.WebSite
{

    public class EmailConfigService
    {
        public EmailConfigService() { }

        public static string SmtpFrom => GetAppSetting(typeof(string), "SMTPFrom").ToString();

        public static string SmtpHost => GetAppSetting(typeof(string), "SMTPHost").ToString();

        public static int SmtpPort => (int)GetAppSetting(typeof(int), "SMTPPort");

        public static string SmtpUsername => GetAppSetting(typeof(string), "SMTPUsername").ToString();

        public static string SmtpPassword => GetAppSetting(typeof(string), "SMTPPassword").ToString();

        public static bool SmtpEnableSsl => (bool)GetAppSetting(typeof(bool), "SMTPEnableSsl");

        private static object GetAppSetting(Type expectedType, string key)
        {
            string value = ConfigurationManager.AppSettings[key]; //.Get(key);
            if (value == null)
            {
                throw new Exception(
                string.Format("The config file does not have the key '{0}' defined section.", key));
            }
            if (expectedType == typeof(int))
            {
                return int.Parse(value);
            }
            if (expectedType == typeof(string))
            {
                return value;
            }
            if (expectedType == typeof(bool))
            {
                return bool.Parse(value);
            }
            else
            {
                throw new Exception("Type not supported.");
            }

        }
    }

    public class EmailHelper
    {
        public static string GetMessageBody(string receiverName, string message)
        {

            return ""
            + string.Format("Dear {0}, ", receiverName)
            + Environment.NewLine
            + Environment.NewLine
            + message
            + Environment.NewLine
            + Environment.NewLine
            + "Yours Sincerely, "
            + Environment.NewLine
            + Environment.NewLine
            + string.Format("{0}", "Technical Support Team");

        }

        public static string GetMessageBody(string receiverName, string message, string userName, string password)
        {

            return ""
           + string.Format("Dear {0}, ", receiverName)
            + Environment.NewLine
            + Environment.NewLine
            + message
            + Environment.NewLine
            + Environment.NewLine
            + "UserName:" + userName
            + Environment.NewLine
            + "Password: " + password
            + Environment.NewLine
            + Environment.NewLine
            + Environment.NewLine
            + "Yours Sincerely, "
            + Environment.NewLine
            + Environment.NewLine
             + string.Format("{0}", "Technical Support Team");

        }
    }
}