﻿using System.Web;
using System.Web.Optimization;
using Antlr.Runtime.Misc;

namespace SilverEdgeProjects.NPMC.WebSite
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/common")
                .IncludeDirectory("~/App_Scripts/Common", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Resources", "*.js", true)
            );

            bundles.Add(new ScriptBundle("~/bundles/account")
                .IncludeDirectory("~/App_Scripts/Routes/Account", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Account", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
            );

            bundles.Add(new ScriptBundle("~/bundles/ict")
                .IncludeDirectory("~/App_Scripts/Routes/ICT", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/ICT", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
            );

            bundles.Add(new ScriptBundle("~/bundles/tam")
                .IncludeDirectory("~/App_Scripts/Routes/Tam", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Tam", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
            );
            bundles.Add(new ScriptBundle("~/bundles/library")
                .IncludeDirectory("~/App_Scripts/Routes/Library", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Library", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
            );

            bundles.Add(new ScriptBundle("~/bundles/museum")
                .IncludeDirectory("~/App_Scripts/Routes/Museum", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Museum", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
            );
            bundles.Add(new ScriptBundle("~/bundles/biomedical")
                .IncludeDirectory("~/App_Scripts/Routes/Biomedical", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Biomedical", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
            );
            bundles.Add(new ScriptBundle("~/bundles/candidate")
                .IncludeDirectory("~/App_Scripts/Routes/Candidate", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Candidate", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
            );
            bundles.Add(new ScriptBundle("~/bundles/associate")
                .IncludeDirectory("~/App_Scripts/Routes/Associate", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Associate", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
            );
            bundles.Add(new ScriptBundle("~/bundles/fellow")
                .IncludeDirectory("~/App_Scripts/Routes/Fellow", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Fellow", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
            );
            bundles.Add(new ScriptBundle("~/bundles/registrar")
                .IncludeDirectory("~/App_Scripts/Routes/Registrar", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Registrar", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
            );
            bundles.Add(new ScriptBundle("~/bundles/transport")
                .IncludeDirectory("~/App_Scripts/Routes/Transport", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Transport", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
            );
            bundles.Add(new ScriptBundle("~/bundles/dracademics")
                .IncludeDirectory("~/App_Scripts/Routes/DrAcademics", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/DrAcademics", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
                
            );
            bundles.Add(new ScriptBundle("~/bundles/exam")
                .IncludeDirectory("~/App_Scripts/Routes/Exam", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Exam", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
                
            );
            bundles.Add(new ScriptBundle("~/bundles/finance")
                .IncludeDirectory("~/App_Scripts/Routes/Finance", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Finance", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
               
            );

            bundles.Add(new ScriptBundle("~/bundles/president")
                .IncludeDirectory("~/App_Scripts/Routes/President", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/President", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
                
            );
            bundles.Add(new ScriptBundle("~/bundles/sgba")
                .IncludeDirectory("~/App_Scripts/Routes/Sgba", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Sgba", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/Memo", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Controllers/MailClient", "*.js", true)
                .IncludeDirectory("~/App_Scripts/Routes/MailClient", "*.js", true)
               
            );
            BundleTable.EnableOptimizations = false;
        }
    }
}