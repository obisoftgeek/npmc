﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;
using SilverEdgeProjects.NPMC.WebSite.Areas.Tam.Controllers;
using SilverEdgeProjects.NPMC.WebSite.Models;

namespace SilverEdgeProjects.NPMC.WebSite
{
    public class AutoMapperConfig
    {
        public static void Register()
        {
            UserProfile();
            Faculty();
            PaymentCategory();
            PaymentFee();
            Account();
            AccreditationStatus();
            SubSpecialty();
            Workshop();
            Memo();
            Department();
            PaymentTransaction();
            Institution();
            State();
            Exam();
            ExamRequirement();
            ExamType();
            ExamDiet();
            Mail();
            ExamCenter(); 
        }

        private static void Mail()
        {
            Mapper.CreateMap<Mail, MailModel>();
            Mapper.CreateMap<Mail, MailItem>();
            Mapper.CreateMap<MailModel, Mail>();
            Mapper.CreateMap<MailModel, MailItem>();
        }
        private static void ExamCenter()
        {
            Mapper.CreateMap<ExamCenter, ExamCenterModel>();
            Mapper.CreateMap<ExamCenter, ExamCenterItem>();
            Mapper.CreateMap<ExamCenterModel, ExamCenter>();
            Mapper.CreateMap<ExamCenterModel, ExamCenterItem>();
        }

        private static void Exam()
        {
            Mapper.CreateMap<Exam, ExamModel>();
            Mapper.CreateMap<Exam, ExamItem>();
            Mapper.CreateMap<ExamModel, Exam>();
            Mapper.CreateMap<ExamModel, ExamItem>();
        }

        private static void ExamRequirement()
        {
            Mapper.CreateMap<ExamRequirement, ExamRequirementModel>();
            Mapper.CreateMap<ExamRequirement, ExamRequirementItem>();
            Mapper.CreateMap<ExamRequirementModel, ExamRequirement>();
            Mapper.CreateMap<ExamRequirementItem, ExamRequirement>();
        }

        private static void ExamType()
        {
            Mapper.CreateMap<ExamType, ExamTypeModel>();
            Mapper.CreateMap<ExamType, ExamTypeItem>();
            Mapper.CreateMap<ExamTypeModel, ExamType>();
            Mapper.CreateMap<ExamTypeItem, ExamType>();
        }

        private static void ExamDiet()
        {
            Mapper.CreateMap<ExamDiet, ExamDietModel>();
            Mapper.CreateMap<ExamDiet, ExamDietItem>();
            Mapper.CreateMap<ExamDietModel, ExamDiet>();
            Mapper.CreateMap<ExamDietItem, ExamDiet>();
        }

        private static void State()
        {
            Mapper.CreateMap<State, StateModel>();
            Mapper.CreateMap<State, StateItem>();
            Mapper.CreateMap<StateModel, State>();
            Mapper.CreateMap<StateItem, State>();
        }


        private static void Institution()
        {
            Mapper.CreateMap<Institution, InstitutionModel>();
            Mapper.CreateMap<Institution, InstitutionItem>();
            Mapper.CreateMap<InstitutionModel, Institution>();
            Mapper.CreateMap<InstitutionItem, Institution>();
        }

        private static void Department()
        {
            Mapper.CreateMap<DepartmentItem, DepartmentModel>();
            Mapper.CreateMap<DepartmentModel, Department>();
            Mapper.CreateMap<DepartmentItem, Department>();
            Mapper.CreateMap<Department, DepartmentItem>();
        }

        private static void Memo()
        {
            Mapper.CreateMap<MemoThread, MemoThreadModel>();
            Mapper.CreateMap<MemoThreadModel, MemoThread>();
            Mapper.CreateMap<MemoThread, MemoThreadItem>().AfterMap((src, dest) =>
            {
                dest.RecipientName = src.MemoRecipient.UserProfile.FirstName + " " +
                                     src.MemoRecipient.UserProfile.LastName + " " +
                                     src.MemoRecipient.UserProfile.OtherNames;

                if (src.MemoRecipient.IsForwarded && src.MemoRecipient.MemoRecipient2 != null)
                {
                    dest.ForwardedBy = src.MemoRecipient.MemoRecipient2.UserProfile.FirstName + " " +
                                       src.MemoRecipient.MemoRecipient2.UserProfile.LastName + " " +
                                       src.MemoRecipient.MemoRecipient2.UserProfile.OtherNames;
                    dest.DateForwarded = src.MemoRecipient.DateForwarded;
                    dest.IsForwarded = src.MemoRecipient.IsForwarded;
                }
            });
            Mapper.CreateMap<MemoModel, Memo>();
            Mapper.CreateMap<Memo, MemoModel>().AfterMap((src, dest) => { });

            Mapper.CreateMap<Memo, MemoItem>().AfterMap((src, dest) =>
            {

                dest.TotalRecipients = src.MemoRecipients.Count();
                dest.SenderName = src.UserProfile.FirstName + " " + src.UserProfile.LastName + " " + src.UserProfile.OtherNames;
                dest.MemoBoxType = (int) MemoBoxType.SentBox;

                dest.SenderName = src.UserProfile.LastName + " " + src.UserProfile.FirstName + " " +
                                  src.UserProfile.OtherNames;
                dest.HasBeenRead = src.MemoRecipients.Any(x => x.ReadStatus == 1);
                dest.MemoStatusName = src.MemoStatu.Name;
                if (src.UserProfile.Staff != null)
                {
                    dest.SenderDesignationName = src.UserProfile.Staff.Designation != null
                        ? src.UserProfile.Staff.Designation.Name
                        : "";
                    dest.SenderProfilePhoto = src.UserProfile.ProfilePhoto;
                }
            });
            Mapper.CreateMap<MemoRecipientModel, MemoRecipient>();
            Mapper.CreateMap<MemoRecipient, MemoRecipientModel>();
            Mapper.CreateMap<MemoRecipient, MemoRecipientItem>().AfterMap((src, dest) =>
            {
                dest.Name = src.UserProfile.LastName + " " + src.UserProfile.FirstName + " " +
                            src.UserProfile.OtherNames;
                dest.ProfilePhoto = src.UserProfile.ProfilePhoto;
                if (src.IsForwarded && src.MemoRecipient2 != null)
                {
                    dest.ForwardedByName = src.MemoRecipient2.UserProfile.FirstName + " " +
                                       src.MemoRecipient2.UserProfile.LastName + " " +
                                       src.MemoRecipient2.UserProfile.OtherNames;
                    dest.DateForwarded = src.DateForwarded;
                }
            });
            Mapper.CreateMap<Memo, MemoDetail>().AfterMap((src, dest) =>
            {
                dest.SenderName = src.UserProfile.LastName + " " + src.UserProfile.FirstName + " " +
                                  src.UserProfile.OtherNames;


                dest.SenderName = src.UserProfile.FirstName + " " + src.UserProfile.LastName + " " + src.UserProfile.OtherNames;
                dest.SenderUnitName = src.UserProfile.Unit.Name;
                dest.SenderDepartmentName = src.UserProfile.Unit.Department.Name;
                dest.SenderLastName = src.UserProfile.LastName;
                dest.SenderFirstName = src.UserProfile.FirstName;
                dest.TotalRecipients = src.MemoRecipients.Count();
                dest.MemoStatusName = src.MemoStatu.Name;
                dest.MemoRecipients = Mapper.Map<IEnumerable<MemoRecipient>, List<MemoRecipientItem>>(src.MemoRecipients);
                dest.MemoThreads = Mapper.Map<IEnumerable<MemoThread>, List<MemoThreadItem>>(src.MemoThreads.OrderByDescending(c => c.Id));

                dest.SenderLastName = src.UserProfile.LastName;
                dest.SenderFirstName = src.UserProfile.FirstName;

                dest.MemoStatusName = src.MemoStatu.Name;
                dest.MemoRecipients =
                    Mapper.Map<IEnumerable<MemoRecipient>, List<MemoRecipientItem>>(src.MemoRecipients);

                if (src.UserProfile.Staff != null)
                {
                    dest.SenderDesignationName = src.UserProfile.Staff.Designation != null
                        ? src.UserProfile.Staff.Designation.Name
                        : "";
                    dest.SenderProfilePhoto = src.UserProfile.ProfilePhoto;
                }
            });

            Mapper.CreateMap<MemoRecipient, MemoItem>().AfterMap((src, dest) =>
            {
                dest.Id = src.MemoId;
                dest.Subject = src.Memo.Subject;
                dest.Body = src.Memo.Body;

                dest.TotalRecipients = src.Memo.MemoRecipients.Count();
                dest.MemoBoxType = (int)MemoBoxType.InBox;
                dest.MemoStatusId = src.Memo.MemoStatusId;
                dest.MemoStatusName = src.Memo.MemoStatu.Name;
                dest.FolderType = src.Memo.FolderType;
                dest.SenderName = src.Memo.UserProfile.FirstName + " " + src.Memo.UserProfile.LastName + " " + src.Memo.UserProfile.OtherNames;
                dest.SenderUnitName = src.Memo.UserProfile.Unit.Name;
                dest.HasBeenRead = src.Memo.MemoRecipients.Any(x => x.ReadStatus == 1);


                dest.MemoStatusId = src.Memo.MemoStatusId;
                dest.MemoStatusName = src.Memo.MemoStatu.Name;
                dest.FolderType = src.Memo.FolderType;
                dest.SenderName = src.Memo.UserProfile.LastName + " " + src.Memo.UserProfile.FirstName + " " +
                                  src.Memo.UserProfile.OtherNames;

                dest.MemoStatusName = src.Memo.MemoStatu.Name;
                if (src.Memo.UserProfile.Staff != null)
                {
                    dest.SenderDesignationName = src.Memo.UserProfile.Staff.Designation != null
                        ? src.Memo.UserProfile.Staff.Designation.Name
                        : "";
                    dest.SenderProfilePhoto = src.Memo.UserProfile.ProfilePhoto;
                }
            });
        }

        private static void PaymentTransaction()
        {
            Mapper.CreateMap<PaymentTransaction, PaymentTransactionModel>();
            Mapper.CreateMap<PaymentTransactionModel, PaymentTransaction>();
            Mapper.CreateMap<PaymentTransactionItem, PaymentTransaction>();
            Mapper.CreateMap<PaymentTransaction, PaymentTransactionItem>();
        }


        private static void SubSpecialty()
        {
            Mapper.CreateMap<SubSpecialty, SubSpecialtyModel>().AfterMap((src, dest) =>
            {
                dest.PaymentCategoryId = src.PaymentFee != null ? src.PaymentFee.PaymentCategoryId : 0;
            });
            Mapper.CreateMap<SubSpecialtyModel, SubSpecialty>();
            Mapper.CreateMap<SubSpecialtyItem, SubSpecialty>();
            Mapper.CreateMap<SubSpecialty, SubSpecialtyItem>();
        }

        private static void Workshop()
        {
            Mapper.CreateMap<Workshop, WorkshopModel>();
            Mapper.CreateMap<WorkshopModel, Workshop>();
            Mapper.CreateMap<WorkshopItem, Workshop>();
            Mapper.CreateMap<Workshop, WorkshopItem>();
        }


        private static void AccreditationStatus()
        {
            Mapper.CreateMap<AccreditationStatu, AccreditationStatusModel>();
            Mapper.CreateMap<AccreditationStatusModel, AccreditationStatu>();
            Mapper.CreateMap<AccreditationStatusItem, AccreditationStatu>();
            Mapper.CreateMap<AccreditationStatu, AccreditationStatusItem>();
        }

        private static void Faculty()
        {
            Mapper.CreateMap<Faculty, FacultyModel>().AfterMap((src, dest) =>
            {
                dest.PaymentCategoryId = src.PaymentFee != null ? src.PaymentFee.PaymentCategoryId : 0;
            });
            Mapper.CreateMap<FacultyModel, Faculty>();
            Mapper.CreateMap<FacultyItem, Faculty>();
            Mapper.CreateMap<Faculty, FacultyItem>();
        }

        private static void Account()
        {
            Mapper.CreateMap<Account, AccountModel>();
            Mapper.CreateMap<AccountModel, Account>();
            Mapper.CreateMap<AccountItem, Account>();
            Mapper.CreateMap<Account, AccountItem>();
        }

        private static void PaymentCategory()
        {
            Mapper.CreateMap<PaymentCategory, PaymentCategoryModel>();
            Mapper.CreateMap<PaymentCategoryModel, PaymentCategory>();
            Mapper.CreateMap<PaymentCategoryItem, PaymentCategory>();
            Mapper.CreateMap<PaymentCategory, PaymentCategoryItem>();
        }

        private static void PaymentFee()
        {
            Mapper.CreateMap<PaymentFee, PaymentFeeModel>();
            Mapper.CreateMap<PaymentFeeModel, PaymentFee>();
            Mapper.CreateMap<PaymentFeeItem, PaymentFee>();
            Mapper.CreateMap<PaymentFee, PaymentFeeItem>();
        }

        private static void UserProfile()
        {
            Mapper.CreateMap<ApplicationUser, AspNetUser>();
            Mapper.CreateMap<UserProfileModel, ApplicationUser>();
            Mapper.CreateMap<UserProfileModel, UserProfile>();
            Mapper.CreateMap<UserProfile, UserProfileModel>().AfterMap((src, dest) =>
            {
                dest.Id = src.AspNetUserId;
                dest.FullName = src.FirstName + " " + src.LastName + " " + src.OtherNames;
                dest.FirstAndLastName = src.FirstName + " " + src.LastName;
                dest.UserName = src.AspNetUser.UserName;
                dest.Email = src.AspNetUser.Email;
                dest.PhoneNumber = src.AspNetUser.PhoneNumber;
                dest.DepartmentId = src.Unit.DepartmentId;
                dest.DepartmentName = src.Unit.Department.Name;
                dest.EmailConfirmed = src.AspNetUser.EmailConfirmed;
            });
            Mapper.CreateMap<UserProfile, UserProfileItem>().AfterMap((src, dest) =>
            {
                dest.Id = src.AspNetUserId;
                dest.FullName = src.FirstName + " " + src.LastName + " " + src.OtherNames;
                dest.FirstAndLastName = src.FirstName + " " + src.LastName;
                dest.UserName = src.AspNetUser.UserName;
                dest.Email = src.AspNetUser.Email;
                dest.PhoneNumber = src.AspNetUser.PhoneNumber;
                dest.DepartmentId = src.Unit.DepartmentId;
                dest.DepartmentName = src.Unit.Department.Name;
                dest.EmailConfirmed = src.AspNetUser.EmailConfirmed;
            });
            Mapper.CreateMap<UserProfile, UserProfileDetail>().AfterMap((src, dest) =>
            {
                dest.Id = src.AspNetUserId;
                dest.FullName = src.FirstName + " " + src.LastName + " " + src.OtherNames;
                dest.FirstAndLastName = src.FirstName + " " + src.LastName;
                dest.UserName = src.AspNetUser.UserName;
                dest.Email = src.AspNetUser.Email;
                dest.PhoneNumber = src.AspNetUser.PhoneNumber;
                dest.DepartmentId = src.Unit.DepartmentId;
                dest.DepartmentName = src.Unit.Department.Name;
                dest.EmailConfirmed = src.AspNetUser.EmailConfirmed;
            });
        }
    }
}