﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SilverEdgeProjects.NPMC.WebSite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            ////routes.MapRoute(
            ////    name: "Default",
            ////    url: "{*catchall}",
            ////    defaults: new { controller = "Site", action = "Index" },
            ////    namespaces: new[] { "NPMC.WebSite.Controllers" }
            ////);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Site", action = "Account", id = UrlParameter.Optional },
                 namespaces: new[] { "NPMC.WebSite.Controllers" }
            );
        }
    }
}
