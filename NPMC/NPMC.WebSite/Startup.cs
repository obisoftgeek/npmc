﻿using Microsoft.Owin;
using Owin;
using SilverEdgeProjects.NPMC.WebSite;

[assembly: OwinStartupAttribute(typeof(Startup))]
namespace SilverEdgeProjects.NPMC.WebSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
