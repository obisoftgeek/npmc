﻿using System;
using System.Web.Http;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    [RoutePrefix("Api/Memo")]
    public class MemoController : BaseApiController
    {
        private readonly IMemoService _unitService;
        public MemoController(IUserProfileService userProfileService, IMemoService unitService) 
            : base(userProfileService)
        {
            _unitService = unitService;
        }

        [Route("Count")]
        public IHttpActionResult GetCount(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _unitService.GetCount(page, size, GetCurrentUserId(), whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/Memo
        public IHttpActionResult Get(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _unitService.GetQuery(page, size, GetCurrentUserId(), whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("Create")]
        public IHttpActionResult Post(MemoModel model)
        {
            try
            {
                _unitService.Create(model, GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
       
        [Route("Update")]
        public IHttpActionResult Put(MemoModel model)
        {
            try
            {
                _unitService.Update(model, GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        // DELETE: api/Memo/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _unitService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetItem/{id}")]
        public IHttpActionResult GetItem(int id)
        {
            try
            {
                var responseData = _unitService.GetItem(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetDetail/{id}")]
        public IHttpActionResult GetDetail(int id)
        {
            try
            {
                var responseData = _unitService.GetDetail(id, GetCurrentUserId());
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("CreateMemoThread")]
        public IHttpActionResult PostMemoThread(MemoThreadModel model)
        {
            try
            {
                _unitService.CreateMemoThread(model, GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("ForwardMemo")]
        public IHttpActionResult PostForwardMemo(ForwardMemoModel model)
        {
            try
            {
                _unitService.ForwardMemo(model, GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
