﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    public class FileManagerController : ApiController
    {
        [Route("api/FileManager/uploadFiles")]
        public IHttpActionResult UploadFiles()
        {
            var files = new List<string>();
            if (!HttpContext.Current.Request.Files.AllKeys.Any())
                return BadRequest("Could not upload File");

            // Get the uploaded image from the Files collection

            foreach (var file in HttpContext.Current.Request.Files.AllKeys)
            {

                var httpPostedFile = HttpContext.Current.Request.Files[file];
                if (httpPostedFile != null)
                {
                    // Validate the uploaded image(optional)

                    // Get the complete file path
                    var filename = Path.GetFileNameWithoutExtension(httpPostedFile.FileName) +
                                   Guid.NewGuid() + Path.GetExtension(httpPostedFile.FileName);
                    var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Uploads/Documents"),
                        filename);

                    // Save the uploaded file to "UploadedFiles" folder
                    httpPostedFile.SaveAs(fileSavePath);
                    files.Add(filename);
                }
            }
            return Ok(files.ToArray());
        }
        [Route("api/FileManager/uploadProfileImage")]
        public IHttpActionResult UploadProfileImage()
        {
            var files = new List<string>();
            if (!HttpContext.Current.Request.Files.AllKeys.Any())
                return BadRequest("Could not upload File");

            // Get the uploaded image from the Files collection

            foreach (var file in HttpContext.Current.Request.Files.AllKeys)
            {

                var httpPostedFile = HttpContext.Current.Request.Files[file];

                if (httpPostedFile != null)
                {
                    var fileSize = httpPostedFile.ContentLength;
                    if (fileSize > 1048576)  // 1MB
                    {
                        // File exceeds the file maximum size
                        return BadRequest("Picture size exceeded 1mb!");
                    }
                    // Validate the uploaded image(optional)
                    var existingFileName = httpPostedFile.FileName.Replace("\"", string.Empty);

                    var ext = Path.GetExtension(existingFileName.Replace("\"", string.Empty));
                    var validExtensions = new List<string>() { ".jpg", ".jpeg", ".png" };

                    if (!validExtensions.Contains(ext, StringComparer.OrdinalIgnoreCase))
                    {
                        return BadRequest("Invalid file type. File type must be either a .jpg, .jpeg or .png type");
                    }
                    // Get the complete file path
                    var filename = Path.GetFileNameWithoutExtension(httpPostedFile.FileName) +
                                   Guid.NewGuid() + Path.GetExtension(httpPostedFile.FileName);
                    var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Uploads/ProfilePhotos"),
                        filename);

                    // Save the uploaded file to "UploadedFiles" folder
                    httpPostedFile.SaveAs(fileSavePath);
                    files.Add(filename);
                }
            }
            return Ok(files.ToArray());
        }
    }
    
}
