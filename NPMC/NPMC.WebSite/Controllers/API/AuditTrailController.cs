﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Library.ScvManager;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    public class AuditTrailController : BaseApiController
    {
        private readonly IAuditTrailService _auditTrailService;

        public AuditTrailController(IAuditTrailService auditTrailService, IUserProfileService userProfileService)
            : base(userProfileService)
        {
            _auditTrailService = auditTrailService;
        }
         [Route("api/AuditTrail/AuditSections")]
        public IEnumerable<TextAndValue> GetAuditSections()
        {
            return  _auditTrailService.GetAuditSections();
        }
         [Route("api/AuditTrail/AuditActions")]
         public IEnumerable<TextAndValue> GetAuditActions(int auditSectionId)
        {
            return _auditTrailService.GetAuditActions(0,auditSectionId);
        }
        // GET: api/Client
        [Route("api/AuditTrail/Count")]
        public object GetCount(int page, int size,string orderByExpression = null, string whereCondition = null)
        {
            var filter = AuditTrailFilter.Deserilize(whereCondition);
            return _auditTrailService.GetCount(page, size, filter, orderByExpression);
        }
        public IEnumerable<AuditTrailModel> Get(int page, int size,string orderByExpression = null, string whereCondition = null)
        {
            var filter = AuditTrailFilter.Deserilize(whereCondition);
            return _auditTrailService.Query(page, size, filter,orderByExpression);
        }
        [Route("api/AuditTrail/DownloadReport")]
        public HttpResponseMessage GetExportEmail(string whereCondition)
        {
            var filter = AuditTrailFilter.Deserilize(whereCondition);

            var csv = new CsvExport<AuditTrailItem>(new List<int>() { 0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
                _auditTrailService.DownloadReport(filter));

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(csv.ExportToBytes())
            };
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "audits.csv"
            };
            return result;
        }
    }
}
