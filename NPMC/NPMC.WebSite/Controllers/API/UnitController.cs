﻿using System;
using System.Web.Http;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    [RoutePrefix("Api/Unit")]
    public class UnitController : BaseApiController
    {
        private readonly IUnitService _unitService;
        public UnitController(IUserProfileService userProfileService, IUnitService unitService) 
            : base(userProfileService)
        {
            _unitService = unitService;
        }

        [Route("Count")]
        public IHttpActionResult GetCount(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _unitService.GetCount(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/Unit
        public IHttpActionResult Get(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _unitService.GetQuery(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("Create")]
        public IHttpActionResult Post(UnitModel model)
        {
            try
            {
                try
                {
                    _unitService.Create(model, GetCurrentUserId());
                    return Ok();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("Update")]
        public IHttpActionResult Put(UnitModel model)
        {
            try
            {
                _unitService.Update(model, GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("Delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _unitService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetItem/{id}")]
        public IHttpActionResult GetItem(int id)
        {
            try
            {
                var responseData = _unitService.GetItem(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetDetail/{id}")]
        public IHttpActionResult GetDetail(int id)
        {
            try
            {
                var responseData = _unitService.GetDetail(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetLookup/{departmentId}")]
        public IHttpActionResult GetLookup(int departmentId)
        {
            try
            {
                var responseData = _unitService.GetLookup(departmentId);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
