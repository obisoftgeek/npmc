﻿using System;
using System.Web.Http;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    [RoutePrefix("Api/AccreditationStatus")]
    public class AccreditationStatusController : BaseApiController
    {
        private readonly IAccreditationStatusService _departmentService;

        public AccreditationStatusController(IUserProfileService userProfileService,
            IAccreditationStatusService departmentService)
            : base(userProfileService)
        {
            _departmentService = departmentService;
        }

        [Route("Count")]
        public IHttpActionResult GetCount(int page, int size, string whereCondition = null,
            string orderByExpression = null)
        {
            try
            {
                var responseData = _departmentService.GetCount(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                var responseData = _departmentService.GetAccreditationStatus(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/AccreditationStatus
        public IHttpActionResult Get(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _departmentService.GetQuery(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Post(AccreditationStatusModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _departmentService.Create(model, GetCurrentUserId());
                    return Ok();
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Put(AccreditationStatusModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _departmentService.Update(model, GetCurrentUserId());
                    return Ok();
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                _departmentService.Delete(id, GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("GetDetail/{id}")]
        public IHttpActionResult GetDetail(int id)
        {
            try
            {
                var responseData = _departmentService.GetDetail(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("GetLookup")]
        public IHttpActionResult GetLookup()
        {
            try
            {
                var responseData = _departmentService.GetLookup();
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}