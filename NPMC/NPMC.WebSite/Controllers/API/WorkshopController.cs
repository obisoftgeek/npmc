﻿using System;
using System.Data.Entity.Core;
using System.Web.Http;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    [RoutePrefix("Api/Workshop")]
    public class WorkshopController : BaseApiController
    {
        private readonly IWorkshopService _WorkshopService;
        public WorkshopController(IUserProfileService userProfileService, IWorkshopService WorkshopService) 
            : base(userProfileService)
        {
            _WorkshopService = WorkshopService;
        }

        [Route("Count")]
        public IHttpActionResult GetCount(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _WorkshopService.GetCount(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/Workshop
        public IHttpActionResult Get(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _WorkshopService.GetQuery(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("Create")]
        public IHttpActionResult Post(WorkshopModel model)
        {

           
                try
                {
                    if (ModelState.IsValid)
                    {
                    _WorkshopService.Create(model, GetCurrentUserId());
                    return Ok();
                    }
                    return BadRequest(ModelState);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
          
        }
        [Route("Update")]
        public IHttpActionResult Put(WorkshopModel model)
        {
            try
            {
                _WorkshopService.Update(model, GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
      
        // DELETE: api/Workshop/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _WorkshopService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(_WorkshopService.GetWorkshop((id)));
            }
            catch (ObjectNotFoundException  ex)
            {

                return BadRequest(ex.Message);
            }
        }
        
        [Route("GetDetail/{id}")]
        public IHttpActionResult GetDetail(int id)
        {
            try
            {
                var responseData = _WorkshopService.GetDetail(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetLookup")]
        public IHttpActionResult GetLookup()
        {
            try
            {
                var responseData = _WorkshopService.GetLookup();
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
