﻿using System;
using System.Web.Http;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    [RoutePrefix("Api/State")]
    public class StateController : BaseApiController
    {
        private readonly IStateService _stateService;

        public StateController(IUserProfileService userProfileService, IStateService stateService) : base(userProfileService)
        {
            _stateService = stateService;
        }
        [Route("GetLookUp")]
        // GET: api/State
        public IHttpActionResult GetLookup()
        {
            try
            {
                var responseData = _stateService.GetLookup();
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
