﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Web.Http;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    [RoutePrefix("api/SubSpecialty")]
    public class SubSpecialtyController : BaseApiController
    {
        private readonly ISubSpecialtyService _subSpecialtyService;
        public SubSpecialtyController(IUserProfileService userProfileService, ISubSpecialtyService subSpecialtyService) 
            : base(userProfileService)
        {
            _subSpecialtyService = subSpecialtyService;
        }

        [Route("Count")]
        public IHttpActionResult GetCount(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _subSpecialtyService.GetCount(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(_subSpecialtyService.GetById(id));
            }
            catch (ObjectNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/SubSpecialty
        public IHttpActionResult Get(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _subSpecialtyService.GetQuery(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult Post(SubSpecialtyModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _subSpecialtyService.Create(model, GetCurrentUserId());
                    return Ok();
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult Put(SubSpecialtyModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _subSpecialtyService.Update(model, GetCurrentUserId());
                    return Ok();
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _subSpecialtyService.Delete(id, GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
         
        [Route("GetDetail/{id}")]
        public IHttpActionResult GetDetail(int id)
        {
            try
            {
                var responseData = _subSpecialtyService.GetDetail(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("NameAndValues")]
        public IEnumerable<LookupModel> GetLookup()
        {
            var filter = new SubSpecialtyFilter();
            var responseData = _subSpecialtyService.GetLookup(filter);
            return responseData;
        }

    }
}
