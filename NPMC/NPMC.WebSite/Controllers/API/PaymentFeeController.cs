﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Web.Http;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    [RoutePrefix("api/PaymentFee")]
    public class PaymentFeeController : BaseApiController
    {
        private readonly IPaymentFeeService _paymentFeeService;
        public PaymentFeeController(IUserProfileService userProfileService, IPaymentFeeService paymentFeeService) 
            : base(userProfileService)
        {
            _paymentFeeService = paymentFeeService;
        }

        [Route("Count")]
        public IHttpActionResult GetCount(int page, int size, int paymentCategoryId,string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var filter = PaymentFeeFilter.Deserilize(whereCondition);
                filter.PaymentCategoryId = paymentCategoryId;
                var responseData = _paymentFeeService.GetCount(page, size, filter, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(_paymentFeeService.GetById(id));
            }
            catch (ObjectNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/PaymentFee
        public IHttpActionResult Get(int page, int size, int paymentCategoryId,string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var filter = PaymentFeeFilter.Deserilize(whereCondition);
                filter.PaymentCategoryId = paymentCategoryId;
                var responseData = _paymentFeeService.GetQuery(page, size, filter, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult Post(PaymentFeeModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _paymentFeeService.Create(model, GetCurrentUserId());
                    return Ok();
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult Put(PaymentFeeModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _paymentFeeService.Update(model, GetCurrentUserId());
                    return Ok();
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _paymentFeeService.Delete(id, GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
         
        [Route("GetDetail/{id}")]
        public IHttpActionResult GetDetail(int id)
        {
            try
            {
                var responseData = _paymentFeeService.GetDetail(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("NameAndValues")]
        public IEnumerable<LookupModel> GetLookup(int paymentCategoryId=0)
        {
            var filter = new PaymentFeeFilter() { PaymentCategoryId = paymentCategoryId };
            var responseData = _paymentFeeService.GetLookup(filter);
            return responseData;
        }

    }
}
