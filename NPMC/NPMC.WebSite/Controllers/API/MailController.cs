﻿using System;
using System.Data.Entity.Core;
using System.Web.Http;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models.ViewModels;
using SilverEdgeProjects.NPMC.Models;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    [RoutePrefix("api/Mail")]
    public class MailController : BaseApiController
    {
        private readonly IMailService _mailService;
        public MailController(IUserProfileService userProfileService, IMailService MailService) 
            : base(userProfileService)
        {
            _mailService = MailService;
        }

        [Route("Count")]
        public IHttpActionResult GetCount(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var filter = MailFilter.Deserilize(whereCondition);
                if(CurrentUserInfo.UnitId != (int)UserUnit.Registrar)
                {
                    filter.RecepientId = GetCurrentUserId();
                }
                var responseData = _mailService.GetCount(page, size, filter, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(_mailService.GetById(id));
            }
            catch (ObjectNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/Mail
        public IHttpActionResult Get(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var filter = MailFilter.Deserilize(whereCondition);
                if (CurrentUserInfo.UnitId != (int)UserUnit.Registrar)
                {
                    filter.RecepientId = GetCurrentUserId();
                }
                var responseData = _mailService.GetQuery(page, size, filter, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult Post(MailModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _mailService.Create(model, GetCurrentUserId());
                    return Ok();
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("UpdateToReceived"), HttpPut]
        public IHttpActionResult UpdateToReceived(MailModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _mailService.UpdateToReceived(model, GetCurrentUserId());
                    return Ok();
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        // DELETE: api/Mail/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _mailService.Delete(id, GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
         
        [Route("GetDetail/{id}")]
        public IHttpActionResult GetDetail(int id)
        {
            try
            {
                var responseData = _mailService.GetDetail(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
       
    }
}
