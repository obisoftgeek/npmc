﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
﻿using Microsoft.AspNet.Identity;
﻿using Microsoft.AspNet.Identity.Owin;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;
using SilverEdgeProjects.NPMC.WebSite.Models;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    [RoutePrefix("Api/UserProfile")]
    public class UserProfileController : BaseApiController
    {
        public UserProfileController(IUserProfileService userProfileService) 
            : base(userProfileService)
        {
             
        }
        [Route("Count")]
        public IHttpActionResult GetCount(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _userProfileService.GetCount(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/UserProfile
        public IHttpActionResult Get(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _userProfileService.GetQuery(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("Create")]
        public async Task<IHttpActionResult> Post(UserProfileModel model)
        {
            try
            {
                var password = GetPassword();
                GlobalVariables.UserProfileData = model;
                GlobalVariables.UserProfileType = UserProfileTypeEnum.StaffUser;
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, password);         
                if (result.Succeeded)
                {
                    await SendEmailConfirmationLinkAsync(user, password);
                    return Ok();                   
                }
                return BadRequest(result.Errors.First());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [AllowAnonymous]
        [Route("SignupCandidate")]
        public async Task<IHttpActionResult> PostCandidate(UserProfileModel model)
        {
            try
            {
                var password = GetPassword();
                model.UnitId = (int) UserUnit.Candidate;
                GlobalVariables.UserProfileData = model;
                GlobalVariables.UserProfileType = UserProfileTypeEnum.Candidate;
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                    await SendEmailConfirmationLinkAsync(user, password);
                    return Ok();
                }
                return BadRequest(result.Errors.First());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [AllowAnonymous]
        [Route("ConfirmEmail")]
        public async Task<IHttpActionResult> PostConfirmEmail(EmailConfirmationModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Token))
                    throw new Exception("Missing email confirmation token");

                if (model.UserId == 0)
                    throw new Exception("Missing user Id");

                var result = await UserManager.ConfirmEmailAsync(model.UserId, model.Token);
                if (result.Succeeded)
                {
                    _userProfileService.UpdateMustChangePassword(model.UserId, true);
                    return Ok();
                }
                return BadRequest(result.Errors.First());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("Update")]
        public IHttpActionResult Put(UserProfileModel model)
        {
            try
            {
                _userProfileService.Update(model,GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        // DELETE: api/UserProfile/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _userProfileService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetItem/{id}")]
        public IHttpActionResult GetItem(int id)
        {
            try
            {
                var responseData = _userProfileService.GetItem(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetDetail/{id}")]
        public IHttpActionResult GetDetail(int id)
        {
            try
            {
                var responseData = _userProfileService.GetDetail(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetCurrentUserProfile")]
        public IHttpActionResult GetCurrentUserProfile()
        {
            try
            {
                var responseData = _userProfileService.GetDetail(GetCurrentUserId());
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("UpdatePassword")]
        public async Task<IHttpActionResult> PutChangePassword(ChangePasswordModel model)
        {
            try
            {
               
                if (!model.NewPassword.Equals(model.ConfirmPassword))
                {
                    return BadRequest("The new password and confirmation password do not match.");

                }
                var result = await UserManager.ChangePasswordAsync(GetCurrentUserId(), model.OldPassword,model.NewPassword);

                if (result.Succeeded)
                {
                    _userProfileService.UpdateMustChangePassword(GetCurrentUserId(), false);
                    return Ok();
                }
          
                return BadRequest(result.Errors.First());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [AllowAnonymous]
        [Route("ForgotPassword")]
        public async Task<IHttpActionResult> PostForgotPassword(ForgotPasswordModel model)
        {
            try
            {
                var password = GetPassword();
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    return BadRequest("Invalid email address!");

                }
                if (!user.EmailConfirmed)
                {
                    return BadRequest("Your email has not been confirmed! Please check the mail we sent to your email address.");
                }
                await SendEmailConfirmationLinkAsync(user, password);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetLookupByDept/{departmentId}")]
        public IHttpActionResult GetLookupByDept(int departmentId)
        {
            try
            {
                var responseData = _userProfileService.GetLookupByDept(departmentId);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
