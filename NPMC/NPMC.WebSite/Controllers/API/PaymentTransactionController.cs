﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Library.ScvManager;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    [RoutePrefix("api/PaymentTransaction")]
    public class PaymentTransactionController : BaseApiController
    {
        private readonly IPaymentTransactionService _paymentTransactionService;
        public PaymentTransactionController(IUserProfileService userProfileService, IPaymentTransactionService paymentTransactionService) 
            : base(userProfileService)
        {
            _paymentTransactionService = paymentTransactionService;
        }

        [Route("Count")]
        public IHttpActionResult GetCount(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var filter = PaymentTransactionFilter.Deserialize(whereCondition);
                var responseData = _paymentTransactionService.GetCount(page, size, filter, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(_paymentTransactionService.GetById(id));
            }
            catch (ObjectNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/PaymentTransaction
        public IHttpActionResult Get(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var filter = PaymentTransactionFilter.Deserialize(whereCondition);
                var responseData = _paymentTransactionService.Query(page, size, filter, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("MakePayment")]
        public IHttpActionResult MakePayment(PayModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _paymentTransactionService.MakePayment(model);
                    return Ok();
                }
                return BadRequest(ModelState);
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong. Please try again");
            }
        }

        [Route("PaymentDetails")]
        public PaymentDetail GetPaymentDetails(int id)
        {
            var response = _paymentTransactionService.GetPaymentDetails(id);
            return response;
        }
        [Route("DownloadReport")]
        public HttpResponseMessage GetExportEmail(string whereCondition)
        {
            var filter = PaymentTransactionFilter.Deserialize(whereCondition);

            var csv = new CsvExport<PaymentTransactionItem>(new List<int>() { 0, 1, 2, 3, 4, 5, 7, 8, 9, 10,11,12,13,14,15 },
                _paymentTransactionService.DownloadReport(filter));

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(csv.ExportToBytes())
            };
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "payments.csv"
            };
            return result;
        }
    }
}
