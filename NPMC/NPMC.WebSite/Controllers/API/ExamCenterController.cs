﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Web.Http;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;
using SilverEdgeProjects.NPMC.DataAccess.EF;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    [RoutePrefix("api/ExamCenter")]
    public class ExamCenterController : BaseApiController
    {
        private readonly IExamCenterService _ExamCenterService;
        public ExamCenterController(IUserProfileService userProfileService, IExamCenterService ExamCenterService) 
            : base(userProfileService)
        {
            _ExamCenterService = ExamCenterService;
        }

        [Route("Count")]
        public IHttpActionResult GetCount(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _ExamCenterService.GetCount(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(_ExamCenterService.GetById(id));
            }
            catch (ObjectNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/ExamCenter
        public IHttpActionResult Get(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _ExamCenterService.GetQuery(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult Post(ExamCenterModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _ExamCenterService.Create(model, GetCurrentUserId());
                    return Ok();
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult Put(ExamCenterModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _ExamCenterService.Update(model, GetCurrentUserId());
                    return Ok();
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        // DELETE: api/ExamCenter/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _ExamCenterService.Delete(id, GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
         
        [Route("GetDetail/{id}")]
        public IHttpActionResult GetDetail(int id)
        {
            try
            {
                var responseData = _ExamCenterService.GetDetail(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("NameAndValues")]
        public IEnumerable<LookupModel> GetLookup()
        {
            var filter = new ExamCenterFilter();
            var responseData = _ExamCenterService.GetLookup(filter);
            return responseData;
        }
        [Route("ToggleActive/{id}")]
        public IHttpActionResult ToggleActive(int id)
        {
            try
            {
                _ExamCenterService.ToggleActive(id, GetCurrentUserId());
                return Ok(_ExamCenterService.GetDetail(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
