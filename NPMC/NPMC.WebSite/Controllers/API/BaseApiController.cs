using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.WebSite.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    [Authorize]
    public class BaseApiController : ApiController
    {
        private ApplicationUserManager _userManager;
        protected readonly IUserProfileService _userProfileService;

        public BaseApiController(IUserProfileService userProfileService)
        {
            _userProfileService = userProfileService;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public int GetCurrentUserId()
        {

            return Convert.ToInt32(User.Identity.GetUserId());
        }
        
        public UserProfileModel CurrentUserInfo
        {
            get
            {
                var currentUserId = GetCurrentUserId();
                if (currentUserId == 0) return new UserProfileModel();
                var user = _userProfileService.GetUserProfile(currentUserId);
                return user;
            }
        } 
        [NonAction]
        public string GetPassword()
        {
            return "password1";
        }
        public async Task SendEmailConfirmationLinkAsync(ApplicationUser user, string password)
        {
            try
            {
                string token = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                var callbackUrl = string.Format("{0}/Site/Account#/account/emailconfirmation?token={1}&userId={2}", baseUrl, HttpUtility.UrlEncode(token), HttpUtility.UrlEncode(user.Id.ToString()));

                var theMessage = "Please confirm your email by clicking this link: " + callbackUrl;
                var userProfile = _userProfileService.GetUserProfile(user.Id);
                _userProfileService.UpdateMustChangePassword(user.Id, true);

                var identitMmessage = new IdentityMessage
                {
                    Destination = user.Email,
                    Subject = "Email Confirmation",
                    Body = EmailHelper.GetMessageBody(userProfile.FullName, theMessage, user.UserName, password)
                };
                await UserManager.EmailService.SendAsync(identitMmessage);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
