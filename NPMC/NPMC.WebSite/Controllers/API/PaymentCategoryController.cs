﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Web.Http;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    [RoutePrefix("api/PaymentCategory")]
    public class PaymentCategoryController : BaseApiController
    {
        private readonly IPaymentCategoryService _paymentCategoryService;
        public PaymentCategoryController(IUserProfileService userProfileService, IPaymentCategoryService paymentCategoryService) 
            : base(userProfileService)
        {
            _paymentCategoryService = paymentCategoryService;
        }

        [Route("Count")]
        public IHttpActionResult GetCount(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _paymentCategoryService.GetCount(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(_paymentCategoryService.GetById(id));
            }
            catch (ObjectNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/PaymentCategory
        public IHttpActionResult Get(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _paymentCategoryService.GetQuery(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult Post(PaymentCategoryModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _paymentCategoryService.Create(model, GetCurrentUserId());
                    return Ok();
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult Put(PaymentCategoryModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _paymentCategoryService.Update(model, GetCurrentUserId());
                    return Ok();
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _paymentCategoryService.Delete(id, GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
         
        [Route("GetDetail/{id}")]
        public IHttpActionResult GetDetail(int id)
        {
            try
            {
                var responseData = _paymentCategoryService.GetDetail(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("NameAndValues")]
        public IEnumerable<LookupModel> GetLookup()
        {
            var filter = new PaymentCategoryFilter();
            return _paymentCategoryService.GetLookup(filter);
        }

    }
}
