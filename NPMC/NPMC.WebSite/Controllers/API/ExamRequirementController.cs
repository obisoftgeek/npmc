﻿using System;
using System.Web.Http;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.API
{
    [RoutePrefix("Api/ExamRequirement")]
    public class ExamRequirementController : BaseApiController
    {
        private readonly IExamRequirementService _ExamRequirementService;
        public ExamRequirementController(IUserProfileService userProfileService, IExamRequirementService ExamRequirementService)
            : base(userProfileService)
        {
            _ExamRequirementService = ExamRequirementService;
        }

        [Route("Count")]
        public IHttpActionResult GetCount(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _ExamRequirementService.GetCount(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }




        // GET: api/ExamRequirement
        public IHttpActionResult Get(int page, int size, string whereCondition = null, string orderByExpression = null)
        {
            try
            {
                var responseData = _ExamRequirementService.GetQuery(page, size, whereCondition, orderByExpression);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [Route("getExamRequirements")]
        public IHttpActionResult GetExamRequirement(int examId)
        {
            try
            {
                var responseData = _ExamRequirementService.GetExamRequirements(examId);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("Create")]
        public IHttpActionResult Post(ExamRequirementModel model)
        {
            try
            {
                try
                {
                    _ExamRequirementService.Create(model, GetCurrentUserId());
                    return Ok();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("Update")]
        public IHttpActionResult Put(ExamRequirementModel model)
        {
            try
            {
                _ExamRequirementService.Update(model, GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                _ExamRequirementService.Delete(id,GetCurrentUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetItem/{id}")]
        public IHttpActionResult GetItem(int id)
        {
            try
            {
                var responseData = _ExamRequirementService.GetItem(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetDetail/{id}")]
        public IHttpActionResult GetDetail(int id)
        {
            try
            {
                var responseData = _ExamRequirementService.GetDetail(id);
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetLookup")]
        public IHttpActionResult GetLookup()
        {
            try
            {
                var responseData = _ExamRequirementService.GetLookup();
                return Ok(responseData);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}

