﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;


namespace SilverEdgeProjects.NPMC.WebSite.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
      
        private ApplicationUserManager _userManager;
        protected readonly IUserProfileService _userProfileService;

        public BaseController(IUserProfileService userProfileService)
        {
            _userProfileService = userProfileService;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
              
            }
            private set
            {
                _userManager = value;
            }
        }
        public int GetUserId()
        {
           return Convert.ToInt32(User.Identity.GetUserId());
        }

    }
}