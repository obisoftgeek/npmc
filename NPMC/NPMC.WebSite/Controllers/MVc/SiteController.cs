﻿using System;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Library.AutoFac;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.Utilities;

namespace SilverEdgeProjects.NPMC.WebSite.Controllers.MVc
{
 

    public class SiteController : BaseController
    {
        // private IAuthenticationManager AuthenticationManager { get  HttpContext.GetOwinContext().Authentication; }
        private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        public SiteController(IUserProfileService userProfileService)
            : base(userProfileService)
        {
 
        }
        [AllowAnonymous]
        public ActionResult Account()
        {
            return View();
        }
 
        public ActionResult Logout()
        {
            var userProfile = _userProfileService.GetUserProfile(GetUserId());
            AuthenticationManager.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            var auditTrailService = EngineContext.Current.Resolve<IAuditTrailService>();


            auditTrailService.Log(userProfile.FirstName + " " + userProfile.LastName + " logged out", GetUserId(), AuditConstants.UserManagementSection, AuditConstants.LogoutFromTheSystemAction);
            return RedirectToAction("Account", "Site");
        }
        [AllowAnonymous]
        public ActionResult Index()
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    var userProfile = _userProfileService.GetUserProfile(GetUserId());
                    ViewBag.User = userProfile.FirstAndLastName;
                    switch (userProfile.UnitId)
                    {
                        case (int)UserUnit.Ict:
                            ViewBag.SideBar = "_ICTSideBar";
                            return View("ICT");
                        case (int)UserUnit.Registrar:
                            ViewBag.SideBar = "_RegistrarSideBar";
                            return View("Registrar");
                        case (int)UserUnit.Biomedical:
                            ViewBag.SideBar = "_BiomedicalSideBar";
                            return View("Biomedical");
                        case (int)UserUnit.DrAcademics:
                            ViewBag.SideBar = "_DrAcademicsSideBar";
                            return View("DrAcademics");
                        case (int)UserUnit.Exam:
                            ViewBag.SideBar = "_ExamSideBar";
                            return View("Exam");
                        case (int)UserUnit.Library:
                            ViewBag.SideBar = "_LibrarySideBar";
                            return View("Library");
                        case (int)UserUnit.Museum:
                            ViewBag.SideBar = "_MuseumSideBar";
                            return View("Museum");
                        case (int)UserUnit.Tam:
                            ViewBag.SideBar = "_TamSideBar";
                            return View("Tam");
                        case (int)UserUnit.Transport:
                            ViewBag.SideBar = "_TransportSideBar";
                            return View("Transport");
                        case (int)UserUnit.Candidate:
                            ViewBag.SideBar = "_CandidateSideBar";
                            return View("Candidate");
                        case (int)UserUnit.Fellow:
                            ViewBag.SideBar = "_FellowSideBar";
                            return View("Fellow");
                        case (int)UserUnit.Finance:
                            ViewBag.SideBar = "_FinanceSideBar";
                            return View("Finance");
                        case (int)UserUnit.Sgba:
                            ViewBag.SideBar = "_SGBASideBar";
                            return View("SGBA");
                        case (int)UserUnit.President:
                            ViewBag.SideBar = "_PresidentSideBar";
                            return View("President");
                        default:
                            return RedirectToAction("Account");
                    }
                }
                return RedirectToAction("Account");

            }
            catch (Exception ex)
            {
                return RedirectToAction("Account");
            }
        }
 
    }

}