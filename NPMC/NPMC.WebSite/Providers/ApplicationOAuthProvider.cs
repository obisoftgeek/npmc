﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Autofac;
using Library.AutoFac;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.Utilities;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.WebSite.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;
     
        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
           
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                var appUserManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

                var appUser = await appUserManager.FindAsync(context.UserName, context.Password);

                if (appUser == null)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                  
                    return;
                }

                //if (!appUser.EmailConfirmed)
                //{
                //    context.SetError("email_not_confirmed", "Your account has not been confrimed! Please check the mail we sent to your email address.");

                //    return;
                //}
                var auditTrailService = EngineContext.Current.Resolve<IAuditTrailService>();
                var userProfile = EngineContext.Current.Resolve<IUserProfileService>().GetUserProfile(appUser.Id);
 
                auditTrailService.Log(userProfile.FirstName + " " + userProfile.LastName + " signed in", appUser.Id, AuditConstants.UserManagementSection, AuditConstants.LoginToTheSystemAction);
                var claimsIdentity = new ClaimsIdentity(context.Options.AuthenticationType);

                var cookiesIdentity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.NameIdentifier, appUser.Id.ToString()),
                            new Claim(ClaimTypes.Name, appUser.Id.ToString()) }, CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = CreateProperties(userProfile, false);
                AuthenticationTicket authTicket = new AuthenticationTicket(claimsIdentity, properties);

                context.Validated(authTicket);
                context.Request.Context.Authentication.SignIn(cookiesIdentity);
                if (userProfile.UnitId == (int)UserUnit.Ict)
                {
                    SeedStartupItems.SetupStates(appUser.Id);
                }

            }
            catch (Exception ex)
            {
                context.SetError("Error", ex.Message);
                
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(UserProfileModel user, bool isPersistent)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "UserId", user.Id.ToString() },
                { "UserName", user.UserName },
                {"MustChangePassword", user.MustChangePassword.ToString().ToLower()}
            };

            var properties = new AuthenticationProperties(data)
            {
                ExpiresUtc = DateTime.Now.AddDays(30),
                IsPersistent = isPersistent,
            };

            return properties;
        }
    }
 
}