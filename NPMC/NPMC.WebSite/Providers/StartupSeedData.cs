﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web; 
using Newtonsoft.Json;
using System.Threading.Tasks;
using SilverEdgeProjects.NPMC.Models.ViewModels;
using Library.AutoFac;
using SilverEdgeProjects.NPMC.BusinessLogic.Services;

namespace SilverEdgeProjects.NPMC.WebSite.Providers
{
    public class SeedStartupItems
    {
        public static void SetupStates(int currentUserId)
        {
            
            var states = new List<StateModel>();
            try
            {
                using (StreamReader r = new StreamReader(HttpContext.Current.Server.MapPath("state.json")))
                {
                    string json = r.ReadToEnd();
                    states = JsonConvert.DeserializeObject<List<StateModel>>(json);
                }
                 
                    var stateService = EngineContext.Resolve<IStateService>();

                    foreach (var state in states)
                    {
                        if (!stateService.NameExist(state))
                        {                             
                            stateService.Create(state, currentUserId);
                        }
                    }
            }
            catch (Exception ec)
            {
                //
            }            
             
        }

         
        
    }
}