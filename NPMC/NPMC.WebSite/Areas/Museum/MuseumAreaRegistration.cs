﻿using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.Museum
{
    public class MuseumAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Museum";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Museum_default",
                "Museum/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}