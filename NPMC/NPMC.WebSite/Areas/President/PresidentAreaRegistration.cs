﻿using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.President
{
    public class PresidentAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "President";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "President_default",
                "President/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}