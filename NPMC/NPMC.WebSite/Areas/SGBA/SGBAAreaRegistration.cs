﻿using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.SGBA
{
    public class SGBAAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "SGBA";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "SGBA_default",
                "SGBA/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}