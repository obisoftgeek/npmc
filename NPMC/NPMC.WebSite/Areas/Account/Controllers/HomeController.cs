﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.Account.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult MyProfile()
        {

            return PartialView();
        }
        public ActionResult Login()
        {

            return PartialView();
        }
        public ActionResult ChangePassword()
        {
            return PartialView();
        }
        public ActionResult ForgotPassword()
        {
            return PartialView();
        }
        public ActionResult ForgotPasswordCompleted()
        {
            return PartialView();
        }
        public ActionResult CandidateSignup()
        {
            return PartialView();
        }
        public ActionResult SignupCompleted()
        {
            return PartialView();
        }
        public ActionResult EmailConfirmed()
        {
            return PartialView();
        }
        public ActionResult EmailNotConfirmed()
        {
            return PartialView();
        }
    }
}