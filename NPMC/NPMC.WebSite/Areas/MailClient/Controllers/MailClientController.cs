﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.MailClient.Controllers
{
    public class MailClientController : Controller
    {
        // GET: MailClient/MailClient
        public ActionResult List()
        {
            return PartialView();
        }
        public ActionResult Details()
        {
            return PartialView();
        }
    }
}