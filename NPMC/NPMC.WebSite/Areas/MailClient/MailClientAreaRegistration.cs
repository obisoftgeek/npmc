﻿using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.MailClient
{
    public class MailClientAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "MailClient";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "MailClient_default",
                "MailClient/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}