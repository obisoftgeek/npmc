﻿using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.Memo
{
    public class MemoAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Memo";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Memo_default",
                "Memo/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}