﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.Memo.Controllers
{
    public class MemoBoxController : Controller
    {
     
        public ActionResult List()
        {
            return PartialView();
        }
        public ActionResult Form()
        {
            return PartialView();
        }
        public ActionResult Details()
        {
            return PartialView();
        }
        public ActionResult Forward()
        {
            return PartialView();
        }
    }
}