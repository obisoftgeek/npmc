﻿using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.Tam
{
    public class TamAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Tam";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Tam_default",
                "Tam/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}