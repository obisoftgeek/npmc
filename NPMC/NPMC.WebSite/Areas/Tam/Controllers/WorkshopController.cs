﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.Tam.Controllers
{
    public class WorkshopController : Controller
    {
        // GET: Tam/Workshop
        // shows the list of all workshops
       public ActionResult List()
        {
            return PartialView();
        }
        //form for creation of new workshop
        public ActionResult Form()
        {
            return PartialView();
        }
      
    }
}