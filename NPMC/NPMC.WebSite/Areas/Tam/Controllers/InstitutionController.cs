﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.Tam.Controllers
{
    public class InstitutionController : Controller
    {
        // GET: Tam/Institution
        // shows the list of all Institutions
        public ActionResult List()
        {
            return PartialView();
        }

        //form for creation of new Institution
        public ActionResult Form()
        {
            return PartialView();
        }

    }
}