﻿using System.Web.Mvc;

namespace SilverEdgeProjects.SilverEdgeProjects.NPMC.WebSite.Areas.Biomedical
{
    public class BiomedicalAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Biomedical";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Biomedical_default",
                "Biomedical/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}