﻿
using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.Biomedical.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Dashboard()
        {
            return PartialView();
        }
    }
}