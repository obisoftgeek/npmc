﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.Finance.Controllers
{
    public class ProductController : Controller
    {
        // GET: Finance/Account
        public ActionResult List()
        {
            return PartialView();
        }
        public ActionResult Form()
        {
            return PartialView();
        }
    }
}