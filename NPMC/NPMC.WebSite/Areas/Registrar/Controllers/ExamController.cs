﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.Registrar.Controllers
{
    public class ExamController : Controller
    {
        // GET: Registrar/Audit
        public ActionResult List()
        {
            return PartialView();
        }

        public ActionResult Form()
        {
            return PartialView();
        }
        public ActionResult Details()
        {
            return PartialView();
        }
    }
}