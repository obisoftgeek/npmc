﻿
using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.Registrar.Controllers
{
    public class DepartmentController : Controller
    {
        public ActionResult List()
        {
            return PartialView();
        }
        public ActionResult Form()
        {
            return PartialView();
        }
        public ActionResult Details()
        {
            return PartialView();
        }


    }

}