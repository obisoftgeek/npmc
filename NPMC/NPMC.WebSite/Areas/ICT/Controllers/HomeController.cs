﻿
using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.ICT.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Dashboard()
        {
            return PartialView();
        }
    }
}