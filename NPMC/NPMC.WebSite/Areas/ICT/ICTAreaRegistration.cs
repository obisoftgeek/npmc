﻿using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.ICT
{
    public class ICTAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ICT";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ICT_default",
                "ICT/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}