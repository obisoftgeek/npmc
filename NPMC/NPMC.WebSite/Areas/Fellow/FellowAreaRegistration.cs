﻿using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.Fellow
{
    public class FellowAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Fellow";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Fellow_default",
                "Fellow/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}