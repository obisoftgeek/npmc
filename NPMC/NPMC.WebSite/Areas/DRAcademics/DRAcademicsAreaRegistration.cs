﻿using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.DRAcademics
{
    public class DRAcademicsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "DRAcademics";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "DRAcademics_default",
                "DRAcademics/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}