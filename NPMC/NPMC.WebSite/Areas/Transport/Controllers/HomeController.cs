﻿
using System.Web.Mvc;

namespace SilverEdgeProjects.NPMC.WebSite.Areas.Transport.Controllers
{
    public class HomeController : Controller
    {
       
        public ActionResult Dashboard()
        {
            return PartialView();
        }
    }
}