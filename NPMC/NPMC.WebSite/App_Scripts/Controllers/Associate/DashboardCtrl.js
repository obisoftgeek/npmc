var AssociateDashboardCtrl = (function () {
    function AssociateDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    AssociateDashboardCtrl.$inject = ["$scope"];
    return AssociateDashboardCtrl;
}());
angular.module("app").controller("AssociateDashboardCtrl", AssociateDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map