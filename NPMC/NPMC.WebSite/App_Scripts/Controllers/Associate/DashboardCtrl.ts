﻿


interface IAssociateDashboardCtrlScope extends IRootScope {
   
    item;
    
}


class AssociateDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: IAssociateDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("AssociateDashboardCtrl", AssociateDashboardCtrl);;