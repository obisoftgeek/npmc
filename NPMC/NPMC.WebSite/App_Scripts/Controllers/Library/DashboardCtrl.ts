﻿

 
interface ILibraryDashboardCtrlScope extends IRootScope {
   
    item;
    
}


class LibraryDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: ILibraryDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("LibraryDashboardCtrl", LibraryDashboardCtrl);;