var LibraryDashboardCtrl = (function () {
    function LibraryDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    LibraryDashboardCtrl.$inject = ["$scope"];
    return LibraryDashboardCtrl;
}());
angular.module("app").controller("LibraryDashboardCtrl", LibraryDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map