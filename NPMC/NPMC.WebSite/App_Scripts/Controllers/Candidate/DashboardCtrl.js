// Install the angularjs.TypeScript.DefinitelyTyped NuGet package
var CandidateDashboardCtrl = (function () {
    function CandidateDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    CandidateDashboardCtrl.$inject = ["$scope"];
    return CandidateDashboardCtrl;
}());
angular.module("app").controller("CandidateDashboardCtrl", CandidateDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map