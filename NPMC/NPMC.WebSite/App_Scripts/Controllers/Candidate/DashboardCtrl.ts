﻿

// Install the angularjs.TypeScript.DefinitelyTyped NuGet package

interface ICandidateDashboardCtrlScope extends IRootScope {
   
    item;
    
}


class CandidateDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: ICandidateDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("CandidateDashboardCtrl", CandidateDashboardCtrl);;