var PresidentDashboardCtrl = (function () {
    function PresidentDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    PresidentDashboardCtrl.$inject = ["$scope"];
    return PresidentDashboardCtrl;
}());
angular.module("app").controller("PresidentDashboardCtrl", PresidentDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map