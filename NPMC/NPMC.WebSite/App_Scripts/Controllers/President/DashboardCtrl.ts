﻿


interface IPresidentDashboardCtrlScope extends IRootScope {
   
    item;
    
}


class PresidentDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: IPresidentDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("PresidentDashboardCtrl", PresidentDashboardCtrl);;