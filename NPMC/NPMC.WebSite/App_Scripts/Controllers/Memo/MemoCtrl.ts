﻿
//******************************* List Controller *******************************

interface IMemoListCtrlScope extends IListCtrlScope {

    items: any[];
    getByFolderType: any;
    rowClicked: any;
}


class MemoListCtrl extends BaseListCtrl {
    static $inject: string[] = ["$scope", "MemoResource"];

  
    constructor($scope: IMemoListCtrlScope, private memoResource: IMemoResource) {
        super($scope, memoResource);
      
        $scope.filter.FolderType = 1;
        $scope.formTitle = "Memo Box";
        $scope.getByFolderType = (title, folderType) => this.getByFolderType(title, folderType);
        $scope.rowClicked = (item) => this.rowClicked(item);
        this.init();
       
    }
    getByFolderType(title, folderType) {
        this.$scope.filter.FolderType = folderType;
        this.$scope.formTitle = title;
        this.setupPagination();
    }

    rowClicked(item) {
        if (item.folderType === 1) { // memo has been sent
            this.$scope.$state.go('memo.details', { id: item.id });
        }
        if (item.folderType === 2) { //memo is draft
            this.$scope.$state.go('memo.edit', { id: item.id });
        }
    }
}
angular.module("app").controller("MemoListCtrl", MemoListCtrl);

//******************************* End List Controller *******************************








//******************************* Create Controller *******************************
interface IMemoCreateCtrlScope extends ICreateEditCtrlScope {
  
  
}
interface IMemoCreateCtrl {

}

class MemoCreateCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "MemoResource"];

    constructor($scope: IMemoCreateCtrlScope, private memoResource: IMemoResource) {
        super($scope, memoResource);

        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Compose";
        $scope.submitForm = () => this.submitForm();
        $scope.submitAsDraft = () => this.submitAsDraft();
    }

    submitForm() {

        if (this.$scope.validationSvc.isFormValid('inputForm')) {

           
            //var file = null;
            //var fileUploader = $("#file").get(0).files;
            //if (fileUploader.length > 0) {

            //    file = fileUploader[0];
            //}
            //var formdata = new FormData();
            //formdata.append("formModel", angular.toJson($scope.model));
            //formdata.append('uploadFile', file);


            this.$scope.item.selectedRecipients = this.$scope.demoOptions.selectedItems;

            if (this.$scope.item.selectedRecipients.length == 0) {
                this.$scope.notificationSvc.showWarningToast("Please select the recipient!");
                return;
            }
            if (!this.$scope.item.body) {
                this.$scope.notificationSvc.showWarningToast("Please enter your message.!");
                return;
            }
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to send now?").then(() => {
                this.$scope.waiting = 1;
                this.$scope.item.folderType = 1;
                this.memoResource.create(this.$scope.item).$promise.then((data) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showSuccessToast("Memo sent successfully!");
                    this.$scope.$state.go('memo.list');

                }, (error) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showErrorToast(error.data.message);
                });

            });
        }
       
    }
    submitAsDraft() {

        if (this.$scope.validationSvc.isFormValid('inputForm')) {

           
            if (!this.$scope.item.body) {
                this.$scope.notificationSvc.showWarningToast("Please enter your message.!");
                return;
            }
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to save as draft?").then(() => {
                this.$scope.waiting = 1;
                this.$scope.item.folderType = 2;
                this.memoResource.create(this.$scope.item).$promise.then((data) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showSuccessToast("Memo saved successfully!");
                    this.$scope.$state.go('memo.list');

                }, (error) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showErrorToast(error.data.message);
                });

            });
        }

    }
}

angular.module("app").controller("MemoCreateCtrl", MemoCreateCtrl);

//******************************* End Create Controller *******************************











//******************************* Edit Controller *******************************
interface IMemoEditCtrlScope extends ICreateEditCtrlScope {

}
interface IMemoEditCtrl {

}

class MemoEditCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "MemoResource","UnitResource"];
   
    constructor($scope: IMemoEditCtrlScope,  private memoResource: IMemoResource,
        private unitResource: IUnitResource) {
        super($scope, memoResource);

       this.$scope.isEditMode = true;
       this.$scope.item = {};
        $scope.formTitle = "Edit";
        $scope.submitForm = () => this.submitForm();
        $scope.submitAsDraft = () => this.submitAsDraft();
        this.$scope.waiting = 1;
        this.memoResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then((data) => {
            this.$scope.waiting = 0;
            this.$scope.item = data;
         
        }, (error) => {
            this.$scope.waiting = 0;
            this.$scope.notificationSvc.showErrorToast(error.data.message);
        });
      
    }

    submitForm() {

       
        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            this.$scope.item.selectedRecipients = this.$scope.demoOptions.selectedItems;

            if (this.$scope.item.selectedRecipients.length === 0) {
                this.$scope.notificationSvc.showWarningToast("Please select the recipient!");
                return;
            }
            if (!this.$scope.item.body) {
                this.$scope.notificationSvc.showWarningToast("Please enter your message.!");
                return;
            }
           
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to send now?").then(() => {
                this.$scope.waiting = 1;
                this.$scope.item.folderType = 1;
                this.memoResource.update(this.$scope.item).$promise.then((data) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showSuccessToast("Memo sent successfully!");
                    this.$scope.$state.go('memo.list');

                }, (error) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showErrorToast(error.data.message);
                });

            });
        }
    }
    submitAsDraft() {


        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            if (!this.$scope.item.body) {
                this.$scope.notificationSvc.showWarningToast("Please enter your message.!");
                return;
            }
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to save as draft?").then(() => {
                this.$scope.waiting = 1;
                this.$scope.item.folderType = 2;
                this.memoResource.update(this.$scope.item).$promise.then((data) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showSuccessToast("Memo saved successfully!");
                    this.$scope.$state.go('memo.list');

                }, (error) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showErrorToast(error.data.message);
                });

            });
        }
    }
}

angular.module("app").controller("MemoEditCtrl", MemoEditCtrl);

//******************************* End Edit Controller *******************************











//******************************* Form Controller *******************************

interface IMemoFormCtrlScope extends ICreateEditCtrlScope {

    departmentChanged(): any;
    departments:any;
    memoCategories: any;
   
}


class MemoFormCtrl {
    static $inject: string[] = ["$scope", "MemoResource", "DepartmentResource", "UnitResource",
        "MemoCategoryResource", "UserProfileResource"];

    constructor(private $scope: IMemoFormCtrlScope, private memoResource: IMemoResource,
        private departmentResource: IDepartmentResource, private unitResource: IUnitResource,
        private memoCategoryResource: IMemoCategoryResource, private userProfileResource: IUserProfileResource) {

       
        $scope.departments = this.departmentResource.getLookkup();
        $scope.departmentChanged = () => this.departmentChanged();
        $scope.memoCategories = this.memoCategoryResource.getLookkup();
        this.$scope.demoOptions.items = this.userProfileResource.getLookupByDept({ departmentId: 0 });
       
    }
    departmentChanged() {

        this.$scope.demoOptions.items = [];
        var departmentId = 0;
        if (this.$scope.item.departmentId) {
            departmentId = this.$scope.item.departmentId;           
        };
        this.$scope.demoOptions.items = this.userProfileResource.getLookupByDept({ departmentId: departmentId });
    }

}



angular.module("app").controller("MemoFormCtrl", MemoFormCtrl);

//******************************* End Form Controller *******************************





//******************************* Details Controller *******************************

interface IMemoDetailCtrlScope extends IBaseDetailCtrlScope {

   
    submitFeedback: any;
    forwardMemo: any;
}


class MemoDetailCtrl extends BaseDetailCtrl {

    static $inject: string[] = ["$scope", "MemoResource"];

    localScope: IMemoDetailCtrlScope;
   
    constructor($scope: IMemoDetailCtrlScope, private memoResource: IMemoResource) {

        super($scope, memoResource,null);
        $scope.submitFeedback = () => this.submitFeedback();
       
        $scope.forwardMemo = () => this.forwardMemo();
        this.init();
    }
    submitFeedback() {

         if (this.$scope.validationSvc.isFormValid('inputForm')) {
                             
                            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(() => {
                                this.$scope.waiting = 1;
                                
                                this.$scope.feedback.memoId = this.$scope.item.id;

                                this.memoResource.createMemoThread(this.$scope.feedback).$promise.then((data) => {
                                    this.$scope.waiting = 0;
                                    this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                                    this.$scope.feedback = {};
                                    this.reloadPage();

                                }, (error) => {
                                    this.$scope.waiting = 0;
                                    this.$scope.notificationSvc.showErrorToast(error.data.message);
                                });
                             
                            });
                        }
       
    }
  
    forwardMemo() {
        this.$scope.$state.go('memo.forward',{id: this.$scope.$stateParams.id});
    }
}

angular.module("app").controller("MemoDetailCtrl", MemoDetailCtrl);

//******************************* End Details Controller *******************************








//******************************* Forward Controller *******************************
interface IMemoForwardCtrlScope extends ICreateEditCtrlScope {

}
interface IMemoForwardCtrl {

}

class MemoForwardCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "MemoResource", "UnitResource"];

    constructor($scope: IMemoForwardCtrlScope, private memoResource: IMemoResource,
        private unitResource: IUnitResource) {
        super($scope, memoResource);

        this.$scope.isEditMode = true;
        this.$scope.item = {};
        $scope.formTitle = "Forward";
        $scope.submitAsForward = () => this.submitAsForward();
        
        this.$scope.waiting = 1;
        this.memoResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then((data) => {
            this.$scope.waiting = 0;
            this.$scope.item = data;

        }, (error) => {
            this.$scope.waiting = 0;
            this.$scope.notificationSvc.showErrorToast(error.data.message);
        });

    }

    submitAsForward() {


        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            this.$scope.item.selectedRecipients = this.$scope.demoOptions.selectedItems;

            if (this.$scope.item.selectedRecipients.length === 0) {
                this.$scope.notificationSvc.showWarningToast("Please select the recipient!");
                return;
            }
            if (!this.$scope.item.comment) {
                this.$scope.notificationSvc.showWarningToast("Please enter your message.!");
                return;
            }
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to send now?").then(() => {
                this.$scope.waiting = 1;
                this.$scope.item.memoId = this.$scope.$stateParams.id;
                this.memoResource.forwardMemo(this.$scope.item).$promise.then((data) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showSuccessToast("Memo sent successfully!");
                    this.$scope.$state.go('memo.list');

                }, (error) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showErrorToast(error.data.message);
                });

            });
        }
    }
    
}

angular.module("app").controller("MemoForwardCtrl", MemoForwardCtrl);

//******************************* End Forward Controller *******************************






////this.$mdDialog.show({
////    templateUrl: 'memoFeedback.html',
////    parent: angular.element(document.body),
////    targetEvent: ev,
////    clickOutsideToClose: false,
////    controller: ['$scope', "$mdDialog", ($scope, $mdDialog) => {

////        $scope.title = 'Memo Feedback';

////        $scope.submit = () => {


////        }
////        $scope.closeModal = () => {
////            $mdDialog.cancel();
////        }
////    }
////    ]

////}).then(() => {

////}, () => {

////});