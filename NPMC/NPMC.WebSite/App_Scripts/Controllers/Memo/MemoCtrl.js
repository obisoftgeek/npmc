//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MemoListCtrl = (function (_super) {
    __extends(MemoListCtrl, _super);
    function MemoListCtrl($scope, memoResource) {
        var _this = this;
        _super.call(this, $scope, memoResource);
        this.memoResource = memoResource;
        $scope.filter.FolderType = 1;
        $scope.formTitle = "Memo Box";
        $scope.getByFolderType = function (title, folderType) { return _this.getByFolderType(title, folderType); };
        $scope.rowClicked = function (item) { return _this.rowClicked(item); };
        this.init();
    }
    MemoListCtrl.prototype.getByFolderType = function (title, folderType) {
        this.$scope.filter.FolderType = folderType;
        this.$scope.formTitle = title;
        this.setupPagination();
    };
    MemoListCtrl.prototype.rowClicked = function (item) {
        if (item.folderType === 1) {
            this.$scope.$state.go('memo.details', { id: item.id });
        }
        if (item.folderType === 2) {
            this.$scope.$state.go('memo.edit', { id: item.id });
        }
    };
    MemoListCtrl.$inject = ["$scope", "MemoResource"];
    return MemoListCtrl;
}(BaseListCtrl));
angular.module("app").controller("MemoListCtrl", MemoListCtrl);
var MemoCreateCtrl = (function (_super) {
    __extends(MemoCreateCtrl, _super);
    function MemoCreateCtrl($scope, memoResource) {
        var _this = this;
        _super.call(this, $scope, memoResource);
        this.memoResource = memoResource;
        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Compose";
        $scope.submitForm = function () { return _this.submitForm(); };
        $scope.submitAsDraft = function () { return _this.submitAsDraft(); };
    }
    MemoCreateCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            //var file = null;
            //var fileUploader = $("#file").get(0).files;
            //if (fileUploader.length > 0) {
            //    file = fileUploader[0];
            //}
            //var formdata = new FormData();
            //formdata.append("formModel", angular.toJson($scope.model));
            //formdata.append('uploadFile', file);
            this.$scope.item.selectedRecipients = this.$scope.demoOptions.selectedItems;
            if (this.$scope.item.selectedRecipients.length == 0) {
                this.$scope.notificationSvc.showWarningToast("Please select the recipient!");
                return;
            }
            if (!this.$scope.item.body) {
                this.$scope.notificationSvc.showWarningToast("Please enter your message.!");
                return;
            }
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to send now?").then(function () {
                _this.$scope.waiting = 1;
                _this.$scope.item.folderType = 1;
                _this.memoResource.create(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Memo sent successfully!");
                    _this.$scope.$state.go('memo.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    MemoCreateCtrl.prototype.submitAsDraft = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            if (!this.$scope.item.body) {
                this.$scope.notificationSvc.showWarningToast("Please enter your message.!");
                return;
            }
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to save as draft?").then(function () {
                _this.$scope.waiting = 1;
                _this.$scope.item.folderType = 2;
                _this.memoResource.create(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Memo saved successfully!");
                    _this.$scope.$state.go('memo.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    MemoCreateCtrl.$inject = ["$scope", "MemoResource"];
    return MemoCreateCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("MemoCreateCtrl", MemoCreateCtrl);
var MemoEditCtrl = (function (_super) {
    __extends(MemoEditCtrl, _super);
    function MemoEditCtrl($scope, memoResource, unitResource) {
        var _this = this;
        _super.call(this, $scope, memoResource);
        this.memoResource = memoResource;
        this.unitResource = unitResource;
        this.$scope.isEditMode = true;
        this.$scope.item = {};
        $scope.formTitle = "Edit";
        $scope.submitForm = function () { return _this.submitForm(); };
        $scope.submitAsDraft = function () { return _this.submitAsDraft(); };
        this.$scope.waiting = 1;
        this.memoResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then(function (data) {
            _this.$scope.waiting = 0;
            _this.$scope.item = data;
        }, function (error) {
            _this.$scope.waiting = 0;
            _this.$scope.notificationSvc.showErrorToast(error.data.message);
        });
    }
    MemoEditCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.item.selectedRecipients = this.$scope.demoOptions.selectedItems;
            if (this.$scope.item.selectedRecipients.length === 0) {
                this.$scope.notificationSvc.showWarningToast("Please select the recipient!");
                return;
            }
            if (!this.$scope.item.body) {
                this.$scope.notificationSvc.showWarningToast("Please enter your message.!");
                return;
            }
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to send now?").then(function () {
                _this.$scope.waiting = 1;
                _this.$scope.item.folderType = 1;
                _this.memoResource.update(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Memo sent successfully!");
                    _this.$scope.$state.go('memo.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    MemoEditCtrl.prototype.submitAsDraft = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            if (!this.$scope.item.body) {
                this.$scope.notificationSvc.showWarningToast("Please enter your message.!");
                return;
            }
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to save as draft?").then(function () {
                _this.$scope.waiting = 1;
                _this.$scope.item.folderType = 2;
                _this.memoResource.update(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Memo saved successfully!");
                    _this.$scope.$state.go('memo.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    MemoEditCtrl.$inject = ["$scope", "MemoResource", "UnitResource"];
    return MemoEditCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("MemoEditCtrl", MemoEditCtrl);
var MemoFormCtrl = (function () {
    function MemoFormCtrl($scope, memoResource, departmentResource, unitResource, memoCategoryResource, userProfileResource) {
        var _this = this;
        this.$scope = $scope;
        this.memoResource = memoResource;
        this.departmentResource = departmentResource;
        this.unitResource = unitResource;
        this.memoCategoryResource = memoCategoryResource;
        this.userProfileResource = userProfileResource;
        $scope.departments = this.departmentResource.getLookkup();
        $scope.departmentChanged = function () { return _this.departmentChanged(); };
        $scope.memoCategories = this.memoCategoryResource.getLookkup();
        this.$scope.demoOptions.items = this.userProfileResource.getLookupByDept({ departmentId: 0 });
    }
    MemoFormCtrl.prototype.departmentChanged = function () {
        this.$scope.demoOptions.items = [];
        var departmentId = 0;
        if (this.$scope.item.departmentId) {
            departmentId = this.$scope.item.departmentId;
        }
        ;
        this.$scope.demoOptions.items = this.userProfileResource.getLookupByDept({ departmentId: departmentId });
    };
    MemoFormCtrl.$inject = ["$scope", "MemoResource", "DepartmentResource", "UnitResource",
        "MemoCategoryResource", "UserProfileResource"];
    return MemoFormCtrl;
}());
angular.module("app").controller("MemoFormCtrl", MemoFormCtrl);
var MemoDetailCtrl = (function (_super) {
    __extends(MemoDetailCtrl, _super);
    function MemoDetailCtrl($scope, memoResource) {
        var _this = this;
        _super.call(this, $scope, memoResource, null);
        this.memoResource = memoResource;
        $scope.submitFeedback = function () { return _this.submitFeedback(); };
        $scope.forwardMemo = function () { return _this.forwardMemo(); };
        this.init();
    }
    MemoDetailCtrl.prototype.submitFeedback = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.$scope.feedback.memoId = _this.$scope.item.id;
                _this.memoResource.createMemoThread(_this.$scope.feedback).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.feedback = {};
                    _this.reloadPage();
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    MemoDetailCtrl.prototype.forwardMemo = function () {
        this.$scope.$state.go('memo.forward', { id: this.$scope.$stateParams.id });
    };
    MemoDetailCtrl.$inject = ["$scope", "MemoResource"];
    return MemoDetailCtrl;
}(BaseDetailCtrl));
angular.module("app").controller("MemoDetailCtrl", MemoDetailCtrl);
var MemoForwardCtrl = (function (_super) {
    __extends(MemoForwardCtrl, _super);
    function MemoForwardCtrl($scope, memoResource, unitResource) {
        var _this = this;
        _super.call(this, $scope, memoResource);
        this.memoResource = memoResource;
        this.unitResource = unitResource;
        this.$scope.isEditMode = true;
        this.$scope.item = {};
        $scope.formTitle = "Forward";
        $scope.submitAsForward = function () { return _this.submitAsForward(); };
        this.$scope.waiting = 1;
        this.memoResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then(function (data) {
            _this.$scope.waiting = 0;
            _this.$scope.item = data;
        }, function (error) {
            _this.$scope.waiting = 0;
            _this.$scope.notificationSvc.showErrorToast(error.data.message);
        });
    }
    MemoForwardCtrl.prototype.submitAsForward = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.item.selectedRecipients = this.$scope.demoOptions.selectedItems;
            if (this.$scope.item.selectedRecipients.length === 0) {
                this.$scope.notificationSvc.showWarningToast("Please select the recipient!");
                return;
            }
            if (!this.$scope.item.comment) {
                this.$scope.notificationSvc.showWarningToast("Please enter your message.!");
                return;
            }
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to send now?").then(function () {
                _this.$scope.waiting = 1;
                _this.$scope.item.memoId = _this.$scope.$stateParams.id;
                _this.memoResource.forwardMemo(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Memo sent successfully!");
                    _this.$scope.$state.go('memo.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    MemoForwardCtrl.$inject = ["$scope", "MemoResource", "UnitResource"];
    return MemoForwardCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("MemoForwardCtrl", MemoForwardCtrl);
//******************************* End Forward Controller *******************************
////this.$mdDialog.show({
////    templateUrl: 'memoFeedback.html',
////    parent: angular.element(document.body),
////    targetEvent: ev,
////    clickOutsideToClose: false,
////    controller: ['$scope', "$mdDialog", ($scope, $mdDialog) => {
////        $scope.title = 'Memo Feedback';
////        $scope.submit = () => {
////        }
////        $scope.closeModal = () => {
////            $mdDialog.cancel();
////        }
////    }
////    ]
////}).then(() => {
////}, () => {
////}); 
//# sourceMappingURL=MemoCtrl.js.map