// Install the angularjs.TypeScript.DefinitelyTyped NuGet package
var DrAcademicsDashboardCtrl = (function () {
    function DrAcademicsDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    DrAcademicsDashboardCtrl.$inject = ["$scope"];
    return DrAcademicsDashboardCtrl;
}());
angular.module("app").controller("DrAcademicsDashboardCtrl", DrAcademicsDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map