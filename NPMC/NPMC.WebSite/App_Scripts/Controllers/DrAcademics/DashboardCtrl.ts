﻿

// Install the angularjs.TypeScript.DefinitelyTyped NuGet package

interface IDrAcademicsDashboardCtrlScope extends IRootScope {
   
    item;
    
}


class DrAcademicsDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: IDrAcademicsDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("DrAcademicsDashboardCtrl", DrAcademicsDashboardCtrl);;