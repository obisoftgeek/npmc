//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ExamDietListCtrl = (function (_super) {
    __extends(ExamDietListCtrl, _super);
    function ExamDietListCtrl($scope, ExamDietResource) {
        _super.call(this, $scope, ExamDietResource);
        this.ExamDietResource = ExamDietResource;
        this.localResource = ExamDietResource;
        this.init();
    }
    ExamDietListCtrl.$inject = ["$scope", "ExamDietResource"];
    return ExamDietListCtrl;
}(BaseListCtrl));
angular.module("app").controller("ExamDietListCtrl", ExamDietListCtrl);
var ExamDietCreateCtrl = (function (_super) {
    __extends(ExamDietCreateCtrl, _super);
    function ExamDietCreateCtrl($scope, ExamDietResource) {
        var _this = this;
        _super.call(this, $scope, ExamDietResource);
        this.ExamDietResource = ExamDietResource;
        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Create Exam Diet";
        $scope.submitForm = function () { return _this.submitForm(); };
    }
    ExamDietCreateCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.ExamDietResource.create(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.$state.go('examdietsetup.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    ExamDietCreateCtrl.$inject = ["$scope", "ExamDietResource"];
    return ExamDietCreateCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("ExamDietCreateCtrl", ExamDietCreateCtrl);
var ExamDietEditCtrl = (function (_super) {
    __extends(ExamDietEditCtrl, _super);
    function ExamDietEditCtrl($scope, ExamDietResource) {
        var _this = this;
        _super.call(this, $scope, ExamDietResource);
        this.ExamDietResource = ExamDietResource;
        this.$scope.isEditMode = true;
        this.$scope.item = {};
        $scope.formTitle = "Edit Exam Diets ";
        $scope.submitForm = function () { return _this.submitForm(); };
        this.$scope.waiting = 1;
        this.ExamDietResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then(function (data) {
            _this.$scope.waiting = 0;
            _this.$scope.item = data;
            //if (this.$scope.item.departmentId) {
            //    $scope.units = this.unitResource.getLookkup({ departmentId: this.$scope.item.departmentId });
            //};
        }, function (error) {
            _this.$scope.waiting = 0;
            _this.$scope.notificationSvc.showErrorToast(error.data.message);
        });
    }
    ExamDietEditCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.ExamDietResource.update(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.$state.go('ExamDiet.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    ExamDietEditCtrl.$inject = ["$scope", "ExamDietResource"];
    return ExamDietEditCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("ExamDietEditCtrl", ExamDietEditCtrl);
var ExamDietFormCtrl = (function () {
    function ExamDietFormCtrl($scope, ExamDietResource) {
        this.$scope = $scope;
        this.ExamDietResource = ExamDietResource;
        // $scope.departments = this.departmentResource.getLookkup();
        //$scope.departmentChanged = () => this.departmentChanged();
    }
    ExamDietFormCtrl.$inject = ["$scope", "ExamDietResource"];
    return ExamDietFormCtrl;
}());
angular.module("app").controller("ExamDietFormCtrl", ExamDietFormCtrl);
//******************************* End Form Controller ******************************* 
//# sourceMappingURL=DietSetupCtrl.js.map