//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ExamRequirementListCtrl = (function (_super) {
    __extends(ExamRequirementListCtrl, _super);
    function ExamRequirementListCtrl($scope, ExamRequirementResource, ExamResource) {
        var _this = this;
        _super.call(this, $scope, ExamRequirementResource);
        this.ExamRequirementResource = ExamRequirementResource;
        this.ExamResource = ExamResource;
        this.localResource = ExamRequirementResource;
        $scope.examtypeChanged = function () { return _this.examtypeChanged(); };
        ExamResource.getLookup().$promise.then(function (data) {
            $scope.exams = data;
            if (data.length) {
                $scope.filter.examId = data[0].id;
                _this.init();
            }
        });
    }
    ExamRequirementListCtrl.prototype.examtypeChanged = function () {
        this.init();
    };
    ExamRequirementListCtrl.prototype.init = function () {
        var _this = this;
        this.$scope.items = [];
        this.localResource.getExamRequirements({ examId: this.$scope.filter.examId }).$promise.then(function (data) {
            _this.$scope.items = data;
        });
    };
    ExamRequirementListCtrl.$inject = ["$scope", "ExamRequirementResource", "ExamResource"];
    return ExamRequirementListCtrl;
}(BaseListCtrl));
angular.module("app").controller("ExamRequirementListCtrl", ExamRequirementListCtrl);
var ExamRequirementCreateCtrl = (function (_super) {
    __extends(ExamRequirementCreateCtrl, _super);
    function ExamRequirementCreateCtrl($scope, ExamRequirementResource, ExamResource) {
        var _this = this;
        _super.call(this, $scope, ExamRequirementResource);
        this.ExamRequirementResource = ExamRequirementResource;
        this.ExamResource = ExamResource;
        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Create Exam Requirement";
        $scope.item.examId = $scope.$stateParams.examId;
        $scope.submitForm = function () { return _this.submitForm(); };
    }
    ExamRequirementCreateCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.ExamRequirementResource.create(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.$state.go('ExamRequirement.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    ExamRequirementCreateCtrl.$inject = ["$scope", "ExamRequirementResource", "ExamResource"];
    return ExamRequirementCreateCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("ExamRequirementCreateCtrl", ExamRequirementCreateCtrl);
var ExamRequirementEditCtrl = (function (_super) {
    __extends(ExamRequirementEditCtrl, _super);
    function ExamRequirementEditCtrl($scope, ExamRequirementResource) {
        var _this = this;
        _super.call(this, $scope, ExamRequirementResource);
        this.ExamRequirementResource = ExamRequirementResource;
        this.$scope.isEditMode = true;
        this.$scope.item = {};
        $scope.formTitle = "Edit Diets ";
        $scope.submitForm = function () { return _this.submitForm(); };
        this.$scope.waiting = 1;
        this.ExamRequirementResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then(function (data) {
            _this.$scope.waiting = 0;
            _this.$scope.item = data;
            //if (this.$scope.item.departmentId) {
            //    $scope.units = this.unitResource.getLookkup({ departmentId: this.$scope.item.departmentId });
            //};
        }, function (error) {
            _this.$scope.waiting = 0;
            _this.$scope.notificationSvc.showErrorToast(error.data.message);
        });
    }
    ExamRequirementEditCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.ExamRequirementResource.update(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.$state.go('examrequirement.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    ExamRequirementEditCtrl.$inject = ["$scope", "ExamRequirementResource"];
    return ExamRequirementEditCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("ExamRequirementEditCtrl", ExamRequirementEditCtrl);
var ExamRequirementFormCtrl = (function () {
    function ExamRequirementFormCtrl($scope, ExamRequirementResource) {
        this.$scope = $scope;
        this.ExamRequirementResource = ExamRequirementResource;
        // $scope.departments = this.departmentResource.getLookkup();
        //$scope.departmentChanged = () => this.departmentChanged();
    }
    ExamRequirementFormCtrl.$inject = ["$scope", "ExamRequirementResource"];
    return ExamRequirementFormCtrl;
}());
angular.module("app").controller("ExamRequirementFormCtrl", ExamRequirementFormCtrl);
//******************************* End Form Controller ******************************* 
//# sourceMappingURL=ExamRequirementCtrl.js.map