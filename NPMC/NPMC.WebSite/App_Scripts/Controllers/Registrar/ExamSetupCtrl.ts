﻿//******************************* List Controller *******************************

interface IExamListCtrlScope extends IRootListCtrlScope {

    items: any[];
    showCreateForm: (value: any) => void;
    showForm;
    addItem: (item) => void;
}


class ExamListCtrl extends RootListCtrl {
    static $inject: string[] = ["$scope", "ExamResource"];

    localScope: IExamListCtrlScope;
    localResource: IExamResource;
    constructor($scope: IExamListCtrlScope, private ExamResource: IExamResource) {
        super($scope, ExamResource);
        this.localResource = ExamResource;
        this.localScope = $scope;
        $scope.backLink.state = "exam.list";
        this.init();
        $scope.showForm = false;
        $scope.showCreateForm = (value) => {
            $scope.showForm = value;
        }
        $scope.addItem = (item) => {
            ExamResource.save(item).$promise.then((data) => {
                toastr.success("Saved successfully", "Information");
                this.$scope.$state.reload();
            }, (response) => {
                toastr.error("Error occured while trying to save the item. please try again", "Error!");
            });
        }
    }


    downloadAsExcel() {
        location.href = "/api/Exam/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    }

}
angular.module("app").controller("ExamListCtrl", ExamListCtrl);

//******************************* End List Controller *******************************


interface IExamCreateCtrlScope extends IRootCreateEditCtrlScope {
    title: string;
    item;
}

interface IExamCreateCtrl {

}

class ExamCreateCtrl extends BaseCreateCtrl {
    static $inject: string[] = ["$scope", "ExamResource"];

    constructor($scope: IExamCreateCtrlScope,
        ExamResource: IExamResource) {
        super($scope, ExamResource);
        $scope.title = "Create Exam ";
        $scope.item.stageModels = [{ isActive: true }];
        $scope.item.typeModels = [{ isActive: true }];
    }

}

angular.module("app").controller("ExamCreateCtrl", ExamCreateCtrl);

interface IExamFormCtrlScope extends IRootCreateEditCtrlScope {
    backLink: {
        state: string;
        forwardLink: string;
        config?: {};
    };

    states;
    examCenters
    item;
    addStage;
    removeStage
    addType;
    removeType
}


class ExamFormCtrl {
    static $inject: string[] = ["$scope", "ExamCenterResource"];
    localItem: IExam;
    constructor(private $scope: IExamFormCtrlScope, private examCenterResource: IExamCenterResource) {
        $scope.backLink.state = "examsetup.list";
        $scope.backLink.forwardLink = "examsetup.list";
        this.localItem = <IExam>this.$scope.item;
        $scope.examCenters = examCenterResource.nameAndValues();
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                }
            },
            messages: {

            }
        };
        $scope.addStage = () => {
            if (this.$scope.item.stageModels == null) {
                this.$scope.item.stageModels = [];
            }
            this.$scope.item.stageModels.push({isActive:true})
        }
        $scope.removeStage = (index) => {
            this.$scope.item.stageModels.splice(index, 1);
        }
        $scope.addType = () => {
            if (this.$scope.item.typeModels == null) {
                this.$scope.item.typeModels = [];
            }
            this.$scope.item.typeModels.push({ isActive: true })
        }
        $scope.removeType = (index) => {
            this.$scope.item.typeModels.splice(index, 1);
        }
    }
}

angular.module("app").controller("ExamFormCtrl", ExamFormCtrl);

interface IExamEditCtrlScope extends IRootCreateEditCtrlScope {

}

interface IExamEditCtrl {

}

class ExamEditCtrl extends BaseEditCtrl {
    static $inject: string[] = ["$scope", "ExamResource"];

    constructor($scope: IExamEditCtrlScope,
        ExamResource: IExamResource) {
        super($scope, ExamResource);
        $scope.title = "Edit Exam ";
    }

}

angular.module("app").controller("ExamEditCtrl", ExamEditCtrl);


interface IExamDetailCtrlScope extends IRootBaseDetailCtrlScope {
    title: string;    
}

interface IExamDetailCtrl {

}

class ExamDetailCtrl extends RootBaseDetailCtrl {
    static $inject: string[] = ["$scope", "ExamResource"];

    constructor($scope: IExamDetailCtrlScope,
        private ExamResource: IExamResource) {
        super($scope, ExamResource, "examsetup.list");
        $scope.title = "Details"; 
    }

     
}

angular.module("app").controller("ExamDetailCtrl", ExamDetailCtrl);