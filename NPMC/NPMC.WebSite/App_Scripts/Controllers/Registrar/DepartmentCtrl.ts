﻿
//******************************* List Controller *******************************

interface IDepartmentListCtrlScope extends IListCtrlScope {

    items: any[];
     
}


class DepartmentListCtrl extends BaseListCtrl {
    static $inject: string[] = ["$scope", "DepartmentResource"];

    localScope: IDepartmentListCtrlScope;
    localResource: IDepartmentResource;
    constructor($scope: IDepartmentListCtrlScope, private departmentResource: IDepartmentResource) {
        super($scope, departmentResource);
        this.localResource = departmentResource;
      
        this.init();
       
    }
   


}
angular.module("app").controller("DepartmentListCtrl", DepartmentListCtrl);

//******************************* End List Controller *******************************








//******************************* Create Controller *******************************
interface IDepartmentCreateCtrlScope extends ICreateEditCtrlScope {
  
  
}
interface IDepartmentCreateCtrl {

}

class DepartmentCreateCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "DepartmentResource"];

    constructor($scope: IDepartmentCreateCtrlScope, private departmentResource: IDepartmentResource) {
        super($scope, departmentResource);

        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Create Department";
        $scope.submitForm = () => this.submitForm();
    }

    submitForm() {

        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(() => {
                this.$scope.waiting = 1;
                this.departmentResource.create(this.$scope.item).$promise.then((data) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    this.$scope.$state.go('department.list');

                }, (error) => {   
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showErrorToast(error.data.message);
                });

            });
        }
       
    }
}

angular.module("app").controller("DepartmentCreateCtrl", DepartmentCreateCtrl);

//******************************* End Create Controller *******************************











//******************************* Edit Controller *******************************
interface IDepartmentEditCtrlScope extends ICreateEditCtrlScope {
   
    units: any[];

}
interface IDepartmentEditCtrl {

}

class DepartmentEditCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "DepartmentResource"];
   
    constructor($scope: IDepartmentEditCtrlScope, private departmentResource: IDepartmentResource) {
        super($scope, departmentResource);

       this.$scope.isEditMode = true;
       this.$scope.item = {};
        $scope.formTitle = "Edit Department";
        $scope.submitForm = () => this.submitForm();
        this.$scope.waiting = 1;
        this.departmentResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then((data) => {
            this.$scope.waiting = 0;
            this.$scope.item = data;
           
        }, (error) => {
            this.$scope.waiting = 0;
            this.$scope.notificationSvc.showErrorToast(error.data.message);
        });
      
    }

    submitForm() {
      
        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(() => {
                this.$scope.waiting = 1;
                this.departmentResource.update(this.$scope.item).$promise.then((data) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    this.$scope.$state.go('department.list');

                }, (error) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showErrorToast(error.data.message);
                });

            });
        }
    }
}

angular.module("app").controller("DepartmentEditCtrl", DepartmentEditCtrl);

//******************************* End Edit Controller *******************************











//******************************* Form Controller *******************************

interface IDepartmentFormCtrlScope extends ICreateEditCtrlScope {

   
}


class DepartmentFormCtrl {
    static $inject: string[] = ["$scope", "DepartmentResource"];

    constructor(private $scope: IDepartmentFormCtrlScope, private departmentResource: IDepartmentResource) {


    }

}

angular.module("app").controller("DepartmentFormCtrl", DepartmentFormCtrl);

//******************************* End Form Controller *******************************