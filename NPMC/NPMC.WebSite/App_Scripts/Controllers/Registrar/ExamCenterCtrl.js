//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ExamCenterListCtrl = (function (_super) {
    __extends(ExamCenterListCtrl, _super);
    function ExamCenterListCtrl($scope, ExamCenterResource) {
        var _this = this;
        _super.call(this, $scope, ExamCenterResource);
        this.ExamCenterResource = ExamCenterResource;
        this.localResource = ExamCenterResource;
        this.localScope = $scope;
        $scope.backLink.state = "examcenter.list";
        this.init();
        $scope.showForm = false;
        $scope.showCreateForm = function (value) {
            $scope.showForm = value;
        };
        $scope.addItem = function (item) {
            ExamCenterResource.save(item).$promise.then(function (data) {
                toastr.success("Saved successfully", "Information");
                _this.$scope.$state.reload();
            }, function (response) {
                toastr.error("Error occured while trying to save the item. please try again", "Error!");
            });
        };
    }
    ExamCenterListCtrl.prototype.downloadAsExcel = function () {
        location.href = "/api/ExamCenter/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    };
    ExamCenterListCtrl.$inject = ["$scope", "ExamCenterResource"];
    return ExamCenterListCtrl;
}(RootListCtrl));
angular.module("app").controller("ExamCenterListCtrl", ExamCenterListCtrl);
var ExamCenterCreateCtrl = (function (_super) {
    __extends(ExamCenterCreateCtrl, _super);
    function ExamCenterCreateCtrl($scope, ExamCenterResource) {
        _super.call(this, $scope, ExamCenterResource);
        $scope.title = "Create Exam Center";
    }
    ExamCenterCreateCtrl.$inject = ["$scope", "ExamCenterResource"];
    return ExamCenterCreateCtrl;
}(BaseCreateCtrl));
angular.module("app").controller("ExamCenterCreateCtrl", ExamCenterCreateCtrl);
var ExamCenterFormCtrl = (function () {
    function ExamCenterFormCtrl($scope, stateResource) {
        this.$scope = $scope;
        this.stateResource = stateResource;
        $scope.backLink.state = "examcenter.list";
        $scope.backLink.forwardLink = "examcenter.list";
        this.localItem = this.$scope.item;
        $scope.states = stateResource.getLookup();
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                }
            },
            messages: {}
        };
    }
    ExamCenterFormCtrl.$inject = ["$scope", "StateResource"];
    return ExamCenterFormCtrl;
}());
angular.module("app").controller("ExamCenterFormCtrl", ExamCenterFormCtrl);
var ExamCenterEditCtrl = (function (_super) {
    __extends(ExamCenterEditCtrl, _super);
    function ExamCenterEditCtrl($scope, ExamCenterResource) {
        _super.call(this, $scope, ExamCenterResource);
        $scope.title = "Edit Center";
    }
    ExamCenterEditCtrl.$inject = ["$scope", "ExamCenterResource"];
    return ExamCenterEditCtrl;
}(BaseEditCtrl));
angular.module("app").controller("ExamCenterEditCtrl", ExamCenterEditCtrl);
//# sourceMappingURL=ExamCenterCtrl.js.map