﻿

// Install the angularjs.TypeScript.DefinitelyTyped NuGet package

interface IRegistrarDashboardCtrlScope extends IRootScope {
   
    item;
    
}


class RegistrarDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: IRegistrarDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("RegistrarDashboardCtrl", RegistrarDashboardCtrl);;