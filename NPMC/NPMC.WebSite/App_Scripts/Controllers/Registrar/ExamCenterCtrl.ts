﻿//******************************* List Controller *******************************

interface IExamCenterListCtrlScope extends IRootListCtrlScope {

    items: any[];
    showCreateForm: (value: any) => void;
    showForm;
    addItem: (item) => void;
}


class ExamCenterListCtrl extends RootListCtrl {
    static $inject: string[] = ["$scope", "ExamCenterResource"];

    localScope: IExamCenterListCtrlScope;
    localResource: IExamCenterResource;
    constructor($scope: IExamCenterListCtrlScope, private ExamCenterResource: IExamCenterResource) {
        super($scope, ExamCenterResource);
        this.localResource = ExamCenterResource;
        this.localScope = $scope;
        $scope.backLink.state = "examcenter.list";
        this.init();
        $scope.showForm = false;
        $scope.showCreateForm = (value) => {
            $scope.showForm = value;
        }
        $scope.addItem = (item) => {
            ExamCenterResource.save(item).$promise.then((data) => {
                toastr.success("Saved successfully", "Information");
                this.$scope.$state.reload();
            }, (response) => {
                toastr.error("Error occured while trying to save the item. please try again", "Error!");
            });
        }
    }

     
    downloadAsExcel() {
        location.href = "/api/ExamCenter/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    }

}
angular.module("app").controller("ExamCenterListCtrl", ExamCenterListCtrl);

//******************************* End List Controller *******************************


interface IExamCenterCreateCtrlScope extends IRootCreateEditCtrlScope {
    title: string;
}

interface IExamCenterCreateCtrl {

}

class ExamCenterCreateCtrl extends BaseCreateCtrl {
    static $inject: string[] = ["$scope", "ExamCenterResource"];

    constructor($scope: IExamCenterCreateCtrlScope,
        ExamCenterResource: IExamCenterResource) {
        super($scope, ExamCenterResource);
        $scope.title = "Create Exam Center"; 
    }

}

angular.module("app").controller("ExamCenterCreateCtrl", ExamCenterCreateCtrl);

interface IExamCenterFormCtrlScope extends IRootCreateEditCtrlScope {
    backLink: {
        state: string;
        forwardLink:string;
        config?: {};
    };
     
    states;
}


class ExamCenterFormCtrl {
    static $inject: string[] = ["$scope","StateResource"];
    localItem: IExamCenter;
    constructor(private $scope: IExamCenterFormCtrlScope, private stateResource:IStateResource) {
        $scope.backLink.state = "examcenter.list"; 
        $scope.backLink.forwardLink = "examcenter.list"; 
        this.localItem = <IExamCenter>this.$scope.item;
        $scope.states = stateResource.getLookup();
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                } 
            },
            messages: {

            }
        };
    }
}

angular.module("app").controller("ExamCenterFormCtrl", ExamCenterFormCtrl);

interface IExamCenterEditCtrlScope extends IRootCreateEditCtrlScope {

}

interface IExamCenterEditCtrl {

}

class ExamCenterEditCtrl extends BaseEditCtrl {
    static $inject: string[] = ["$scope", "ExamCenterResource"];

    constructor($scope: IExamCenterEditCtrlScope,
        ExamCenterResource: IExamCenterResource) {
        super($scope, ExamCenterResource);
        $scope.title = "Edit Center";
    }

}

angular.module("app").controller("ExamCenterEditCtrl", ExamCenterEditCtrl);