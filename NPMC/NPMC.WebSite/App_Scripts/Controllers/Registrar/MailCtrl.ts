﻿
//******************************* List Controller *******************************

interface IMailListCtrlScope extends IRootListCtrlScope {

    items: any[];
}


class MailListCtrl extends RootListCtrl {
    static $inject: string[] = ["$scope", "MailResource"];

    localScope: IMailListCtrlScope;
    localResource: IMailResource;

    constructor($scope: IMailListCtrlScope, private MailResource: IMailResource) {
        super($scope, MailResource);
        this.localResource = MailResource;
        this.localScope = $scope;
        $scope.backLink.state = "mail.list";
        this.init();
    }

    downloadAsExcel() {
        location.href = "/api/Mail/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    }

}

angular.module("app").controller("MailListCtrl", MailListCtrl);

//******************************* End List Controller *******************************


interface IMailCreateCtrlScope extends IRootCreateEditCtrlScope {
    title: string;
}

interface IMailCreateCtrl {

}

class MailCreateCtrl extends BaseCreateCtrl {
    static $inject: string[] = ["$scope", "MailResource"];

    constructor($scope: IMailCreateCtrlScope,
        MailResource: IMailResource) {
        super($scope, MailResource);
        $scope.title = "Manage Mail";
    }
}

angular.module("app").controller("MailCreateCtrl", MailCreateCtrl);

interface IMailFormCtrlScope extends IRootCreateEditCtrlScope {
    backLink: {
        state: string;
        forwardLink: string;
        config?: {};
    };
    departments;
    departmentSelected;
    departmentUsers;
    item;
    fileSelected;
    upload;
    uploading;
    recipient;
    uploadCount;
    errors;
    percent;
    uploadFiles;
    errorRecipient: boolean;
    clearError;
}


class MailFormCtrl {
    static $inject: string[] = ["$scope", "DepartmentResource", "UserProfileResource", "FileUploadSvc"];
    localItem: IMail;
    files: string[] = [];

    constructor(private $scope: IMailFormCtrlScope,
        private departmentResource: IDepartmentResource,
        private userProfileResource: IUserProfileResource,
        private fileUploadService: IFileUploadSvc) {
        $scope.backLink.state = "mail.list";
        $scope.backLink.forwardLink = "mail.list";
        this.localItem = <IMail>this.$scope.item;
        $scope.departments = departmentResource.getLookup();
        this.$scope.departmentSelected = () => {
            //Lookup users in the department
            if ($scope.item.departmentId > 1) {
                $scope.departmentUsers =
                    userProfileResource.getLookupByDept({
                        departmentId: $scope.item.departmentId
                    });
            }
        }
        $scope.fileSelected = (file) => this.fileSelected(file);
        $scope.submitForm = () => this.submitForm();
        this.$scope.percent = 0;
        this.$scope.uploading = 0;

    }

    fileSelected(file: any) {
        this.files = file;
    }

    clearError = () => {
        alert();
        this.$scope.errorRecipient = false;
    }

    submitForm(): void {
        if (this.$scope.item.recipient === undefined) {
            this.$scope.errorRecipient = true;
        } else {
            this.$scope.item.recepientId = this.$scope.item.recipient.id;
            if (this.files.length > 0) {
                this.upload(this.files);
            } else {
                this.doActualSubmit();
                console.log(this.$scope.item);
            }
        }

    }

    upload(files) {
        this.$scope.uploading++;
        this.$scope.waiting++;
        this.fileUploadService.startUpload(files,
            (resp) => {
                console.log(resp);
                this.$scope.uploading--;
                this.$scope.item.attachment = resp;
                console.log(this.$scope.uploading);
                this.doActualSubmit();
            },
            (error) => {
                this.$scope.uploading--;
                this.$scope.waiting--;
                this.$scope.errors = this.$scope.errorHandle.handleError(error);
            },
            (percent) => {
                this.$scope.percent = percent;
            }
        );
    }

    doActualSubmit() {
        console.log(this.$scope.uploading);
        if (this.$scope.uploading === 0) {
            this.$scope.waiting--;
            (<any>this.$scope.$parent).submitForm();
        }
    }


}

angular.module("app").controller("MailFormCtrl", MailFormCtrl);

interface IMailDetailCtrlScope extends IRootBaseDetailCtrlScope {
    title: string;
    reload;
    numberOfAttachments;
    properties;
    getColor;
    getIcon;
    fileTypes;
}

interface IMailDetailCtrl {

}

class MailDetailCtrl extends RootBaseDetailCtrl {
    static $inject: string[] = ["$scope", "MailResource"];

    constructor($scope: IMailDetailCtrlScope,
        private MailResource: IMailResource) {
        super($scope, MailResource, "mail.list");
        $scope.title = "Details";
        $scope.getIcon = (type) => this.getIcon(type);
        $scope.fileTypes = this.fileTypes;
    }

    fileTypes = {
        Image: 1,
        Pdf: 2,
        Doc: 3,
        Xls: 4
    };
     
    getIcon =(type: number) => {
        switch (type) {
        case this.fileTypes.Pdf:
            return "fa-file-pdf-o text-red";
        case this.fileTypes.Doc:
            return "fa-file-word-o text-blue";
        case this.fileTypes.Xls:
            return "fa-file-excel-o text-green";
        default:
            return "fa-file-image-o text-purple";
        }
    }
}

angular.module("app").controller("MailDetailCtrl", MailDetailCtrl);