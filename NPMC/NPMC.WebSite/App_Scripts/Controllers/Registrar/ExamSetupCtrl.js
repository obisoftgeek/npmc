//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ExamListCtrl = (function (_super) {
    __extends(ExamListCtrl, _super);
    function ExamListCtrl($scope, ExamResource) {
        var _this = this;
        _super.call(this, $scope, ExamResource);
        this.ExamResource = ExamResource;
        this.localResource = ExamResource;
        this.localScope = $scope;
        $scope.backLink.state = "exam.list";
        this.init();
        $scope.showForm = false;
        $scope.showCreateForm = function (value) {
            $scope.showForm = value;
        };
        $scope.addItem = function (item) {
            ExamResource.save(item).$promise.then(function (data) {
                toastr.success("Saved successfully", "Information");
                _this.$scope.$state.reload();
            }, function (response) {
                toastr.error("Error occured while trying to save the item. please try again", "Error!");
            });
        };
    }
    ExamListCtrl.prototype.downloadAsExcel = function () {
        location.href = "/api/Exam/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    };
    ExamListCtrl.$inject = ["$scope", "ExamResource"];
    return ExamListCtrl;
}(RootListCtrl));
angular.module("app").controller("ExamListCtrl", ExamListCtrl);
var ExamCreateCtrl = (function (_super) {
    __extends(ExamCreateCtrl, _super);
    function ExamCreateCtrl($scope, ExamResource) {
        _super.call(this, $scope, ExamResource);
        $scope.title = "Create Exam ";
        $scope.item.stageModels = [{ isActive: true }];
        $scope.item.typeModels = [{ isActive: true }];
    }
    ExamCreateCtrl.$inject = ["$scope", "ExamResource"];
    return ExamCreateCtrl;
}(BaseCreateCtrl));
angular.module("app").controller("ExamCreateCtrl", ExamCreateCtrl);
var ExamFormCtrl = (function () {
    function ExamFormCtrl($scope, examCenterResource) {
        var _this = this;
        this.$scope = $scope;
        this.examCenterResource = examCenterResource;
        $scope.backLink.state = "examsetup.list";
        $scope.backLink.forwardLink = "examsetup.list";
        this.localItem = this.$scope.item;
        $scope.examCenters = examCenterResource.nameAndValues();
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                }
            },
            messages: {}
        };
        $scope.addStage = function () {
            if (_this.$scope.item.stageModels == null) {
                _this.$scope.item.stageModels = [];
            }
            _this.$scope.item.stageModels.push({ isActive: true });
        };
        $scope.removeStage = function (index) {
            _this.$scope.item.stageModels.splice(index, 1);
        };
        $scope.addType = function () {
            if (_this.$scope.item.typeModels == null) {
                _this.$scope.item.typeModels = [];
            }
            _this.$scope.item.typeModels.push({ isActive: true });
        };
        $scope.removeType = function (index) {
            _this.$scope.item.typeModels.splice(index, 1);
        };
    }
    ExamFormCtrl.$inject = ["$scope", "ExamCenterResource"];
    return ExamFormCtrl;
}());
angular.module("app").controller("ExamFormCtrl", ExamFormCtrl);
var ExamEditCtrl = (function (_super) {
    __extends(ExamEditCtrl, _super);
    function ExamEditCtrl($scope, ExamResource) {
        _super.call(this, $scope, ExamResource);
        $scope.title = "Edit Exam ";
    }
    ExamEditCtrl.$inject = ["$scope", "ExamResource"];
    return ExamEditCtrl;
}(BaseEditCtrl));
angular.module("app").controller("ExamEditCtrl", ExamEditCtrl);
var ExamDetailCtrl = (function (_super) {
    __extends(ExamDetailCtrl, _super);
    function ExamDetailCtrl($scope, ExamResource) {
        _super.call(this, $scope, ExamResource, "examsetup.list");
        this.ExamResource = ExamResource;
        $scope.title = "Details";
    }
    ExamDetailCtrl.$inject = ["$scope", "ExamResource"];
    return ExamDetailCtrl;
}(RootBaseDetailCtrl));
angular.module("app").controller("ExamDetailCtrl", ExamDetailCtrl);
//# sourceMappingURL=ExamSetupCtrl.js.map