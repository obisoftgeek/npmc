﻿

//******************************* List Controller *******************************

interface IExamDietListCtrlScope extends IListCtrlScope {

    items: any[];

}


class ExamDietListCtrl extends BaseListCtrl {
    static $inject: string[] = ["$scope", "ExamDietResource"];

    localScope: IExamDietListCtrlScope;
    localResource: IExamDietResource;
    constructor($scope: IExamDietListCtrlScope, private ExamDietResource: IExamDietResource) {
        super($scope, ExamDietResource);
        this.localResource = ExamDietResource;

        this.init();

    }



}
angular.module("app").controller("ExamDietListCtrl", ExamDietListCtrl);

//******************************* End List Controller *******************************




//******************************* Create Controller *******************************
interface IExamDietCreateCtrlScope extends ICreateEditCtrlScope {


}
interface IExamDietCreateCtrl {

}

class ExamDietCreateCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "ExamDietResource"];

    constructor($scope: IExamDietCreateCtrlScope, private ExamDietResource: IExamDietResource) {
        super($scope, ExamDietResource);

        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Create Exam Diet";
        $scope.submitForm = () => this.submitForm();
    }

    submitForm() {

        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(() => {
                this.$scope.waiting = 1;
                this.ExamDietResource.create(this.$scope.item).$promise.then((data) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    this.$scope.$state.go('examdietsetup.list');

                }, (error) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showErrorToast(error.data.message);
                });

            });
        }

    }
}

angular.module("app").controller("ExamDietCreateCtrl", ExamDietCreateCtrl);

//******************************* End Create Controller *******************************











//******************************* Edit Controller *******************************
interface IExamDietEditCtrlScope extends ICreateEditCtrlScope {

    units: any[];

}
interface IExamDietEditCtrl {

}

class ExamDietEditCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "ExamDietResource"];

    constructor($scope: IExamDietEditCtrlScope, private ExamDietResource: IExamDietResource) {
        super($scope, ExamDietResource);

        this.$scope.isEditMode = true;
        this.$scope.item = {};
        $scope.formTitle = "Edit Exam Diets ";
        $scope.submitForm = () => this.submitForm();
        this.$scope.waiting = 1;
        this.ExamDietResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then((data) => {
            this.$scope.waiting = 0;
            this.$scope.item = data;
            //if (this.$scope.item.departmentId) {
            //    $scope.units = this.unitResource.getLookkup({ departmentId: this.$scope.item.departmentId });
            //};
        }, (error) => {
            this.$scope.waiting = 0;
            this.$scope.notificationSvc.showErrorToast(error.data.message);
        });

    }

    submitForm() {

        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(() => {
                this.$scope.waiting = 1;
                this.ExamDietResource.update(this.$scope.item).$promise.then((data) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    this.$scope.$state.go('ExamDiet.list');

                }, (error) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showErrorToast(error.data.message);
                });

            });
        }
    }
}

angular.module("app").controller("ExamDietEditCtrl", ExamDietEditCtrl);

//******************************* End Edit Controller *******************************





//******************************* Form Controller *******************************

interface IExamDietFormCtrlScope extends ICreateEditCtrlScope {

    //departmentChanged(): any;
    //departments;
    //units;
}


class ExamDietFormCtrl {
    static $inject: string[] = ["$scope", "ExamDietResource"];

    constructor(private $scope: IExamDietFormCtrlScope, private ExamDietResource: IExamDietResource) {

       // $scope.departments = this.departmentResource.getLookkup();
        //$scope.departmentChanged = () => this.departmentChanged();
    }

    //departmentChanged() {

    //    this.$scope.units = [];
    //    if (this.$scope.item.departmentId) {
    //        this.$scope.units = this.unitResource.getLookkup({ departmentId: this.$scope.item.departmentId })
    //    };
    //}

}


angular.module("app").controller("ExamDietFormCtrl", ExamDietFormCtrl);

//******************************* End Form Controller *******************************