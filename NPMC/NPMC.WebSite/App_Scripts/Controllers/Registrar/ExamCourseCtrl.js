//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ExamTypeListCtrl = (function (_super) {
    __extends(ExamTypeListCtrl, _super);
    function ExamTypeListCtrl($scope, ExamTypeResource) {
        _super.call(this, $scope, ExamTypeResource);
        this.ExamTypeResource = ExamTypeResource;
        this.localResource = ExamTypeResource;
        this.init();
    }
    ExamTypeListCtrl.$inject = ["$scope", "ExamTypeResource"];
    return ExamTypeListCtrl;
}(BaseListCtrl));
angular.module("app").controller("ExamTypeListCtrl", ExamTypeListCtrl);
var ExamTypeCreateCtrl = (function (_super) {
    __extends(ExamTypeCreateCtrl, _super);
    function ExamTypeCreateCtrl($scope, ExamTypeResource) {
        var _this = this;
        _super.call(this, $scope, ExamTypeResource);
        this.ExamTypeResource = ExamTypeResource;
        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Create ExamType";
        $scope.submitForm = function () { return _this.submitForm(); };
        $scope.item.examId = this.$scope.$stateParams.id;
    }
    ExamTypeCreateCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.ExamTypeResource.create(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.$state.go('examsetup.ExamTypes');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    ExamTypeCreateCtrl.$inject = ["$scope", "ExamTypeResource"];
    return ExamTypeCreateCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("ExamTypeCreateCtrl", ExamTypeCreateCtrl);
var ExamTypeEditCtrl = (function (_super) {
    __extends(ExamTypeEditCtrl, _super);
    function ExamTypeEditCtrl($scope, ExamTypeResource) {
        var _this = this;
        _super.call(this, $scope, ExamTypeResource);
        this.ExamTypeResource = ExamTypeResource;
        this.$scope.isEditMode = true;
        this.$scope.item = {};
        $scope.formTitle = "Edit ExamType ";
        $scope.submitForm = function () { return _this.submitForm(); };
        this.$scope.waiting = 1;
        console.log(this.$scope.$stateParams.id);
        this.ExamTypeResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then(function (data) {
            _this.$scope.waiting = 0;
            _this.$scope.item = data;
        }, function (error) {
            _this.$scope.waiting = 0;
            _this.$scope.notificationSvc.showErrorToast(error.data.message);
        });
    }
    ExamTypeEditCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.ExamTypeResource.update(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.$state.go('examsetup.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    ExamTypeEditCtrl.$inject = ["$scope", "ExamTypeResource"];
    return ExamTypeEditCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("ExamTypeEditCtrl", ExamTypeEditCtrl);
var ExamTypeFormCtrl = (function () {
    function ExamTypeFormCtrl($scope, ExamTypeResource) {
        this.$scope = $scope;
        this.ExamTypeResource = ExamTypeResource;
        // $scope.departments = this.departmentResource.getLookkup();
        //$scope.departmentChanged = () => this.departmentChanged();
    }
    ExamTypeFormCtrl.$inject = ["$scope", "ExamTypeResource"];
    return ExamTypeFormCtrl;
}());
angular.module("app").controller("ExamTypeFormCtrl", ExamTypeFormCtrl);
//******************************* End Form Controller ******************************* 
//# sourceMappingURL=ExamCourseCtrl.js.map