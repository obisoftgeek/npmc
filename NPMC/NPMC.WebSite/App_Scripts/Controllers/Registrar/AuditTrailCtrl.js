//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AuditTrailCtrl = (function (_super) {
    __extends(AuditTrailCtrl, _super);
    function AuditTrailCtrl($scope, AuditTrailResource) {
        var _this = this;
        _super.call(this, $scope, AuditTrailResource);
        this.AuditTrailResource = AuditTrailResource;
        this.localResource = AuditTrailResource;
        this.localScope = $scope;
        $scope.auditSections = AuditTrailResource.auditSections();
        $scope.sectionChanged = function () { return _this.sectionChanged(); };
        this.init();
    }
    AuditTrailCtrl.prototype.sectionChanged = function () {
        this.$scope.filter.auditActionId = null;
        this.$scope.filter.auditActions = [];
        if (this.$scope.filter.auditSectionId) {
            this.localScope.auditActions = this.AuditTrailResource
                .auditActions({ auditSectionId: this.$scope.filter.auditSectionId });
        }
    };
    AuditTrailCtrl.prototype.downloadAsExcel = function () {
        location.href = "/api/Audittrail/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    };
    AuditTrailCtrl.$inject = ["$scope", "AuditTrailResource"];
    return AuditTrailCtrl;
}(BaseListCtrl));
angular.module("app").controller("AuditTrailCtrl", AuditTrailCtrl);
//******************************* End List Controller ******************************* 
//# sourceMappingURL=AuditTrailCtrl.js.map