﻿

//******************************* List Controller *******************************

interface IExamRequirementListCtrlScope extends IListCtrlScope {

    items: any[];
    exams
    examtypeChanged

}


class ExamRequirementListCtrl extends BaseListCtrl {
    static $inject: string[] = ["$scope", "ExamRequirementResource", "ExamResource"];

    localScope: IExamRequirementListCtrlScope;
    localResource: IExamRequirementResource;
    constructor($scope: IExamRequirementListCtrlScope, private ExamRequirementResource: IExamRequirementResource, private ExamResource: IExamResource) {
        super($scope, ExamRequirementResource);
        this.localResource = ExamRequirementResource;


        $scope.examtypeChanged = () => this.examtypeChanged();

        ExamResource.getLookup().$promise.then((data) => {
            $scope.exams = data;
            if (data.length) {
                $scope.filter.examId = data[0].id;
                this.init();
            }
        })
    }

    examtypeChanged() {
        this.init();
    }

    init() {

        this.$scope.items = [];
        this.localResource.getExamRequirements({ examId: this.$scope.filter.examId }).$promise.then((data) => {
            this.$scope.items = data;

        })
    }


}
angular.module("app").controller("ExamRequirementListCtrl", ExamRequirementListCtrl);

//******************************* End List Controller *******************************




//******************************* Create Controller *******************************
interface IExamRequirementCreateCtrlScope extends ICreateEditCtrlScope {

    examName
    examdetail
}
interface IExamRequirementCreateCtrl {

}

class ExamRequirementCreateCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "ExamRequirementResource","ExamResource"];

    constructor($scope: IExamRequirementCreateCtrlScope, private ExamRequirementResource: IExamRequirementResource, private ExamResource: IExamResource) {
        super($scope, ExamRequirementResource);

        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Create Exam Requirement";

        $scope.item.examId = $scope.$stateParams.examId;

        
        $scope.submitForm = () => this.submitForm();
    }

    submitForm() {

        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(() => {
                this.$scope.waiting = 1;
                this.ExamRequirementResource.create(this.$scope.item).$promise.then((data) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    this.$scope.$state.go('ExamRequirement.list');

                }, (error) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showErrorToast(error.data.message);
                });

            });
        }

    }
}

angular.module("app").controller("ExamRequirementCreateCtrl", ExamRequirementCreateCtrl);

//******************************* End Create Controller *******************************











//******************************* Edit Controller *******************************
interface IExamRequirementEditCtrlScope extends ICreateEditCtrlScope {

    units: any[];

}
interface IExamRequirementEditCtrl {

}

class ExamRequirementEditCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "ExamRequirementResource"];

    constructor($scope: IExamRequirementEditCtrlScope, private ExamRequirementResource: IExamRequirementResource) {
        super($scope, ExamRequirementResource);

        this.$scope.isEditMode = true;
        this.$scope.item = {};
        $scope.formTitle = "Edit Diets ";
        $scope.submitForm = () => this.submitForm();
        this.$scope.waiting = 1;
        this.ExamRequirementResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then((data) => {
            this.$scope.waiting = 0;
            this.$scope.item = data;
            //if (this.$scope.item.departmentId) {
            //    $scope.units = this.unitResource.getLookkup({ departmentId: this.$scope.item.departmentId });
            //};
        }, (error) => {
            this.$scope.waiting = 0;
            this.$scope.notificationSvc.showErrorToast(error.data.message);
        });

    }

    submitForm() {

        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(() => {
                this.$scope.waiting = 1;
                this.ExamRequirementResource.update(this.$scope.item).$promise.then((data) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    this.$scope.$state.go('examrequirement.list');

                }, (error) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showErrorToast(error.data.message);
                });

            });
        }
    }
}

angular.module("app").controller("ExamRequirementEditCtrl", ExamRequirementEditCtrl);

//******************************* End Edit Controller *******************************





//******************************* Form Controller *******************************

interface IExamRequirementFormCtrlScope extends ICreateEditCtrlScope {

    //departmentChanged(): any;
    //departments;
    //units;
}


class ExamRequirementFormCtrl {
    static $inject: string[] = ["$scope", "ExamRequirementResource"];

    constructor(private $scope: IExamRequirementFormCtrlScope, private ExamRequirementResource: IExamRequirementResource) {

       // $scope.departments = this.departmentResource.getLookkup();
        //$scope.departmentChanged = () => this.departmentChanged();
    }

    //departmentChanged() {

    //    this.$scope.units = [];
    //    if (this.$scope.item.departmentId) {
    //        this.$scope.units = this.unitResource.getLookkup({ departmentId: this.$scope.item.departmentId })
    //    };
    //}

}


angular.module("app").controller("ExamRequirementFormCtrl", ExamRequirementFormCtrl);

//******************************* End Form Controller *******************************