﻿//******************************* List Controller *******************************

interface IAuditTrailCtrlScope extends IListCtrlScope {

    items: any[];
    auditSections;
    auditActions; 
    sectionChanged;
}


class AuditTrailCtrl extends BaseListCtrl {
    static $inject: string[] = ["$scope", "AuditTrailResource"];

    localScope: IAuditTrailCtrlScope;
    localResource: IAuditTrailResource;
    constructor($scope: IAuditTrailCtrlScope, private AuditTrailResource: IAuditTrailResource) {
        super($scope, AuditTrailResource);
        this.localResource = AuditTrailResource;
        this.localScope = $scope;
        $scope.auditSections = AuditTrailResource.auditSections();
        $scope.sectionChanged = () => this.sectionChanged();
        this.init();
        
    }

    sectionChanged() { 
        this.$scope.filter.auditActionId = null;
        this.$scope.filter.auditActions = [];
        if (this.$scope.filter.auditSectionId) {
            this.localScope.auditActions = this.AuditTrailResource
                .auditActions({ auditSectionId: this.$scope.filter.auditSectionId });
        }
    }
    
    downloadAsExcel() {
        location.href = "/api/Audittrail/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    }

}
angular.module("app").controller("AuditTrailCtrl", AuditTrailCtrl);

//******************************* End List Controller *******************************