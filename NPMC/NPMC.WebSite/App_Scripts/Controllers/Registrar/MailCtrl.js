//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MailListCtrl = (function (_super) {
    __extends(MailListCtrl, _super);
    function MailListCtrl($scope, MailResource) {
        _super.call(this, $scope, MailResource);
        this.MailResource = MailResource;
        this.localResource = MailResource;
        this.localScope = $scope;
        $scope.backLink.state = "mail.list";
        this.init();
    }
    MailListCtrl.prototype.downloadAsExcel = function () {
        location.href = "/api/Mail/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    };
    MailListCtrl.$inject = ["$scope", "MailResource"];
    return MailListCtrl;
}(RootListCtrl));
angular.module("app").controller("MailListCtrl", MailListCtrl);
var MailCreateCtrl = (function (_super) {
    __extends(MailCreateCtrl, _super);
    function MailCreateCtrl($scope, MailResource) {
        _super.call(this, $scope, MailResource);
        $scope.title = "Manage Mail";
    }
    MailCreateCtrl.$inject = ["$scope", "MailResource"];
    return MailCreateCtrl;
}(BaseCreateCtrl));
angular.module("app").controller("MailCreateCtrl", MailCreateCtrl);
var MailFormCtrl = (function () {
    function MailFormCtrl($scope, departmentResource, userProfileResource, fileUploadService) {
        var _this = this;
        this.$scope = $scope;
        this.departmentResource = departmentResource;
        this.userProfileResource = userProfileResource;
        this.fileUploadService = fileUploadService;
        this.files = [];
        this.clearError = function () {
            alert();
            _this.$scope.errorRecipient = false;
        };
        $scope.backLink.state = "mail.list";
        $scope.backLink.forwardLink = "mail.list";
        this.localItem = this.$scope.item;
        $scope.departments = departmentResource.getLookup();
        this.$scope.departmentSelected = function () {
            //Lookup users in the department
            if ($scope.item.departmentId > 1) {
                $scope.departmentUsers =
                    userProfileResource.getLookupByDept({
                        departmentId: $scope.item.departmentId
                    });
            }
        };
        $scope.fileSelected = function (file) { return _this.fileSelected(file); };
        $scope.submitForm = function () { return _this.submitForm(); };
        this.$scope.percent = 0;
        this.$scope.uploading = 0;
    }
    MailFormCtrl.prototype.fileSelected = function (file) {
        this.files = file;
    };
    MailFormCtrl.prototype.submitForm = function () {
        if (this.$scope.item.recipient === undefined) {
            this.$scope.errorRecipient = true;
        }
        else {
            this.$scope.item.recepientId = this.$scope.item.recipient.id;
            if (this.files.length > 0) {
                this.upload(this.files);
            }
            else {
                this.doActualSubmit();
                console.log(this.$scope.item);
            }
        }
    };
    MailFormCtrl.prototype.upload = function (files) {
        var _this = this;
        this.$scope.uploading++;
        this.$scope.waiting++;
        this.fileUploadService.startUpload(files, function (resp) {
            console.log(resp);
            _this.$scope.uploading--;
            _this.$scope.item.attachment = resp;
            console.log(_this.$scope.uploading);
            _this.doActualSubmit();
        }, function (error) {
            _this.$scope.uploading--;
            _this.$scope.waiting--;
            _this.$scope.errors = _this.$scope.errorHandle.handleError(error);
        }, function (percent) {
            _this.$scope.percent = percent;
        });
    };
    MailFormCtrl.prototype.doActualSubmit = function () {
        console.log(this.$scope.uploading);
        if (this.$scope.uploading === 0) {
            this.$scope.waiting--;
            this.$scope.$parent.submitForm();
        }
    };
    MailFormCtrl.$inject = ["$scope", "DepartmentResource", "UserProfileResource", "FileUploadSvc"];
    return MailFormCtrl;
}());
angular.module("app").controller("MailFormCtrl", MailFormCtrl);
var MailDetailCtrl = (function (_super) {
    __extends(MailDetailCtrl, _super);
    function MailDetailCtrl($scope, MailResource) {
        var _this = this;
        _super.call(this, $scope, MailResource, "mail.list");
        this.MailResource = MailResource;
        this.fileTypes = {
            Image: 1,
            Pdf: 2,
            Doc: 3,
            Xls: 4
        };
        this.getIcon = function (type) {
            switch (type) {
                case _this.fileTypes.Pdf:
                    return "fa-file-pdf-o text-red";
                case _this.fileTypes.Doc:
                    return "fa-file-word-o text-blue";
                case _this.fileTypes.Xls:
                    return "fa-file-excel-o text-green";
                default:
                    return "fa-file-image-o text-purple";
            }
        };
        $scope.title = "Details";
        $scope.getIcon = function (type) { return _this.getIcon(type); };
        $scope.fileTypes = this.fileTypes;
    }
    MailDetailCtrl.$inject = ["$scope", "MailResource"];
    return MailDetailCtrl;
}(RootBaseDetailCtrl));
angular.module("app").controller("MailDetailCtrl", MailDetailCtrl);
//# sourceMappingURL=MailCtrl.js.map