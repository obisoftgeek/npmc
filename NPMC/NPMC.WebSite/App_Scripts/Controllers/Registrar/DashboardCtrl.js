// Install the angularjs.TypeScript.DefinitelyTyped NuGet package
var RegistrarDashboardCtrl = (function () {
    function RegistrarDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    RegistrarDashboardCtrl.$inject = ["$scope"];
    return RegistrarDashboardCtrl;
}());
angular.module("app").controller("RegistrarDashboardCtrl", RegistrarDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map