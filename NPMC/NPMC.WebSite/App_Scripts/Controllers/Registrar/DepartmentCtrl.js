//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var DepartmentListCtrl = (function (_super) {
    __extends(DepartmentListCtrl, _super);
    function DepartmentListCtrl($scope, departmentResource) {
        _super.call(this, $scope, departmentResource);
        this.departmentResource = departmentResource;
        this.localResource = departmentResource;
        this.init();
    }
    DepartmentListCtrl.$inject = ["$scope", "DepartmentResource"];
    return DepartmentListCtrl;
}(BaseListCtrl));
angular.module("app").controller("DepartmentListCtrl", DepartmentListCtrl);
var DepartmentCreateCtrl = (function (_super) {
    __extends(DepartmentCreateCtrl, _super);
    function DepartmentCreateCtrl($scope, departmentResource) {
        var _this = this;
        _super.call(this, $scope, departmentResource);
        this.departmentResource = departmentResource;
        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Create Department";
        $scope.submitForm = function () { return _this.submitForm(); };
    }
    DepartmentCreateCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.departmentResource.create(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.$state.go('department.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    DepartmentCreateCtrl.$inject = ["$scope", "DepartmentResource"];
    return DepartmentCreateCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("DepartmentCreateCtrl", DepartmentCreateCtrl);
var DepartmentEditCtrl = (function (_super) {
    __extends(DepartmentEditCtrl, _super);
    function DepartmentEditCtrl($scope, departmentResource) {
        var _this = this;
        _super.call(this, $scope, departmentResource);
        this.departmentResource = departmentResource;
        this.$scope.isEditMode = true;
        this.$scope.item = {};
        $scope.formTitle = "Edit Department";
        $scope.submitForm = function () { return _this.submitForm(); };
        this.$scope.waiting = 1;
        this.departmentResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then(function (data) {
            _this.$scope.waiting = 0;
            _this.$scope.item = data;
        }, function (error) {
            _this.$scope.waiting = 0;
            _this.$scope.notificationSvc.showErrorToast(error.data.message);
        });
    }
    DepartmentEditCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.departmentResource.update(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.$state.go('department.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    DepartmentEditCtrl.$inject = ["$scope", "DepartmentResource"];
    return DepartmentEditCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("DepartmentEditCtrl", DepartmentEditCtrl);
var DepartmentFormCtrl = (function () {
    function DepartmentFormCtrl($scope, departmentResource) {
        this.$scope = $scope;
        this.departmentResource = departmentResource;
    }
    DepartmentFormCtrl.$inject = ["$scope", "DepartmentResource"];
    return DepartmentFormCtrl;
}());
angular.module("app").controller("DepartmentFormCtrl", DepartmentFormCtrl);
//******************************* End Form Controller ******************************* 
//# sourceMappingURL=DepartmentCtrl.js.map