// Install the angularjs.TypeScript.DefinitelyTyped NuGet package
var IctDashboardCtrl = (function () {
    function IctDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    IctDashboardCtrl.$inject = ["$scope"];
    return IctDashboardCtrl;
}());
angular.module("app").controller("IctDashboardCtrl", IctDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map