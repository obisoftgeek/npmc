﻿

// Install the angularjs.TypeScript.DefinitelyTyped NuGet package

interface IIctDashboardCtrlScope extends IRootScope {
   
    item;
    departmentLinks;
}


class IctDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: IIctDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("IctDashboardCtrl", IctDashboardCtrl);;