//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ManageUserListCtrl = (function (_super) {
    __extends(ManageUserListCtrl, _super);
    function ManageUserListCtrl($scope, userProfileResource) {
        _super.call(this, $scope, userProfileResource);
        this.userProfileResource = userProfileResource;
        this.localResource = userProfileResource;
        this.init();
    }
    ManageUserListCtrl.$inject = ["$scope", "UserProfileResource"];
    return ManageUserListCtrl;
}(BaseListCtrl));
angular.module("app").controller("ManageUserListCtrl", ManageUserListCtrl);
var ManageUserCreateCtrl = (function (_super) {
    __extends(ManageUserCreateCtrl, _super);
    function ManageUserCreateCtrl($scope, userProfileResource) {
        var _this = this;
        _super.call(this, $scope, userProfileResource);
        this.userProfileResource = userProfileResource;
        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Create Profile";
        $scope.submitForm = function () { return _this.submitForm(); };
    }
    ManageUserCreateCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.userProfileResource.create(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.$state.go('manageuser.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    ManageUserCreateCtrl.$inject = ["$scope", "UserProfileResource"];
    return ManageUserCreateCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("ManageUserCreateCtrl", ManageUserCreateCtrl);
var ManageUserEditCtrl = (function (_super) {
    __extends(ManageUserEditCtrl, _super);
    function ManageUserEditCtrl($scope, userProfileResource, unitResource) {
        var _this = this;
        _super.call(this, $scope, userProfileResource);
        this.userProfileResource = userProfileResource;
        this.unitResource = unitResource;
        this.$scope.isEditMode = true;
        this.$scope.item = {};
        $scope.formTitle = "Edit Profile";
        $scope.submitForm = function () { return _this.submitForm(); };
        this.$scope.waiting = 1;
        this.userProfileResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then(function (data) {
            _this.$scope.waiting = 0;
            _this.$scope.item = data;
            if (_this.$scope.item.departmentId) {
                $scope.units = _this.unitResource.getLookkup({ departmentId: _this.$scope.item.departmentId });
            }
            ;
        }, function (error) {
            _this.$scope.waiting = 0;
            _this.$scope.notificationSvc.showErrorToast(error.data.message);
        });
    }
    ManageUserEditCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.userProfileResource.update(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.$state.go('manageuser.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    ManageUserEditCtrl.$inject = ["$scope", "UserProfileResource", "UnitResource"];
    return ManageUserEditCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("ManageUserEditCtrl", ManageUserEditCtrl);
var ManageUserFormCtrl = (function () {
    function ManageUserFormCtrl($scope, userProfileResource, departmentResource, unitResource) {
        var _this = this;
        this.$scope = $scope;
        this.userProfileResource = userProfileResource;
        this.departmentResource = departmentResource;
        this.unitResource = unitResource;
        $scope.departments = this.departmentResource.getLookkup();
        $scope.departmentChanged = function () { return _this.departmentChanged(); };
    }
    ManageUserFormCtrl.prototype.departmentChanged = function () {
        this.$scope.units = [];
        if (this.$scope.item.departmentId) {
            this.$scope.units = this.unitResource.getLookkup({ departmentId: this.$scope.item.departmentId });
        }
        ;
    };
    ManageUserFormCtrl.$inject = ["$scope", "UserProfileResource", "DepartmentResource", "UnitResource"];
    return ManageUserFormCtrl;
}());
angular.module("app").controller("ManageUserFormCtrl", ManageUserFormCtrl);
//******************************* End Form Controller ******************************* 
//# sourceMappingURL=ManageUserCtrl.js.map