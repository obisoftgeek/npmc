﻿
//******************************* List Controller *******************************

interface IManageUserListCtrlScope extends IListCtrlScope {

    items: any[];
     
}


class ManageUserListCtrl extends BaseListCtrl {
    static $inject: string[] = ["$scope", "UserProfileResource"];

    localScope: IManageUserListCtrlScope;
    localResource: IUserProfileResource;
    constructor($scope: IManageUserListCtrlScope, private userProfileResource: IUserProfileResource) {
        super($scope, userProfileResource);
        this.localResource = userProfileResource;
      
        this.init();
       
    }
   


}
angular.module("app").controller("ManageUserListCtrl", ManageUserListCtrl);

//******************************* End List Controller *******************************








//******************************* Create Controller *******************************
interface IManageUserCreateCtrlScope extends ICreateEditCtrlScope {
  
  
}
interface IManageUserCreateCtrl {

}

class ManageUserCreateCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "UserProfileResource"];

    constructor($scope: IManageUserCreateCtrlScope, private userProfileResource: IUserProfileResource) {
        super($scope, userProfileResource);

        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Create Profile";
        $scope.submitForm = () => this.submitForm();
    }

    submitForm() {

        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(() => {
                this.$scope.waiting = 1;
                this.userProfileResource.create(this.$scope.item).$promise.then((data) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    this.$scope.$state.go('manageuser.list');

                }, (error) => {   
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showErrorToast(error.data.message);
                });

            });
        }
       
    }
}

angular.module("app").controller("ManageUserCreateCtrl", ManageUserCreateCtrl);

//******************************* End Create Controller *******************************











//******************************* Edit Controller *******************************
interface IManageUserEditCtrlScope extends ICreateEditCtrlScope {
   
    units: any[];

}
interface IManageUserEditCtrl {

}

class ManageUserEditCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "UserProfileResource","UnitResource"];
   
    constructor($scope: IManageUserEditCtrlScope, private userProfileResource: IUserProfileResource, private unitResource: IUnitResource) {
        super($scope, userProfileResource);

       this.$scope.isEditMode = true;
       this.$scope.item = {};
        $scope.formTitle = "Edit Profile";
        $scope.submitForm = () => this.submitForm();
        this.$scope.waiting = 1;
        this.userProfileResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then((data) => {
            this.$scope.waiting = 0;
            this.$scope.item = data;
            if (this.$scope.item.departmentId) {
                $scope.units = this.unitResource.getLookkup({ departmentId: this.$scope.item.departmentId });
            };
        }, (error) => {
            this.$scope.waiting = 0;
            this.$scope.notificationSvc.showErrorToast(error.data.message);
        });
      
    }

    submitForm() {
      
        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(() => {
                this.$scope.waiting = 1;
                this.userProfileResource.update(this.$scope.item).$promise.then((data) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    this.$scope.$state.go('manageuser.list');

                }, (error) => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showErrorToast(error.data.message);
                });

            });
        }
    }
}

angular.module("app").controller("ManageUserEditCtrl", ManageUserEditCtrl);

//******************************* End Edit Controller *******************************











//******************************* Form Controller *******************************

interface IManageUserFormCtrlScope extends ICreateEditCtrlScope {

    departmentChanged(): any;
    departments;
    units;
}


class ManageUserFormCtrl {
    static $inject: string[] = ["$scope", "UserProfileResource", "DepartmentResource","UnitResource"];

    constructor(private $scope: IManageUserFormCtrlScope, private userProfileResource: IUserProfileResource,
        private departmentResource: IDepartmentResource, private unitResource: IUnitResource) {

        $scope.departments = this.departmentResource.getLookkup();     
        $scope.departmentChanged = () => this.departmentChanged();
    }
    
    departmentChanged() {

        this.$scope.units = [];
        if (this.$scope.item.departmentId) {
            this.$scope.units = this.unitResource.getLookkup({ departmentId: this.$scope.item.departmentId })};
        }
       
    }


angular.module("app").controller("ManageUserFormCtrl", ManageUserFormCtrl);

//******************************* End Form Controller *******************************