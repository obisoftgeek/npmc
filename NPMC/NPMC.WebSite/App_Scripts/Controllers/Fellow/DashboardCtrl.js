var FellowDashboardCtrl = (function () {
    function FellowDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    FellowDashboardCtrl.$inject = ["$scope"];
    return FellowDashboardCtrl;
}());
angular.module("app").controller("FellowDashboardCtrl", FellowDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map