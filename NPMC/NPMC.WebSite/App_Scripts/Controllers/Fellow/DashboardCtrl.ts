﻿


interface IFellowDashboardCtrlScope extends IRootScope {
   
    item;
    
}


class FellowDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: IFellowDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("FellowDashboardCtrl", FellowDashboardCtrl);;