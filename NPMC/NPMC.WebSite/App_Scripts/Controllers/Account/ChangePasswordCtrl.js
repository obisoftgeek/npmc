var ChangePasswordCtrl = (function () {
    function ChangePasswordCtrl($scope, userProfileResource, authService) {
        var _this = this;
        this.$scope = $scope;
        this.userProfileResource = userProfileResource;
        this.authService = authService;
        $scope.item = {};
        $scope.submit = function () { return _this.submit(); };
        $scope.cancel = function () { return _this.cancel(); };
        $scope.userProfile = this.userProfileResource.getCurrentUserProfile();
    }
    ChangePasswordCtrl.prototype.submit = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('dataForm')) {
            this.$scope.waiting = 1;
            this.userProfileResource.changePassword(this.$scope.item).$promise.then(function (data) {
                _this.$scope.waiting = 0;
                _this.$scope.notificationSvc.showSuccessToast("Password changed successfully!");
                _this.authService.continueLogin();
            }, function (error) {
                _this.$scope.waiting = 0;
                _this.$scope.notificationSvc.showErrorToast(error.data.message);
            });
        }
    };
    ChangePasswordCtrl.prototype.cancel = function () {
        this.authService.logout();
    };
    ChangePasswordCtrl.$inject = ["$scope", "UserProfileResource", "AuthService"];
    return ChangePasswordCtrl;
}());
angular.module("app").controller("ChangePasswordCtrl", ChangePasswordCtrl);
;
//# sourceMappingURL=ChangePasswordCtrl.js.map