﻿

 
interface IEmailConfirmationCtrlScope extends IRootScope {
  
}

class EmailConfirmationCtrl {
    static $inject: string[] = ["$scope", "UserProfileResource"];

    constructor(private $scope: IEmailConfirmationCtrlScope, private userProfileResource: IUserProfileResource) {

        $scope.item = {};       
        this.$scope.item.Token = $scope.$stateParams.token;
        this.$scope.item.UserId = $scope.$stateParams.userId;
        this.$scope.waiting = 1;
        this.userProfileResource.confirmEmail(this.$scope.item).$promise.then((data) => {
            this.$scope.waiting = 0;
            this.$scope.notificationSvc.showSuccessToast("Your email was confirmed successfully!");
            
        }, (error) => {
            this.$scope.waiting = 0;
            this.$scope.$state.go('account.emailnotconfirmed');
            this.$scope.notificationSvc.showErrorToast(error.data.Message);
          
        });
       
    }
   

   
}
angular.module("app").controller("EmailConfirmationCtrl", EmailConfirmationCtrl);;