﻿

 
interface IForgotPasswordCtrlScope extends IRootScope {
   
    submit;  
}


class ForgotPasswordCtrl {
    static $inject: string[] = ["$scope","UserProfileResource"];

    constructor(private $scope: IForgotPasswordCtrlScope, private userProfileResource: IUserProfileResource) {

        $scope.item = {};
        $scope.submit = () => this.submit();
       
    }
    submit() {
        if (this.$scope.validationSvc.isFormValid('dataForm')) {
            this.$scope.waiting = 1;
            this.userProfileResource.forgotPassword(this.$scope.item).$promise.then((data) => {
                this.$scope.waiting = 0;
                this.$scope.$state.go('account.forgotpasswordcompleted');
                this.$scope.notificationSvc.showSuccessToast("The operation was successful. A confirmation link has been sent to your email.");

            }, (error) => {
                this.$scope.waiting = 0;
                this.$scope.notificationSvc.showErrorToast(error.data.message);
            });

        }      
    }
    

}
angular.module("app").controller("ForgotPasswordCtrl", ForgotPasswordCtrl);;