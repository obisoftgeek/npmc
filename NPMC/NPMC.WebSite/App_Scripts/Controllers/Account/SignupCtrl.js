var SignupCtrl = (function () {
    function SignupCtrl($scope, userProfileResource) {
        var _this = this;
        this.$scope = $scope;
        this.userProfileResource = userProfileResource;
        $scope.item = {};
        $scope.candidateSignup = function () { return _this.candidateSignup(); };
    }
    SignupCtrl.prototype.candidateSignup = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('dataForm')) {
            this.$scope.waiting = 1;
            this.userProfileResource.signupCandidate(this.$scope.item).$promise.then(function (data) {
                _this.$scope.waiting = 0;
                _this.$scope.notificationSvc.showSuccessToast("Signup completed successfully!");
                _this.$scope.$state.go('account.signupcompleted');
            }, function (error) {
                _this.$scope.waiting = 0;
                _this.$scope.notificationSvc.showErrorToast(error.data.Message);
            });
        }
    };
    SignupCtrl.$inject = ["$scope", "UserProfileResource"];
    return SignupCtrl;
}());
angular.module("app").controller("SignupCtrl", SignupCtrl);
;
//# sourceMappingURL=SignupCtrl.js.map