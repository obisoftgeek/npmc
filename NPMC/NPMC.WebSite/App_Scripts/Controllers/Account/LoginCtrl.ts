﻿

 
interface ILoginCtrlScope extends IRootScope {
   
    login; any;
    logout: any; 
    
}

class LoginCtrl {
    static $inject: string[] = ["$scope", "AuthService"];

    constructor(private $scope: ILoginCtrlScope, private authService: IAuthService) {

        $scope.item = {};
        $scope.login = () => this.login();
        $scope.logout = () => this.logout();
        
       
    }
    login() {

        if (this.$scope.validationSvc.isFormValid('dataForm')) {

            this.$scope.waiting = 1;
         
            this.authService.authenticateUser(this.$scope.item, (data) => {

                this.$scope.waiting = 0;
                if (data.MustChangePassword === 'true') {
                    this.$scope.$state.go('account.changepassword');
                }
                else {
                    this.authService.continueLogin();
                }

            }, (response) => {
                this.$scope.$apply(() => {
                    this.$scope.waiting = 0;
                    this.$scope.notificationSvc.showErrorToast(this.$scope.errorHandleSvc.handleError(response));
                });
            });
           
               
        }      
    }
    
    logout() {
        this.authService.logout();
    }
}
angular.module("app").controller("LoginCtrl", LoginCtrl);;