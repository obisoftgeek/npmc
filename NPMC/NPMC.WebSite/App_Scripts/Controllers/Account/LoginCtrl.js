var LoginCtrl = (function () {
    function LoginCtrl($scope, authService) {
        var _this = this;
        this.$scope = $scope;
        this.authService = authService;
        $scope.item = {};
        $scope.login = function () { return _this.login(); };
        $scope.logout = function () { return _this.logout(); };
    }
    LoginCtrl.prototype.login = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('dataForm')) {
            this.$scope.waiting = 1;
            this.authService.authenticateUser(this.$scope.item, function (data) {
                _this.$scope.waiting = 0;
                if (data.MustChangePassword === 'true') {
                    _this.$scope.$state.go('account.changepassword');
                }
                else {
                    _this.authService.continueLogin();
                }
            }, function (response) {
                _this.$scope.$apply(function () {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(_this.$scope.errorHandleSvc.handleError(response));
                });
            });
        }
    };
    LoginCtrl.prototype.logout = function () {
        this.authService.logout();
    };
    LoginCtrl.$inject = ["$scope", "AuthService"];
    return LoginCtrl;
}());
angular.module("app").controller("LoginCtrl", LoginCtrl);
;
//# sourceMappingURL=LoginCtrl.js.map