var ForgotPasswordCtrl = (function () {
    function ForgotPasswordCtrl($scope, userProfileResource) {
        var _this = this;
        this.$scope = $scope;
        this.userProfileResource = userProfileResource;
        $scope.item = {};
        $scope.submit = function () { return _this.submit(); };
    }
    ForgotPasswordCtrl.prototype.submit = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('dataForm')) {
            this.$scope.waiting = 1;
            this.userProfileResource.forgotPassword(this.$scope.item).$promise.then(function (data) {
                _this.$scope.waiting = 0;
                _this.$scope.$state.go('account.forgotpasswordcompleted');
                _this.$scope.notificationSvc.showSuccessToast("The operation was successful. A confirmation link has been sent to your email.");
            }, function (error) {
                _this.$scope.waiting = 0;
                _this.$scope.notificationSvc.showErrorToast(error.data.message);
            });
        }
    };
    ForgotPasswordCtrl.$inject = ["$scope", "UserProfileResource"];
    return ForgotPasswordCtrl;
}());
angular.module("app").controller("ForgotPasswordCtrl", ForgotPasswordCtrl);
;
//# sourceMappingURL=ForgotPasswordCtrl.js.map