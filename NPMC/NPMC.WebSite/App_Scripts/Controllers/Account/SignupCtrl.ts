﻿

 
interface ISignupCtrlScope extends IRootScope {
   
    candidateSignup:any; 
}

class SignupCtrl {
    static $inject: string[] = ["$scope", "UserProfileResource"];

    constructor(private $scope: ISignupCtrlScope, private userProfileResource: IUserProfileResource) {

        $scope.item = {};
        $scope.candidateSignup = () => this.candidateSignup();
       
    }
    candidateSignup() {

        if (this.$scope.validationSvc.isFormValid('dataForm')) {
            this.$scope.waiting = 1;
             this.userProfileResource.signupCandidate(this.$scope.item).$promise.then((data) => {
                this.$scope.waiting = 0;
                this.$scope.notificationSvc.showSuccessToast("Signup completed successfully!");
                this.$scope.$state.go('account.signupcompleted');

            }, (error) => {
                this.$scope.waiting = 0;
                this.$scope.notificationSvc.showErrorToast(error.data.Message);
            });
               
        }      
    }
   
}
angular.module("app").controller("SignupCtrl", SignupCtrl);;