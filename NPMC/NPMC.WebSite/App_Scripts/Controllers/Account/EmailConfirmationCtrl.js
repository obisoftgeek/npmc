var EmailConfirmationCtrl = (function () {
    function EmailConfirmationCtrl($scope, userProfileResource) {
        var _this = this;
        this.$scope = $scope;
        this.userProfileResource = userProfileResource;
        $scope.item = {};
        this.$scope.item.Token = $scope.$stateParams.token;
        this.$scope.item.UserId = $scope.$stateParams.userId;
        this.$scope.waiting = 1;
        this.userProfileResource.confirmEmail(this.$scope.item).$promise.then(function (data) {
            _this.$scope.waiting = 0;
            _this.$scope.notificationSvc.showSuccessToast("Your email was confirmed successfully!");
        }, function (error) {
            _this.$scope.waiting = 0;
            _this.$scope.$state.go('account.emailnotconfirmed');
            _this.$scope.notificationSvc.showErrorToast(error.data.Message);
        });
    }
    EmailConfirmationCtrl.$inject = ["$scope", "UserProfileResource"];
    return EmailConfirmationCtrl;
}());
angular.module("app").controller("EmailConfirmationCtrl", EmailConfirmationCtrl);
;
//# sourceMappingURL=EmailConfirmationCtrl.js.map