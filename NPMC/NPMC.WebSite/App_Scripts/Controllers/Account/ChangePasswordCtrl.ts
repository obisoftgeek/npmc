﻿

 
interface IChangePasswordCtrlScope extends IRootScope {
   
    submit:any; 
    cancel:any;  
    userProfile:any; 
}


class ChangePasswordCtrl {
    static $inject: string[] = ["$scope", "UserProfileResource","AuthService"];

    constructor(private $scope: IChangePasswordCtrlScope, private userProfileResource: IUserProfileResource, private authService: IAuthService) {

        $scope.item = {};
        $scope.submit = () => this.submit();
        $scope.cancel = () => this.cancel();
        $scope.userProfile = this.userProfileResource.getCurrentUserProfile();
    }
    submit() {

        if (this.$scope.validationSvc.isFormValid('dataForm')) {
            this.$scope.waiting = 1;
            this.userProfileResource.changePassword(this.$scope.item).$promise.then((data) => {
                this.$scope.waiting = 0;
                this.$scope.notificationSvc.showSuccessToast("Password changed successfully!");
                this.authService.continueLogin();

            }, (error) => {
                this.$scope.waiting = 0;
                this.$scope.notificationSvc.showErrorToast(error.data.message);
            });

        }      
    }
    cancel() {
        this.authService.logout();
    }

}
angular.module("app").controller("ChangePasswordCtrl", ChangePasswordCtrl);;