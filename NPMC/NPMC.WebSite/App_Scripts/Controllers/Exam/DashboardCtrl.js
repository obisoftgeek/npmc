// Install the angularjs.TypeScript.DefinitelyTyped NuGet package
var ExaminationDashboardCtrl = (function () {
    function ExaminationDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    ExaminationDashboardCtrl.$inject = ["$scope"];
    return ExaminationDashboardCtrl;
}());
angular.module("app").controller("ExaminationDashboardCtrl", ExaminationDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map