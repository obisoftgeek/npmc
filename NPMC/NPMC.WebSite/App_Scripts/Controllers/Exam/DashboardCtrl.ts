﻿

// Install the angularjs.TypeScript.DefinitelyTyped NuGet package

interface IExaminationDashboardCtrlScope extends IRootScope {
   
    item;
    
}


class ExaminationDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: IExaminationDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("ExaminationDashboardCtrl", ExaminationDashboardCtrl);;