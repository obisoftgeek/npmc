var SgbaDashboardCtrl = (function () {
    function SgbaDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    SgbaDashboardCtrl.$inject = ["$scope"];
    return SgbaDashboardCtrl;
}());
angular.module("app").controller("SgbaDashboardCtrl", SgbaDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map