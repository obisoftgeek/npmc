﻿


interface ISgbaDashboardCtrlScope extends IRootScope {
   
    item;
    
}


class SgbaDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: ISgbaDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("SgbaDashboardCtrl", SgbaDashboardCtrl);;