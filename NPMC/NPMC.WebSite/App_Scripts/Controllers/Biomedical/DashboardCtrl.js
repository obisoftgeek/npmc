// Install the angularjs.TypeScript.DefinitelyTyped NuGet package
var BiomedicalDashboardCtrl = (function () {
    function BiomedicalDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    BiomedicalDashboardCtrl.$inject = ["$scope"];
    return BiomedicalDashboardCtrl;
}());
angular.module("app").controller("BiomedicalDashboardCtrl", BiomedicalDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map