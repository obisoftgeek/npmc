﻿

// Install the angularjs.TypeScript.DefinitelyTyped NuGet package

interface IBiomedicalDashboardCtrlScope extends IRootScope {
   
    item;
    
}


class BiomedicalDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: IBiomedicalDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("BiomedicalDashboardCtrl", BiomedicalDashboardCtrl);;