﻿

 
interface IMuseumDashboardCtrlScope extends IRootScope {
   
    item;
    
}


class MuseumDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: IMuseumDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("MuseumDashboardCtrl", MuseumDashboardCtrl);;