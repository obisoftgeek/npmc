var MuseumDashboardCtrl = (function () {
    function MuseumDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    MuseumDashboardCtrl.$inject = ["$scope"];
    return MuseumDashboardCtrl;
}());
angular.module("app").controller("MuseumDashboardCtrl", MuseumDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map