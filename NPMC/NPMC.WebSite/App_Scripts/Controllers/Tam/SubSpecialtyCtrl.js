//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var SubSpecialtyListCtrl = (function (_super) {
    __extends(SubSpecialtyListCtrl, _super);
    function SubSpecialtyListCtrl($scope, SubSpecialtyResource) {
        _super.call(this, $scope, SubSpecialtyResource);
        this.SubSpecialtyResource = SubSpecialtyResource;
        this.localResource = SubSpecialtyResource;
        this.localScope = $scope;
        this.init();
        this.$scope.title = "Sub Specialties";
    }
    SubSpecialtyListCtrl.prototype.downloadAsExcel = function () {
        location.href = "/api/SubSpecialty/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    };
    SubSpecialtyListCtrl.$inject = ["$scope", "SubSpecialtyResource"];
    return SubSpecialtyListCtrl;
}(RootListCtrl));
angular.module("app").controller("SubSpecialtyListCtrl", SubSpecialtyListCtrl);
var SubSpecialtyCreateCtrl = (function (_super) {
    __extends(SubSpecialtyCreateCtrl, _super);
    function SubSpecialtyCreateCtrl($scope, SubSpecialtyResource) {
        _super.call(this, $scope, SubSpecialtyResource);
        $scope.title = "Create Sub Specialty";
    }
    SubSpecialtyCreateCtrl.$inject = ["$scope", "SubSpecialtyResource"];
    return SubSpecialtyCreateCtrl;
}(BaseCreateCtrl));
angular.module("app").controller("SubSpecialtyCreateCtrl", SubSpecialtyCreateCtrl);
var SubSpecialtyFormCtrl = (function () {
    function SubSpecialtyFormCtrl($scope, paymentCategoryResource, paymentFeeResource, facultyResource) {
        this.$scope = $scope;
        this.paymentCategoryResource = paymentCategoryResource;
        this.paymentFeeResource = paymentFeeResource;
        this.facultyResource = facultyResource;
        $scope.backLink.state = "subspecialty.list";
        $scope.backLink.forwardLink = "subspecialty.list";
        this.localItem = this.$scope.item;
        $scope.categories = paymentCategoryResource.nameAndValues();
        $scope.faculties = facultyResource.nameAndValues();
        $scope.paymentCategoryChanged = function () {
            if ($scope.item.paymentCategoryId > 1) {
                $scope.paymentFees = [];
                $scope.paymentFees = paymentFeeResource.nameAndValues({ paymentCategoryId: $scope.item.paymentCategoryId });
            }
        };
    }
    SubSpecialtyFormCtrl.$inject = ["$scope", "PaymentCategoryResource", "PaymentFeeResource", "FacultyResource"];
    return SubSpecialtyFormCtrl;
}());
angular.module("app").controller("SubSpecialtyFormCtrl", SubSpecialtyFormCtrl);
var SubSpecialtyEditCtrl = (function (_super) {
    __extends(SubSpecialtyEditCtrl, _super);
    function SubSpecialtyEditCtrl($scope, SubSpecialtyResource, paymentFeeResource) {
        _super.call(this, $scope, SubSpecialtyResource);
        $scope.title = "Edit Sub Specialty";
        $scope.item.$promise.then(function (data) {
            if (data.paymentCategoryId > 0) {
                $scope.paymentFees = paymentFeeResource.nameAndValues({ paymentCategoryId: $scope.item.paymentCategoryId });
            }
        });
    }
    SubSpecialtyEditCtrl.$inject = ["$scope", "SubSpecialtyResource", "PaymentFeeResource"];
    return SubSpecialtyEditCtrl;
}(BaseEditCtrl));
angular.module("app").controller("SubSpecialtyEditCtrl", SubSpecialtyEditCtrl);
//# sourceMappingURL=SubSpecialtyCtrl.js.map