var TamDashboardCtrl = (function () {
    function TamDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    TamDashboardCtrl.$inject = ["$scope"];
    return TamDashboardCtrl;
}());
angular.module("app").controller("TamDashboardCtrl", TamDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map