﻿
//******************************* List Controller *******************************

interface IInstitutionListCtrlScope extends IListCtrlScope {
    items: any[];
}


class InstitutionListCtrl extends BaseListCtrl {
    static $inject: string[] = ["$scope", "InstitutionResource"];

    localScope: IInstitutionListCtrlScope;
    localResource: IInstitutionResource;

    constructor($scope: IInstitutionListCtrlScope,
        private institutionResource: IInstitutionResource) {
        super($scope, institutionResource);
        this.localResource = institutionResource;

        this.init();
    }
}

angular.module("app").controller("InstitutionListCtrl", InstitutionListCtrl);

//******************************* End List Controller *******************************


//******************************* Create Controller *******************************
interface IInstitutionCreateCtrlScope extends ICreateEditCtrlScope {


}

interface IInstitutionCreateCtrl {

}

class InstitutionCreateCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "InstitutionResource"];

    constructor($scope: IInstitutionCreateCtrlScope, private institutionResource: IInstitutionResource) {
        super($scope, institutionResource);

        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Create Institution";
        $scope.submitForm = () => this.submitForm();
    }

    submitForm() {

        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(() => {
                this.$scope.waiting = 1;
                this.institutionResource.create(this.$scope.item).$promise.then((data) => {
                        this.$scope.waiting = 0;
                        this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                        this.$scope.$state.go('institution.list');

                    },
                    (error) => {
                        this.$scope.waiting = 0;
                        this.$scope.notificationSvc.showErrorToast(error.data.message);
                    });

            });
        }

    }
}

angular.module("app").controller("InstitutionCreateCtrl", InstitutionCreateCtrl);

//******************************* End Create Controller *******************************


//******************************* Edit Controller *******************************
interface IInstitutionEditCtrlScope extends ICreateEditCtrlScope {

    units: any[];

}

interface IInstitutionEditCtrl {

}

class InstitutionEditCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "InstitutionResource"];

    constructor($scope: IInstitutionEditCtrlScope,
        private institutionResource: IInstitutionResource) {
        super($scope, institutionResource);

        this.$scope.isEditMode = true;
        this.$scope.item = {};
        $scope.formTitle = "Edit Institution";
        $scope.submitForm = () => this.submitForm();
        this.$scope.waiting = 1;
        this.institutionResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then((data) => {
                this.$scope.waiting = 0;
                this.$scope.item = data;

            },
            (error) => {
                this.$scope.waiting = 0;
                this.$scope.notificationSvc.showErrorToast(error.data.message);
            });

    }

    submitForm() {

        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(() => {
                this.$scope.waiting = 1;
                this.institutionResource.update(this.$scope.item).$promise.then((data) => {
                        this.$scope.waiting = 0;
                        this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                        this.$scope.$state.go('institution.list');

                    },
                    (error) => {
                        this.$scope.waiting = 0;
                        this.$scope.notificationSvc.showErrorToast(error.data.message);
                    });

            });
        }
    }
}

angular.module("app").controller("InstitutionEditCtrl", InstitutionEditCtrl);

//******************************* End Edit Controller *******************************


//******************************* Form Controller *******************************

interface IInstitutionFormCtrlScope extends ICreateEditCtrlScope {
    openDate;
    opened;
    users;
    states;
}


class InstitutionFormCtrl {
    static $inject: string[] = ["$scope", "UserProfileResource","StateResource"];

    constructor(private $scope: IInstitutionFormCtrlScope,
        private userProfileResource: IUserProfileResource,
        private stateResource: IStateResource) {

        this.$scope.openDate = ($event, property: string) => {
            $event.preventDefault();
            $event.stopPropagation();
            this.$scope.opened[property] = true;
        };

        this.$scope.users = userProfileResource.getLookup();
        this.$scope.states = stateResource.getLookup();
        
        this.$scope.opened = {};
    }
}


angular.module("app").controller("InstitutionFormCtrl", InstitutionFormCtrl);

//******************************* End Form Controller *******************************