﻿
//******************************* List Controller *******************************

interface IWorkshopListCtrlScope extends IListCtrlScope {

    items: any[];

}


class WorkshopListCtrl extends BaseListCtrl {
    static $inject: string[] = ["$scope", "WorkshopResource"];

    localScope: IWorkshopListCtrlScope;
    localResource: IWorkshopResource;

    constructor($scope: IWorkshopListCtrlScope,
        private workshopResource: IWorkshopResource) {
        super($scope, workshopResource);
        this.localResource = workshopResource;

        this.init();

    }


}

angular.module("app").controller("WorkshopListCtrl", WorkshopListCtrl);

//******************************* End List Controller *******************************


//******************************* Create Controller *******************************
interface IWorkshopCreateCtrlScope extends ICreateEditCtrlScope {


}

interface IWorkshopCreateCtrl {

}

class WorkshopCreateCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "WorkshopResource"];

    constructor($scope: IWorkshopCreateCtrlScope, private WorkshopResource: IWorkshopResource) {
        super($scope, WorkshopResource);

        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Create Workshop";
        $scope.submitForm = () => this.submitForm();
    }

    submitForm() {

        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(() => {
                this.$scope.waiting = 1;
                this.WorkshopResource.create(this.$scope.item).$promise.then((data) => {
                        this.$scope.waiting = 0;
                        this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                        this.$scope.$state.go('workshop.list');

                    },
                    (error) => {
                        this.$scope.waiting = 0;
                        this.$scope.notificationSvc.showErrorToast(error.data.message);
                    });

            });
        }

    }
}

angular.module("app").controller("WorkshopCreateCtrl", WorkshopCreateCtrl);

//******************************* End Create Controller *******************************


//******************************* Edit Controller *******************************
interface IWorkshopEditCtrlScope extends ICreateEditCtrlScope {

    units: any[];

}

interface IWorkshopEditCtrl {

}

class WorkshopEditCtrl extends BaseCreateEditCtrl {
    static $inject: string[] = ["$scope", "WorkshopResource"];

    constructor($scope: IWorkshopEditCtrlScope,
        private WorkshopResource: IWorkshopResource) {
        super($scope, WorkshopResource);

        this.$scope.isEditMode = true;
        this.$scope.item = {};
        $scope.formTitle = "Edit Workshop";
        $scope.submitForm = () => this.submitForm();
        this.$scope.waiting = 1;
        this.WorkshopResource.get({ id: this.$scope.$stateParams.id }).$promise.then((data) => {
                this.$scope.waiting = 0;
                this.$scope.item = data;
                
            },
            (error) => {
                this.$scope.waiting = 0;
                this.$scope.notificationSvc.showErrorToast(error.data.message);
            });

    }

    submitForm() {

        if (this.$scope.validationSvc.isFormValid('inputForm')) {

            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(() => {
                this.$scope.waiting = 1;
                this.WorkshopResource.update(this.$scope.item).$promise.then((data) => {
                        this.$scope.waiting = 0;
                        this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                        this.$scope.$state.go('workshop.list');

                    },
                    (error) => {
                        this.$scope.waiting = 0;
                        this.$scope.notificationSvc.showErrorToast(error.data.message);
                    });

            });
        }
    }
}

angular.module("app").controller("WorkshopEditCtrl", WorkshopEditCtrl);

//******************************* End Edit Controller *******************************


//******************************* Form Controller *******************************

interface IWorkshopFormCtrlScope extends ICreateEditCtrlScope {

    openDate;
    opened;
    getPaymentFees;
    categories;
    validationRule: { rules: { name: { required: boolean };number: { required: boolean } };messages: {} };
    paymentCategoryChanged: () => void;
    paymentFees: any[];
    backLink;
}


class WorkshopFormCtrl {
    static $inject: string[] = ["$scope", "PaymentCategoryResource", "PaymentFeeResource"];
    localItem: IWorkshop;
    constructor(private $scope: IWorkshopFormCtrlScope, private paymentCategoryResource: IPaymentCategoryResource, private paymentFeeResource: IPaymentFeeResource) {

        this.$scope.openDate = ($event, property: string) => {
            $event.preventDefault();
            $event.stopPropagation();
            this.$scope.opened[property] = true;
            //this.$scope.getPaymentFees = ['amount 1', 'amount 2', 'amount 3'];
        };

        this.$scope.opened = {};
        $scope.paymentCategoryChanged = () => {
            if ((<any>$scope.item).paymentCategoryId > 0) {
                $scope.paymentFees = [];
                $scope.paymentFees = paymentFeeResource.nameAndValues({ paymentCategoryId: (<any>$scope.item).paymentCategoryId });
            }
        }
 
        this.$scope.categories = paymentCategoryResource.nameAndValues();
         
    }

   
}


angular.module("app").controller("WorkshopFormCtrl", WorkshopFormCtrl);

//******************************* End Form Controller *******************************