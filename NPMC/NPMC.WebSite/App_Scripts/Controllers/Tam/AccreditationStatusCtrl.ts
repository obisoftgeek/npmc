﻿//******************************* List Controller *******************************

interface IAccreditationStatusListCtrlScope extends IRootListCtrlScope {

    items: any[];
    showCreateForm: (value: any) => void;
    showForm;
    addItem: (item) => void;
}


class AccreditationStatusListCtrl extends RootListCtrl {
    static $inject: string[] = ["$scope", "AccreditationStatusResource"];

    localScope: IAccreditationStatusListCtrlScope;
    localResource: IAccreditationStatusResource;

    constructor($scope: IAccreditationStatusListCtrlScope, private AccreditationStatusResource: IAccreditationStatusResource) {
        super($scope, AccreditationStatusResource);
        this.localResource = AccreditationStatusResource;
        this.localScope = $scope;
        this.init();
        // $scope.showForm = false;
        // $scope.showCreateForm = (value) => {
        //     $scope.showForm = value;
        // };
        // $scope.addItem = (item) => {
        //     AccreditationStatusResource.save(item).$promise.then((data) => {
        //         toastr.success("Saved successfully", "Information");
        //         this.$scope.$state.reload();
        //     }, (response) => {
        //         toastr.error("Error occured while trying to save the item. please try again", "Error!");
        //     });
        // }
    }
    downloadAsExcel() {
        location.href = "/api/AccreditationStatus/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    }

}
angular.module("app").controller("AccreditationStatusListCtrl", AccreditationStatusListCtrl);

//******************************* End List Controller *******************************


interface IAccreditationStatusCreateCtrlScope extends IRootCreateEditCtrlScope {
    title: string;
}

interface IAccreditationStatusCreateCtrl {

}

class AccreditationStatusCreateCtrl extends BaseCreateCtrl {
    static $inject: string[] = ["$scope", "AccreditationStatusResource"];

    constructor($scope: IAccreditationStatusCreateCtrlScope,
                AccreditationStatusResource: IAccreditationStatusResource) {
        super($scope, AccreditationStatusResource);
        $scope.title = "Create Status";
    }

}

angular.module("app").controller("AccreditationStatusCreateCtrl", AccreditationStatusCreateCtrl);

interface IAccreditationStatusFormCtrlScope extends IRootCreateEditCtrlScope {
    backLink: {
        state: string;
        config?: {};
        forwardLink: string;
    };
    states;
    dynamicPopover
}


class AccreditationStatusFormCtrl {
    static $inject: string[] = ["$scope"];
    localItem: IAccreditationStatus;

    constructor(private $scope: IAccreditationStatusFormCtrlScope) {
        $scope.backLink.state = "accreditationstatus.list";
        $scope.backLink.forwardLink = "accreditationstatus.list";
        this.localItem = <IAccreditationStatus>this.$scope.item;
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                },
                number: {
                    required: true
                }
            },
            messages: {}
        };
    }
}

angular.module("app").controller("AccreditationStatusFormCtrl", AccreditationStatusFormCtrl);

interface IAccreditationStatusEditCtrlScope extends IRootCreateEditCtrlScope {

}

interface IAccreditationStatusEditCtrl {

}

class AccreditationStatusEditCtrl extends BaseEditCtrl {
    static $inject: string[] = ["$scope", "AccreditationStatusResource"];

    constructor($scope: IAccreditationStatusEditCtrlScope,
                AccreditationStatusResource: IAccreditationStatusResource) {
        super($scope, AccreditationStatusResource);
        $scope.title = "Edit Status";
    }

}

angular.module("app").controller("AccreditationStatusEditCtrl", AccreditationStatusEditCtrl);