//******************************* List Controller *******************************
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var AccountListCtrl = (function (_super) {
    __extends(AccountListCtrl, _super);
    function AccountListCtrl($scope, AccountResource) {
        var _this = _super.call(this, $scope, AccountResource) || this;
        _this.AccountResource = AccountResource;
        _this.localResource = AccountResource;
        _this.localScope = $scope;
        _this.init();
        $scope.showForm = false;
        $scope.showCreateForm = function (value) {
            $scope.showForm = value;
        };
        $scope.addItem = function (item) {
            AccountResource.save(item).$promise.then(function (data) {
                toastr.success("Saved successfully", "Information");
                _this.$scope.$state.reload();
            }, function (response) {
                toastr.error("Error occured while trying to save the item. please try again", "Error!");
            });
        };
        return _this;
    }
    AccountListCtrl.prototype.downloadAsExcel = function () {
        location.href = "/api/Account/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    };
    return AccountListCtrl;
}(RootListCtrl));
AccountListCtrl.$inject = ["$scope", "AccountResource"];
angular.module("app").controller("AccountListCtrl", AccountListCtrl);
var AccountCreateCtrl = (function (_super) {
    __extends(AccountCreateCtrl, _super);
    function AccountCreateCtrl($scope, AccountResource) {
        var _this = _super.call(this, $scope, AccountResource) || this;
        $scope.title = "Create Account";
        return _this;
    }
    return AccountCreateCtrl;
}(BaseCreateCtrl));
AccountCreateCtrl.$inject = ["$scope", "AccountResource"];
angular.module("app").controller("AccountCreateCtrl", AccountCreateCtrl);
var AccountFormCtrl = (function () {
    function AccountFormCtrl($scope) {
        this.$scope = $scope;
        $scope.backLink.state = "account.list";
        this.localItem = this.$scope.item;
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                },
                number: {
                    required: true
                }
            },
            messages: {}
        };
    }
    return AccountFormCtrl;
}());
AccountFormCtrl.$inject = ["$scope"];
angular.module("app").controller("AccountFormCtrl", AccountFormCtrl);
var AccountEditCtrl = (function (_super) {
    __extends(AccountEditCtrl, _super);
    function AccountEditCtrl($scope, AccountResource) {
        var _this = _super.call(this, $scope, AccountResource) || this;
        $scope.title = "Edit Account";
        return _this;
    }
    return AccountEditCtrl;
}(BaseEditCtrl));
AccountEditCtrl.$inject = ["$scope", "AccountResource"];
angular.module("app").controller("AccountEditCtrl", AccountEditCtrl);
//# sourceMappingURL=AccountCtrl.js.map