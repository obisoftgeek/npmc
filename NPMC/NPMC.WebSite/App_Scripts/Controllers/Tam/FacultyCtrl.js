//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var FacultyListCtrl = (function (_super) {
    __extends(FacultyListCtrl, _super);
    function FacultyListCtrl($scope, FacultyResource) {
        var _this = this;
        _super.call(this, $scope, FacultyResource);
        this.FacultyResource = FacultyResource;
        this.localResource = FacultyResource;
        this.localScope = $scope;
        $scope.backLink.state = "faculty.list";
        this.init();
        $scope.showForm = false;
        $scope.showCreateForm = function (value) {
            $scope.showForm = value;
        };
        $scope.addItem = function (item) {
            FacultyResource.save(item).$promise.then(function (data) {
                toastr.success("Saved successfully", "Information");
                _this.$scope.$state.reload();
            }, function (response) {
                toastr.error("Error occured while trying to save the item. please try again", "Error!");
            });
        };
    }
    FacultyListCtrl.prototype.downloadAsExcel = function () {
        location.href = "/api/Faculty/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    };
    FacultyListCtrl.$inject = ["$scope", "FacultyResource"];
    return FacultyListCtrl;
}(RootListCtrl));
angular.module("app").controller("FacultyListCtrl", FacultyListCtrl);
var FacultyCreateCtrl = (function (_super) {
    __extends(FacultyCreateCtrl, _super);
    function FacultyCreateCtrl($scope, FacultyResource) {
        _super.call(this, $scope, FacultyResource);
        $scope.title = "Create Faculty";
        console.log($scope.$stateParams);
    }
    FacultyCreateCtrl.$inject = ["$scope", "FacultyResource"];
    return FacultyCreateCtrl;
}(BaseCreateCtrl));
angular.module("app").controller("FacultyCreateCtrl", FacultyCreateCtrl);
var FacultyFormCtrl = (function () {
    function FacultyFormCtrl($scope, PaymentCategoryResource, paymentFeeResource) {
        this.$scope = $scope;
        this.PaymentCategoryResource = PaymentCategoryResource;
        this.paymentFeeResource = paymentFeeResource;
        $scope.backLink.state = "faculty.list";
        $scope.backLink.forwardLink = "faculty.list";
        this.localItem = this.$scope.item;
        $scope.categories = PaymentCategoryResource.nameAndValues();
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                },
                number: {
                    required: true
                }
            },
            messages: {}
        };
        $scope.paymentCategoryChanged = function () {
            if ($scope.item.paymentCategoryId > 0) {
                $scope.paymentFees = [];
                $scope.paymentFees = paymentFeeResource.nameAndValues({ paymentCategoryId: $scope.item.paymentCategoryId });
            }
        };
    }
    FacultyFormCtrl.$inject = ["$scope", "PaymentCategoryResource", "PaymentFeeResource"];
    return FacultyFormCtrl;
}());
angular.module("app").controller("FacultyFormCtrl", FacultyFormCtrl);
var FacultyEditCtrl = (function (_super) {
    __extends(FacultyEditCtrl, _super);
    function FacultyEditCtrl($scope, FacultyResource, paymentFeeResource) {
        _super.call(this, $scope, FacultyResource);
        $scope.title = "Edit Faculty";
        $scope.item.$promise.then(function (data) {
            if (data.paymentCategoryId > 0) {
                $scope.paymentFees = paymentFeeResource.nameAndValues({ paymentCategoryId: $scope.item.paymentCategoryId });
            }
        });
    }
    FacultyEditCtrl.$inject = ["$scope", "FacultyResource", "PaymentFeeResource"];
    return FacultyEditCtrl;
}(BaseEditCtrl));
angular.module("app").controller("FacultyEditCtrl", FacultyEditCtrl);
//# sourceMappingURL=FacultyCtrl.js.map