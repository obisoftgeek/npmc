﻿
//******************************* List Controller *******************************

interface IFacultyListCtrlScope extends IRootListCtrlScope {

    items: any[];
    showCreateForm: (value: any) => void;
    showForm;
    addItem: (item) => void;
}


class FacultyListCtrl extends RootListCtrl {
    static $inject: string[] = ["$scope", "FacultyResource"];

    localScope: IFacultyListCtrlScope;
    localResource: IFacultyResource;
    constructor($scope: IFacultyListCtrlScope, private FacultyResource: IFacultyResource) {
        super($scope, FacultyResource);
        this.localResource = FacultyResource;
        this.localScope = $scope;
        $scope.backLink.state = "faculty.list";
        this.init();
        $scope.showForm = false;
        $scope.showCreateForm = (value) => {
            $scope.showForm = value;
        }
        $scope.addItem = (item) => {
            FacultyResource.save(item).$promise.then((data) => {
                toastr.success("Saved successfully", "Information");
                this.$scope.$state.reload();
            }, (response) => {
                toastr.error("Error occured while trying to save the item. please try again", "Error!");
            });
        }
    }

     
    downloadAsExcel() {
        location.href = "/api/Faculty/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    }

}
angular.module("app").controller("FacultyListCtrl", FacultyListCtrl);

//******************************* End List Controller *******************************


interface IFacultyCreateCtrlScope extends IRootCreateEditCtrlScope {
    title: string;
}

interface IFacultyCreateCtrl {

}

class FacultyCreateCtrl extends BaseCreateCtrl {
    static $inject: string[] = ["$scope", "FacultyResource"];

    constructor($scope: IFacultyCreateCtrlScope,
        FacultyResource: IFacultyResource) {
        super($scope, FacultyResource);
        $scope.title = "Create Faculty";
        console.log($scope.$stateParams)
    }

}

angular.module("app").controller("FacultyCreateCtrl", FacultyCreateCtrl);

interface IFacultyFormCtrlScope extends IRootCreateEditCtrlScope {
    backLink: {
        state: string;
        forwardLink:string;
        config?: {};
    };
    states;
    dynamicPopover;
    categories;
    paymentCategoryChanged;
    paymentFees;
}


class FacultyFormCtrl {
    static $inject: string[] = ["$scope", "PaymentCategoryResource","PaymentFeeResource"];
    localItem: IFaculty;
    constructor(private $scope: IFacultyFormCtrlScope,
        private PaymentCategoryResource: IPaymentCategoryResource, private paymentFeeResource:IPaymentFeeResource) {
        $scope.backLink.state = "faculty.list"; 
        $scope.backLink.forwardLink = "faculty.list"; 
        this.localItem = <IFaculty>this.$scope.item;
        $scope.categories = PaymentCategoryResource.nameAndValues();
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                },
                number: {
                    required: true
                }
            },
            messages: {

            }
        };
        $scope.paymentCategoryChanged=() => {
            if ((<any>$scope.item).paymentCategoryId > 0) {
                $scope.paymentFees = [];
                $scope.paymentFees = paymentFeeResource.nameAndValues({ paymentCategoryId: (<any>$scope.item).paymentCategoryId });
            }
        }
    }
}

angular.module("app").controller("FacultyFormCtrl", FacultyFormCtrl);

interface IFacultyEditCtrlScope extends IRootCreateEditCtrlScope {
    paymentFees;
}

interface IFacultyEditCtrl {

}

class FacultyEditCtrl extends BaseEditCtrl {
    static $inject: string[] = ["$scope", "FacultyResource","PaymentFeeResource"];

    constructor($scope: IFacultyEditCtrlScope,
        FacultyResource: IFacultyResource, paymentFeeResource: IPaymentFeeResource) {
        super($scope, FacultyResource);
        $scope.title = "Edit Faculty";
        $scope.item.$promise.then((data) => {
            if ((<any>data).paymentCategoryId > 0) {
                $scope.paymentFees = paymentFeeResource.nameAndValues({ paymentCategoryId: (<any>$scope.item).paymentCategoryId });
            }
        });
    }

}

angular.module("app").controller("FacultyEditCtrl", FacultyEditCtrl);