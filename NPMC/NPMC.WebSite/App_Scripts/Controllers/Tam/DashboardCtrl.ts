﻿


interface ITamDashboardCtrlScope extends IRootScope {
   
    item;
    
}


class TamDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: ITamDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("TamDashboardCtrl", TamDashboardCtrl);;