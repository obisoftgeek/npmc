﻿//******************************* List Controller *******************************

interface ISubSpecialtyListCtrlScope extends IRootListCtrlScope {

    items: any[];
    showCreateForm: (value: any) => void;
    showForm;
    addItem: (item) => void;
}


class SubSpecialtyListCtrl extends RootListCtrl {
    static $inject: string[] = ["$scope", "SubSpecialtyResource"];

    localScope: ISubSpecialtyListCtrlScope;
    localResource: ISubSpecialtyResource;

    constructor($scope: ISubSpecialtyListCtrlScope, private SubSpecialtyResource: ISubSpecialtyResource) {
        super($scope, SubSpecialtyResource);
        this.localResource = SubSpecialtyResource;
        this.localScope = $scope;
        this.init();
        this.$scope.title = "Sub Specialties";
    }

    downloadAsExcel() {
        location.href = "/api/SubSpecialty/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    }

}
angular.module("app").controller("SubSpecialtyListCtrl", SubSpecialtyListCtrl);

//******************************* End List Controller *******************************


interface ISubSpecialtyCreateCtrlScope extends IRootCreateEditCtrlScope {
    title: string;
}

interface ISubSpecialtyCreateCtrl {

}

class SubSpecialtyCreateCtrl extends BaseCreateCtrl {
    static $inject: string[] = ["$scope", "SubSpecialtyResource"];

    constructor($scope: ISubSpecialtyCreateCtrlScope,
                SubSpecialtyResource: ISubSpecialtyResource) {
        super($scope, SubSpecialtyResource);
        $scope.title = "Create Sub Specialty";
    }

}

angular.module("app").controller("SubSpecialtyCreateCtrl", SubSpecialtyCreateCtrl);

interface ISubSpecialtyFormCtrlScope extends IRootCreateEditCtrlScope {
    backLink: {
        state: string;
        config?: {};
        forwardLink: string;
    };
    states;
    dynamicPopover;
    categories: any;
    faculties: any;
    paymentFees;
    paymentCategoryChanged;
}


class SubSpecialtyFormCtrl {
    static $inject: string[] = ["$scope", "PaymentCategoryResource", "PaymentFeeResource", "FacultyResource"];
    localItem: ISubSpecialty;

    constructor(private $scope: ISubSpecialtyFormCtrlScope,
        private paymentCategoryResource: IPaymentCategoryResource,
        private paymentFeeResource: IPaymentFeeResource,
        private facultyResource: IFacultyResource) {
        $scope.backLink.state = "subspecialty.list";
        $scope.backLink.forwardLink = "subspecialty.list";
        this.localItem = <ISubSpecialty>this.$scope.item;
        $scope.categories = paymentCategoryResource.nameAndValues();
        $scope.faculties = facultyResource.nameAndValues();
        $scope.paymentCategoryChanged = () => {
            if ((<any>$scope.item).paymentCategoryId > 1) {
                $scope.paymentFees = [];
                $scope.paymentFees = paymentFeeResource.nameAndValues({paymentCategoryId: (<any>$scope.item).paymentCategoryId});
            }
        }
    }


}

angular.module("app").controller("SubSpecialtyFormCtrl", SubSpecialtyFormCtrl);

interface ISubSpecialtyEditCtrlScope extends IRootCreateEditCtrlScope {
    paymentFees;
}

interface ISubSpecialtyEditCtrl {
    
}

class SubSpecialtyEditCtrl extends BaseEditCtrl {
    static $inject: string[] = ["$scope", "SubSpecialtyResource","PaymentFeeResource"];

    constructor($scope: ISubSpecialtyEditCtrlScope,
        SubSpecialtyResource: ISubSpecialtyResource, paymentFeeResource:IPaymentFeeResource) {
        super($scope, SubSpecialtyResource);
        $scope.title = "Edit Sub Specialty";
        $scope.item.$promise.then((data) => {
            if ((<any>data).paymentCategoryId > 0) {
                $scope.paymentFees = paymentFeeResource.nameAndValues({ paymentCategoryId: (<any>$scope.item).paymentCategoryId });
            }
        });
        
    }

}

angular.module("app").controller("SubSpecialtyEditCtrl", SubSpecialtyEditCtrl);