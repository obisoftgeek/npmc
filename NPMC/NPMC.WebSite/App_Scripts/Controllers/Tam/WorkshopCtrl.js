//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var WorkshopListCtrl = (function (_super) {
    __extends(WorkshopListCtrl, _super);
    function WorkshopListCtrl($scope, workshopResource) {
        _super.call(this, $scope, workshopResource);
        this.workshopResource = workshopResource;
        this.localResource = workshopResource;
        this.init();
    }
    WorkshopListCtrl.$inject = ["$scope", "WorkshopResource"];
    return WorkshopListCtrl;
}(BaseListCtrl));
angular.module("app").controller("WorkshopListCtrl", WorkshopListCtrl);
var WorkshopCreateCtrl = (function (_super) {
    __extends(WorkshopCreateCtrl, _super);
    function WorkshopCreateCtrl($scope, WorkshopResource) {
        var _this = this;
        _super.call(this, $scope, WorkshopResource);
        this.WorkshopResource = WorkshopResource;
        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Create Workshop";
        $scope.submitForm = function () { return _this.submitForm(); };
    }
    WorkshopCreateCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.WorkshopResource.create(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.$state.go('workshop.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    WorkshopCreateCtrl.$inject = ["$scope", "WorkshopResource"];
    return WorkshopCreateCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("WorkshopCreateCtrl", WorkshopCreateCtrl);
var WorkshopEditCtrl = (function (_super) {
    __extends(WorkshopEditCtrl, _super);
    function WorkshopEditCtrl($scope, WorkshopResource) {
        var _this = this;
        _super.call(this, $scope, WorkshopResource);
        this.WorkshopResource = WorkshopResource;
        this.$scope.isEditMode = true;
        this.$scope.item = {};
        $scope.formTitle = "Edit Workshop";
        $scope.submitForm = function () { return _this.submitForm(); };
        this.$scope.waiting = 1;
        this.WorkshopResource.get({ id: this.$scope.$stateParams.id }).$promise.then(function (data) {
            _this.$scope.waiting = 0;
            _this.$scope.item = data;
        }, function (error) {
            _this.$scope.waiting = 0;
            _this.$scope.notificationSvc.showErrorToast(error.data.message);
        });
    }
    WorkshopEditCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.WorkshopResource.update(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.$state.go('workshop.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    WorkshopEditCtrl.$inject = ["$scope", "WorkshopResource"];
    return WorkshopEditCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("WorkshopEditCtrl", WorkshopEditCtrl);
var WorkshopFormCtrl = (function () {
    function WorkshopFormCtrl($scope, paymentCategoryResource, paymentFeeResource) {
        var _this = this;
        this.$scope = $scope;
        this.paymentCategoryResource = paymentCategoryResource;
        this.paymentFeeResource = paymentFeeResource;
        this.$scope.openDate = function ($event, property) {
            $event.preventDefault();
            $event.stopPropagation();
            _this.$scope.opened[property] = true;
            //this.$scope.getPaymentFees = ['amount 1', 'amount 2', 'amount 3'];
        };
        this.$scope.opened = {};
        $scope.paymentCategoryChanged = function () {
            if ($scope.item.paymentCategoryId > 0) {
                $scope.paymentFees = [];
                $scope.paymentFees = paymentFeeResource.nameAndValues({ paymentCategoryId: $scope.item.paymentCategoryId });
            }
        };
        this.$scope.categories = paymentCategoryResource.nameAndValues();
    }
    WorkshopFormCtrl.$inject = ["$scope", "PaymentCategoryResource", "PaymentFeeResource"];
    return WorkshopFormCtrl;
}());
angular.module("app").controller("WorkshopFormCtrl", WorkshopFormCtrl);
//******************************* End Form Controller ******************************* 
//# sourceMappingURL=WorkshopCtrl.js.map