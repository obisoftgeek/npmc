//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InstitutionListCtrl = (function (_super) {
    __extends(InstitutionListCtrl, _super);
    function InstitutionListCtrl($scope, institutionResource) {
        _super.call(this, $scope, institutionResource);
        this.institutionResource = institutionResource;
        this.localResource = institutionResource;
        this.init();
    }
    InstitutionListCtrl.$inject = ["$scope", "InstitutionResource"];
    return InstitutionListCtrl;
}(BaseListCtrl));
angular.module("app").controller("InstitutionListCtrl", InstitutionListCtrl);
var InstitutionCreateCtrl = (function (_super) {
    __extends(InstitutionCreateCtrl, _super);
    function InstitutionCreateCtrl($scope, institutionResource) {
        var _this = this;
        _super.call(this, $scope, institutionResource);
        this.institutionResource = institutionResource;
        this.$scope.isEditMode = false;
        this.$scope.item = {};
        $scope.formTitle = "Create Institution";
        $scope.submitForm = function () { return _this.submitForm(); };
    }
    InstitutionCreateCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.institutionResource.create(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.$state.go('institution.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    InstitutionCreateCtrl.$inject = ["$scope", "InstitutionResource"];
    return InstitutionCreateCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("InstitutionCreateCtrl", InstitutionCreateCtrl);
var InstitutionEditCtrl = (function (_super) {
    __extends(InstitutionEditCtrl, _super);
    function InstitutionEditCtrl($scope, institutionResource) {
        var _this = this;
        _super.call(this, $scope, institutionResource);
        this.institutionResource = institutionResource;
        this.$scope.isEditMode = true;
        this.$scope.item = {};
        $scope.formTitle = "Edit Institution";
        $scope.submitForm = function () { return _this.submitForm(); };
        this.$scope.waiting = 1;
        this.institutionResource.getItem({ id: this.$scope.$stateParams.id }).$promise.then(function (data) {
            _this.$scope.waiting = 0;
            _this.$scope.item = data;
        }, function (error) {
            _this.$scope.waiting = 0;
            _this.$scope.notificationSvc.showErrorToast(error.data.message);
        });
    }
    InstitutionEditCtrl.prototype.submitForm = function () {
        var _this = this;
        if (this.$scope.validationSvc.isFormValid('inputForm')) {
            this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to submit now?").then(function () {
                _this.$scope.waiting = 1;
                _this.institutionResource.update(_this.$scope.item).$promise.then(function (data) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showSuccessToast("Operation completed successfully!");
                    _this.$scope.$state.go('institution.list');
                }, function (error) {
                    _this.$scope.waiting = 0;
                    _this.$scope.notificationSvc.showErrorToast(error.data.message);
                });
            });
        }
    };
    InstitutionEditCtrl.$inject = ["$scope", "InstitutionResource"];
    return InstitutionEditCtrl;
}(BaseCreateEditCtrl));
angular.module("app").controller("InstitutionEditCtrl", InstitutionEditCtrl);
var InstitutionFormCtrl = (function () {
    function InstitutionFormCtrl($scope, userProfileResource, stateResource) {
        var _this = this;
        this.$scope = $scope;
        this.userProfileResource = userProfileResource;
        this.stateResource = stateResource;
        this.$scope.openDate = function ($event, property) {
            $event.preventDefault();
            $event.stopPropagation();
            _this.$scope.opened[property] = true;
        };
        this.$scope.users = userProfileResource.getLookup();
        this.$scope.states = stateResource.getLookup();
        this.$scope.opened = {};
    }
    InstitutionFormCtrl.$inject = ["$scope", "UserProfileResource", "StateResource"];
    return InstitutionFormCtrl;
}());
angular.module("app").controller("InstitutionFormCtrl", InstitutionFormCtrl);
//******************************* End Form Controller ******************************* 
//# sourceMappingURL=InstitutionCtrl.js.map