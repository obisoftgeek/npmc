//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AccreditationStatusListCtrl = (function (_super) {
    __extends(AccreditationStatusListCtrl, _super);
    function AccreditationStatusListCtrl($scope, AccreditationStatusResource) {
        _super.call(this, $scope, AccreditationStatusResource);
        this.AccreditationStatusResource = AccreditationStatusResource;
        this.localResource = AccreditationStatusResource;
        this.localScope = $scope;
        this.init();
        // $scope.showForm = false;
        // $scope.showCreateForm = (value) => {
        //     $scope.showForm = value;
        // };
        // $scope.addItem = (item) => {
        //     AccreditationStatusResource.save(item).$promise.then((data) => {
        //         toastr.success("Saved successfully", "Information");
        //         this.$scope.$state.reload();
        //     }, (response) => {
        //         toastr.error("Error occured while trying to save the item. please try again", "Error!");
        //     });
        // }
    }
    AccreditationStatusListCtrl.prototype.downloadAsExcel = function () {
        location.href = "/api/AccreditationStatus/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    };
    AccreditationStatusListCtrl.$inject = ["$scope", "AccreditationStatusResource"];
    return AccreditationStatusListCtrl;
}(RootListCtrl));
angular.module("app").controller("AccreditationStatusListCtrl", AccreditationStatusListCtrl);
var AccreditationStatusCreateCtrl = (function (_super) {
    __extends(AccreditationStatusCreateCtrl, _super);
    function AccreditationStatusCreateCtrl($scope, AccreditationStatusResource) {
        _super.call(this, $scope, AccreditationStatusResource);
        $scope.title = "Create Status";
    }
    AccreditationStatusCreateCtrl.$inject = ["$scope", "AccreditationStatusResource"];
    return AccreditationStatusCreateCtrl;
}(BaseCreateCtrl));
angular.module("app").controller("AccreditationStatusCreateCtrl", AccreditationStatusCreateCtrl);
var AccreditationStatusFormCtrl = (function () {
    function AccreditationStatusFormCtrl($scope) {
        this.$scope = $scope;
        $scope.backLink.state = "accreditationstatus.list";
        $scope.backLink.forwardLink = "accreditationstatus.list";
        this.localItem = this.$scope.item;
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                },
                number: {
                    required: true
                }
            },
            messages: {}
        };
    }
    AccreditationStatusFormCtrl.$inject = ["$scope"];
    return AccreditationStatusFormCtrl;
}());
angular.module("app").controller("AccreditationStatusFormCtrl", AccreditationStatusFormCtrl);
var AccreditationStatusEditCtrl = (function (_super) {
    __extends(AccreditationStatusEditCtrl, _super);
    function AccreditationStatusEditCtrl($scope, AccreditationStatusResource) {
        _super.call(this, $scope, AccreditationStatusResource);
        $scope.title = "Edit Status";
    }
    AccreditationStatusEditCtrl.$inject = ["$scope", "AccreditationStatusResource"];
    return AccreditationStatusEditCtrl;
}(BaseEditCtrl));
angular.module("app").controller("AccreditationStatusEditCtrl", AccreditationStatusEditCtrl);
//# sourceMappingURL=AccreditationStatusCtrl.js.map