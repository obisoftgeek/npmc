﻿

// Install the angularjs.TypeScript.DefinitelyTyped NuGet package

interface ITransportDashboardCtrlScope extends IRootScope {
   
    item;
    
}


class TransportDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: ITransportDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("TransportDashboardCtrl", TransportDashboardCtrl);;