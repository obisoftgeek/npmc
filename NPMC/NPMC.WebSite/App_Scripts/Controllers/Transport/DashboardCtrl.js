// Install the angularjs.TypeScript.DefinitelyTyped NuGet package
var TransportDashboardCtrl = (function () {
    function TransportDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    TransportDashboardCtrl.$inject = ["$scope"];
    return TransportDashboardCtrl;
}());
angular.module("app").controller("TransportDashboardCtrl", TransportDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map