﻿
//******************************* List Controller *******************************

interface IMailClientListCtrlScope extends IRootListCtrlScope {

    items: any[];
}

class MailClientListCtrl extends RootListCtrl {
    static $inject: string[] = ["$scope", "MailResource"];

    localScope: IMailClientListCtrlScope;
    localResource: IMailResource;

    constructor($scope: IMailClientListCtrlScope, private MailResource: IMailResource) {
        super($scope, MailResource);
        this.localResource = MailResource;
        this.localScope = $scope;
        $scope.backLink.state = "mailclient.list";
        this.init();
    }

    downloadAsExcel() {
        location.href = "/api/MailClient/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    }

}

angular.module("app").controller("MailClientListCtrl", MailClientListCtrl);

//******************************* End List Controller *******************************


interface IMailClientCreateCtrlScope extends IRootCreateEditCtrlScope {
    title: string;
}

interface IMailClientCreateCtrl {

}

interface IMailClientFormCtrlScope extends IRootCreateEditCtrlScope {
    backLink: {
        state: string;
        forwardLink: string;
        config?: {};
    };
    departments;
    departmentSelected;
    departmentUsers;
    item;
    fileSelected;
    upload;
    uploading;
    recipient;
    uploadCount;
    errors;
    percent;
    uploadFiles;
    errorRecipient: boolean;
    clearError;
}
interface IMailClientDetailCtrlScope extends IRootBaseDetailCtrlScope {
    title: string;
    reload;
    numberOfAttachments;
    properties;
    getColor;
    getIcon;
    fileTypes;
}

interface IMailClientDetailCtrl {

}

class MailClientDetailCtrl extends RootBaseDetailCtrl {
    static $inject: string[] = ["$scope", "MailResource"];

    constructor($scope: IMailClientDetailCtrlScope,
        private MailResource: IMailResource) {
        super($scope, MailResource, "mailclient.list");
        $scope.title = "Details";
        $scope.getIcon = (type) => this.getIcon(type);
        $scope.fileTypes = this.fileTypes;
    }

    fileTypes = {
        Image: 1,
        Pdf: 2,
        Doc: 3,
        Xls: 4
    };
     
    getIcon =(type: number) => {
        switch (type) {
        case this.fileTypes.Pdf:
            return "fa-file-pdf-o text-red";
        case this.fileTypes.Doc:
            return "fa-file-word-o text-blue";
        case this.fileTypes.Xls:
            return "fa-file-excel-o text-green";
        default:
            return "fa-file-image-o text-purple";
        }
    }
}

angular.module("app").controller("MailClientDetailCtrl", MailClientDetailCtrl);