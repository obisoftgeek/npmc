//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MailClientListCtrl = (function (_super) {
    __extends(MailClientListCtrl, _super);
    function MailClientListCtrl($scope, MailResource) {
        _super.call(this, $scope, MailResource);
        this.MailResource = MailResource;
        this.localResource = MailResource;
        this.localScope = $scope;
        $scope.backLink.state = "mailclient.list";
        this.init();
    }
    MailClientListCtrl.prototype.downloadAsExcel = function () {
        location.href = "/api/MailClient/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    };
    MailClientListCtrl.$inject = ["$scope", "MailResource"];
    return MailClientListCtrl;
}(RootListCtrl));
angular.module("app").controller("MailClientListCtrl", MailClientListCtrl);
var MailClientDetailCtrl = (function (_super) {
    __extends(MailClientDetailCtrl, _super);
    function MailClientDetailCtrl($scope, MailResource) {
        var _this = this;
        _super.call(this, $scope, MailResource, "mailclient.list");
        this.MailResource = MailResource;
        this.fileTypes = {
            Image: 1,
            Pdf: 2,
            Doc: 3,
            Xls: 4
        };
        this.getIcon = function (type) {
            switch (type) {
                case _this.fileTypes.Pdf:
                    return "fa-file-pdf-o text-red";
                case _this.fileTypes.Doc:
                    return "fa-file-word-o text-blue";
                case _this.fileTypes.Xls:
                    return "fa-file-excel-o text-green";
                default:
                    return "fa-file-image-o text-purple";
            }
        };
        $scope.title = "Details";
        $scope.getIcon = function (type) { return _this.getIcon(type); };
        $scope.fileTypes = this.fileTypes;
    }
    MailClientDetailCtrl.$inject = ["$scope", "MailResource"];
    return MailClientDetailCtrl;
}(RootBaseDetailCtrl));
angular.module("app").controller("MailClientDetailCtrl", MailClientDetailCtrl);
//# sourceMappingURL=MailClientCtrl.js.map