//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var PaymentFeeListCtrl = (function (_super) {
    __extends(PaymentFeeListCtrl, _super);
    function PaymentFeeListCtrl($scope, PaymentFeeResource) {
        var _this = this;
        _super.call(this, $scope, PaymentFeeResource);
        this.PaymentFeeResource = PaymentFeeResource;
        this.localResource = PaymentFeeResource;
        this.localScope = $scope;
        $scope.backLink.state = "paymentcategory.list";
        this.init();
        $scope.showForm = false;
        $scope.showCreateForm = function (value) {
            $scope.showForm = value;
        };
        $scope.addItem = function (item) {
            PaymentFeeResource.save(item).$promise.then(function (data) {
                toastr.success("Saved successfully", "Information");
                _this.$scope.$state.reload();
            }, function (response) {
                toastr.error("Error occured while trying to save the item. please try again", "Error!");
            });
        };
    }
    PaymentFeeListCtrl.prototype.downloadAsExcel = function () {
        location.href = "/api/PaymentFee/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    };
    PaymentFeeListCtrl.$inject = ["$scope", "PaymentFeeResource"];
    return PaymentFeeListCtrl;
}(RootListCtrl));
angular.module("app").controller("PaymentFeeListCtrl", PaymentFeeListCtrl);
var PaymentFeeCreateCtrl = (function (_super) {
    __extends(PaymentFeeCreateCtrl, _super);
    function PaymentFeeCreateCtrl($scope, PaymentFeeResource) {
        _super.call(this, $scope, PaymentFeeResource);
        $scope.title = "Create Payment Fee";
        console.log($scope.$stateParams);
    }
    PaymentFeeCreateCtrl.$inject = ["$scope", "PaymentFeeResource"];
    return PaymentFeeCreateCtrl;
}(BaseCreateCtrl));
angular.module("app").controller("PaymentFeeCreateCtrl", PaymentFeeCreateCtrl);
var PaymentFeeFormCtrl = (function () {
    function PaymentFeeFormCtrl($scope, accountResource) {
        this.$scope = $scope;
        this.accountResource = accountResource;
        $scope.backLink.state = "paymentfee.list";
        $scope.backLink.forwardLink = "paymentfee.list";
        this.localItem = this.$scope.item;
        $scope.accounts = accountResource.nameAndValues();
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                },
                number: {
                    required: true
                }
            },
            messages: {}
        };
    }
    PaymentFeeFormCtrl.$inject = ["$scope", "AccountResource"];
    return PaymentFeeFormCtrl;
}());
angular.module("app").controller("PaymentFeeFormCtrl", PaymentFeeFormCtrl);
var PaymentFeeEditCtrl = (function (_super) {
    __extends(PaymentFeeEditCtrl, _super);
    function PaymentFeeEditCtrl($scope, PaymentFeeResource) {
        _super.call(this, $scope, PaymentFeeResource);
        $scope.title = "Edit Payment Fee";
    }
    PaymentFeeEditCtrl.$inject = ["$scope", "PaymentFeeResource"];
    return PaymentFeeEditCtrl;
}(BaseEditCtrl));
angular.module("app").controller("PaymentFeeEditCtrl", PaymentFeeEditCtrl);
//# sourceMappingURL=PaymentFeeCtrl.js.map