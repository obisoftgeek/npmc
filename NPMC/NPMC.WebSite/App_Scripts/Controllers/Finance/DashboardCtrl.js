var FinanceDashboardCtrl = (function () {
    function FinanceDashboardCtrl($scope) {
        this.$scope = $scope;
        $scope.formTitle = "Dashboard";
    }
    FinanceDashboardCtrl.$inject = ["$scope"];
    return FinanceDashboardCtrl;
}());
angular.module("app").controller("FinanceDashboardCtrl", FinanceDashboardCtrl);
;
//# sourceMappingURL=DashboardCtrl.js.map