//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AccountListCtrl = (function (_super) {
    __extends(AccountListCtrl, _super);
    function AccountListCtrl($scope, AccountResource) {
        var _this = this;
        _super.call(this, $scope, AccountResource);
        this.AccountResource = AccountResource;
        this.localResource = AccountResource;
        this.localScope = $scope;
        this.init();
        $scope.showForm = false;
        $scope.showCreateForm = function (value) {
            $scope.showForm = value;
        };
        $scope.addItem = function (item) {
            AccountResource.save(item).$promise.then(function (data) {
                toastr.success("Saved successfully", "Information");
                _this.$scope.$state.reload();
            }, function (response) {
                toastr.error("Error occured while trying to save the item. please try again", "Error!");
            });
        };
    }
    AccountListCtrl.prototype.downloadAsExcel = function () {
        location.href = "/api/Account/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    };
    AccountListCtrl.$inject = ["$scope", "AccountResource"];
    return AccountListCtrl;
}(RootListCtrl));
angular.module("app").controller("AccountListCtrl", AccountListCtrl);
var AccountCreateCtrl = (function (_super) {
    __extends(AccountCreateCtrl, _super);
    function AccountCreateCtrl($scope, AccountResource) {
        _super.call(this, $scope, AccountResource);
        $scope.title = "Create Account";
    }
    AccountCreateCtrl.$inject = ["$scope", "AccountResource"];
    return AccountCreateCtrl;
}(BaseCreateCtrl));
angular.module("app").controller("AccountCreateCtrl", AccountCreateCtrl);
var AccountFormCtrl = (function () {
    function AccountFormCtrl($scope) {
        this.$scope = $scope;
        $scope.backLink.state = "account.list";
        $scope.backLink.forwardLink = "account.list";
        this.localItem = this.$scope.item;
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                },
                number: {
                    required: true
                }
            },
            messages: {}
        };
    }
    AccountFormCtrl.$inject = ["$scope"];
    return AccountFormCtrl;
}());
angular.module("app").controller("AccountFormCtrl", AccountFormCtrl);
var AccountEditCtrl = (function (_super) {
    __extends(AccountEditCtrl, _super);
    function AccountEditCtrl($scope, AccountResource) {
        _super.call(this, $scope, AccountResource);
        $scope.title = "Edit Account";
    }
    AccountEditCtrl.$inject = ["$scope", "AccountResource"];
    return AccountEditCtrl;
}(BaseEditCtrl));
angular.module("app").controller("AccountEditCtrl", AccountEditCtrl);
//# sourceMappingURL=AccountCtrl.js.map