//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var PaymentTransactionListCtrl = (function (_super) {
    __extends(PaymentTransactionListCtrl, _super);
    function PaymentTransactionListCtrl($scope, PaymentTransactionResource) {
        _super.call(this, $scope, PaymentTransactionResource);
        this.PaymentTransactionResource = PaymentTransactionResource;
        this.localResource = PaymentTransactionResource;
        this.localScope = $scope;
        this.init();
        $scope.enableDownload = true;
        $scope.title = "Payments Transactions";
    }
    PaymentTransactionListCtrl.prototype.downloadAsExcel = function () {
        location.href = "/api/PaymentTransaction/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    };
    PaymentTransactionListCtrl.$inject = ["$scope", "PaymentTransactionResource"];
    return PaymentTransactionListCtrl;
}(RootListCtrl));
angular.module("app").controller("PaymentTransactionListCtrl", PaymentTransactionListCtrl);
//# sourceMappingURL=PaymentTransactionCtrl.js.map