﻿//******************************* List Controller *******************************

interface IPaymentCategoryListCtrlScope extends IRootListCtrlScope {

    items: any[];
    showCreateForm: (value: any) => void;
    showForm;
    addItem: (item) => void;
}


class PaymentCategoryListCtrl extends RootListCtrl {
    static $inject: string[] = ["$scope", "PaymentCategoryResource"];

    localScope: IPaymentCategoryListCtrlScope;
    localResource: IPaymentCategoryResource;
    constructor($scope: IPaymentCategoryListCtrlScope, private PaymentCategoryResource: IPaymentCategoryResource) {
        super($scope, PaymentCategoryResource);
        this.localResource = PaymentCategoryResource;
        this.localScope = $scope;
        
        this.init();
        $scope.showForm = false;
        $scope.showCreateForm = (value) => {
            $scope.showForm = value;
        }
        $scope.addItem = (item) => {
            PaymentCategoryResource.save(item).$promise.then((data) => {
                toastr.success("Saved successfully", "Information");
                this.$scope.$state.reload();
            }, (response) => {
                toastr.error("Error occured while trying to save the item. please try again", "Error!");
            });
        }
    }

     
    downloadAsExcel() {
        location.href = "/api/PaymentCategory/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    }

}
angular.module("app").controller("PaymentCategoryListCtrl", PaymentCategoryListCtrl);

//******************************* End List Controller *******************************


interface IPaymentCategoryCreateCtrlScope extends IRootCreateEditCtrlScope {
    title: string;
}

interface IPaymentCategoryCreateCtrl {

}

class PaymentCategoryCreateCtrl extends BaseCreateCtrl {
    static $inject: string[] = ["$scope", "PaymentCategoryResource"];

    constructor($scope: IPaymentCategoryCreateCtrlScope,
        PaymentCategoryResource: IPaymentCategoryResource) {
        super($scope, PaymentCategoryResource);
        $scope.title = "Create Payment Category";
    }

}

angular.module("app").controller("PaymentCategoryCreateCtrl", PaymentCategoryCreateCtrl);

interface IPaymentCategoryFormCtrlScope extends IRootCreateEditCtrlScope {
    backLink: {
        state: string;
        forwardLink:string;
        config?: {};
    };
    states;
    dynamicPopover
}


class PaymentCategoryFormCtrl {
    static $inject: string[] = ["$scope"];
    localItem: IPaymentCategory;
    constructor(private $scope: IPaymentCategoryFormCtrlScope) {
        $scope.backLink.state = "paymentcategory.list"; 
        $scope.backLink.forwardLink = "paymentcategory.list"; 
        this.localItem = <IPaymentCategory>this.$scope.item;
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                },
                number: {
                    required: true
                }
            },
            messages: {

            }
        };
    }
}

angular.module("app").controller("PaymentCategoryFormCtrl", PaymentCategoryFormCtrl);

interface IPaymentCategoryEditCtrlScope extends IRootCreateEditCtrlScope {

}

interface IPaymentCategoryEditCtrl {

}

class PaymentCategoryEditCtrl extends BaseEditCtrl {
    static $inject: string[] = ["$scope", "PaymentCategoryResource"];

    constructor($scope: IPaymentCategoryEditCtrlScope,
        PaymentCategoryResource: IPaymentCategoryResource) {
        super($scope, PaymentCategoryResource);
        $scope.title = "Edit Payment Category";
    }

}

angular.module("app").controller("PaymentCategoryEditCtrl", PaymentCategoryEditCtrl);