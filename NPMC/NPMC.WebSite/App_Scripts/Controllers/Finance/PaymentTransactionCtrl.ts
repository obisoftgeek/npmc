﻿//******************************* List Controller *******************************

interface IPaymentTransactionListCtrlScope extends IRootListCtrlScope {

    items: any[]; 
}


class PaymentTransactionListCtrl extends RootListCtrl {
    static $inject: string[] = ["$scope", "PaymentTransactionResource"];

    localScope: IPaymentTransactionListCtrlScope;
    localResource: IPaymentTransactionResource;
    constructor($scope: IPaymentTransactionListCtrlScope, private PaymentTransactionResource: IPaymentTransactionResource) {
        super($scope, PaymentTransactionResource);
        this.localResource = PaymentTransactionResource;
        this.localScope = $scope; 
        this.init();
        $scope.enableDownload = true;
        $scope.title = "Payments Transactions";
    }

     
    downloadAsExcel() {
        location.href = "/api/PaymentTransaction/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    }

}
angular.module("app").controller("PaymentTransactionListCtrl", PaymentTransactionListCtrl); 