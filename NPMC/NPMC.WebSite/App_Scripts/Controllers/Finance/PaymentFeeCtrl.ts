﻿//******************************* List Controller *******************************

interface IPaymentFeeListCtrlScope extends IRootListCtrlScope {

    items: any[];
    showCreateForm: (value: any) => void;
    showForm;
    addItem: (item) => void;
}


class PaymentFeeListCtrl extends RootListCtrl {
    static $inject: string[] = ["$scope", "PaymentFeeResource"];

    localScope: IPaymentFeeListCtrlScope;
    localResource: IPaymentFeeResource;
    constructor($scope: IPaymentFeeListCtrlScope, private PaymentFeeResource: IPaymentFeeResource) {
        super($scope, PaymentFeeResource);
        this.localResource = PaymentFeeResource;
        this.localScope = $scope;
        $scope.backLink.state = "paymentcategory.list";
        this.init();
        $scope.showForm = false;
        $scope.showCreateForm = (value) => {
            $scope.showForm = value;
        }
        $scope.addItem = (item) => {
            PaymentFeeResource.save(item).$promise.then((data) => {
                toastr.success("Saved successfully", "Information");
                this.$scope.$state.reload();
            }, (response) => {
                toastr.error("Error occured while trying to save the item. please try again", "Error!");
            });
        }
    }

     
    downloadAsExcel() {
        location.href = "/api/PaymentFee/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    }

}
angular.module("app").controller("PaymentFeeListCtrl", PaymentFeeListCtrl);

//******************************* End List Controller *******************************


interface IPaymentFeeCreateCtrlScope extends IRootCreateEditCtrlScope {
    title: string;
}

interface IPaymentFeeCreateCtrl {

}

class PaymentFeeCreateCtrl extends BaseCreateCtrl {
    static $inject: string[] = ["$scope", "PaymentFeeResource"];

    constructor($scope: IPaymentFeeCreateCtrlScope,
        PaymentFeeResource: IPaymentFeeResource) {
        super($scope, PaymentFeeResource);
        $scope.title = "Create Payment Fee";
        console.log($scope.$stateParams)
    }

}

angular.module("app").controller("PaymentFeeCreateCtrl", PaymentFeeCreateCtrl);

interface IPaymentFeeFormCtrlScope extends IRootCreateEditCtrlScope {
    backLink: {
        state: string;
        forwardLink:string;
        config?: {};
    };
    states;
    dynamicPopover;
    accounts;
}


class PaymentFeeFormCtrl {
    static $inject: string[] = ["$scope","AccountResource"];
    localItem: IPaymentFee;
    constructor(private $scope: IPaymentFeeFormCtrlScope, private accountResource:IAccountResource) {
        $scope.backLink.state = "paymentfee.list"; 
        $scope.backLink.forwardLink = "paymentfee.list"; 
        this.localItem = <IPaymentFee>this.$scope.item;
        $scope.accounts = accountResource.nameAndValues();
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                },
                number: {
                    required: true
                }
            },
            messages: {

            }
        };
    }
}

angular.module("app").controller("PaymentFeeFormCtrl", PaymentFeeFormCtrl);

interface IPaymentFeeEditCtrlScope extends IRootCreateEditCtrlScope {

}

interface IPaymentFeeEditCtrl {

}

class PaymentFeeEditCtrl extends BaseEditCtrl {
    static $inject: string[] = ["$scope", "PaymentFeeResource"];

    constructor($scope: IPaymentFeeEditCtrlScope,
        PaymentFeeResource: IPaymentFeeResource) {
        super($scope, PaymentFeeResource);
        $scope.title = "Edit Payment Fee";
    }

}

angular.module("app").controller("PaymentFeeEditCtrl", PaymentFeeEditCtrl);