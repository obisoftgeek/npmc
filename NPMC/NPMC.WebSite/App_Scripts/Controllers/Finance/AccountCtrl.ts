﻿//******************************* List Controller *******************************

interface IAccountListCtrlScope extends IRootListCtrlScope {

    items: any[];
    showCreateForm: (value: any) => void;
    showForm;
    addItem: (item) => void;
}


class AccountListCtrl extends RootListCtrl {
    static $inject: string[] = ["$scope", "AccountResource"];

    localScope: IAccountListCtrlScope;
    localResource: IAccountResource;
    constructor($scope: IAccountListCtrlScope, private AccountResource: IAccountResource) {
        super($scope, AccountResource);
        this.localResource = AccountResource;
        this.localScope = $scope;
        
        this.init();
        $scope.showForm = false;
        $scope.showCreateForm = (value) => {
            $scope.showForm = value;
        }
        $scope.addItem = (item) => {
            AccountResource.save(item).$promise.then((data) => {
                toastr.success("Saved successfully", "Information");
                this.$scope.$state.reload();
            }, (response) => {
                toastr.error("Error occured while trying to save the item. please try again", "Error!");
            });
        }
    }

     
    downloadAsExcel() {
        location.href = "/api/Account/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    }

}
angular.module("app").controller("AccountListCtrl", AccountListCtrl);

//******************************* End List Controller *******************************


interface IAccountCreateCtrlScope extends IRootCreateEditCtrlScope {
    title: string;
}

interface IAccountCreateCtrl {

}

class AccountCreateCtrl extends BaseCreateCtrl {
    static $inject: string[] = ["$scope", "AccountResource"];

    constructor($scope: IAccountCreateCtrlScope,
        AccountResource: IAccountResource) {
        super($scope, AccountResource);
        $scope.title = "Create Account";
    }

}

angular.module("app").controller("AccountCreateCtrl", AccountCreateCtrl);

interface IAccountFormCtrlScope extends IRootCreateEditCtrlScope {
    backLink: {
        state: string;
        forwardLink:string;
        config?: {};
    };
    states;
    dynamicPopover
}


class AccountFormCtrl {
    static $inject: string[] = ["$scope"];
    localItem: IAccount;
    constructor(private $scope: IAccountFormCtrlScope) {
        $scope.backLink.state = "account.list"; 
        $scope.backLink.forwardLink = "account.list"; 
        this.localItem = <IAccount>this.$scope.item;
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                },
                number: {
                    required: true
                }
            },
            messages: {

            }
        };
    }
}

angular.module("app").controller("AccountFormCtrl", AccountFormCtrl);

interface IAccountEditCtrlScope extends IRootCreateEditCtrlScope {

}

interface IAccountEditCtrl {

}

class AccountEditCtrl extends BaseEditCtrl {
    static $inject: string[] = ["$scope", "AccountResource"];

    constructor($scope: IAccountEditCtrlScope,
        AccountResource: IAccountResource) {
        super($scope, AccountResource);
        $scope.title = "Edit Account";
    }

}

angular.module("app").controller("AccountEditCtrl", AccountEditCtrl);