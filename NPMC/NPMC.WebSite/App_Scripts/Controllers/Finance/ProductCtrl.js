//******************************* List Controller *******************************
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var ProductListCtrl = (function (_super) {
    __extends(ProductListCtrl, _super);
    function ProductListCtrl($scope, ProductResource) {
        var _this = _super.call(this, $scope, ProductResource) || this;
        _this.ProductResource = ProductResource;
        _this.localResource = ProductResource;
        _this.localScope = $scope;
        $scope.backLink.state = "product.list";
        _this.init();
        $scope.showForm = false;
        $scope.showCreateForm = function (value) {
            $scope.showForm = value;
        };
        $scope.addItem = function (item) {
            ProductResource.save(item).$promise.then(function (data) {
                toastr.success("Saved successfully", "Information");
                _this.$scope.$state.reload();
            }, function (response) {
                toastr.error("Error occured while trying to save the item. please try again", "Error!");
            });
        };
        return _this;
    }
    ProductListCtrl.prototype.downloadAsExcel = function () {
        location.href = "/api/Product/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    };
    return ProductListCtrl;
}(RootListCtrl));
ProductListCtrl.$inject = ["$scope", "ProductResource"];
angular.module("app").controller("ProductListCtrl", ProductListCtrl);
var ProductCreateCtrl = (function (_super) {
    __extends(ProductCreateCtrl, _super);
    function ProductCreateCtrl($scope, ProductResource) {
        var _this = _super.call(this, $scope, ProductResource) || this;
        $scope.title = "Create Payment Fee";
        console.log($scope.$stateParams);
        return _this;
    }
    return ProductCreateCtrl;
}(BaseCreateCtrl));
ProductCreateCtrl.$inject = ["$scope", "ProductResource"];
angular.module("app").controller("ProductCreateCtrl", ProductCreateCtrl);
var ProductFormCtrl = (function () {
    function ProductFormCtrl($scope, accountResource) {
        this.$scope = $scope;
        this.accountResource = accountResource;
        $scope.backLink.state = "paymentfee.list";
        $scope.backLink.forwardLink = "paymentfee.list";
        this.localItem = this.$scope.item;
        $scope.accounts = accountResource.nameAndValues();
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                },
                number: {
                    required: true
                }
            },
            messages: {}
        };
    }
    return ProductFormCtrl;
}());
ProductFormCtrl.$inject = ["$scope", "AccountResource"];
angular.module("app").controller("ProductFormCtrl", ProductFormCtrl);
var ProductEditCtrl = (function (_super) {
    __extends(ProductEditCtrl, _super);
    function ProductEditCtrl($scope, ProductResource) {
        var _this = _super.call(this, $scope, ProductResource) || this;
        $scope.title = "Edit Payment Fee";
        return _this;
    }
    return ProductEditCtrl;
}(BaseEditCtrl));
ProductEditCtrl.$inject = ["$scope", "ProductResource"];
angular.module("app").controller("ProductEditCtrl", ProductEditCtrl);
//# sourceMappingURL=ProductCtrl.js.map