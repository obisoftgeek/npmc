//******************************* List Controller *******************************
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var PaymentCategoryListCtrl = (function (_super) {
    __extends(PaymentCategoryListCtrl, _super);
    function PaymentCategoryListCtrl($scope, PaymentCategoryResource) {
        var _this = this;
        _super.call(this, $scope, PaymentCategoryResource);
        this.PaymentCategoryResource = PaymentCategoryResource;
        this.localResource = PaymentCategoryResource;
        this.localScope = $scope;
        this.init();
        $scope.showForm = false;
        $scope.showCreateForm = function (value) {
            $scope.showForm = value;
        };
        $scope.addItem = function (item) {
            PaymentCategoryResource.save(item).$promise.then(function (data) {
                toastr.success("Saved successfully", "Information");
                _this.$scope.$state.reload();
            }, function (response) {
                toastr.error("Error occured while trying to save the item. please try again", "Error!");
            });
        };
    }
    PaymentCategoryListCtrl.prototype.downloadAsExcel = function () {
        location.href = "/api/PaymentCategory/DownloadReport?whereCondition=" + JSON.stringify(this.$scope.filter);
    };
    PaymentCategoryListCtrl.$inject = ["$scope", "PaymentCategoryResource"];
    return PaymentCategoryListCtrl;
}(RootListCtrl));
angular.module("app").controller("PaymentCategoryListCtrl", PaymentCategoryListCtrl);
var PaymentCategoryCreateCtrl = (function (_super) {
    __extends(PaymentCategoryCreateCtrl, _super);
    function PaymentCategoryCreateCtrl($scope, PaymentCategoryResource) {
        _super.call(this, $scope, PaymentCategoryResource);
        $scope.title = "Create Payment Category";
    }
    PaymentCategoryCreateCtrl.$inject = ["$scope", "PaymentCategoryResource"];
    return PaymentCategoryCreateCtrl;
}(BaseCreateCtrl));
angular.module("app").controller("PaymentCategoryCreateCtrl", PaymentCategoryCreateCtrl);
var PaymentCategoryFormCtrl = (function () {
    function PaymentCategoryFormCtrl($scope) {
        this.$scope = $scope;
        $scope.backLink.state = "paymentcategory.list";
        $scope.backLink.forwardLink = "paymentcategory.list";
        this.localItem = this.$scope.item;
        $scope.validationRule = {
            rules: {
                name: {
                    required: true
                },
                number: {
                    required: true
                }
            },
            messages: {}
        };
    }
    PaymentCategoryFormCtrl.$inject = ["$scope"];
    return PaymentCategoryFormCtrl;
}());
angular.module("app").controller("PaymentCategoryFormCtrl", PaymentCategoryFormCtrl);
var PaymentCategoryEditCtrl = (function (_super) {
    __extends(PaymentCategoryEditCtrl, _super);
    function PaymentCategoryEditCtrl($scope, PaymentCategoryResource) {
        _super.call(this, $scope, PaymentCategoryResource);
        $scope.title = "Edit Payment Category";
    }
    PaymentCategoryEditCtrl.$inject = ["$scope", "PaymentCategoryResource"];
    return PaymentCategoryEditCtrl;
}(BaseEditCtrl));
angular.module("app").controller("PaymentCategoryEditCtrl", PaymentCategoryEditCtrl);
//# sourceMappingURL=PaymentCategoryCtrl.js.map