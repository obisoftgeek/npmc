﻿


interface IFinanceDashboardCtrlScope extends IRootScope {
   
    item;
    
}


class FinanceDashboardCtrl {
    static $inject: string[] = ["$scope"];

    constructor(private $scope: IFinanceDashboardCtrlScope) {

        $scope.formTitle = "Dashboard";
      
       
    }
   

}
angular.module("app").controller("FinanceDashboardCtrl", FinanceDashboardCtrl);;