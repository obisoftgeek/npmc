﻿interface IFileUploadSvc {
    startUpload;
    uploadItem;
}

class FileUploadSvc implements IFileUploadSvc {
    static $inject: string[] = [
        "Upload"
    ];

    constructor(private Upload) {

    }

    startUpload(files: any, successCallback: any, onFailureCallback: any, onProgessCallback?: any) {
        this.uploadItem(files, successCallback, onFailureCallback, onProgessCallback);
    }

    uploadItem = (file, onSuccess, onFailure, onProgress) => {
        this.Upload.upload({
            data: { file: file },
            url: "/api/FileManager/uploadFiles"
        }).then(
            (resp) => {
                onSuccess(resp.data.join(","));
            },
            onFailure,
            (percent) => {
                var progressPercentage = 100.0 * percent.loaded / percent.total;
                onProgress(progressPercentage);
            }
        );
    };
}

angular.module("app").service("FileUploadSvc", FileUploadSvc);