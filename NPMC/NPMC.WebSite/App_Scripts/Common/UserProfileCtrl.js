var UserProfileCtrl = (function () {
    function UserProfileCtrl($scope, authService, userProfileResource) {
        var _this = this;
        this.$scope = $scope;
        this.authService = authService;
        this.userProfileResource = userProfileResource;
        $scope.currentUserProfile = this.userProfileResource.getCurrentUserProfile();
        $scope.logout = function () { return _this.logout(); };
    }
    UserProfileCtrl.prototype.logout = function () {
        this.authService.logout();
    };
    UserProfileCtrl.$inject = ["$scope", "AuthService", "UserProfileResource"];
    return UserProfileCtrl;
}());
angular.module("app").controller("UserProfileCtrl", UserProfileCtrl);
;
//# sourceMappingURL=UserProfileCtrl.js.map