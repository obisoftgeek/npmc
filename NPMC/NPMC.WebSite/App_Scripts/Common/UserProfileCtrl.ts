﻿

 
interface IUserProfileCtrlScope extends IRootScope {
  
    logout: any;
    currentUserProfile:any;  
}

class UserProfileCtrl {
    static $inject: string[] = ["$scope", "AuthService","UserProfileResource"];

    constructor(private $scope: IUserProfileCtrlScope, private authService: IAuthService,
                         private userProfileResource: IUserProfileResource) {

        $scope.currentUserProfile = this.userProfileResource.getCurrentUserProfile();
        $scope.logout = () => this.logout();
       
    }
    logout() {
        this.authService.logout();
    }
}
angular.module("app").controller("UserProfileCtrl", UserProfileCtrl);;