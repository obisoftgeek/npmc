var app = angular.module("app", [
    "ui.router",
    "ui.bootstrap",
    "ngSanitize",
    'ui.router.stateHelper',
    'ngResource',
    'ngAnimate',
    'angularUtils.directives.uiBreadcrumbs',
    'kendo.directives',
    'ui.tinymce',
    'ngMaterial',
    'dualmultiselect',
    'ui.select',
    'ngFileUpload'
]);
//# sourceMappingURL=app.js.map