﻿

interface IAuthService {

    authenticateUser(loginModel, successCallBack: Function, failureCallBack: Function);
   // authenticateUser(loginModel:any);
    continueLogin(): any;
    logout(): any;
}

class AuthService implements IAuthService {
    static $inject: string[] = ["$http"];

    constructor(private $http: ng.IHttpService) {

    }
    authenticateUser(loginModel, successCallBack: Function, failureCallBack: Function) {
        loginModel.grant_type = "password";
        $.ajax({
            url: '/token',
            type: "POST",
            data: loginModel
        }).done((data) => {
            $.cookie("token", data.access_token, { path: '/' });
            successCallBack(data);
        }).fail((response) => {
            failureCallBack(response);
        });
    }
    //authenticateUser(loginModel:any) {
    //    loginModel.grant_type = "password";
    //   return $.ajax({
    //        url: '/token',
    //        type: "POST",
    //        data: loginModel
    //    });
    //}
    continueLogin() {    
   
       window.location.href = '/Site/Index';
    }
    logout() {

        $.removeCookie('token', { path: '/' });
        window.location.href = '/Site/Logout';
    }
}

angular.module("app").service("AuthService", AuthService);
