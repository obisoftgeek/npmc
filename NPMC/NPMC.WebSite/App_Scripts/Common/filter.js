filter.$inject = ["$window"];
function filter($window) {
    return {
        restrict: "EA",
        link: link,
        templateUrl: "/app_scripts/partials/filter.html"
    };
    function link(scope, element, attrs) {
        scope.filterTemplate = attrs.filter;
    }
}
angular.module("app").directive("filter", filter);
//# sourceMappingURL=filter.js.map