var FileUploadSvc = (function () {
    function FileUploadSvc(Upload) {
        var _this = this;
        this.Upload = Upload;
        this.uploadItem = function (file, onSuccess, onFailure, onProgress) {
            _this.Upload.upload({
                data: { file: file },
                url: "/api/FileManager/uploadFiles"
            }).then(function (resp) {
                onSuccess(resp.data.join(","));
            }, onFailure, function (percent) {
                var progressPercentage = 100.0 * percent.loaded / percent.total;
                onProgress(progressPercentage);
            });
        };
    }
    FileUploadSvc.prototype.startUpload = function (files, successCallback, onFailureCallback, onProgessCallback) {
        this.uploadItem(files, successCallback, onFailureCallback, onProgessCallback);
    };
    FileUploadSvc.$inject = [
        "Upload"
    ];
    return FileUploadSvc;
}());
angular.module("app").service("FileUploadSvc", FileUploadSvc);
//# sourceMappingURL=FileUploadSvc.js.map