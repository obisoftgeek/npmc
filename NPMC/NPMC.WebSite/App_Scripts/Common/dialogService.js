(function () {
    'use strict';
    angular.module('app').factory('DialogService', ['$q', DialogService]);
    function DialogService($q) {
        function showConfirm(title, message) {
            var deferred = $q.defer();
            var html = '<div id="myDialogWindow"> ' +
                ' <div style="text-align: center; width:100%"> ' +
                '   <div style="margin:10px 0 15px 0">' + message + '</div> ' +
                '   <button class="btn btn-success" id="yesButton"><i class="fa fa-ok"></i> Yes</button> ' +
                '   <button class="btn btn-danger" id="noButton""><i class="fa fa-remove"></i> No</button> ' +
                '   </div> ' +
                '</div> ';
            $('body').append(html);
            var windowDiv = $('#myDialogWindow');
            windowDiv.kendoWindow({
                width: "350px",
                title: title,
                modal: true,
                visible: false
            });
            var dialog = windowDiv.data("kendoWindow");
            $('#yesButton').click(function (e) {
                dialog.close();
                $('#myDialogWindow').remove();
                deferred.resolve();
            });
            $('#noButton').click(function (e) {
                dialog.close();
                $('#myDialogWindow').remove();
                deferred.reject();
            });
            dialog.center();
            dialog.open();
            return deferred.promise;
        }
        var service = {
            showConfirm: showConfirm
        };
        return service;
    }
})();
//# sourceMappingURL=dialogService.js.map