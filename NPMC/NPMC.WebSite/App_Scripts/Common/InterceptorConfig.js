app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
        $stateProvider
            .state('error500', {
            url: '/error500',
            views: {
                "main@": { templateUrl: '/Site/Error500' }
            }
        }).state('error404', {
            url: '/error404',
            views: {
                "main@": { templateUrl: '/Site/Error404' }
            }
        });
        $urlRouterProvider.rule(function ($injector, $location) {
            var path = $location.path(), normalized = path.toLowerCase();
            if (path != normalized) {
                $location.replace().path(normalized);
            }
        });
        // $locationProvider.html5Mode(true);
        $httpProvider.interceptors.push('ResponseInterceptorSvc');
        // $httpProvider.interceptors.push('AuthInterceptorSvc');
    }]);
//# sourceMappingURL=InterceptorConfig.js.map