interface IRootObject extends ng.resource.IResource<IRootObject> {
    id: number;
    $update(config?): IRootObject;
    selected: boolean;
    $count(config?): any;
}

interface IRootResource<T extends IRootObject> extends ng.resource.IResourceClass<T> {
    update(config?): IRootObject;
    count: any;
    deleteAll: any;
    toggleActive(config?): any;
    details(params): any;
    approve(p: any): any;
    getApprovals(p: { stateId }): any;
    reject(approval): any;
    makePayment;
    downloadExcelSheet: any;
    downloadExcel(config?);
    getDetails:any;
}

interface IRootScope extends ng.IScope {
    formTitle: any;
    $state;
    $stateParams;
    errorHandleSvc: any;
    validationSvc: any;
    waiting: any;
    wait: any;
    item: any;
    notificationSvc: INotificationService;
    dialogSvc: any;
    pageSize: any;
    isEditMode: boolean;
    memoType:any;
    errorHandle;
    months: any[];
    days;
    getOrdinalFor;
    getMonthText;
    tinymceOptions;
}

interface ICreateEditCtrlScope extends IRootScope {
   
    submitForm(data?: any); 
    submitAsDraft(data?: any);
    submitAsForward():any;
    demoOptions: any;
}

class BaseCreateEditCtrl {

    constructor(public $scope: ICreateEditCtrlScope, public resource: IRootResource<IRootObject>) {

        $scope.demoOptions = {
            title: 'Select recipient',
            filterPlaceHolder: 'Start typing to filter the lists below.',
            labelAll: 'All',
            labelSelected: 'Selected',
            helpMessage: 'click recipient to transfer them between fields.',
            /* angular will use this to filter your lists */
            orderProperty: 'Name',
            /* this contains the initial list of all items (i.e. the left sIde) */
            items: [],
            /* this list should be initialized as empty or with any pre-selected items */
            selectedItems: []
        };

    }
} 

interface IListCtrlScope extends IRootScope {

    items: IRootObject[];
    filter: any;
    page: number;
    itemsPerPage: number;
    selectedIdexes: number[];
    setupPagination();
    deleteItem(item: IRootObject);
    getIndexSeed(index: number);
    getTotalPages();
    getPageInfoDescription();
    loadNextContent(page, count, jq);
    filterData(); 
    reloadPage();
    pager;
    paginationConfig: { count: number; page: number; total: number };
    pageChanged: () => void;
    order: { column?; direction?};
    orderExpressionChanged: () => void;
    orderByExpression: {}
    whereCondition: string;
    downloadAsExcel;
    opened;
    openDate;
}



class BaseListCtrl {


    constructor(public $scope: IListCtrlScope, public rootResource: IRootResource<IRootObject>, public config?) {

        this.$scope.waiting = 0;
        this.$scope.items = [];
        this.$scope.orderByExpression = { column: 1, direction: 1 };
        this.$scope.filter = $.extend({}, config);
        $scope.downloadAsExcel = () => this.downloadAsExcel();

    }
    toggleActive(item) { 
    }
    resetFiltering() {
        this.$scope.filter = {};
        this.setupPagination();
    }
    init() { 
        this.initScope();
        this.setupPagination();
        this.bindScopeMethods();
    }
    downloadAsExcel() {

    }
    initScope() {

        this.$scope.openDate = ($event, property: string) => {
            $event.preventDefault();
            $event.stopPropagation();
            this.$scope.opened[property] = true;
        }; 
        this.$scope.opened = {};
        this.$scope.waiting = 0;
        this.$scope.page = 1;
        this.$scope.paginationConfig = {
            count: 10,
            page: 1,
            total: 0
        }; 
        this.$scope.pager = { page: 1 };

        this.$scope.setupPagination = () => this.setupPagination();
        this.$scope.getTotalPages = () => this.getTotalPages();
        this.$scope.getPageInfoDescription = () => this.getPageInfoDescription(); 
        this.$scope.getPageInfoDescription = () => this.getPageInfoDescription();
         
        this.$scope.orderExpressionChanged = () => this.orderExpressionChanged();
    }
    changeStateForAll() {
        this.$scope.items.forEach((item: any) => {
            item.checked = this.$scope.item.checkAll;
        });
    }
     
    getIndexSeed($index: number) {
        return (this.$scope.pager.page - 1) * this.$scope.paginationConfig.count + $index + 1;
    }

    getPageInfoDescription() {
        if (this.$scope.items) {
            return "Showing " + (this.$scope.paginationConfig.count * (this.$scope.pager.page - 1) + 1) + " to " +
                (this.$scope.paginationConfig.count * (this.$scope.pager.page - 1)
                    + this.$scope.items.length) + " of " + this.$scope.paginationConfig.total;
        }
        return "";
    }

    getTotalPages() {
        return Math.ceil(this.$scope.paginationConfig.total / this.$scope.paginationConfig.count);
    }

    bindScopeMethods() {

        this.$scope.pageChanged = () => this.pageChanged();
        this.$scope.deleteItem = (item) => this.deleteItem(item);
        this.$scope.reloadPage = () => this.reloadPage();
        this.$scope.getIndexSeed = (index) => this.getIndexSeed(index);
    }

    setupPagination() {
        this.$scope.waiting = 1;
         
        this.rootResource.count($.extend({
            size: this.$scope.paginationConfig.count,
            page: this.$scope.paginationConfig.page,
            orderByExpression: this.$scope.orderByExpression,
            whereCondition: JSON.stringify($.extend(this.$scope.filter, this.$scope.$stateParams))
        }, this.$scope.$stateParams, this.config, this.$scope.filter)).$promise.then((data) => {

            this.$scope.waiting = 0;
            this.$scope.paginationConfig.total = data.total;
            this.$scope.items = data.items; 
            //this.$scope.info = data.info;

        }, () => {
            this.$scope.waiting = 0;
        });
    }
 
    pageChanged() {

        this.$scope.waiting = 1;
        this.rootResource.query($.extend({
            size: this.$scope.paginationConfig.count,
            page: this.$scope.paginationConfig.page,
            orderByExpression: this.$scope.orderByExpression,
            whereCondition: JSON.stringify(this.$scope.filter)
        },
            this.$scope.$stateParams, this.config, this.$scope.filter)).$promise.then((data) => {
                this.$scope.waiting = 0;
                this.$scope.items = data;
                this.$scope.pager.page = this.$scope.paginationConfig.page;

            }, (data) => {
                this.$scope.waiting = 0;
            });
    }
    deleteItem(item: IRootObject) {
        this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to delete this record?").then(() => {

            this.$scope.waiting = 1;
            this.rootResource.delete(item).$promise.then(() => {
                this.$scope.waiting = 0;
                this.$scope.notificationSvc.showSuccessToast("Record was successfully deleted");
                this.setupPagination();
            }, (error) => {
                this.$scope.waiting = 0;
                this.$scope.notificationSvc.showErrorToast(error.data.message);
            });
        });
    }
    reloadPage() {
        this.$scope.$state.reload();
    }
 
    orderExpressionChanged() {
        if (this.$scope.order.column && this.$scope.order.direction) {
            this.$scope.orderByExpression = this.$scope.order.column + " " + this.$scope.order.direction;
            this.pageChanged();
        } else
            this.$scope.orderByExpression = null;
    } 
} 








interface IBaseDetailCtrlScope extends IRootScope {
    item: any;
    waiting: any;
    canApprove: any;
    reloadPage: any;
    feedback: any;

}


class BaseDetailCtrl {

    constructor(public $scope: IBaseDetailCtrlScope, public rootResource: IRootResource<IRootObject>, public config?) {


        this.$scope.reloadPage = () => this.reloadPage();
    }

    init() {



        this.$scope.waiting = 1;
        this.rootResource.getDetails({ id: this.$scope.$stateParams.id }).$promise.then((data) => {
            this.$scope.waiting = 0;
            this.$scope.item = data;
        }, (error) => {
            this.$scope.waiting = 0;
            this.$scope.notificationSvc.showErrorToast(error.data.message);
        });
    }

    reloadPage() {
        this.init();
       
    }


}
