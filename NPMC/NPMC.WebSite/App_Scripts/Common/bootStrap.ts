﻿

app.run([
    '$rootScope', '$state', '$stateParams', 'ErrorHandleSvc', 'NotificationService', 'ValidationSvc', 'DialogService',
    ($rootScope: IRootScope, $state: ng.ui.IStateService, $stateParams, errorHandleSvc,
        notificationSvc: INotificationService, validationSvc, dialogService) => {


      
        $rootScope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams) => {


        });


        $rootScope.$state = $state; // state to be accessed from view
        $rootScope.$stateParams = $stateParams;
        $rootScope.errorHandleSvc = errorHandleSvc;
        $rootScope.errorHandle = errorHandleSvc;
        $rootScope.notificationSvc = notificationSvc;
        $rootScope.validationSvc = validationSvc;
        $rootScope.dialogSvc = dialogService;
       
        

        $rootScope.pageSize = [
            { id: 10, name: 10 },
            { id: 25, name: 25 },
            { id: 50, name: 50 },
            { id: 100, name: 100 }

        ];

        $rootScope.memoType = [
            { id: 1, name: 'Retirement'},
            { id: 2, name: 'Requisition' },
            { id: 3, name: 'Query' },
            { id: 4, name: 'Procurement' },
            { id: 5, name: 'Circular' },
            { id: 6, name: 'Result' },
            { id: 7, name: 'Mail' }

        ];
        $rootScope.months = [
            { value: 1, name: "January" },
            { value: 2, name: "February" },
            { value: 3, name: "March" },
            { value: 4, name: "April" },
            { value: 5, name: "May" },
            { value: 6, name: "June" },
            { value: 7, name: "July" },
            { value: 8, name: "August" },
            { value: 9, name: "September" },
            { value: 10, name: "October" },
            { value: 11, name: "November" },
            { value: 12, name: "December" }
        ];
        $rootScope.days = [];
        for (var i = 1; i <= 31; i++) {
            $rootScope.days.push({ value: i, name: "" + i + "" });
        }
        $rootScope.getOrdinalFor = (index) => {
            var hundredRemainder = index % 100;
            var tenRemainder = index % 10;
            if (hundredRemainder - tenRemainder == 10) {
                return index + "th";
            }

            switch (tenRemainder) {
                case 1:
                    return index + "st";
                case 2:
                    return index + "nd";
                case 3:
                    return index + "rd";
                default:
                    return index + "th";
            }
        }
        $rootScope.tinymceOptions = {
            onChange: (e) => {
                // put logic here for keypress and cut/paste changes
            },
            plugins: 'textcolor',
            toolbar: "sizeselect | bold italic | fontselect |  fontsizeselect | forecolor backcolor"
        };
        $rootScope.getMonthText = (index) => {
            switch (index) {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "october";
                case 11:
                    return "November";
                case 12:
                    return "December";
            }
        };
    }
]
);