var BaseCreateEditCtrl = (function () {
    function BaseCreateEditCtrl($scope, resource) {
        this.$scope = $scope;
        this.resource = resource;
        $scope.demoOptions = {
            title: 'Select recipient',
            filterPlaceHolder: 'Start typing to filter the lists below.',
            labelAll: 'All',
            labelSelected: 'Selected',
            helpMessage: 'click recipient to transfer them between fields.',
            /* angular will use this to filter your lists */
            orderProperty: 'Name',
            /* this contains the initial list of all items (i.e. the left sIde) */
            items: [],
            /* this list should be initialized as empty or with any pre-selected items */
            selectedItems: []
        };
    }
    return BaseCreateEditCtrl;
}());
var BaseListCtrl = (function () {
    function BaseListCtrl($scope, rootResource, config) {
        var _this = this;
        this.$scope = $scope;
        this.rootResource = rootResource;
        this.config = config;
        this.$scope.waiting = 0;
        this.$scope.items = [];
        this.$scope.orderByExpression = { column: 1, direction: 1 };
        this.$scope.filter = $.extend({}, config);
        $scope.downloadAsExcel = function () { return _this.downloadAsExcel(); };
    }
    BaseListCtrl.prototype.toggleActive = function (item) {
    };
    BaseListCtrl.prototype.resetFiltering = function () {
        this.$scope.filter = {};
        this.setupPagination();
    };
    BaseListCtrl.prototype.init = function () {
        this.initScope();
        this.setupPagination();
        this.bindScopeMethods();
    };
    BaseListCtrl.prototype.downloadAsExcel = function () {
    };
    BaseListCtrl.prototype.initScope = function () {
        var _this = this;
        this.$scope.openDate = function ($event, property) {
            $event.preventDefault();
            $event.stopPropagation();
            _this.$scope.opened[property] = true;
        };
        this.$scope.opened = {};
        this.$scope.waiting = 0;
        this.$scope.page = 1;
        this.$scope.paginationConfig = {
            count: 10,
            page: 1,
            total: 0
        };
        this.$scope.pager = { page: 1 };
        this.$scope.setupPagination = function () { return _this.setupPagination(); };
        this.$scope.getTotalPages = function () { return _this.getTotalPages(); };
        this.$scope.getPageInfoDescription = function () { return _this.getPageInfoDescription(); };
        this.$scope.getPageInfoDescription = function () { return _this.getPageInfoDescription(); };
        this.$scope.orderExpressionChanged = function () { return _this.orderExpressionChanged(); };
    };
    BaseListCtrl.prototype.changeStateForAll = function () {
        var _this = this;
        this.$scope.items.forEach(function (item) {
            item.checked = _this.$scope.item.checkAll;
        });
    };
    BaseListCtrl.prototype.getIndexSeed = function ($index) {
        return (this.$scope.pager.page - 1) * this.$scope.paginationConfig.count + $index + 1;
    };
    BaseListCtrl.prototype.getPageInfoDescription = function () {
        if (this.$scope.items) {
            return "Showing " + (this.$scope.paginationConfig.count * (this.$scope.pager.page - 1) + 1) + " to " +
                (this.$scope.paginationConfig.count * (this.$scope.pager.page - 1)
                    + this.$scope.items.length) + " of " + this.$scope.paginationConfig.total;
        }
        return "";
    };
    BaseListCtrl.prototype.getTotalPages = function () {
        return Math.ceil(this.$scope.paginationConfig.total / this.$scope.paginationConfig.count);
    };
    BaseListCtrl.prototype.bindScopeMethods = function () {
        var _this = this;
        this.$scope.pageChanged = function () { return _this.pageChanged(); };
        this.$scope.deleteItem = function (item) { return _this.deleteItem(item); };
        this.$scope.reloadPage = function () { return _this.reloadPage(); };
        this.$scope.getIndexSeed = function (index) { return _this.getIndexSeed(index); };
    };
    BaseListCtrl.prototype.setupPagination = function () {
        var _this = this;
        this.$scope.waiting = 1;
        this.rootResource.count($.extend({
            size: this.$scope.paginationConfig.count,
            page: this.$scope.paginationConfig.page,
            orderByExpression: this.$scope.orderByExpression,
            whereCondition: JSON.stringify($.extend(this.$scope.filter, this.$scope.$stateParams))
        }, this.$scope.$stateParams, this.config, this.$scope.filter)).$promise.then(function (data) {
            _this.$scope.waiting = 0;
            _this.$scope.paginationConfig.total = data.total;
            _this.$scope.items = data.items;
            //this.$scope.info = data.info;
        }, function () {
            _this.$scope.waiting = 0;
        });
    };
    BaseListCtrl.prototype.pageChanged = function () {
        var _this = this;
        this.$scope.waiting = 1;
        this.rootResource.query($.extend({
            size: this.$scope.paginationConfig.count,
            page: this.$scope.paginationConfig.page,
            orderByExpression: this.$scope.orderByExpression,
            whereCondition: JSON.stringify(this.$scope.filter)
        }, this.$scope.$stateParams, this.config, this.$scope.filter)).$promise.then(function (data) {
            _this.$scope.waiting = 0;
            _this.$scope.items = data;
            _this.$scope.pager.page = _this.$scope.paginationConfig.page;
        }, function (data) {
            _this.$scope.waiting = 0;
        });
    };
    BaseListCtrl.prototype.deleteItem = function (item) {
        var _this = this;
        this.$scope.dialogSvc.showConfirm("Confirm", "Are you sure you want to delete this record?").then(function () {
            _this.$scope.waiting = 1;
            _this.rootResource.delete(item).$promise.then(function () {
                _this.$scope.waiting = 0;
                _this.$scope.notificationSvc.showSuccessToast("Record was successfully deleted");
                _this.setupPagination();
            }, function (error) {
                _this.$scope.waiting = 0;
                _this.$scope.notificationSvc.showErrorToast(error.data.message);
            });
        });
    };
    BaseListCtrl.prototype.reloadPage = function () {
        this.$scope.$state.reload();
    };
    BaseListCtrl.prototype.orderExpressionChanged = function () {
        if (this.$scope.order.column && this.$scope.order.direction) {
            this.$scope.orderByExpression = this.$scope.order.column + " " + this.$scope.order.direction;
            this.pageChanged();
        }
        else
            this.$scope.orderByExpression = null;
    };
    return BaseListCtrl;
}());
var BaseDetailCtrl = (function () {
    function BaseDetailCtrl($scope, rootResource, config) {
        var _this = this;
        this.$scope = $scope;
        this.rootResource = rootResource;
        this.config = config;
        this.$scope.reloadPage = function () { return _this.reloadPage(); };
    }
    BaseDetailCtrl.prototype.init = function () {
        var _this = this;
        this.$scope.waiting = 1;
        this.rootResource.getDetails({ id: this.$scope.$stateParams.id }).$promise.then(function (data) {
            _this.$scope.waiting = 0;
            _this.$scope.item = data;
        }, function (error) {
            _this.$scope.waiting = 0;
            _this.$scope.notificationSvc.showErrorToast(error.data.message);
        });
    };
    BaseDetailCtrl.prototype.reloadPage = function () {
        this.init();
    };
    return BaseDetailCtrl;
}());
//# sourceMappingURL=baseclasses.js.map