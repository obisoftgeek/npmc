﻿

    interface Ifilter extends ng.IDirective {
    }

    interface IfilterScope extends ng.IScope {
      
        filterTemplate
    }

    interface IfilterAttributes extends ng.IAttributes {
        filter
    }

    filter.$inject = ["$window"];
    function filter($window: ng.IWindowService): Ifilter {
        return {
            restrict: "EA",
            link: link,
            templateUrl:"/app_scripts/partials/filter.html"
        };
        function link(scope: IfilterScope, element: ng.IAugmentedJQuery,
            attrs: IfilterAttributes) {
            scope.filterTemplate = attrs.filter;
        }
    }

    angular.module("app").directive("filter", filter);


