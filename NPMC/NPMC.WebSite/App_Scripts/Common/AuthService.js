var AuthService = (function () {
    function AuthService($http) {
        this.$http = $http;
    }
    AuthService.prototype.authenticateUser = function (loginModel, successCallBack, failureCallBack) {
        loginModel.grant_type = "password";
        $.ajax({
            url: '/token',
            type: "POST",
            data: loginModel
        }).done(function (data) {
            $.cookie("token", data.access_token, { path: '/' });
            successCallBack(data);
        }).fail(function (response) {
            failureCallBack(response);
        });
    };
    //authenticateUser(loginModel:any) {
    //    loginModel.grant_type = "password";
    //   return $.ajax({
    //        url: '/token',
    //        type: "POST",
    //        data: loginModel
    //    });
    //}
    AuthService.prototype.continueLogin = function () {
        window.location.href = '/Site/Index';
    };
    AuthService.prototype.logout = function () {
        $.removeCookie('token', { path: '/' });
        window.location.href = '/Site/Logout';
    };
    AuthService.$inject = ["$http"];
    return AuthService;
}());
angular.module("app").service("AuthService", AuthService);
//# sourceMappingURL=AuthService.js.map