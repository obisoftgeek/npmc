app.config([
    '$stateProvider', function ($stateProvider) {
        var viewPath = '/Account/Home';
        $stateProvider
            .state('user', {
            abstract: true,
            url: '/user',
            data: {
                breadcrumbProxy: 'user.myprofile',
                pageTitle: "My Profile"
            }
        }).state('user.myprofile', {
            url: '/myprofile',
            views: {
                "main@": {
                    templateUrl: viewPath + '/MyProfile',
                    controller: "UserProfileCtrl"
                }
            },
            data: { displayName: 'My Profile' }
        });
    }
]);
//# sourceMappingURL=UserProfileRoute.js.map