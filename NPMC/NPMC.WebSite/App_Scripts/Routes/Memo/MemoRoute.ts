﻿


app.config(['$stateProvider', ($stateProvider) => {
    var viewPath = '/Memo/MemoBox';
    $stateProvider
        .state('memo', {
            abstract: true,
            url: '/memo',
            data: {
                breadcrumbProxy: 'memo.list',
                pageTitle: "Manage Memo"                          
            }

        }).state('memo.list', {
            url: '/list',
            views: { "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "MemoListCtrl"
                }
            },         
            data: { displayName: 'List' }

        }).state('memo.create', {
            url: '/create',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "MemoCreateCtrl"
                }
            },
            data: { displayName: 'Create' }

        }).state('memo.edit', {
            url: '/edit/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "MemoEditCtrl"
                }
            },
            data: { displayName: 'Edit' }

        }).state('memo.details', {
            url: '/details/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Details',
                    controller: "MemoDetailCtrl"
                }
            },
            data: { displayName: 'Details' }

        }).state('memo.forward', {
            url: '/forward/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Forward',
                    controller: "MemoForwardCtrl"
                }
            },
            data: { displayName: 'Forward' }

        });
}]);




