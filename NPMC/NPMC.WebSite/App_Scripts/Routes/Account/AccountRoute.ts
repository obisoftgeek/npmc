﻿



app.config(['$stateProvider', ($stateProvider) => {

    var viewPath = '/Account/Home';

                       $stateProvider
                           .state('account', {
                               abstract: true,
                               url: '/account',
                               data: {
                                   breadcrumbProxy: 'account.login'
                               }
                           }).state('account.login', {
                               url: '/login',
                               views: {
                                   "main@": {
                                       templateUrl: viewPath + '/Login',
                                       controller: "LoginCtrl"
                                   }
                               }
                           }).state('account.forgotpassword', {
                               url: '/forgotpassword',
                               views: {
                                   "main@": {
                                       templateUrl: viewPath + '/ForgotPassword',
                                       controller: "ForgotPasswordCtrl"
                                   }
                               }

                           }).state('account.forgotpasswordcompleted', {
                               url: '/forgotpasswordcompleted',
                               views: {
                                   "main@": {
                                       templateUrl: viewPath + '/ForgotPasswordCompleted',
                                       controller: ""
                                   }
                               }

                           }).state('account.candidatesignup', {
                               url: '/candidatesignup',
                               views: {
                                   "main@": {
                                       templateUrl: viewPath + '/CandidateSignup',
                                       controller: "SignupCtrl"
                                   }
                               }

                           }).state('account.signupcompleted', {
                               url: '/signupcompleted',
                               views: {
                                   "main@": {
                                       templateUrl: viewPath + '/SignupCompleted',
                                       controller: ""
                                   }
                               }

                           }).state('account.emailconfirmation', {
                               url: '/emailconfirmation?token&userId',
                               views: {
                                   "main@": {
                                       templateUrl: viewPath + '/EmailConfirmed',
                                       controller: "EmailConfirmationCtrl"
                                   }
                               }

                           }).state('account.emailnotconfirmed', {
                               url: '/emailnotconfirmed',
                               views: {
                                   "main@": {
                                       templateUrl: viewPath + '/emailnotconfirmed',
                                       controller: ""
                                   }
                               }

                           }).state('account.changepassword', {
                               url: '/changepassword',
                               views: {
                                   "main@": {
                                       templateUrl: viewPath + '/ChangePassword',
                                       controller: "ChangePasswordCtrl"
                                   }
                               }

                           });

 }]).run(($state) => {
    $state.go('account.login');
});
