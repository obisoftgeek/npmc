﻿


app.config(['$stateProvider', ($stateProvider) => {
    var viewPath = '/Finance/PaymentCategory';
    $stateProvider
        .state('paymentcategory', {
            abstract: true,
            url: '/paymentcategory',
            data: {
                breadcrumbProxy: 'paymentcategory.list',
                pageTitle: "Payment Category"
                           
            }

        }).state('paymentcategory.list', {
            url: '/list',
            views: { "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "PaymentCategoryListCtrl"
                }
            },         
            data: { displayName: 'Payment Categories' }

        }).state('paymentcategory.create', {
            url: '/create',
            views: { "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "PaymentCategoryCreateCtrl"
                }
            },         
            data: { displayName: 'Add Payment Category' }

        }).state('paymentcategory.edit', {
            url: '/edit/:id',
            views: { "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "PaymentCategoryEditCtrl"
                }
            },         
            data: { displayName: 'Edit Payment Category' }

        });

   

}]);

