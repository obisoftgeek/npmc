app.config(['$stateProvider', function ($stateProvider) {
        var viewPath = '/Finance/Product';
        $stateProvider
            .state('product', {
            abstract: true,
            url: '/product',
            data: {
                breadcrumbProxy: 'product.list',
                pageTitle: "Producs"
            }
        }).state('product.list', {
            url: '/list',
            views: { "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "ProductListCtrl"
                }
            },
            data: { displayName: 'Products' }
        }).state('product.create', {
            url: '/create',
            views: { "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "ProductCreateCtrl"
                }
            },
            data: { displayName: 'Add Product' }
        }).state('product.edit', {
            url: '/edit/:id',
            views: { "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "ProductEditCtrl"
                }
            },
            data: { displayName: 'Edit Product' }
        });
    }]);
//# sourceMappingURL=ProductRoute.js.map