app.config(['$stateProvider', function ($stateProvider) {
        var viewPath = '/Finance/PaymentFee';
        $stateProvider
            .state('paymentfee', {
            abstract: true,
            url: '/paymentfee',
            data: {
                breadcrumbProxy: 'paymentfee.list',
                pageTitle: "Payment Fee"
            }
        }).state('paymentfee.list', {
            url: '/list/:paymentCategoryId',
            views: { "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "PaymentFeeListCtrl"
                }
            },
            data: { displayName: 'Payment Fess' }
        }).state('paymentfee.create', {
            url: '/create/:paymentCategoryId',
            views: { "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "PaymentFeeCreateCtrl"
                }
            },
            data: { displayName: 'Add Payment Fee' }
        }).state('paymentfee.edit', {
            url: '/:paymentCategoryId/edit/:id',
            views: { "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "PaymentFeeEditCtrl"
                }
            },
            data: { displayName: 'Edit Payment Fee' }
        });
    }]);
//# sourceMappingURL=PaymentFeeRoute.js.map