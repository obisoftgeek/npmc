app.config(['$stateProvider', function ($stateProvider) {
        var viewPath = '/Finance/Account';
        $stateProvider
            .state('account', {
            abstract: true,
            url: '/account',
            data: {
                breadcrumbProxy: 'account.list',
                pageTitle: "Account"
            }
        }).state('account.list', {
            url: '/list',
            views: { "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "AccountListCtrl"
                }
            },
            data: { displayName: 'Accounts' }
        }).state('account.create', {
            url: '/create',
            views: { "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "AccountCreateCtrl"
                }
            },
            data: { displayName: 'Add Account' }
        }).state('account.edit', {
            url: '/edit/:id',
            views: { "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "AccountEditCtrl"
                }
            },
            data: { displayName: 'Edit Account' }
        });
    }]);
//# sourceMappingURL=AccountRoute.js.map