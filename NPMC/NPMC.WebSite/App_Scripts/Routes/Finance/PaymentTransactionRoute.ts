﻿


app.config(['$stateProvider', ($stateProvider) => {
    var viewPath = '/Finance/PaymentTransaction';
    $stateProvider
        .state('paymenttransaction', {
            abstract: true,
            url: '/paymenttransaction',
            data: {
                breadcrumbProxy: 'paymenttransaction.list',
                pageTitle: "Payment Transactions"
            }

        }).state('paymenttransaction.list', {
            url: '/list',
            views: { "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "PaymentTransactionListCtrl"
                }
            },         
            data: { displayName: 'Payment Transactions' }

        });

   

}]);

