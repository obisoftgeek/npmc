﻿


app.config(['$stateProvider', ($stateProvider) => {
    var viewPath = '/Biomedical/Home';
    $stateProvider
        .state('home', {
            abstract: true,
            url: '/home',
            data: {
                breadcrumbProxy: 'home.dashboard',
                pageTitle: "Dashboard"
                           
            }

        }).state('home.dashboard', {
            url: '/dashboard',
            views: { "main@": {
                    templateUrl: viewPath + '/Dashboard',
                    controller: "BiomedicalDashboardCtrl"
                }
            },         
            data: { displayName: 'Dashboard' }

        });

   

}]).run(($state) => {
    $state.go('home.dashboard');
});

