﻿


app.config([
    '$stateProvider', ($stateProvider) => {

        var viewPath = '/TAM/AccreditationStatus';

        $stateProvider
            .state('accreditationstatus', {
                abstract: true,
                url: '/accreditationstatus',
                data: {
                    breadcrumbProxy: 'accreditationstatus.list',
                    pageTitle: "Accreditation Status"
                   
                }
            }).state('accreditationstatus.list', {
                url: '/list',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/List',
                        controller: "AccreditationStatusListCtrl"
                    }
                },
                data: { displayName: 'List' }

            }).state('accreditationstatus.create', {
                url: '/create',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/Form',
                        controller: "AccreditationStatusCreateCtrl"
                    }
                },
                data: { displayName: 'Create' }

            }).state('accreditationstatus.edit', {
                url: '/edit/:id',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/Form',
                        controller: "AccreditationStatusEditCtrl"
                    }
                },
                data: { displayName: 'Edit' }

            }).state('accreditationstatus.details', {
                url: '/details/:id',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/Details',
                        controller: ""
                    }
                },
                data: { displayName: 'Details' }

            });
    }
]);

