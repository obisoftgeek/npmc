﻿app.config([
    '$stateProvider', ($stateProvider) => {

        var viewPath = '/TAM/Institution';

        $stateProvider
            .state('institution', {
                abstract: true,
                url: '/institution',
                data: {
                    breadcrumbProxy: 'institution.list',
                    pageTitle: "Institution"
                }
            }).state('institution.list', {
                url: '/list',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/List',
                        controller: "InstitutionListCtrl"
                    }
                },
                data: { displayName: 'List' }

            }).state('institution.create', {
                url: '/create',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/Form',
                        controller: "InstitutionCreateCtrl"
                    }
                },
                data: { displayName: 'Create' }

            }).state('institution.edit', {
                url: '/edit/:id',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/Form',
                        controller: "InstitutionEditCtrl"
                    }
                },
                data: { displayName: 'Edit' }

            }).state('institution.details', {
                url: '/details/:id',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/Details',
                        controller: ""
                    }
                },
                data: { displayName: 'Details' }

            });
    }
]);

