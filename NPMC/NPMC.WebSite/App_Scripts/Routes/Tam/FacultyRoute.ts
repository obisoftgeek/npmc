﻿


app.config(['$stateProvider', ($stateProvider) => {
    var viewPath = '/Tam/Faculty';
    $stateProvider
        .state('faculty', {
            abstract: true,
            url: '/faculty',
            data: {
                breadcrumbProxy: 'faculty.list',
                pageTitle: "Faculties"
                           
            }

        }).state('faculty.list', {
            url: '/list',
            views: { "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "FacultyListCtrl"
                }
            },         
            data: { displayName: 'Faculties' }

        }).state('faculty.create', {
            url: '/create',
            views: { "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "FacultyCreateCtrl"
                }
            },         
            data: { displayName: 'Add Faculty' }

        }).state('faculty.edit', {
            url: '/edit/:id',
            views: { "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "FacultyEditCtrl"
                }
            },         
            data: { displayName: 'Edit Faculty' }

        });

   

}]).run(($state) => {
    $state.go('faculty.list');
});

