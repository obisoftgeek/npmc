 
﻿app.config(['$stateProvider', ($stateProvider) => {
    var viewPath = '/Tam/Home';
    $stateProvider
        .state('home', {
            abstract: true,
            url: '/home',
            data: {
                breadcrumbProxy: 'home.dashboard',
                pageTitle: "Dashboard"
                           
            }

        }).state('home.dashboard', {
            url: '/dashboard',
            views: { "main@": {
                    templateUrl: viewPath + '/Dashboard',
                    controller: "TamDashboardCtrl"
                }
            },         
            data: { displayName: 'Dashboard' }

        });

   

}]).run(($state) => {
    $state.go('home.dashboard');
});

