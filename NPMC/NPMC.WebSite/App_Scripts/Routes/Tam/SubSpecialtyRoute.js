app.config([
    '$stateProvider', function ($stateProvider) {
        var viewPath = '/TAM/SubSpecialty';
        $stateProvider
            .state('subspecialty', {
            abstract: true,
            url: '/subspecialty',
            data: {
                breadcrumbProxy: 'subspecialty.list',
                pageTitle: "Sub Specialty"
            }
        }).state('subspecialty.list', {
            url: '/list',
            views: {
                "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "SubSpecialtyListCtrl"
                }
            },
            data: { displayName: 'List' }
        }).state('subspecialty.create', {
            url: '/create',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "SubSpecialtyCreateCtrl"
                }
            },
            data: { displayName: 'Create' }
        }).state('subspecialty.edit', {
            url: '/edit/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "SubSpecialtyEditCtrl"
                }
            },
            data: { displayName: 'Edit' }
        }).state('subspecialty.details', {
            url: '/details/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Details',
                    controller: ""
                }
            },
            data: { displayName: 'Details' }
        });
    }
]);
//# sourceMappingURL=SubSpecialtyRoute.js.map