app.config([
    '$stateProvider', function ($stateProvider) {
        var viewPath = '/TAM/Workshop';
        $stateProvider
            .state('workshop', {
            abstract: true,
            url: '/workshop',
            data: {
                breadcrumbProxy: 'workshop.list',
                pageTitle: "Workshop"
            }
        }).state('workshop.list', {
            url: '/list',
            views: {
                "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "WorkshopListCtrl"
                }
            },
            data: { displayName: 'List' }
        }).state('workshop.create', {
            url: '/create',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "WorkshopCreateCtrl"
                }
            },
            data: { displayName: 'Create' }
        }).state('workshop.edit', {
            url: '/edit/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "WorkshopEditCtrl"
                }
            },
            data: { displayName: 'Edit' }
        }).state('workshop.details', {
            url: '/details/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Details',
                    controller: ""
                }
            },
            data: { displayName: 'Details' }
        });
    }
]);
//# sourceMappingURL=WorkshopRoute.js.map