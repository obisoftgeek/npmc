app.config([
    '$stateProvider', function ($stateProvider) {
        var viewPath = '/MailClient/MailClient';
        $stateProvider
            .state('mailclient', {
            abstract: true,
            url: '/mailclient',
            data: {
                breadcrumbProxy: 'mailclient.list',
                pageTitle: "Mails"
            }
        }).state('mailclient.list', {
            url: '/list',
            views: {
                "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "MailClientListCtrl"
                }
            },
            data: { displayName: 'Mail List' }
        }).state('mailclient.details', {
            url: '/details/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Details',
                    controller: "MailClientDetailCtrl"
                }
            },
            data: { displayName: 'Details' }
        });
    }
]);
//# sourceMappingURL=MailClientRoute.js.map