app.config(['$stateProvider', function ($stateProvider) {
        var viewPath = '/Candidate/Home';
        $stateProvider
            .state('home', {
            abstract: true,
            url: '/home',
            data: {
                breadcrumbProxy: 'home.dashboard',
                pageTitle: "Dashboard"
            }
        }).state('home.dashboard', {
            url: '/dashboard',
            views: { "main@": {
                    templateUrl: viewPath + '/Dashboard',
                    controller: "CandidateDashboardCtrl"
                }
            },
            data: { displayName: 'Dashboard' }
        });
    }]).run(function ($state) {
    $state.go('home.dashboard');
});
//# sourceMappingURL=DashboardRoute.js.map