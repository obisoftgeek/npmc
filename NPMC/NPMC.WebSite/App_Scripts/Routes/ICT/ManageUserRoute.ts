﻿


app.config([
    '$stateProvider', ($stateProvider) => {

        var viewPath = '/ICT/ManageUser';

        $stateProvider
            .state('manageuser', {
                abstract: true,
                url: '/manageuser',
                data: {
                    breadcrumbProxy: 'manageuser.list',
                    pageTitle: "User Management"
                   
                }
            }).state('manageuser.list', {
                url: '/list',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/List',
                        controller: "ManageUserListCtrl"
                    }
                },
                data: { displayName: 'Users' }

            }).state('manageuser.create', {
                url: '/create',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/Form',
                        controller: "ManageUserCreateCtrl"
                    }
                },
                data: { displayName: 'Create' }

            }).state('manageuser.edit', {
                url: '/edit/:id',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/Form',
                        controller: "ManageUserEditCtrl"
                    }
                },
                data: { displayName: 'Edit' }

            }).state('manageuser.details', {
                url: '/details/:id',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/Details',
                        controller: ""
                    }
                },
                data: { displayName: 'Details' }

            });
    }
]);

