app.config([
    '$stateProvider', function ($stateProvider) {
        var viewPath = '/Registrar/Mail';
        $stateProvider
            .state('mail', {
            abstract: true,
            url: '/mail',
            data: {
                breadcrumbProxy: 'mail.list',
                pageTitle: "Mails"
            }
        }).state('mail.list', {
            url: '/list',
            views: {
                "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "MailListCtrl"
                }
            },
            data: { displayName: 'Mail List' }
        }).state('mail.details', {
            url: '/details/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Details',
                    controller: "MailDetailCtrl"
                }
            },
            data: { displayName: 'Details' }
        }).state('mail.create', {
            url: '/create',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "MailCreateCtrl"
                }
            },
            data: { displayName: 'Create Mail' }
        });
    }
]);
//# sourceMappingURL=MailRoute.js.map