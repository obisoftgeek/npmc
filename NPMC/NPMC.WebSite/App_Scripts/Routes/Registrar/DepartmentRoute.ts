﻿


app.config(['$stateProvider', ($stateProvider) => {
    var viewPath = '/Registrar/Department';
    $stateProvider
        .state('department', {
            abstract: true,
            url: '/department',
            data: {
                breadcrumbProxy: 'department.list',
                pageTitle: "Department"
                           
            }

        }).state('department.list', {
            url: '/list',
            views: { "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "DepartmentListCtrl"
                }
            },         
            data: { displayName: 'List' }

        }).state('department.create', {
            url: '/create',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "DepartmentCreateCtrl"
                }
            },
            data: { displayName: 'Create' }

        }).state('department.edit', {
            url: '/edit/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "DepartmentEditCtrl"
                }
            },
            data: { displayName: 'Edit' }

        }).state('department.details', {
            url: '/details/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Details',
                    controller: "DepartmentDetailCtrl"
                }
            },
            data: { displayName: 'Details' }

        });

   

}]);

