app.config([
    '$stateProvider', function ($stateProvider) {
        var viewPath = '/Registrar/Diet';
        $stateProvider
            .state('dietsetup', {
            abstract: true,
            url: '/dietsetup',
            data: {
                breadcrumbProxy: 'dietsetup.list',
                pageTitle: "Diet "
            }
        }).state('dietsetup.list', {
            url: '/list',
            views: {
                "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "DietListCtrl"
                }
            },
            data: { displayName: 'Diets' }
        }).state('dietsetup.create', {
            url: '/create',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "DietCreateCtrl"
                }
            },
            data: { displayName: 'Create' }
        }).state('dietsetup.edit', {
            url: '/edit/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "DietEditCtrl"
                }
            },
            data: { displayName: 'Edit' }
        }).state('dietsetup.details', {
            url: '/details/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Details',
                    controller: ""
                }
            },
            data: { displayName: 'Details' }
        });
    }
]);
//# sourceMappingURL=ExamDietSetupRoute.js.map