app.config([
    '$stateProvider', function ($stateProvider) {
        var viewPath = '/Registrar/Exam';
        $stateProvider
            .state('examsetup', {
            abstract: true,
            url: '/exams',
            data: {
                breadcrumbProxy: 'examsetup.list',
                pageTitle: "Exam "
            }
        }).state('examsetup.list', {
            url: '/list',
            views: {
                "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "ExamListCtrl"
                }
            },
            data: { displayName: 'Exams' }
        }).state('examsetup.create', {
            url: '/create',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "ExamCreateCtrl"
                }
            },
            data: { displayName: 'Create' }
        }).state('examsetup.edit', {
            url: '/edit/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "ExamEditCtrl"
                }
            },
            data: { displayName: 'Edit' }
        }).state('examsetup.details', {
            url: '/details/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Details',
                    controller: "ExamDetailCtrl"
                }
            },
            data: { displayName: 'Details' }
        });
    }
]);
//# sourceMappingURL=ExamSetupRoute.js.map