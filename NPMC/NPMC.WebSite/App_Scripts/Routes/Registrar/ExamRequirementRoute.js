app.config([
    '$stateProvider', function ($stateProvider) {
        var viewPath = '/Registrar/ExamRequirement';
        $stateProvider
            .state('examrequirement', {
            abstract: true,
            url: '/examrequirement',
            data: {
                breadcrumbProxy: 'examrequirement.list',
                pageTitle: "Exam Requirement"
            }
        }).state('examrequirement.list', {
            url: '/list',
            views: {
                "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "ExamRequirementListCtrl"
                }
            },
            data: { displayName: 'Exam Requirements' }
        }).state('examrequirement.create', {
            url: '/create/:examId',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "ExamRequirementCreateCtrl"
                }
            },
            data: { displayName: 'Create' }
        }).state('examrequirement.edit', {
            url: '/edit/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "ExamRequirementEditCtrl"
                }
            },
            data: { displayName: 'Edit' }
        }).state('examrequirement.details', {
            url: '/details/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Details',
                    controller: ""
                }
            },
            data: { displayName: 'Exam Requirements Details' }
        });
    }
]);
//# sourceMappingURL=ExamRequirementRoute.js.map