﻿
app.config([
    '$stateProvider', ($stateProvider) => {

        var viewPath = '/Registrar/ExamCenter';

        $stateProvider
            .state('examcenter', {
                abstract: true,
                url: '/examcenters',
                data: {
                    breadcrumbProxy: 'examcenter.list',
                    pageTitle: "Exam Centers"

                }
            }).state('examcenter.list', {
                url: '/centers',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/List',
                        controller: "ExamCenterListCtrl"
                    }
                },
                data: {
                    displayName: 'Centers',
                     pageTitle: "Center Setup"
                }

            }).state('examcenter.create', {
                url: '/create',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/Form',
                        controller: "ExamCenterCreateCtrl"
                    }
                },
                data: { displayName: 'Create' }

            }).state('examcenter.edit', {
                url: '/edit/:id',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/Form',
                        controller: "ExamCenterEditCtrl"
                    }
                },
                data: { displayName: 'Edit' }

            });
    }
]);

