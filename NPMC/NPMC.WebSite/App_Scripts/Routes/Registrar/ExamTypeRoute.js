app.config([
    '$stateProvider', function ($stateProvider) {
        var viewPath = '/Registrar/ExamType';
        $stateProvider
            .state('examsetup.types', {
            url: '/Types/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/List',
                    controller: "ExamTypeListCtrl"
                }
            },
            data: {
                displayName: 'Types',
                pageTitle: "Type Setup"
            }
        }).state('examsetup.types.create', {
            url: '/create',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "ExamTypeCreateCtrl"
                }
            },
            data: { displayName: 'Create' }
        }).state('examsetup.types.edit', {
            url: '/edit/:id',
            views: {
                "main@": {
                    templateUrl: viewPath + '/Form',
                    controller: "ExamTypeEditCtrl"
                }
            },
            data: { displayName: 'Edit' }
        });
    }
]);
//# sourceMappingURL=ExamTypeRoute.js.map