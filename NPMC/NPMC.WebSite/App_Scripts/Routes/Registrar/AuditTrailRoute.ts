﻿


app.config([
    '$stateProvider', ($stateProvider) => {
        var viewPath = '/Registrar/Audit';
        $stateProvider
            .state('audit', {
                abstract: true,
                url: '/audit',
                data: {
                    breadcrumbProxy: 'audit.list',
                    pageTitle: "Audit Trail"

                }

            }).state('audit.list', {
                url: '/list',
                views: {
                    "main@": {
                        templateUrl: viewPath + '/List',
                        controller: "AuditTrailCtrl"
                    }
                },
                data: { displayName: 'Audit Trail' }

            });


    }
]);

