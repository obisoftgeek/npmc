PaymentCategoryResource.$inject = ["$resource"];
function PaymentCategoryResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/PaymentCategory/count"
    };
    var nameAndValues = {
        method: "GET",
        isArray: true,
        url: "/api/PaymentCategory/NameAndValues"
    };
    var updateAction = {
        method: 'PUT',
        isArray: false
    };
    var service = $resource('/api/PaymentCategory/:id', { id: '@id' }, {
        count: count,
        update: updateAction,
        nameAndValues: nameAndValues,
    });
    return service;
}
angular.module("app").factory("PaymentCategoryResource", PaymentCategoryResource);
//# sourceMappingURL=PaymentCategoryResource.js.map