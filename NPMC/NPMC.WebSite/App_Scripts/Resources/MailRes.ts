﻿interface IMail extends IRootObject {
}

interface IMailResource extends IRootResource<IRootObject> {
}

MailResource.$inject = ["$resource"];

function MailResource($resource: ng.resource.IResourceService):
    IMailResource {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/Mail/count"
    };
    var nameAndValues = {
        method: "GET",
        isArray: true,
        url: "/api/Mail/NameAndValues"
    };
    var details = {
        method: "GET",
        isArray: false,
        url: "/api/Mail/GetDetail/:id"
    };

    var updateAction = {
        method: 'PUT',
        isArray: false
    };
    var updateToReceived = {
        method: 'PUT',
        isArray: false,
        url: "/api/Mail/updateToReceived"
    };
    var service = <IMailResource>$resource('/api/Mail/:id',
        { id: '@id' },
        {
            count: count,
            update: updateAction,
            nameAndValues: nameAndValues,
            updateToReceived: updateToReceived,
            details:details
        });
    return service;
}

angular.module("app").factory("MailResource", MailResource);