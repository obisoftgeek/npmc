﻿interface IFaculty extends IRootObject {
    }

interface IFacultyResource extends IRootResource<IRootObject> {        
    deletAll(config?);
    approveFaculty(config): any;
    rejectFaculty(config): any;
    getApprovalsForFaculty(config): any;
    nameAndValues(): any
    auditActions(config?): any
    auditSections(config?):any
    }

    FacultyResource.$inject = ["$resource"];

    function FacultyResource($resource: ng.resource.IResourceService):
        IFacultyResource {
        var count = {
            method: "GET",
            isArray: false,
            url: "/api/Faculty/count"
        };
        var nameAndValues = {
            method: "GET",
            isArray: true,
            url: "/api/Faculty/NameAndValues"
        };

        var updateAction = {
            method: 'PUT',
            isArray: false
        };
        var service = <IFacultyResource> $resource('/api/Faculty/:id', { id: '@id' }, {
            count: count, 
            update: updateAction,
            nameAndValues: nameAndValues, 
        });
        return service;
    }
    angular.module("app").factory("FacultyResource", FacultyResource);
