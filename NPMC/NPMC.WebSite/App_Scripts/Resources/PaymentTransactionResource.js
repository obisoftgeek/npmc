PaymentTransactionResource.$inject = ["$resource"];
function PaymentTransactionResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/PaymentTransaction/count"
    };
    var service = $resource('/api/PaymentTransaction/:id', { id: '@id' }, {
        count: count
    });
    return service;
}
angular.module("app").factory("PaymentTransactionResource", PaymentTransactionResource);
//# sourceMappingURL=PaymentTransactionResource.js.map