﻿

interface IExamDietResource extends IRootObject {

}

interface IExamDietResource extends IRootResource<IExamDietResource> {
    create(item);
    update(item);
    getLookup(item?): any[];
    approve(item?): any;
    toggleActive(item?): any;
    getDetails(item): any;
    deleteItem(item): any;
    getItem(item): any;
    getLookkup(): any;
}

ExamDietResource.$inject = ["$resource"];

function ExamDietResource($resource: ng.resource.IResourceService):
    IExamDietResource {
    var count = {
        method: "GET",
        isArray: false,
        url: "/Api/ExamDiet/Count"
    };
    var create = {
        method: "Post",
        isArray: false,
        url: "/Api/ExamDiet/Create"
    };
    var update = {
        method: "Put",
        isArray: false,
        url: "/Api/ExamDiet/Update"
    };
    var getLookup = {
        method: "GET",
        isArray: true,
        url: "/Api/ExamDiet/GetLookup"
    };
    var getDetails = {
        method: "GET",
        isArray: false,
        url: "/Api/ExamDiet/GetDetails/:id"
    };
    var toggleActive = {
        method: "Post",
        isArray: false,
        url: "/Api/ExamDiet/ToggleActive/:id"
    };
    var approve = {
        method: "Post",
        isArray: false,
        url: "/Api/ExamDiet/Approve"
    };
    var deleteItem = {
        method: "Delete",
        isArray: false,
        url: "/Api/ExamDiet/Delete/:id"
    };
    var getItem = {
        method: "GET",
        isArray: false,
        url: "/Api/ExamDiet/GetItem/:id"
    };
    var getLookkup = {
        method: "GET",
        isArray: true,
        url: "/Api/ExamDiet/GetLookup/:ExamDietId"
    };
    var service = <IExamDietResource>$resource('/Api/ExamDiet/:id', { id: '@id' }, {
        count: count,
        create: create,
        update: update,
        getLookup: getLookup,
        getDetails: getDetails,
        toggleActive: toggleActive,
        approve: approve,
        deleteItem: deleteItem,
        getItem: getItem,
        getLookkup: getLookkup

    });
    return service;
}
angular.module("app").factory("ExamDietResource", ExamDietResource);
