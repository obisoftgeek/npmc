﻿interface IPaymentFee extends IRootObject {
    }

interface IPaymentFeeResource extends IRootResource<IRootObject> {        
    deletAll(config?); 
    nameAndValues(config?): any 
    }

    PaymentFeeResource.$inject = ["$resource"];

    function PaymentFeeResource($resource: ng.resource.IResourceService):
        IPaymentFeeResource {
        var count = {
            method: "GET",
            isArray: false,
            url: "/api/PaymentFee/count"
        };
        var nameAndValues = {
            method: "GET",
            isArray: true,
            url: "/api/PaymentFee/NameAndValues"
        };

        var updateAction = {
            method: 'PUT',
            isArray: false
        };
        var service = <IPaymentFeeResource> $resource('/api/PaymentFee/:id', { id: '@id' }, {
            count: count, 
            update: updateAction,
            nameAndValues: nameAndValues, 
        });
        return service;
    }
    angular.module("app").factory("PaymentFeeResource", PaymentFeeResource);
