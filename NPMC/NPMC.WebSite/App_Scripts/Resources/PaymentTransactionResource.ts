﻿interface IPaymentTransaction extends IRootObject {
    }

interface IPaymentTransactionResource extends IRootResource<IRootObject> {        
    }

    PaymentTransactionResource.$inject = ["$resource"];

    function PaymentTransactionResource($resource: ng.resource.IResourceService):
        IPaymentTransactionResource {
        var count = {
            method: "GET",
            isArray: false,
            url: "/api/PaymentTransaction/count"
        };
          
        var service = <IPaymentTransactionResource> $resource('/api/PaymentTransaction/:id', { id: '@id' }, {
            count: count 
        });
        return service;
    }
    angular.module("app").factory("PaymentTransactionResource", PaymentTransactionResource);
