SubSpecialtyResource.$inject = ["$resource"];
function SubSpecialtyResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/SubSpecialty/count"
    };
    var nameAndValues = {
        method: "GET",
        isArray: true,
        url: "/api/SubSpecialty/NameAndValues"
    };
    var updateAction = {
        method: 'PUT',
        isArray: false
    };
    var service = $resource('/api/SubSpecialty/:id', { id: '@id' }, {
        count: count,
        update: updateAction,
        nameAndValues: nameAndValues,
    });
    return service;
}
angular.module("app").factory("SubSpecialtyResource", SubSpecialtyResource);
//# sourceMappingURL=SubSpecialtyResource.js.map