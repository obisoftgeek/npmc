unitResource.$inject = ["$resource"];
function unitResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/Api/Unit/Count"
    };
    var create = {
        method: "Post",
        isArray: false,
        url: "/Api/Unit/Create"
    };
    var update = {
        method: "Put",
        isArray: false,
        url: "/Api/Unit/Update"
    };
    var getLookup = {
        method: "GET",
        isArray: true,
        url: "/Api/Unit/GetLookup"
    };
    var getDetails = {
        method: "GET",
        isArray: false,
        url: "/Api/Unit/GetDetails/:id"
    };
    var toggleActive = {
        method: "Post",
        isArray: false,
        url: "/Api/Unit/ToggleActive/:id"
    };
    var approve = {
        method: "Post",
        isArray: false,
        url: "/Api/Unit/Approve"
    };
    var deleteItem = {
        method: "Delete",
        isArray: false,
        url: "/Api/Unit/Delete/:id"
    };
    var getItem = {
        method: "GET",
        isArray: false,
        url: "/Api/Unit/GetItem/:id"
    };
    var getLookkup = {
        method: "GET",
        isArray: true,
        url: "/Api/Unit/GetLookup/:departmentId"
    };
    var service = $resource('/Api/Unit/:id', { id: '@id' }, {
        count: count,
        create: create,
        update: update,
        getLookup: getLookup,
        getDetails: getDetails,
        toggleActive: toggleActive,
        approve: approve,
        deleteItem: deleteItem,
        getItem: getItem,
        getLookkup: getLookkup
    });
    return service;
}
angular.module("app").factory("UnitResource", unitResource);
//# sourceMappingURL=UnitRes.js.map