PaymentFeeResource.$inject = ["$resource"];
function PaymentFeeResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/PaymentFee/count"
    };
    var nameAndValues = {
        method: "GET",
        isArray: true,
        url: "/api/PaymentFee/NameAndValues"
    };
    var updateAction = {
        method: 'PUT',
        isArray: false
    };
    var service = $resource('/api/PaymentFee/:id', { id: '@id' }, {
        count: count,
        update: updateAction,
        nameAndValues: nameAndValues,
    });
    return service;
}
angular.module("app").factory("PaymentFeeResource", PaymentFeeResource);
//# sourceMappingURL=PaymentFeeResource.js.map