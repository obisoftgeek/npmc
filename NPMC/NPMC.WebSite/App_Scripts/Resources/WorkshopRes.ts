﻿interface IWorkshop extends IRootObject {
}

interface IWorkshopResource extends IRootObject {
   
}

interface IWorkshopResource extends IRootResource<IWorkshopResource> {
    create(item);
    update(item);
    getLookup(item?): any[];
    approve(item?): any;
    toggleActive(item?): any;
    getDetails(item): any;
    deleteItem(item): any;
    getItem(item): any;
    getLookkup(): any;
}

workshopResource.$inject = ["$resource"];

function workshopResource($resource: ng.resource.IResourceService):
    IWorkshopResource {
    var count = {
        method: "GET",
        isArray: false,
        url: "/Api/Workshop/Count"
    };
    var create = {
        method: "Post",
        isArray: false,
        url: "/Api/Workshop/Create"
    };
    var update = {
        method: "Put",
        isArray: false,
        url: "/Api/Workshop/Update"
    };
    var getLookup = {
        method: "GET",
        isArray: true,
        url: "/Api/Workshop/GetLookup"
    };
    var getDetails = {
        method: "GET",
        isArray: false,
        url: "/Api/Workshop/GetDetails/:id"
    };
    var toggleActive = {
        method: "Post",
        isArray: false,
        url: "/Api/Workshop/ToggleActive/:id"
    };
    var approve = {
        method: "Post",
        isArray: false,
        url: "/Api/Workshop/Approve"
    };
    var deleteItem = {
        method: "Delete",
        isArray: false,
        url: "/Api/Workshop/Delete/:id"
    };
    var getItem = {
        method: "GET",
        isArray: false,
        url: "/Api/Workshop/GetItem/:id"
    };
    var getLookkup = {
        method: "GET",
        isArray: true,
        url: "/Api/Workshop/GetLookup/:workshopId"
    };
    var service = <IWorkshopResource> $resource('/Api/Workshop/:id',{ id: '@id' }, {
        count: count,
        create: create,
        update: update,
        getLookup: getLookup,
        getDetails: getDetails,
        toggleActive: toggleActive,
        approve: approve,
        deleteItem: deleteItem,
        getItem: getItem,
        getLookkup: getLookkup
           
        });
    return service;
}
angular.module("app").factory("WorkshopResource", workshopResource);
