ExamResource.$inject = ["$resource"];
function ExamResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/Exam/count"
    };
    var getLookup = {
        method: "GET",
        isArray: true,
        url: "/api/Exam/getLookup"
    };
    var details = {
        method: "GET",
        isArray: false,
        url: "/api/Exam/GetDetail/:id"
    };
    var updateAction = {
        method: 'PUT',
        isArray: false
    };
    var toggleActive = {
        method: 'POST',
        isArray: false,
        url: "/api/Exam/ToggleActive/:id"
    };
    var service = $resource('/api/Exam/:id', { id: '@id' }, {
        count: count,
        update: updateAction,
        getLookup: getLookup,
        toggleActive: toggleActive,
        details: details
    });
    return service;
}
angular.module("app").factory("ExamResource", ExamResource);
//# sourceMappingURL=ExamSetupResource.js.map