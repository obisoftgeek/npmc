﻿

interface IMemoResource extends IRootObject {
   
}

interface IMemoResource extends IRootResource<IMemoResource> {
    create(item);
    update(item);
    approve(item?): any;
    toggleActive(item?): any;
    getDetails(item): any;
    deleteItem(item): any;
    getItem(item): any;
    getLookkup(item): any;
    createMemoThread(item): any;
    forwardMemo(item): any;
}

memoResource.$inject = ["$resource"];

function memoResource($resource: ng.resource.IResourceService):
    IMemoResource {
    var count = {
        method: "GET",
        isArray: false,
        url: "/Api/Memo/Count"
    };
    var create = {
        method: "Post",
        isArray: false,
        url: "/Api/Memo/Create"
    };
 
    var forwardMemo = {
        method: "Post",
        isArray: false,
        url: "/Api/Memo/ForwardMemo"
    };
    var createMemoThread = {
        method: "Post",
        isArray: false,
        url: "/Api/Memo/CreateMemoThread"
    };
    var update = {
        method: "Put",
        isArray: false,
        url: "/Api/Memo/Update"
    };
   
    var getDetails = {
        method: "GET",
        isArray: false,
        url: "/Api/Memo/GetDetail/:id"
    };
    var toggleActive = {
        method: "Post",
        isArray: false,
        url: "/Api/Memo/ToggleActive/:id"
    };
    var approve = {
        method: "Post",
        isArray: false,
        url: "/Api/Memo/Approve"
    };
    var deleteItem = {
        method: "Delete",
        isArray: false,
        url: "/Api/Memo/Delete/:id"
    };
    var getItem = {
        method: "GET",
        isArray: false,
        url: "/Api/Memo/GetItem/:id"
    };
    var getLookkup = {
        method: "GET",
        isArray: true,
        url: "/Api/Memo/GetLookup/:departmentId"
    };
    var service = <IMemoResource> $resource('/Api/Memo/:id', { id: '@id' }, {
        count: count,
        create: create,
        createMemoThread: createMemoThread,
        update: update,
        getDetails: getDetails,
        toggleActive: toggleActive,
        approve: approve,
        deleteItem: deleteItem,
        getItem: getItem,
        getLookkup: getLookkup,
        forwardMemo: forwardMemo
           
        });
    return service;
}
angular.module("app").factory("MemoResource", memoResource);
