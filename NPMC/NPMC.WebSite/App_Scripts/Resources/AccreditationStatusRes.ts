﻿interface IAccreditationStatus extends IRootObject {
}

interface IAccreditationStatusResource extends IRootResource<IRootObject> {
    deletAll(config?);
    approveAccreditationStatus(config): any;
    rejectAccreditationStatus(config): any;
    getApprovalsForAccreditationStatus(config): any;
    nameAndValues(): any
    auditActions(config?): any
    auditSections(config?):any
}

AccreditationStatusResource.$inject = ["$resource"];

function AccreditationStatusResource($resource: ng.resource.IResourceService):
IAccreditationStatusResource {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/AccreditationStatus/count"
    };
    var nameAndValues = {
        method: "GET",
        isArray: true,
        url: "/api/AccreditationStatus/NameAndValues"
    };

    var updateAction = {
        method: 'PUT',
        isArray: false
    };
    var service = <IAccreditationStatusResource> $resource('/api/AccreditationStatus/:id', { id: '@id' }, {
        count: count,
        update: updateAction,
        nameAndValues: nameAndValues,
    });
    return service;
}
angular.module("app").factory("AccreditationStatusResource", AccreditationStatusResource);
