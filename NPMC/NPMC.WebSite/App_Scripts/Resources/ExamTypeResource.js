ExamTypeResource.$inject = ["$resource"];
function ExamTypeResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/Api/ExamType/Count"
    };
    var create = {
        method: "Post",
        isArray: false,
        url: "/Api/ExamType/Create"
    };
    var update = {
        method: "Put",
        isArray: false,
        url: "/Api/ExamType/Update"
    };
    var getLookup = {
        method: "GET",
        isArray: true,
        url: "/Api/ExamType/GetLookup"
    };
    var getDetails = {
        method: "GET",
        isArray: false,
        url: "/Api/ExamType/GetDetails/:id"
    };
    var toggleActive = {
        method: "Post",
        isArray: false,
        url: "/Api/ExamType/ToggleActive/:id"
    };
    var approve = {
        method: "Post",
        isArray: false,
        url: "/Api/ExamType/Approve"
    };
    var deleteItem = {
        method: "Delete",
        isArray: false,
        url: "/Api/ExamType/Delete/:id"
    };
    var getItem = {
        method: "GET",
        isArray: false,
        url: "/Api/ExamType/GetItem/:id"
    };
    var getLookkup = {
        method: "GET",
        isArray: true,
        url: "/Api/ExamType/GetLookup/:ExamTypeId"
    };
    var getExamExamTypes = {
        method: "GET",
        isArray: true,
        url: "/Api/ExamType/getExamExamTypes"
    };
    var service = $resource('/Api/ExamType/:id', { id: '@id' }, {
        count: count,
        create: create,
        update: update,
        getLookup: getLookup,
        getDetails: getDetails,
        toggleActive: toggleActive,
        approve: approve,
        deleteItem: deleteItem,
        getItem: getItem,
        getLookkup: getLookkup,
        getExamExamTypes: getExamExamTypes
    });
    return service;
}
angular.module("app").factory("ExamTypeResource", ExamTypeResource);
//# sourceMappingURL=ExamTypeResource.js.map