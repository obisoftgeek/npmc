AccreditationStatusResource.$inject = ["$resource"];
function AccreditationStatusResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/AccreditationStatus/count"
    };
    var nameAndValues = {
        method: "GET",
        isArray: true,
        url: "/api/AccreditationStatus/NameAndValues"
    };
    var updateAction = {
        method: 'PUT',
        isArray: false
    };
    var service = $resource('/api/AccreditationStatus/:id', { id: '@id' }, {
        count: count,
        update: updateAction,
        nameAndValues: nameAndValues,
    });
    return service;
}
angular.module("app").factory("AccreditationStatusResource", AccreditationStatusResource);
//# sourceMappingURL=AccreditationStatusRes.js.map