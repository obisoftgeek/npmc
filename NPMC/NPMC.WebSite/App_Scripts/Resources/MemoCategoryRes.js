memoCategoryResource.$inject = ["$resource"];
function memoCategoryResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/Api/MemoCategory/Count"
    };
    var create = {
        method: "Post",
        isArray: false,
        url: "/Api/MemoCategory/Create"
    };
    var update = {
        method: "Put",
        isArray: false,
        url: "/Api/MemoCategory/Update"
    };
    var getLookup = {
        method: "GET",
        isArray: true,
        url: "/Api/MemoCategory/GetLookup"
    };
    var getDetails = {
        method: "GET",
        isArray: false,
        url: "/Api/MemoCategory/GetDetails/:id"
    };
    var toggleActive = {
        method: "Post",
        isArray: false,
        url: "/Api/MemoCategory/ToggleActive/:id"
    };
    var approve = {
        method: "Post",
        isArray: false,
        url: "/Api/MemoCategory/Approve"
    };
    var deleteItem = {
        method: "Delete",
        isArray: false,
        url: "/Api/MemoCategory/Delete/:id"
    };
    var getItem = {
        method: "GET",
        isArray: false,
        url: "/Api/MemoCategory/GetItem/:id"
    };
    var getLookkup = {
        method: "GET",
        isArray: true,
        url: "/Api/MemoCategory/GetLookup/:memoCategoryId"
    };
    var service = $resource('/Api/MemoCategory/:id', { id: '@id' }, {
        count: count,
        create: create,
        update: update,
        getLookup: getLookup,
        getDetails: getDetails,
        toggleActive: toggleActive,
        approve: approve,
        deleteItem: deleteItem,
        getItem: getItem,
        getLookkup: getLookkup
    });
    return service;
}
angular.module("app").factory("MemoCategoryResource", memoCategoryResource);
//# sourceMappingURL=MemoCategoryRes.js.map