ExamRequirementResource.$inject = ["$resource"];
function ExamRequirementResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/Api/ExamRequirement/Count"
    };
    var create = {
        method: "Post",
        isArray: false,
        url: "/Api/ExamRequirement/Create"
    };
    var update = {
        method: "Put",
        isArray: false,
        url: "/Api/ExamRequirement/Update"
    };
    var getLookup = {
        method: "GET",
        isArray: true,
        url: "/Api/ExamRequirement/GetLookup"
    };
    var getDetails = {
        method: "GET",
        isArray: false,
        url: "/Api/ExamRequirement/GetDetails/:id"
    };
    var toggleActive = {
        method: "Post",
        isArray: false,
        url: "/Api/ExamRequirement/ToggleActive/:id"
    };
    var approve = {
        method: "Post",
        isArray: false,
        url: "/Api/ExamRequirement/Approve"
    };
    var deleteItem = {
        method: "Delete",
        isArray: false,
        url: "/Api/ExamRequirement/Delete/:id"
    };
    var getItem = {
        method: "GET",
        isArray: false,
        url: "/Api/ExamRequirement/GetItem/:id"
    };
    var getLookkup = {
        method: "GET",
        isArray: true,
        url: "/Api/ExamRequirement/GetLookup/:ExamRequirementId"
    };
    var getExamRequirements = {
        method: "GET",
        isArray: true,
        url: "/Api/ExamRequirement/getExamRequirements"
    };
    var service = $resource('/Api/ExamRequirement/:id', { id: '@id' }, {
        count: count,
        create: create,
        update: update,
        getLookup: getLookup,
        getDetails: getDetails,
        toggleActive: toggleActive,
        approve: approve,
        deleteItem: deleteItem,
        getItem: getItem,
        getLookkup: getLookkup,
        getExamRequirements: getExamRequirements
    });
    return service;
}
angular.module("app").factory("ExamRequirementResource", ExamRequirementResource);
//# sourceMappingURL=ExamRequirementResource.js.map