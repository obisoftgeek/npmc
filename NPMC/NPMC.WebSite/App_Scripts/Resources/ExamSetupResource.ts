﻿interface IExam extends IRootObject {
}

interface IExamResource extends IRootResource<IRootObject> {
    toggleActive(config?); any;
    getLookup(config?): any;
    details(config?): any;
}

ExamResource.$inject = ["$resource"];

function ExamResource($resource: ng.resource.IResourceService):
    IExamResource {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/Exam/count"
    };
    var getLookup = {
        method: "GET",
        isArray: true,
        url: "/api/Exam/getLookup"
    };
    var details = {
        method: "GET",
        isArray: false,
        url: "/api/Exam/GetDetail/:id"
    };
    var updateAction = {
        method: 'PUT',
        isArray: false
    };
    var toggleActive = {
        method: 'POST',
        isArray: false,
        url: "/api/Exam/ToggleActive/:id"
    };
    var service = <IExamResource>$resource('/api/Exam/:id', { id: '@id' }, {
        count: count,
        update: updateAction,
        getLookup: getLookup,
        toggleActive: toggleActive,
        details: details
    });
    return service;
}
angular.module("app").factory("ExamResource", ExamResource);
