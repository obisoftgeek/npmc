﻿interface IExamCenter extends IRootObject {
}

interface IExamCenterResource extends IRootResource<IRootObject> {
    deletAll(config?);
    toggleActive(config?); any;
    nameAndValues(): any
    auditActions(config?): any
    auditSections(config?): any
}

ExamCenterResource.$inject = ["$resource"];

function ExamCenterResource($resource: ng.resource.IResourceService):
    IExamCenterResource {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/ExamCenter/count"
    };
    var nameAndValues = {
        method: "GET",
        isArray: true,
        url: "/api/ExamCenter/NameAndValues"
    };

    var updateAction = {
        method: 'PUT',
        isArray: false
    };
    var toggleActive = {
        method: 'POST',
        isArray: false,
        url: "/api/ExamCenter/ToggleActive/:id"
    };
    var service = <IExamCenterResource>$resource('/api/ExamCenter/:id', { id: '@id' }, {
        count: count,
        update: updateAction,
        nameAndValues: nameAndValues,
        toggleActive: toggleActive
    });
    return service;
}
angular.module("app").factory("ExamCenterResource", ExamCenterResource);
