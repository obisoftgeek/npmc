FacultyResource.$inject = ["$resource"];
function FacultyResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/Faculty/count"
    };
    var nameAndValues = {
        method: "GET",
        isArray: true,
        url: "/api/Faculty/NameAndValues"
    };
    var updateAction = {
        method: 'PUT',
        isArray: false
    };
    var service = $resource('/api/Faculty/:id', { id: '@id' }, {
        count: count,
        update: updateAction,
        nameAndValues: nameAndValues,
    });
    return service;
}
angular.module("app").factory("FacultyResource", FacultyResource);
//# sourceMappingURL=FacultyResource.js.map