﻿

interface IStateResource extends IRootObject {
   
}

interface IStateResource extends IRootResource<IStateResource> {
    getLookup(): any;
}

stateResource.$inject = ["$resource"];

function stateResource($resource: ng.resource.IResourceService):
    IStateResource {
    var getLookup = {
        method: "GET",
        isArray: true,
        url: "/Api/State/GetLookUp"
    }; 
    var service = <IStateResource> $resource('/Api/State/:id',{ id: '@id' }, {
        getLookup: getLookup           
        });
    return service;
}
angular.module("app").factory("StateResource", stateResource);
