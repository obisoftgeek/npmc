ExamCenterResource.$inject = ["$resource"];
function ExamCenterResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/ExamCenter/count"
    };
    var nameAndValues = {
        method: "GET",
        isArray: true,
        url: "/api/ExamCenter/NameAndValues"
    };
    var updateAction = {
        method: 'PUT',
        isArray: false
    };
    var toggleActive = {
        method: 'POST',
        isArray: false,
        url: "/api/ExamCenter/ToggleActive/:id"
    };
    var service = $resource('/api/ExamCenter/:id', { id: '@id' }, {
        count: count,
        update: updateAction,
        nameAndValues: nameAndValues,
        toggleActive: toggleActive
    });
    return service;
}
angular.module("app").factory("ExamCenterResource", ExamCenterResource);
//# sourceMappingURL=ExamCenterResource.js.map