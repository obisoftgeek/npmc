﻿

interface IDepartmentResource extends IRootObject {
   
}

interface IDepartmentResource extends IRootResource<IDepartmentResource> {
    create(item);
    update(item);
    getLookup(item?): any[];
    approve(item?): any;
    toggleActive(item?): any;
    getDetails(item): any;
    deleteItem(item): any;
    getItem(item): any;
    getLookkup(): any;
}

departmentResource.$inject = ["$resource"];

function departmentResource($resource: ng.resource.IResourceService):
    IDepartmentResource {
    var count = {
        method: "GET",
        isArray: false,
        url: "/Api/Department/Count"
    };
    var create = {
        method: "Post",
        isArray: false,
        url: "/Api/Department/Create"
    };
    var update = {
        method: "Put",
        isArray: false,
        url: "/Api/Department/Update"
    };
    var getLookup = {
        method: "GET",
        isArray: true,
        url: "/Api/Department/GetLookup"
    };
    var getDetails = {
        method: "GET",
        isArray: false,
        url: "/Api/Department/GetDetails/:id"
    };
    var toggleActive = {
        method: "Post",
        isArray: false,
        url: "/Api/Department/ToggleActive/:id"
    };
    var approve = {
        method: "Post",
        isArray: false,
        url: "/Api/Department/Approve"
    };
    var deleteItem = {
        method: "Delete",
        isArray: false,
        url: "/Api/Department/Delete/:id"
    };
    var getItem = {
        method: "GET",
        isArray: false,
        url: "/Api/Department/GetItem/:id"
    };
    var getLookkup = {
        method: "GET",
        isArray: true,
        url: "/Api/Department/GetLookup/:departmentId"
    };
    var service = <IDepartmentResource> $resource('/Api/Department/:id',{ id: '@id' }, {
        count: count,
        create: create,
        update: update,
        getLookup: getLookup,
        getDetails: getDetails,
        toggleActive: toggleActive,
        approve: approve,
        deleteItem: deleteItem,
        getItem: getItem,
        getLookkup: getLookkup
           
        });
    return service;
}
angular.module("app").factory("DepartmentResource", departmentResource);
