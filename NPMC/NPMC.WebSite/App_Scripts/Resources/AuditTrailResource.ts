﻿

interface IAuditTrailResource extends IRootResource<IRootObject> {        
    deletAll(config?);
    approveAuditTrail(config): any;
    rejectAuditTrail(config): any;
    getApprovalsForAuditTrail(config): any;
    nameAndValues(): any
    auditActions(config?): any
    auditSections(config?):any
    }

    AuditTrailResource.$inject = ["$resource"];

    function AuditTrailResource($resource: ng.resource.IResourceService):
        IAuditTrailResource {
        var count = {
            method: "GET",
            isArray: false,
            url: "/api/audittrail/count"
        };
        var auditActions = {
            method: "GET",
            isArray: true,
            url: "/api/audittrail/AuditActions"
        };
        var auditSections = {
            method: "GET",
            isArray: true,
            url: "/api/audittrail/AuditSections"
        };
        var service = <IAuditTrailResource> $resource('/api/audittrail/:id', { id: '@id' }, {
            count: count,
            auditActions: auditActions,
            auditSections: auditSections
        });
        return service;
    }
    angular.module("app").factory("AuditTrailResource", AuditTrailResource);
