﻿interface IAccount extends IRootObject {
    }

interface IAccountResource extends IRootResource<IRootObject> {        
    deletAll(config?);
    approveAccount(config): any;
    rejectAccount(config): any;
    getApprovalsForAccount(config): any;
    nameAndValues(): any
    auditActions(config?): any
    auditSections(config?):any
    }

    AccountResource.$inject = ["$resource"];

    function AccountResource($resource: ng.resource.IResourceService):
        IAccountResource {
        var count = {
            method: "GET",
            isArray: false,
            url: "/api/Account/count"
        };
        var nameAndValues = {
            method: "GET",
            isArray: true,
            url: "/api/Account/NameAndValues"
        };

        var updateAction = {
            method: 'PUT',
            isArray: false
        };
        var service = <IAccountResource> $resource('/api/Account/:id', { id: '@id' }, {
            count: count, 
            update: updateAction,
            nameAndValues: nameAndValues, 
        });
        return service;
    }
    angular.module("app").factory("AccountResource", AccountResource);
