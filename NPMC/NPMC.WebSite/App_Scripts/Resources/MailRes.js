MailResource.$inject = ["$resource"];
function MailResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/Mail/count"
    };
    var nameAndValues = {
        method: "GET",
        isArray: true,
        url: "/api/Mail/NameAndValues"
    };
    var details = {
        method: "GET",
        isArray: false,
        url: "/api/Mail/GetDetail/:id"
    };
    var updateAction = {
        method: 'PUT',
        isArray: false
    };
    var updateToReceived = {
        method: 'PUT',
        isArray: false,
        url: "/api/Mail/updateToReceived"
    };
    var service = $resource('/api/Mail/:id', { id: '@id' }, {
        count: count,
        update: updateAction,
        nameAndValues: nameAndValues,
        updateToReceived: updateToReceived,
        details: details
    });
    return service;
}
angular.module("app").factory("MailResource", MailResource);
//# sourceMappingURL=MailRes.js.map