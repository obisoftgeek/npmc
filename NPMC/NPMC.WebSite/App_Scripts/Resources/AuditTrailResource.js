AuditTrailResource.$inject = ["$resource"];
function AuditTrailResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/audittrail/count"
    };
    var auditActions = {
        method: "GET",
        isArray: true,
        url: "/api/audittrail/AuditActions"
    };
    var auditSections = {
        method: "GET",
        isArray: true,
        url: "/api/audittrail/AuditSections"
    };
    var service = $resource('/api/audittrail/:id', { id: '@id' }, {
        count: count,
        auditActions: auditActions,
        auditSections: auditSections
    });
    return service;
}
angular.module("app").factory("AuditTrailResource", AuditTrailResource);
//# sourceMappingURL=AuditTrailResource.js.map