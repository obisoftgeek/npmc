userProfileResource.$inject = ["$resource"];
function userProfileResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/Api/UserProfile/Count"
    };
    var create = {
        method: "Post",
        isArray: false,
        url: "/Api/UserProfile/Create"
    };
    var getLookupByDept = {
        method: "GET",
        isArray: true,
        url: "/Api/UserProfile/getLookupByDept/:departmentId"
    };
    var signupCandidate = {
        method: "Post",
        isArray: false,
        url: "/Api/UserProfile/SignupCandidate"
    };
    var update = {
        method: "Put",
        isArray: false,
        url: "/Api/UserProfile/Update"
    };
    var getLookup = {
        method: "GET",
        isArray: true,
        url: "/Api/UserProfile/GetLookup"
    };
    var getDetails = {
        method: "GET",
        isArray: false,
        url: "/Api/UserProfile/GetDetails/:id"
    };
    var toggleActive = {
        method: "Post",
        isArray: false,
        url: "/Api/UserProfile/ToggleActive/:id"
    };
    var approve = {
        method: "Post",
        isArray: false,
        url: "/Api/UserProfile/Approve"
    };
    var deleteItem = {
        method: "Delete",
        isArray: false,
        url: "/Api/UserProfile/Delete/:id"
    };
    var getItem = {
        method: "GET",
        isArray: false,
        url: "/Api/UserProfile/GetItem/:id"
    };
    var getCurrentUserProfile = {
        method: "GET",
        isArray: false,
        url: "/Api/UserProfile/GetCurrentUserProfile"
    };
    var confirmEmail = {
        method: "Post",
        isArray: false,
        url: "/Api/UserProfile/ConfirmEmail"
    };
    var changePassword = {
        method: "Put",
        isArray: false,
        url: "/Api/UserProfile/ChangePassword"
    };
    var forgotPassword = {
        method: "Post",
        isArray: false,
        url: "/Api/UserProfile/ForgotPassword"
    };
    var service = $resource('/Api/UserProfile/:id', { id: '@id' }, {
        count: count,
        create: create,
        signupCandidate: signupCandidate,
        update: update,
        getLookup: getLookup,
        getDetails: getDetails,
        toggleActive: toggleActive,
        approve: approve,
        deleteItem: deleteItem,
        getItem: getItem,
        getCurrentUserProfile: getCurrentUserProfile,
        confirmEmail: confirmEmail,
        changePassword: changePassword,
        forgotPassword: forgotPassword,
        getLookupByDept: getLookupByDept
    });
    return service;
}
angular.module("app").factory("UserProfileResource", userProfileResource);
//# sourceMappingURL=UserProfileRes.js.map