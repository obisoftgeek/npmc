﻿interface IPaymentCategory extends IRootObject {
    }

interface IPaymentCategoryResource extends IRootResource<IRootObject> {        
    deletAll(config?);
    approvePaymentCategory(config): any;
    rejectPaymentCategory(config): any;
    getApprovalsForPaymentCategory(config): any;
    nameAndValues(): any
    auditActions(config?): any
    auditSections(config?):any
    }

    PaymentCategoryResource.$inject = ["$resource"];

    function PaymentCategoryResource($resource: ng.resource.IResourceService):
        IPaymentCategoryResource {
        var count = {
            method: "GET",
            isArray: false,
            url: "/api/PaymentCategory/count"
        };
        var nameAndValues = {
            method: "GET",
            isArray: true,
            url: "/api/PaymentCategory/NameAndValues"
        };

        var updateAction = {
            method: 'PUT',
            isArray: false
        };
        var service = <IPaymentCategoryResource> $resource('/api/PaymentCategory/:id', { id: '@id' }, {
            count: count, 
            update: updateAction,
            nameAndValues: nameAndValues, 
        });
        return service;
    }
    angular.module("app").factory("PaymentCategoryResource", PaymentCategoryResource);
