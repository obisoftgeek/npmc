﻿

interface IInstitutionResource extends IRootObject {
   
}

interface IInstitutionResource extends IRootResource<IInstitutionResource> {
    create(item);
    update(item);
    getLookup(item?): any[];
    approve(item?): any;
    toggleActive(item?): any;
    getDetails(item): any;
    deleteItem(item): any;
    getItem(item): any;
    getLookkup(): any;
}

institutionResource.$inject = ["$resource"];

function institutionResource($resource: ng.resource.IResourceService):
    IInstitutionResource {
    var count = {
        method: "GET",
        isArray: false,
        url: "/Api/Institution/Count"
    };
    var create = {
        method: "Post",
        isArray: false,
        url: "/Api/Institution/Create"
    };
    var update = {
        method: "Put",
        isArray: false,
        url: "/Api/Institution/Update"
    };
    var getLookup = {
        method: "GET",
        isArray: true,
        url: "/Api/Institution/GetLookup"
    };
    var getDetails = {
        method: "GET",
        isArray: false,
        url: "/Api/Institution/GetDetails/:id"
    };
    var toggleActive = {
        method: "Post",
        isArray: false,
        url: "/Api/Institution/ToggleActive/:id"
    };
    var approve = {
        method: "Post",
        isArray: false,
        url: "/Api/Institution/Approve"
    };
    var deleteItem = {
        method: "Delete",
        isArray: false,
        url: "/Api/Institution/Delete/:id"
    };
    var getItem = {
        method: "GET",
        isArray: false,
        url: "/Api/Institution/GetItem/:id"
    };
    var getLookkup = {
        method: "GET",
        isArray: true,
        url: "/Api/Institution/GetLookup/:institutionId"
    };
    var service = <IInstitutionResource> $resource('/Api/Institution/:id',{ id: '@id' }, {
        count: count,
        create: create,
        update: update,
        getLookup: getLookup,
        getDetails: getDetails,
        toggleActive: toggleActive,
        approve: approve,
        deleteItem: deleteItem,
        getItem: getItem,
        getLookkup: getLookkup
           
        });
    return service;
}
angular.module("app").factory("InstitutionResource", institutionResource);
