userManagementResource.$inject = ["$resource"];
function userManagementResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/Api/UserProfile/Count"
    };
    var create = {
        method: "Post",
        isArray: false,
        url: "/Api/UserProfile/Create"
    };
    var update = {
        method: "Put",
        isArray: false,
        url: "/Api/UserProfile/Update"
    };
    var getLookup = {
        method: "GET",
        isArray: true,
        url: "/Api/UserProfile/GetLookup"
    };
    var getDetails = {
        method: "GET",
        isArray: false,
        url: "/Api/UserProfile/GetDetails/:id"
    };
    var toggleActive = {
        method: "Post",
        isArray: false,
        url: "/Api/UserProfile/ToggleActive/:id"
    };
    var approve = {
        method: "Post",
        isArray: false,
        url: "/Api/UserProfile/Approve"
    };
    var deleteItem = {
        method: "Delete",
        isArray: false,
        url: "/Api/UserProfile/Delete/:id"
    };
    var getItem = {
        method: "GET",
        isArray: false,
        url: "/Api/UserProfile/GetItem/:id"
    };
    var service = $resource('/Api/UserProfile/:id', { id: '@id' }, {
        count: count,
        create: create,
        update: update,
        getLookup: getLookup,
        getDetails: getDetails,
        toggleActive: toggleActive,
        approve: approve,
        deleteItem: deleteItem,
        getItem: getItem
    });
    return service;
}
angular.module("app").factory("ManageUserResource", userManagementResource);
