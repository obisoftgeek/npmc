﻿interface ISubSpecialty extends IRootObject {
    }

interface ISubSpecialtyResource extends IRootResource<IRootObject> {        
    deletAll(config?);
    approveSubSpecialty(config): any;
    rejectSubSpecialty(config): any;
    getApprovalsForSubSpecialty(config): any;
    nameAndValues(): any
    auditActions(config?): any
    auditSections(config?):any
    }

    SubSpecialtyResource.$inject = ["$resource"];

    function SubSpecialtyResource($resource: ng.resource.IResourceService):
        ISubSpecialtyResource {
        var count = {
            method: "GET",
            isArray: false,
            url: "/api/SubSpecialty/count"
        };
        var nameAndValues = {
            method: "GET",
            isArray: true,
            url: "/api/SubSpecialty/NameAndValues"
        };

        var updateAction = {
            method: 'PUT',
            isArray: false
        };
        var service = <ISubSpecialtyResource> $resource('/api/SubSpecialty/:id', { id: '@id' }, {
            count: count, 
            update: updateAction,
            nameAndValues: nameAndValues, 
        });
        return service;
    }
    angular.module("app").factory("SubSpecialtyResource", SubSpecialtyResource);
