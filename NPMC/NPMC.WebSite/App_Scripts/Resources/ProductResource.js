ProductResource.$inject = ["$resource"];
function ProductResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/Product/count"
    };
    var nameAndValues = {
        method: "GET",
        isArray: true,
        url: "/api/Product/NameAndValues"
    };
    var updateAction = {
        method: 'PUT',
        isArray: false
    };
    var service = $resource('/api/Product/:id', { id: '@id' }, {
        count: count,
        update: updateAction,
        nameAndValues: nameAndValues,
    });
    return service;
}
angular.module("app").factory("ProductResource", ProductResource);
//# sourceMappingURL=ProductResource.js.map