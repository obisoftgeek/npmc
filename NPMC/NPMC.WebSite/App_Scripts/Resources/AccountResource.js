AccountResource.$inject = ["$resource"];
function AccountResource($resource) {
    var count = {
        method: "GET",
        isArray: false,
        url: "/api/Account/count"
    };
    var nameAndValues = {
        method: "GET",
        isArray: true,
        url: "/api/Account/NameAndValues"
    };
    var updateAction = {
        method: 'PUT',
        isArray: false
    };
    var service = $resource('/api/Account/:id', { id: '@id' }, {
        count: count,
        update: updateAction,
        nameAndValues: nameAndValues,
    });
    return service;
}
angular.module("app").factory("AccountResource", AccountResource);
//# sourceMappingURL=AccountResource.js.map