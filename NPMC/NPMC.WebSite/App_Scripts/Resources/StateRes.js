stateResource.$inject = ["$resource"];
function stateResource($resource) {
    var getLookup = {
        method: "GET",
        isArray: true,
        url: "/Api/State/GetLookUp"
    };
    var service = $resource('/Api/State/:id', { id: '@id' }, {
        getLookup: getLookup
    });
    return service;
}
angular.module("app").factory("StateResource", stateResource);
//# sourceMappingURL=StateRes.js.map