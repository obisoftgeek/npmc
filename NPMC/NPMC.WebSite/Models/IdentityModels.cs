﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.WebSite.Models
{

    public class ApplicationUserLogin : IdentityUserLogin<int> { }
    public class ApplicationUserClaim : IdentityUserClaim<int> { }
    public class ApplicationUserRole : IdentityUserRole<int> { }

    public class ApplicationRole : IdentityRole<int, ApplicationUserRole>, IRole<int>
    {
        public string Description { get; set; }

        public ApplicationRole() : base() { }
        public ApplicationRole(string name)
            : this()
        {
            this.Name = name;
        }

        public ApplicationRole(string name, string description)
            : this(name)
        {
            this.Description = description;
        }
    }


    public class ApplicationUser : IdentityUser<int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>, IUser<int>
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }

     
    }
 

    public class ApplicationDbContext
        : IdentityDbContext<ApplicationUser, ApplicationRole, int,
        ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {

        }

        static ApplicationDbContext()
        {
            //Database.SetInitializer<ApplicationDbContext>(new ApplicationDbInitializer());
            Database.SetInitializer<ApplicationDbContext>(null);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }      
    }
  
    public class ApplicationUserStore :
    UserStore<ApplicationUser, ApplicationRole, int,
    ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>, IUserStore<ApplicationUser, int>,
        IUserPasswordStore<ApplicationUser, int>, IUserSecurityStampStore<ApplicationUser, int>, IDisposable
    {
      
        public ApplicationUserStore()
            : this(new IdentityDbContext())
        {
            base.DisposeContext = true;
        }
     
        private readonly NPMCEntities _npmcEntities;
     
        public ApplicationUserStore(DbContext context)
            : base(context)
        {
            _npmcEntities = new NPMCEntities();
           
        }
       
        public override Task CreateAsync(ApplicationUser user)
        {
            if (user == null) { throw new ArgumentNullException("user"); }

            UserProfileModel profileData = GlobalVariables.UserProfileData;
            var aspNetuser = Mapper.Map<ApplicationUser, AspNetUser>(user);
            aspNetuser.PhoneNumber = profileData.PhoneNumber;
            _npmcEntities.AspNetUsers.Add(aspNetuser);
            var profile = Mapper.Map<UserProfileModel, UserProfile>(profileData);
            profile.AspNetUserId = aspNetuser.Id;
            profile.IsActive = true;
            _npmcEntities.UserProfiles.Add(profile);

            if (GlobalVariables.UserProfileType == UserProfileTypeEnum.StaffUser)
            {
                var staffRecord = new Staff();
                staffRecord.UserProfileId = profile.AspNetUserId;
                _npmcEntities.Staffs.Add(staffRecord);
            }
                   
            _npmcEntities.SaveChanges();          
            user.Id = aspNetuser.Id;          
            return Task.FromResult(user);
        }
      
    }
    public class ApplicationRoleStore
    : RoleStore<ApplicationRole, int, ApplicationUserRole>,
    IQueryableRoleStore<ApplicationRole, int>,IRoleStore<ApplicationRole, int>, IDisposable
    {
        public ApplicationRoleStore()
            : base(new IdentityDbContext())
        {
            base.DisposeContext = true;
        }

        public ApplicationRoleStore(DbContext context)
            : base(context)
        {

        }
       
    }

    public static class GlobalVariables
    {
        public static UserProfileModel UserProfileData { get; set; }
        public static UserProfileTypeEnum UserProfileType { get; set; }
    }
  
}