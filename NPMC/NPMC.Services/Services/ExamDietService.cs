﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.Utilities;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IExamDietService
    {

        void Create(ExamDietModel model, int currentUserId);
        
        void Delete(int id, int currentUserId);
        void Update(ExamDietModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup();
        CountModel<ExamDietItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null);
        IEnumerable<ExamDietItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null);
        ExamDietItem GetDetail(int id);
        ExamDietModel GetById(int id);
      

    }
    public class ExamDietService : IExamDietService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IExamDietRepository _ExamDietRepository;

        private readonly IAuditTrailService _auditTrailService;
        public ExamDietService(IUnitOfWorkAsync unitOfWork, IExamDietRepository ExamDietRepository, IAuditTrailService auditTrailService)
        {
            _unitOfWork = unitOfWork;
            _ExamDietRepository = ExamDietRepository;
            _auditTrailService = auditTrailService;
        }
        public IEnumerable<LookupModel> GetLookup()
        {
            var filterBy = new ExamDietFilter { };
            var entities = _ExamDietRepository.GetExamDietFilteredQueryable(filterBy);
           
            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }
     
        public void Create(ExamDietModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<ExamDietModel, ExamDiet>(model);
                entity.DateCreated = DateTime.Now;
                _ExamDietRepository.Insert(entity);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Exam Diet - {0} created",model.Name),currentUserId,AuditConstants.SetupSection,AuditConstants.SetupAction);

            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetExamDietEntity(id);
                var entityName = entity.Name;
                _ExamDietRepository.Delete(entity);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Exam Diet - {0} deleted", entityName), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        public ExamDietItem GetDetail(int id)
        {
            var entity = GetExamDietEntity(id);
            return Mapper.Map<ExamDiet, ExamDietItem>(entity);
        }

        
        private static IEnumerable<ExamDietItem> ProcessQuery(IEnumerable<ExamDiet> entities)
        {
            return entities.Select(c => new ExamDietItem()
            {
                DateCreated=c.DateCreated,
                DefermentPercentage=c.DefermentPercentage,
                Description=c.Description,
                EndDate=c.EndDate,
                Id=c.Id,
                IsActive=c.IsActive,
                IsApproved=c.IsApproved,
                Name=c.Name,
                StartDate=c.StartDate,
                StatusId=c.StatusId 
            });
        }
        public CountModel<ExamDietItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            int totalCount;
            var filterBy = ExamDietFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _ExamDietRepository.GetExamDietPaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<ExamDietItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<ExamDietItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            var filterBy = ExamDietFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _ExamDietRepository.GetExamDietPaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(ExamDietModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var userProfile = GetExamDietEntity(model.Id);
                Mapper.Map(model, userProfile);
                _ExamDietRepository.Update(userProfile);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Exam Diet - {0} updated", model.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private ExamDiet GetExamDietEntity(int id)
        {
            return _ExamDietRepository.Find(id);

        }
        public ExamDietModel GetById(int userId)
        {
            var entity = GetExamDietEntity(userId);           ;
            return Mapper.Map<ExamDiet, ExamDietModel>(entity);
        }

    }
}
 
