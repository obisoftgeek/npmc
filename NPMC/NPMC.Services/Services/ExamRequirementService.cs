﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.Utilities;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IExamRequirementService
    {

        void Create(ExamRequirementModel model, int currentUserId);
        
        void Delete(int id,int currentUserId);
        void Update(ExamRequirementModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup();
        CountModel<ExamRequirementItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null);
        IEnumerable<ExamRequirementItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null);
        ExamRequirementItem GetItem(int id);
        ExamRequirementDetail GetDetail(int id);
        ExamRequirementModel GetExamRequirement(int userId);

        IEnumerable<ExamRequirementItem> GetExamRequirements(int examId);


    }
    public class ExamRequirementService : IExamRequirementService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IExamRequirementRepository _ExamRequirementRepository;

        private readonly IAuditTrailService _auditTrailService;
        public ExamRequirementService(IUnitOfWorkAsync unitOfWork, IExamRequirementRepository ExamRequirementRepository, IAuditTrailService auditTrailService)
        {
            _unitOfWork = unitOfWork;
            _ExamRequirementRepository = ExamRequirementRepository;
            _auditTrailService = auditTrailService;
        }
        public IEnumerable<LookupModel> GetLookup()
        {
            var filterBy = new ExamRequirementFilter { };
            var entities = _ExamRequirementRepository.GetExamRequirementFilteredQueryable(filterBy);
           
            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }
     
        public void Create(ExamRequirementModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<ExamRequirementModel, ExamRequirement>(model);
                entity.DateCreated = DateTime.Now;
                _ExamRequirementRepository.Insert(entity);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Exam requirement - {0} created", model.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetExamRequirementEntity(id);
                var entityName = entity.Name;
                _ExamRequirementRepository.Delete(entity);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Exam requirement - {0} deleted", entityName), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        public ExamRequirementDetail GetDetail(int id)
        {
            var entity = GetExamRequirementEntity(id);
            return Mapper.Map<ExamRequirement, ExamRequirementDetail>(entity);
        }

        public ExamRequirementItem GetItem(int id)
        {
            var entity = GetExamRequirementEntity(id);
            return Mapper.Map<ExamRequirement, ExamRequirementItem>(entity);
        }
        private static IEnumerable<ExamRequirementItem> ProcessQuery(IEnumerable<ExamRequirement> entities)
        {
            return Mapper.Map<IEnumerable<ExamRequirement>, IEnumerable<ExamRequirementItem>>(entities);
        }
        public CountModel<ExamRequirementItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            int totalCount;
            var filterBy = ExamRequirementFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _ExamRequirementRepository.GetExamRequirementPaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<ExamRequirementItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<ExamRequirementItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            var filterBy = ExamRequirementFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _ExamRequirementRepository.GetExamRequirementPaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(ExamRequirementModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var userProfile = GetExamRequirementEntity(model.Id);
                Mapper.Map(model, userProfile);
                _ExamRequirementRepository.Update(userProfile);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Exam requirement - {0} updated", model.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private ExamRequirement GetExamRequirementEntity(int id)
        {
            return _ExamRequirementRepository.Find(id);

        }
        public ExamRequirementModel GetExamRequirement(int userId)
        {
            var entity = GetExamRequirementEntity(userId);           ;
            return Mapper.Map<ExamRequirement, ExamRequirementModel>(entity);
        }

        public IEnumerable<ExamRequirementItem> GetExamRequirements(int examId)
        {
            var entities = _ExamRequirementRepository.Table.Where(c => c.ExamId == examId);
            return ProcessQuery(entities);
        }


    }
}
 
