﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IWorkshopService
    {

        void Create(WorkshopModel model, int currentUserId);
        
        void Delete(int id);
        void Update(WorkshopModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup();
        CountModel<WorkshopItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null);
        IEnumerable<WorkshopItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null);
     
        WorkshopItem GetDetail(int id);
        WorkshopModel GetWorkshop(int userId);
      

    }
    public class WorkshopService : IWorkshopService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IWorkshopRepository _WorkshopRepository;
    

        public WorkshopService(IUnitOfWorkAsync unitOfWork, IWorkshopRepository WorkshopRepository)
        {
            _unitOfWork = unitOfWork;
            _WorkshopRepository = WorkshopRepository;
           
        }
        public IEnumerable<LookupModel> GetLookup()
        {
            var filterBy = new WorkshopFilter {IsApproved = true};
            var entities = _WorkshopRepository.GetWorkshopFilteredQueryable(filterBy);
           
            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }
     
        public void Create(WorkshopModel model, int currentUserId)
        {
            try
            {
                model.CreatedBy = currentUserId;
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<WorkshopModel, Workshop>(model);
                entity.CreatedDate = DateTime.UtcNow;
                _WorkshopRepository.Insert(entity);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetWorkshopEntity(id);
                _WorkshopRepository.Delete(entity);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        public WorkshopItem GetDetail(int id)
        {
            var entity = GetWorkshopEntity(id);
            return Mapper.Map<Workshop, WorkshopItem>(entity);
        }

        
        private static IEnumerable<WorkshopItem> ProcessQuery(IEnumerable<Workshop> entities)
        {
            return entities.Select(c => new WorkshopItem()
            {
                Id=c.Id,
                Name = c.Name,
                CreatedDate = c.CreatedDate,
                CreatedBy = c.CreatedBy,
                ApprovedBy = c.ApprovedBy,
                Description = c.Description,
                StartDate = c.StartDate,
                EndDate = c.EndDate,
                IsApproved = c.IsApproved,
                AmountPayable = c.PaymentFee.Amount,Venue = c.Venue,
                PaymentFeeId = c.PaymentFeeId
            });
        }
        public CountModel<WorkshopItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            int totalCount;
            var filterBy = WorkshopFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _WorkshopRepository.GetWorkshopPaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<WorkshopItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<WorkshopItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            var filterBy = WorkshopFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _WorkshopRepository.GetWorkshopPaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(WorkshopModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var workshop = GetWorkshopEntity(model.Id);
                if (workshop == null)
                {
                    throw new Exception("workshop not found");
                }
                Mapper.Map(model, workshop);
                _WorkshopRepository.Update(workshop);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private Workshop GetWorkshopEntity(int id)
        {
            return _WorkshopRepository.Find(id);

        }
        public WorkshopModel GetWorkshop(int userId)
        {
            var entity = GetWorkshopEntity(userId);           ;
            return Mapper.Map<Workshop, WorkshopModel>(entity);
        }

    }
}
