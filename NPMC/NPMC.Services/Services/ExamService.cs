﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.Utilities;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IExamService
    {

        void Create(ExamModel model, int currentUserId);
        
        void Delete(int id, int currentUserId);
        void Update(ExamModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup();
        CountModel<ExamItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null);
        IEnumerable<ExamItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null);
        ExamItem GetDetail(int id);
        ExamModel GetById(int id);
        void ToggleActive(int id, int currentUserId);
    }
    public class ExamService : IExamService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IExamRepository _examRepository;
        private readonly IAuditTrailService _auditTrailService;

        public ExamService(IUnitOfWorkAsync unitOfWork, IExamRepository examRepository, IAuditTrailService auditTrailService)
        {
            _unitOfWork = unitOfWork;
            _examRepository = examRepository;
            _auditTrailService = auditTrailService;
        }
        public IEnumerable<LookupModel> GetLookup()
        {
            var filterBy = new ExamFilter { };
            var entities = _examRepository.GetExamFilteredQueryable(filterBy);
           
            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }
     
        public void Create(ExamModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<ExamModel, Exam>(model);
                entity.IsActive = true;
                _examRepository.Insert(entity);
                _unitOfWork.Commit();
                ProcessExamStages(new List<ExamStage>(), model.StageModels, entity.Id);
                ProcessExamTypes(new List<ExamType>(), model.TypeModels, entity.Id);
                _auditTrailService.Log(String.Format("Exam - {0} created", model.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);

            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }

        private void ProcessExamTypes(List<ExamType> entities, List<ExamTypeModel> typeModels, int examId)
        {
            var modelIds = typeModels.Select(c => c.Id).ToList();
            var examTypeRepository = _examRepository.GetRepository<ExamType>();

            var entitiesToDelete = entities.Where(x => !modelIds.Contains(x.Id));
            examTypeRepository.DeleteRange(entitiesToDelete);

            var entitiesToAdd = typeModels.Where(x => x.Id == 0).Select(c => new ExamType()
            {
                Description=c.Description,
                ExamId=examId,
                IsActive=c.IsActive,
                Name=c.Name,
                CreatedDate=DateTime.Now
            }).ToList();

            examTypeRepository.InsertRange(entitiesToAdd);

            var entitiesToUpdate = entities.Where(x => modelIds.Contains(x.Id));
            foreach(var entity in entitiesToUpdate)
            {
                var typeModel = typeModels.Single(x => x.Id == entity.Id);
                entity.Name = typeModel.Name;
                entity.Description = typeModel.Description;
                entity.IsActive = typeModel.IsActive;                
            }
            examTypeRepository.UpdateRange(entitiesToUpdate);

        }

        private void ProcessExamStages(List<ExamStage> entities, List<ExamStageModel> stageModels, int examId)
        {
            var modelIds = stageModels.Select(c => c.Id).ToList();
            var examStageRepository = _examRepository.GetRepository<ExamStage>();

            var entitiesToDelete = entities.Where(x => !modelIds.Contains(x.Id));
            examStageRepository.DeleteRange(entitiesToDelete);

            var examStageCenterRepository=_examRepository.GetRepository<ExamStageCenter>();
            foreach (var model in stageModels)
            {
                if (model.Id == 0)
                {
                    var stage = new ExamStage()
                    {
                        ExamId = examId,
                        Name = model.Name,
                        DateCreated = DateTime.Now,
                        IsActive = model.IsActive,
                        IsApproved = true
                    };
                    examStageRepository.Insert(stage);
                    var centers = model.ExamCenterIds.Select(d => new ExamStageCenter()
                    {
                        ExamCenterId = d,
                        ExamStageId = stage.Id
                    }).ToList();

                    examStageCenterRepository.InsertRange(centers);
                }
            } 

            var entitiesToUpdate = entities.Where(c => modelIds.Contains(c.Id));
            foreach(var entity in entitiesToUpdate)
            {
                var examStage = stageModels.Single(s => s.Id == entity.Id);
                entity.Name = examStage.Name;
                entity.IsActive = examStage.IsActive;
                examStageRepository.Update(entity);
                var centersToupdate = examStage.ExamCenterIds.Select(d => d).ToList();
                foreach(var cnt in centersToupdate)
                {
                    var center = examStageCenterRepository.Find(c=> c.ExamStageId==entity.Id && c.ExamCenterId==cnt);
                    if (center == null)
                    {
                        center = new ExamStageCenter()
                        {
                            ExamCenterId = cnt,
                            ExamStageId = entity.Id
                        };
                        examStageCenterRepository.Insert(center);
                    }
                    else
                    {
                        center.ExamCenterId = cnt;
                        examStageCenterRepository.Update(center);
                    }
                }                
                
            }
        }

        public void Delete(int id, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetExamEntity(id);
                var entityName = entity.Name;
                _examRepository.Delete(entity);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Exam - {0} deleted", entityName), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public ExamModel GetById(int id)
        {
            var entity = GetExamEntity(id);
            var item = Mapper.Map<Exam, ExamModel>(entity);
            item.StageModels = entity.ExamStages.Select(c => new ExamStageModel()
            {
                ExamId = c.ExamId,
                Id = c.Id,
                IsActive = c.IsActive,
                IsApproved = c.IsApproved,
                Name = c.Name,
                ExamCenterIds = c.ExamStageCenters.Select(s => s.ExamCenterId).ToList()
            }).ToList();

            item.TypeModels = Mapper.Map<IEnumerable<ExamType>, List<ExamTypeModel>>(entity.ExamTypes);
            return item;
        }
        public ExamItem GetDetail(int id)
        {
            var entity = GetExamEntity(id);
            var item= Mapper.Map<Exam, ExamItem>(entity);
            item.Stages = entity.ExamStages.Select(c => new ExamStageItem()
            {
                ExamId=c.ExamId,
                Id=c.Id,
                IsActive=c.IsActive,
                IsApproved = c.IsApproved,
                Name =c.Name,
                 Centers = c.ExamStageCenters.Select(s=>new ExamStageCenterItem()
                 {
                     ExamCenterId = s.ExamCenterId,
                     Id =s.Id,
                     ExamCenterName =s.ExamCenter.Name,
                     CenterAddress =s.ExamCenter.Address,
                     CenterState =s.ExamCenter.State.Name
                 }).ToList()
                 
            }).ToList();
            item.TypeModels = Mapper.Map<IEnumerable<ExamType>, List<ExamTypeModel>>(entity.ExamTypes);
            return item;
        }

        private static IEnumerable<ExamItem> ProcessQuery(IEnumerable<Exam> entities)
        {
            return Mapper.Map<IEnumerable<Exam>, IEnumerable<ExamItem>>(entities);
        }
        public CountModel<ExamItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            int totalCount;
            var filterBy = ExamFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _examRepository.GetExamPaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<ExamItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<ExamItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            var filterBy = ExamFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _examRepository.GetExamPaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(ExamModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetExamEntity(model.Id);
                Mapper.Map(model, entity);
                _examRepository.Update(entity);
                _unitOfWork.Commit();
                ProcessExamStages(entity.ExamStages.ToList(), model.StageModels, entity.Id);
                ProcessExamTypes(entity.ExamTypes.ToList(), model.TypeModels, entity.Id);
                _auditTrailService.Log(String.Format("Exam - {0} updated", model.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private Exam GetExamEntity(int id)
        {
            return _examRepository.Find(id);

        }
      

        public void ToggleActive(int id, int currentUserId)
        {
            var entity = GetExamEntity(id);

            entity.IsActive = !entity.IsActive;
            _examRepository.Update(entity);
            var action = entity.IsActive ? " activated" : " deactivated";
            _auditTrailService.Log(String.Format("Exam - {0} {1}", entity.Name, action), currentUserId,
                AuditConstants.ApprovalProcessSection, AuditConstants.ApprovalProcessAction);
        }
    }
}
 
