﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.Utilities;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IPaymentFeeService
    {

        void Create(PaymentFeeModel model, int currentUserId);
        
        void Delete(int id, int currentUserId);
        void Update(PaymentFeeModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup(PaymentFeeFilter filter);
        CountModel<PaymentFeeItem> GetCount(int page, int count, PaymentFeeFilter filter = null, string orderByExpression = null);
        IEnumerable<PaymentFeeItem> GetQuery(int page, int count, PaymentFeeFilter filter = null, string orderByExpression = null); 
        PaymentFeeItem GetDetail(int id);
        PaymentFeeModel GetById(int userId);
      

    }
    public class PaymentFeeService : IPaymentFeeService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IPaymentFeeRepository _paymentFeeRepository;

        private readonly IAuditTrailService _auditService;
        public PaymentFeeService(IUnitOfWorkAsync unitOfWork, IPaymentFeeRepository paymentFeeRepository, IAuditTrailService auditService)
        {
            _unitOfWork = unitOfWork;
            _paymentFeeRepository = paymentFeeRepository;
            _auditService = auditService;
        }
        public IEnumerable<LookupModel> GetLookup(PaymentFeeFilter filter)
        {
            var filterBy = new PaymentFeeFilter {IsActive = true};
            var entities = _paymentFeeRepository.GetPaymentFeeFilteredQueryable(filterBy);
           
            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = String.Format("{0} ({1} {2})",x.Name,StringUtility.NairaSymbol,x.Amount)
            });
        }
     
        public void Create(PaymentFeeModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<PaymentFeeModel, PaymentFee>(model);
                entity.DateCreated = DateTime.Now;
                _paymentFeeRepository.Insert(entity);
                _unitOfWork.Commit();
                _auditService.Log(String.Format("Payment Fee -{0} created ", model.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetPaymentFeeEntity(id);
                var entityName = entity.Name;
                _paymentFeeRepository.Delete(entity);
                _unitOfWork.Commit();
                _auditService.Log(String.Format("Payment Fee -{0} deleted ", entityName), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        public PaymentFeeItem GetDetail(int id)
        {
            var entity = GetPaymentFeeEntity(id);
            return Mapper.Map<PaymentFee, PaymentFeeItem>(entity);
        }

         
        private static IEnumerable<PaymentFeeItem> ProcessQuery(IEnumerable<PaymentFee> entities)
        {
            return entities.Select(c => new PaymentFeeItem()
            {
                Id=c.Id,
                Name=c.Name,
                DateCreated = c.DateCreated,
                AccountId = c.AccountId,
                Amount=c.Amount,
                FormattedAmount = String.Format("{0}{1}",StringUtility.NairaSymbol,c.Amount),
                IsApproved = c.IsApproved,
                AprovedBy = c.AprovedBy,
                DueDay = c.DueDay,
                DueMonth = c.DueMonth,
                Description = c.Description,
                IsActive = c.IsActive,
                PaymentCategoryId = c.PaymentCategoryId
            });
        }
        public CountModel<PaymentFeeItem> GetCount(int page, int count, PaymentFeeFilter filter = null, string orderByExpression = null)
        {
            int totalCount;
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _paymentFeeRepository.GetPaymentFeePaged(page, count, out totalCount, filter, orderBy);

            return new CountModel<PaymentFeeItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<PaymentFeeItem> GetQuery(int page, int count, PaymentFeeFilter filter = null, string orderByExpression = null)
        {
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _paymentFeeRepository.GetPaymentFeePaged(page, count, filter, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(PaymentFeeModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var paymentFee = GetPaymentFeeEntity(model.Id);
                if (paymentFee == null)
                    throw new Exception("Payment Fee not found!");

                Mapper.Map(model, paymentFee);
                _paymentFeeRepository.Update(paymentFee);
                _unitOfWork.Commit();
                _auditService.Log(String.Format("Payment Fee -{0} updated ", model.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private PaymentFee GetPaymentFeeEntity(int id)
        {
            return _paymentFeeRepository.Find(id);
        }
        public PaymentFeeModel GetById(int id)
        {
            var entity = GetPaymentFeeEntity(id);           ;
            return Mapper.Map<PaymentFee, PaymentFeeModel>(entity);
        }

    }
}
