﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.Utilities;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IExamCenterService
    {

        void Create(ExamCenterModel model, int currentUserId);
        
        void Delete(int id, int currentUserId);
        void Update(ExamCenterModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup(ExamCenterFilter filter=null);
        CountModel<ExamCenterItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null);
        IEnumerable<ExamCenterItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null); 
        ExamCenterItem GetDetail(int id);
        ExamCenterModel GetById(int id);
        void ToggleActive(int id, int v);
    }
    public class ExamCenterService : IExamCenterService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IExamCenterRepository _ExamCenterRepository;
        private readonly IAuditTrailService _auditService;

        public ExamCenterService(IUnitOfWorkAsync unitOfWork, IExamCenterRepository ExamCenterRepository, IAuditTrailService auditService)
        {
            _unitOfWork = unitOfWork;
            _ExamCenterRepository = ExamCenterRepository;
            _auditService = auditService;
        }
        public IEnumerable<LookupModel> GetLookup(ExamCenterFilter filter=null)
        {
            var entities = _ExamCenterRepository.GetExamCenterFilteredQueryable(filter);
           
            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }
     
        public void Create(ExamCenterModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<ExamCenterModel, ExamCenter>(model);
                entity.DateCreated = DateTime.Now;
                entity.IsActive = true;
                _ExamCenterRepository.Insert(entity);
                _unitOfWork.Commit();
                _auditService.Log(String.Format("Exam Center {0} created ",model.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id,int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetExamCenterEntity(id);
                var ExamCenterName = entity.Name;
                _ExamCenterRepository.Delete(entity);
                _unitOfWork.Commit();
                 
                _auditService.Log(String.Format("Exam Center {0} deleted ", ExamCenterName), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        public ExamCenterItem GetDetail(int id)
        {
            var entity = GetExamCenterEntity(id);
            return Mapper.Map<ExamCenter, ExamCenterItem>(entity);
        }

         
        private static IEnumerable<ExamCenterItem> ProcessQuery(IEnumerable<ExamCenter> entities)
        {
            return entities.Select(c => new ExamCenterItem()
            {
                Id=c.Id,
                Name=c.Name, 
                Address=c.Address,
                StateName=c.State.Name ,
                IsActive=c.IsActive,
                IsApproved=c.IsApproved
            });
        }
        public CountModel<ExamCenterItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            int totalCount;
            var filterBy = ExamCenterFilter.Deserialize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _ExamCenterRepository.GetExamCenterPaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<ExamCenterItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<ExamCenterItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            var filterBy = ExamCenterFilter.Deserialize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _ExamCenterRepository.GetExamCenterPaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(ExamCenterModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var ExamCenter = GetExamCenterEntity(model.Id);
                if (ExamCenter == null)
                    throw new Exception("Exam Center not found!");

                Mapper.Map(model, ExamCenter);
                _ExamCenterRepository.Update(ExamCenter);
                _unitOfWork.Commit();

                _auditService.Log(String.Format("Exam Center updated from {0} to {1} ", model.Name,ExamCenter.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private ExamCenter GetExamCenterEntity(int id)
        {
            return _ExamCenterRepository.Find(id);
        }
        public ExamCenterModel GetById(int id)
        {
            var entity = GetExamCenterEntity(id);           ;
            return Mapper.Map<ExamCenter, ExamCenterModel>(entity);
        }

        public void ToggleActive(int id, int currentUserId)
        {
            var entity = GetExamCenterEntity(id);

            entity.IsActive = !entity.IsActive;
            _ExamCenterRepository.Update(entity);
            var action = entity.IsActive ? " activated" : " deactivated";
            _auditService.Log(String.Format("Exam Center - {0} {1}",entity.Name, action), currentUserId,
                AuditConstants.ApprovalProcessSection, AuditConstants.ApprovalProcessAction);
        }
    }
}
