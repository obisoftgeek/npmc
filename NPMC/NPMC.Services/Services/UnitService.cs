﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IUnitService
    {

        void Create(UnitModel model, int currentUserId);
        
        void Delete(int id);
        void Update(UnitModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup(int departmentId);
        CountModel<UnitItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null);
        IEnumerable<UnitItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null);
        UnitItem GetItem(int id);
        UnitDetail GetDetail(int id);
        UnitModel GetUnit(int userId);
      

    }
    public class UnitService : IUnitService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IUnitRepository _departmentRepository;
    

        public UnitService(IUnitOfWorkAsync unitOfWork, IUnitRepository departmentRepository)
        {
            _unitOfWork = unitOfWork;
            _departmentRepository = departmentRepository;
           
        }
        public IEnumerable<LookupModel> GetLookup(int departmentId)
        {
            var entities = _departmentRepository.GetUnitFilteredQueryable(new UnitFilter() { DepartmentId = departmentId });
           
            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }
     
        public void Create(UnitModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<UnitModel, Unit>(model);
                _departmentRepository.Insert(entity);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetUnitEntity(id);
                _departmentRepository.Delete(entity);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        public UnitDetail GetDetail(int id)
        {
            var entity = GetUnitEntity(id);
            return Mapper.Map<Unit, UnitDetail>(entity);
        }

        public UnitItem GetItem(int id)
        {
            var entity = GetUnitEntity(id);
            return Mapper.Map<Unit, UnitItem>(entity);
        }
        private static IEnumerable<UnitItem> ProcessQuery(IEnumerable<Unit> entities)
        {
            return Mapper.Map<IEnumerable<Unit>, IEnumerable<UnitItem>>(entities);
        }
        public CountModel<UnitItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            int totalCount;
            var filterBy = UnitFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _departmentRepository.GetUnitPaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<UnitItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<UnitItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            var filterBy = UnitFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _departmentRepository.GetUnitPaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(UnitModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var userProfile = GetUnitEntity(model.Id);
                Mapper.Map(model, userProfile);
                _departmentRepository.Update(userProfile);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private Unit GetUnitEntity(int id)
        {
            return _departmentRepository.Find(id);

        }
        public UnitModel GetUnit(int userId)
        {
            var entity = GetUnitEntity(userId);           ;
            return Mapper.Map<Unit, UnitModel>(entity);
        }

    }
}

