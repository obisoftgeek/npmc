﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.Utilities;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface ISubSpecialtyService
    {

        void Create(SubSpecialtyModel model, int currentUserId);
        
        void Delete(int id, int currentUserId);
        void Update(SubSpecialtyModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup(SubSpecialtyFilter filter = null);
        CountModel<SubSpecialtyItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null);
        IEnumerable<SubSpecialtyItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null); 
        SubSpecialtyItem GetDetail(int id);
        SubSpecialtyModel GetById(int id);
      

    }
    public class SubSpecialtyService : ISubSpecialtyService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly ISubSpecialtyRepository _subSpecialtyRepository;
        private readonly IAuditTrailService _auditService;

        public SubSpecialtyService(IUnitOfWorkAsync unitOfWork, ISubSpecialtyRepository SubSpecialtyRepository, IAuditTrailService auditService)
        {
            _unitOfWork = unitOfWork;
            _subSpecialtyRepository = SubSpecialtyRepository;
            _auditService = auditService;
        }
        public IEnumerable<LookupModel> GetLookup(SubSpecialtyFilter filter = null)
        {
             
            var entities = _subSpecialtyRepository.GetSubSpecialtyFilteredQueryable(filter);
           
            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }
     
        public void Create(SubSpecialtyModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<SubSpecialtyModel, SubSpecialty>(model);
                entity.DateCreated = DateTime.Now;
                entity.IsActive = true;
                _subSpecialtyRepository.Insert(entity);
                _unitOfWork.Commit();
                _auditService.Log(String.Format("Sub Specialty -{0} created ",model.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id,int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetSubSpecialtyEntity(id);
                var SubSpecialtyName = entity.Name;
                _subSpecialtyRepository.Delete(entity);
                _unitOfWork.Commit();
                 
                _auditService.Log(String.Format("Sub Specialty -{0} deleted ", SubSpecialtyName), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        public SubSpecialtyItem GetDetail(int id)
        {
            var entity = GetSubSpecialtyEntity(id);
            return Mapper.Map<SubSpecialty, SubSpecialtyItem>(entity);
        }

         
        private static IEnumerable<SubSpecialtyItem> ProcessQuery(IEnumerable<SubSpecialty> entities)
        {
            return entities.Select(c => new SubSpecialtyItem()
            {
                Id=c.Id,
                Name=c.Name, Description = c.Description,
                PaymentFeeId = c.PaymentFeeId,
                AmountPayable = c.PaymentFee.Amount,
                IsActive=c.IsActive,
                IsApproved=c.IsApproved,
                DateCreated = c.DateCreated ,
                FacultyName = c.Faculty.Name
            });
        }
        public CountModel<SubSpecialtyItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            int totalCount;
            var filterBy = SubSpecialtyFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _subSpecialtyRepository.GetSubSpecialtyPaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<SubSpecialtyItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<SubSpecialtyItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            var filterBy = SubSpecialtyFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _subSpecialtyRepository.GetSubSpecialtyPaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(SubSpecialtyModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var SubSpecialty = GetSubSpecialtyEntity(model.Id);
                if (SubSpecialty == null)
                    throw new Exception("Sub Specialty not found!");

                Mapper.Map(model, SubSpecialty);
                _subSpecialtyRepository.Update(SubSpecialty);
                _unitOfWork.Commit();

                var user = _subSpecialtyRepository.GetRepository<UserProfile>().Find(currentUserId);
                _auditService.Log(String.Format("Sub Specialty updated from {0} to {1} ", model.Name,SubSpecialty.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private SubSpecialty GetSubSpecialtyEntity(int id)
        {
            return _subSpecialtyRepository.Find(id);
        }
        public SubSpecialtyModel GetById(int id)
        {
            var entity = GetSubSpecialtyEntity(id);           ;
            return Mapper.Map<SubSpecialty, SubSpecialtyModel>(entity);
        }

    }
}
