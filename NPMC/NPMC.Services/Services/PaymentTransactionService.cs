﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models.Utilities;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IPaymentTransactionService
    {
        IEnumerable<PaymentTransactionModel> Query(int page, int count, PaymentTransactionFilter filter, string orderByExpression = null);
        CountModel<PaymentTransactionModel> GetCount(int page, int count, PaymentTransactionFilter filter, string orderByExpression = null);
        PaymentDetail GetPaymentDetails(int paymentId);
        void MakePayment(PayModel model); 
        PaymentTransactionItem GetById(int id);
        IList<PaymentTransactionItem> DownloadReport(PaymentTransactionFilter filter);
    }
    public class PaymentTransactionService : IPaymentTransactionService
    {
        private readonly IPaymentTransactionRepository _repository;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;
        private readonly IAuditTrailService _auditTrailService;
        public PaymentTransactionService(IPaymentTransactionRepository repository, IUnitOfWorkAsync unitOfWorkAsync, IAuditTrailService auditTrailService)
        {
            _repository = repository;
            _unitOfWorkAsync = unitOfWorkAsync;
            _auditTrailService = auditTrailService;
        }

        public IEnumerable<PaymentTransactionModel> Query(int page, int count, PaymentTransactionFilter filter, string orderByExpression = null)
        {
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _repository.GetPaymentTransactionPaged(page, count, filter, orderBy).ToList();
            return ProcessItemQuery(entities);
        }

        private static IEnumerable<PaymentTransactionItem> ProcessItemQuery(IEnumerable<PaymentTransaction> entities)
        {
            return entities.Select(c => new PaymentTransactionItem()
            {
                Id=c.Id,
                AccountId = c.AccountId,
                AccountName = c.Account!=null?c.Account.Name:String.Empty,
                Amount = c.Amount,AmountPaid=c.AmountPaid,
                HasPaid = c.HasPaid,
                Narration = c.Narration,
                PaymentDate = c.PaymentDate,
                PaymentType = c.PaymentType,
                ReferenceId = c.ReferenceId,
                UserProfileId = c.UserProfileId,
                Payee = String.Format("{0} {1}",c.UserProfile.FirstName,c.UserProfile.LastName)
            });
        }

        public CountModel<PaymentTransactionModel> GetCount(int page, int count, PaymentTransactionFilter filter, string orderByExpression = null)
        {
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            int totalCount;
            var entities = _repository.GetPaymentTransactionPaged(page, count, out totalCount,filter, orderBy).ToList();
            return new CountModel<PaymentTransactionModel>()
            {
                Total = totalCount,
                Items = ProcessItemQuery(entities)
            };
        }

        public PaymentDetail GetPaymentDetails(int paymentId)
        {
            _unitOfWorkAsync.BeginTransaction();
           
            var paymentTransanction = _repository.Find(paymentId);
            if (paymentTransanction == null) return new PaymentDetail();

            var payee = paymentTransanction.UserProfile;
            var paymentDetail = new PaymentDetail()
            {
                Name = String.Format("{0} {1}", payee.FirstName, payee.LastName),
                PaymentDate = paymentTransanction.PaymentDate,
                EmailAddress = payee.AspNetUser.Email,
                PhoneNumber = payee.AspNetUser.PhoneNumber,
                InvoiceId = paymentTransanction.InvoiceId ?? GenerateInvoice()
            };
            paymentTransanction.InvoiceId = paymentDetail.InvoiceId;
            //Update the invoiceId of the payment transaction table
            _unitOfWorkAsync.GetRepository<PaymentTransaction>().Update(paymentTransanction);

            var paymentTransactionModel = new PaymentBreakDown()
            {
                Description = "Payment Description", //TODO fill in description
                Quantity = 1,
                UnitCost = paymentTransanction.Amount,
                Title = "Payment Title" //TODO fill in title
            };
            paymentDetail.Payments.Add(paymentTransactionModel);
            paymentDetail.Total = paymentTransanction.Amount;
            return paymentDetail;
        }

        private static string GenerateInvoice()
        {
            return string.Format("{0:d9}", new Random().Next(1, 100000000));
        }

        public void MakePayment(PayModel model)
        {
            try
            {
                _unitOfWorkAsync.BeginTransaction();
                var paymentTransaction = _repository.Find(model.ItemId);
                paymentTransaction.PaymentDate = DateTime.Now;
                paymentTransaction.HasPaid = true;
                paymentTransaction.AmountPaid = model.AmountPaid;
                paymentTransaction.ReferenceId = model.ReferenceId;
                _repository.Update(paymentTransaction);

                var debtor = _repository.GetRepository<UserProfile>().Find(paymentTransaction.UserProfileId);
//                var paymentNotification = new PaymentNotificationModel
//                {
//                    Payee = debtor.FirstName + " " + debtor.LastName,
//                    EmailAddress = debtor.AspNetUser.Email,
//                    PhoneNumber = debtor.AspNetUser.PhoneNumber,
//                    Total = paymentTransaction.Amount,
//                    InvoiceId = paymentTransaction.InvoiceId,
//                    Item = "Item paid for",//TODO fill in this
//                    PaymentDate = paymentTransaction.PaymentDate.Value,
//                    ReferenceId = paymentTransaction.ReferenceId
//                };
                _unitOfWorkAsync.Commit();
                 
                _auditTrailService.Log("Payment Made", debtor.AspNetUserId,
                     AuditConstants.PaymentSection, AuditConstants.PaymentAction);
                 //TODO send payment email notification
            }
            catch (Exception)
            {
                _unitOfWorkAsync.Rollback();
                throw;
            }
        }


        public PaymentTransactionItem GetById(int id)
        {
            return Mapper.Map<PaymentTransaction, PaymentTransactionItem>(_repository.Find(id));
        }

        public IList<PaymentTransactionItem> DownloadReport(PaymentTransactionFilter filter)
        {
            return ProcessItemQuery(_repository.GetPaymentTransactionFilteredQueryable(filter)).ToList();
        }
    }

    public class PayModel
    {
        public int ItemId { get; set; }
        public decimal AmountPaid { get; set; }
        public string ReferenceId { get; set; }
    }
}
