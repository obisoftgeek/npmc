﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.Utilities;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IDepartmentService
    {

        void Create(DepartmentModel model, int currentUserId);
        
        void Delete(int id, int currentUserId);
        void Update(DepartmentModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup();
        CountModel<DepartmentItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null);
        IEnumerable<DepartmentItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null);
        DepartmentItem GetItem(int id);
        DepartmentDetail GetDetail(int id);
        DepartmentModel GetDepartment(int userId);
      

    }
    public class DepartmentService : IDepartmentService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IAuditTrailService _auditTrailService;

        public DepartmentService(IUnitOfWorkAsync unitOfWork, IDepartmentRepository departmentRepository, IAuditTrailService auditTrailService)
        {
            _unitOfWork = unitOfWork;
            _departmentRepository = departmentRepository;
            _auditTrailService = auditTrailService;
        }
        public IEnumerable<LookupModel> GetLookup()
        {
            var filterBy = new DepartmentFilter {IsActive = true};
            var entities = _departmentRepository.GetDepartmentFilteredQueryable(filterBy);
           
            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }
     
        public void Create(DepartmentModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<DepartmentModel, Department>(model);
                
                entity.IsActive = true;
                entity.DateCreated = DateTime.Now;
                _departmentRepository.Insert(entity);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Department {0} created ", model.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetDepartmentEntity(id);
                var entityName = entity.Name;

                _departmentRepository.Delete(entity);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Department {0} deleted ", entityName), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        public DepartmentDetail GetDetail(int id)
        {
            var entity = GetDepartmentEntity(id);
            return Mapper.Map<Department, DepartmentDetail>(entity);
        }

        public DepartmentItem GetItem(int id)
        {
            var entity = GetDepartmentEntity(id);
            return Mapper.Map<Department, DepartmentItem>(entity);
        }
        private static IEnumerable<DepartmentItem> ProcessQuery(IEnumerable<Department> entities)
        {
            return Mapper.Map<IEnumerable<Department>, IEnumerable<DepartmentItem>>(entities);
        }
        public CountModel<DepartmentItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            int totalCount; 
            var filterBy = new DepartmentFilter { IsActive = true }; 
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _departmentRepository.GetDepartmentPaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<DepartmentItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<DepartmentItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null)
        { 
            var filterBy = new DepartmentFilter { IsActive = true }; 
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _departmentRepository.GetDepartmentPaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(DepartmentModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var userProfile = GetDepartmentEntity(model.Id);
                Mapper.Map(model, userProfile);
                _departmentRepository.Update(userProfile);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Department {0} updated ", model.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private Department GetDepartmentEntity(int id)
        {
            return _departmentRepository.Find(id);

        }
        public DepartmentModel GetDepartment(int userId)
        {
            var entity = GetDepartmentEntity(userId);           ;
            return Mapper.Map<Department, DepartmentModel>(entity);
        }

    }
}
 
 
