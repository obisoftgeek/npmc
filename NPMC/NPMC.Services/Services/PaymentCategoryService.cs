﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.Utilities;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IPaymentCategoryService
    {

        void Create(PaymentCategoryModel model, int currentUserId);
        
        void Delete(int id, int currentUserId);
        void Update(PaymentCategoryModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup(PaymentCategoryFilter filter=null);
        CountModel<PaymentCategoryItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null);
        IEnumerable<PaymentCategoryItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null); 
        PaymentCategoryItem GetDetail(int id);
        PaymentCategoryModel GetById(int id);
      

    }
    public class PaymentCategoryService : IPaymentCategoryService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IPaymentCategoryRepository _paymentCategoryRepository;
        private readonly IAuditTrailService _auditTrailService;

        public PaymentCategoryService(IUnitOfWorkAsync unitOfWork, IPaymentCategoryRepository paymentCategoryRepository, IAuditTrailService auditService)
        {
            _unitOfWork = unitOfWork;
            _paymentCategoryRepository = paymentCategoryRepository;
            _auditTrailService = auditService;
        }
        public IEnumerable<LookupModel> GetLookup(PaymentCategoryFilter filter=null)
        {
            var entities = _paymentCategoryRepository.GetPaymentCategoryFilteredQueryable(filter);
            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }
     
        public void Create(PaymentCategoryModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<PaymentCategoryModel, PaymentCategory>(model);
                entity.DateCreated = DateTime.Now;
                entity.CreatedBy = currentUserId;
                entity.IsActive = true;
                _paymentCategoryRepository.Insert(entity);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Payment Category -{0} created ", model.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetPaymentCategoryEntity(id);
                var entityName = entity.Name;
                _paymentCategoryRepository.Delete(entity);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Payment category -{0} deleted ", entityName), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        public PaymentCategoryItem GetDetail(int id)
        {
            var entity = GetPaymentCategoryEntity(id);
            return Mapper.Map<PaymentCategory, PaymentCategoryItem>(entity);
        }

         
        private static IEnumerable<PaymentCategoryItem> ProcessQuery(IEnumerable<PaymentCategory> entities)
        {
            return Mapper.Map<IEnumerable<PaymentCategory>, IEnumerable<PaymentCategoryItem>>(entities);
        }
        public CountModel<PaymentCategoryItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            int totalCount;
            var filterBy = PaymentCategoryFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _paymentCategoryRepository.GetPaymentCategoryPaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<PaymentCategoryItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<PaymentCategoryItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            var filterBy = PaymentCategoryFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _paymentCategoryRepository.GetPaymentCategoryPaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(PaymentCategoryModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var paymentCategory = GetPaymentCategoryEntity(model.Id);
                if (paymentCategory == null)
                    throw new Exception("Payment Category not found!");

                Mapper.Map(model, paymentCategory);
                _paymentCategoryRepository.Update(paymentCategory);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Payment Category -{0} updated ", model.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private PaymentCategory GetPaymentCategoryEntity(int id)
        {
            return _paymentCategoryRepository.Find(id);
        }
        public PaymentCategoryModel GetById(int id)
        {
            var entity = GetPaymentCategoryEntity(id);           ;
            return Mapper.Map<PaymentCategory, PaymentCategoryModel>(entity);
        }

    }
}
