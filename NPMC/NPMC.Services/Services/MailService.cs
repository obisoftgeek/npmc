﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.Utilities;
using SilverEdgeProjects.NPMC.Models.ViewModels;
using System.IO;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IMailService
    {

        void Create(MailModel model, int currentUserId);
        
        void Delete(int id, int currentUserId);
        CountModel<MailItem> GetCount(int page, int count, MailFilter filter = null, string orderByExpression = null);
        IEnumerable<MailItem> GetQuery(int page, int count, MailFilter filter = null, string orderByExpression = null); 
        MailItem GetDetail(int id);
        MailModel GetById(int id);
        void UpdateToReceived(MailModel model, int currentUserId);
      

    }
    public class MailService : IMailService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IMailRepository _MailRepository;
        private readonly IAuditTrailService _auditService;

        public MailService(IUnitOfWorkAsync unitOfWork, IMailRepository MailRepository, IAuditTrailService auditService)
        {
            _unitOfWork = unitOfWork;
            _MailRepository = MailRepository;
            _auditService = auditService;
        }
       
        public void Create(MailModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<MailModel, Mail>(model);
                entity.DateCreated = DateTime.Now;
                entity.StatusId = (int)MailReceiptStatus.Dispatched;
                _MailRepository.Insert(entity);
                _unitOfWork.Commit();
                _auditService.Log(String.Format("Document -  {0} dispatched ",model.Title), currentUserId, AuditConstants.DocumentManagementSection, AuditConstants.DocumentManagementAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id,int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetMailEntity(id);
                var title = entity.Title;
                _MailRepository.Delete(entity);
                _unitOfWork.Commit();
                 
                _auditService.Log(String.Format("Document -  {0} deleted ", title), currentUserId, AuditConstants.DocumentManagementSection, AuditConstants.DocumentManagementAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        public MailItem GetDetail(int id)
        {
            var entity = GetMailEntity(id);
            var item = Mapper.Map<Mail, MailItem>(entity);
            item.RecepientName = String.Format("{0} {1}", entity.UserProfile.FirstName, entity.UserProfile.LastName);
            item.UnitName = entity.UserProfile.Unit.Name;
            item.StatusName = MailReceiptStatusConstant.MailReceiptStatusNamesFromIdDictionary[item.StatusId];
            item.AttachmentModel = !String.IsNullOrEmpty(item.Attachment) ? item.Attachment.Split(',').Select(c => new AttachmentModel()
            {
                FileName = c,
                FileTypeName = MailAttachmentFileTypeConstant.MailAttachmentFileTypeNamesFromIdDictionary[GetFileTypeIdFromFileExtension(c)],
                FileTypeId = GetFileTypeIdFromFileExtension(c)

            }).ToList():new List<AttachmentModel>();
            item.HasAttachment = !String.IsNullOrEmpty(item.Attachment) ? item.Attachment.Split(',').Any() : false;
            return item;
        }

        private int GetFileTypeIdFromFileExtension(string c)
        {
            var ext = Path.GetExtension(c);
            switch (ext)
            {
                case ".jpg":
                case ".jpeg":
                case ".png":
                    return (int)MailAttachmentFileType.Image;
                case ".pdf": return (int)MailAttachmentFileType.Pdf;
                case ".xls":
                case ".xlsx": return (int)MailAttachmentFileType.Xls;
                case ".doc":
                case ".docx":
                    return (int)MailAttachmentFileType.Doc;
                default: return (int)MailAttachmentFileType.Image;
            }
        }

        private static IEnumerable<MailItem> ProcessQuery(IEnumerable<Mail> entities)
        {
            return entities.Select(c => new MailItem()
            {
                Id = c.Id,
                Title = c.Title,
                DateCreated = c.DateCreated,
                Attachment = c.Attachment,
                Description = c.Description,
                RecepientId = c.RecepientId,
                RecepientName = String.Format("{0} {1}", c.UserProfile.FirstName, c.UserProfile.LastName),
                UnitName = c.UserProfile.Unit.Name,
                Sender = c.Sender,
                SenderAddress = c.SenderAddress,
                StatusName = MailReceiptStatusConstant.MailReceiptStatusNamesFromIdDictionary[c.StatusId],
                StatusId = c.StatusId,
                HasAttachment = !String.IsNullOrEmpty(c.Attachment) ? c.Attachment.Split(',').Any() : false
            });
        }
        public CountModel<MailItem> GetCount(int page, int count, MailFilter filter = null, string orderByExpression = null)
        {
            int totalCount;
          var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _MailRepository.GetMailPaged(page, count, out totalCount, filter, orderBy);

            return new CountModel<MailItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<MailItem> GetQuery(int page, int count, MailFilter filter = null, string orderByExpression = null)
        {
            
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _MailRepository.GetMailPaged(page, count, filter, orderBy);
            return ProcessQuery(entities);
        }
                 
        public void UpdateToReceived(MailModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var mail = GetMailEntity(model.Id);
                if (mail == null)
                    throw new Exception("document not found!");

                Mapper.Map(model, mail);
                mail.StatusId = (int)MailReceiptStatus.Received;
                _MailRepository.Update(mail);
                _unitOfWork.Commit();

                _auditService.Log(String.Format("Document received by {0} {1} ", mail.UserProfile.FirstName, mail.UserProfile.LastName), currentUserId, AuditConstants.DocumentManagementSection, AuditConstants.DocumentManagementAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private Mail GetMailEntity(int id)
        {
            return _MailRepository.Find(id);
        }
        public MailModel GetById(int id)
        {
            var entity = GetMailEntity(id);           ;
            return Mapper.Map<Mail, MailModel>(entity);
        }

    }
}
