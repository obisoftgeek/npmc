﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;
using SilverEdgeProjects.NPMC.Models.Utilities;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IAccountService
    {

        void Create(AccountModel model, int currentUserId);
        
        void Delete(int id, int currentUserId);
        void Update(AccountModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup();
        CountModel<AccountItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null);
        IEnumerable<AccountItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null); 
        AccountItem GetDetail(int id);
        AccountModel GetById(int id);
      

    }
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IAccountRepository _accountRepository;
        private readonly IAuditTrailService _auditTrailService;

        public AccountService(IUnitOfWorkAsync unitOfWork, IAccountRepository accountRepository, IAuditTrailService auditTrailService)
        {
            _unitOfWork = unitOfWork;
            _accountRepository = accountRepository;
            _auditTrailService = auditTrailService;
        }
        public IEnumerable<LookupModel> GetLookup()
        {
            var filterBy = new AccountFilter {IsActive = true};
            var entities = _accountRepository.GetAccountFilteredQueryable(filterBy);
           
            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }
     
        public void Create(AccountModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<AccountModel, Account>(model);
                entity.IsActive = true;
                entity.DateCreated = DateTime.Now;
                _accountRepository.Insert(entity);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Account name - {0} - Account Number: {1} created ", model.Name, model.Number), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetAccountEntity(id);
                var account = Mapper.Map<Account, AccountModel>(entity); 
                _accountRepository.Delete(entity);
                _unitOfWork.Commit();

                _auditTrailService.Log(String.Format("Account name - {0} - Account Number: {1} deleted ", account.Name, account.Number), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        public AccountItem GetDetail(int id)
        {
            var entity = GetAccountEntity(id);
            return Mapper.Map<Account, AccountItem>(entity);
        }

         
        private static IEnumerable<AccountItem> ProcessQuery(IEnumerable<Account> entities)
        {
            return Mapper.Map<IEnumerable<Account>, IEnumerable<AccountItem>>(entities);
        }
        public CountModel<AccountItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            int totalCount;
            var filterBy = AccountFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _accountRepository.GetAccountPaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<AccountItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<AccountItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            var filterBy = AccountFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _accountRepository.GetAccountPaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(AccountModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var account = GetAccountEntity(model.Id);
                if (account == null)
                    throw new Exception("Account not found!");

                Mapper.Map(model, account);
                _accountRepository.Update(account);
                _unitOfWork.Commit();

                _auditTrailService.Log(String.Format("Account name - {0} - Account Number: {1} updated to {2} ", model.Name, model.Number, account.Number), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private Account GetAccountEntity(int id)
        {
            return _accountRepository.Find(id);
        }
        public AccountModel GetById(int id)
        {
            var entity = GetAccountEntity(id);           ;
            return Mapper.Map<Account, AccountModel>(entity);
        }

    }
}
