﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IStateService
    {

        void Create(StateModel model, int currentUserId);
        
        void Delete(int id);
        void Update(StateModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup();
        CountModel<StateItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null);
        IEnumerable<StateItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null);        
        StateItem GetDetail(int id);
        StateModel GetState(int userId);
        bool NameExist(StateModel state);
    }
    public class StateService : IStateService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IStateRepository _stateRepository;
    

        public StateService(IUnitOfWorkAsync unitOfWork, IStateRepository stateRepository)
        {
            _unitOfWork = unitOfWork;
            _stateRepository = stateRepository;
           
        }
        public IEnumerable<LookupModel> GetLookup()
        {
            var filterBy = new StateFilter {};
            var entities = _stateRepository.GetStateFilteredQueryable(filterBy);
           
            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }
     
        public void Create(StateModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<StateModel, State>(model);                
                _stateRepository.Insert(entity);
                _unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetStateEntity(id);
                _stateRepository.Delete(entity);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        
        public StateItem GetDetail(int id)
        {
            var entity = GetStateEntity(id);
            return Mapper.Map<State, StateItem>(entity);
        }
        private static IEnumerable<StateItem> ProcessQuery(IEnumerable<State> entities)
        {
            return Mapper.Map<IEnumerable<State>, IEnumerable<StateItem>>(entities);
        }
        public CountModel<StateItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            int totalCount;
            var filterBy = StateFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _stateRepository.GetStatePaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<StateItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<StateItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            
            var filterBy = StateFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _stateRepository.GetStatePaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(StateModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var userProfile = GetStateEntity(model.Id);
                Mapper.Map(model, userProfile);
                _stateRepository.Update(userProfile);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private State GetStateEntity(int id)
        {
            return _stateRepository.Find(id);

        }
        public StateModel GetState(int userId)
        {
            var entity = GetStateEntity(userId);           
            return Mapper.Map<State, StateModel>(entity);
        }

        public bool NameExist(StateModel state)
        {
            return _stateRepository.NameExist(state);

        }
    }
}
