﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using LinqKit;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IMemoBoxService
    {

        void Create(MemoModel model, int currentUserId, FolderType folderType);
        
        void Delete(int id);
        void Update(MemoModel model, int currentUserId, FolderType folderType);
        CountModel<MemoItem> GetCount(int page, int count, int currentUserId, string filterExpression = null, string orderByExpression = null);
        IEnumerable<MemoItem> GetQuery(int page, int count, int currentUserId, string filterExpression = null, string orderByExpression = null);
        MemoItem GetItem(int id);
        MemoDetail GetDetail(int id);
        MemoModel GetMemo(int userId);
        void UpdateMemoComment(MemoRecipientModel model, int currentUserId);


    }
    public class MemoBoxService : IMemoBoxService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IMemoBoxRepository _memoBoxRepository;
        private readonly IUserProfileService _userProfileService;
        private readonly IMemoRecipientRepository _memoRecipientRepository;

        public MemoBoxService(IUnitOfWorkAsync unitOfWork, IMemoBoxRepository memoBoxRepository, IUserProfileService userProfileService,
            IMemoRecipientRepository memoRecipientRepository)
        {
            _unitOfWork = unitOfWork;
            _memoBoxRepository = memoBoxRepository;
            _userProfileService = userProfileService;
            _memoRecipientRepository = memoRecipientRepository;
        }
        public void UpdateMemoComment(MemoRecipientModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = _memoRecipientRepository.Table.FirstOrDefault(x => x.MemoId == model.MemoId && x.RecipientId == currentUserId);
                if (entity != null)
                {
                    
                    _memoRecipientRepository.Update(entity);
                    _unitOfWork.Commit();
                }
               
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private List<int> GetUniqueRecipient(List<SelectedRecipientModel> recipientLists)
        {
            //removes duplicate recipient
            var recipients = new List<int>();
            recipientLists.ToList().ForEach(rep =>
            {
                if (recipients.Any(c => c == rep.Id))
                {
                    recipients.Remove(rep.Id);
                }
                else
                {
                    recipients.Add(rep.Id);
                }

            });
            return recipients;
        }
        public void Create(MemoModel model, int currentUserId, FolderType folderType)
        {
            try
            {              
                _unitOfWork.BeginTransaction();
                var userProfile = _userProfileService.GetUserProfile(currentUserId);
                var entity = Mapper.Map<MemoModel, Memo>(model);
                 
                entity.DateCreated = DateTime.Now;
                //entity.SenderId = currentUserId;
                entity.MemoStatusId = (int) MemoStatusEnum.Pending;
                entity.FolderType = (int)folderType;
                _memoBoxRepository.Insert(entity);

                if (folderType == FolderType.Sent)
                {
                    var recipients = GetUniqueRecipient(model.SelectedRecipients);
                    recipients.ForEach(recipientId =>
                    {
                        if (recipientId != currentUserId)
                        {
                            var memoRecipient = new MemoRecipient();
                            memoRecipient.MemoId = entity.Id;
                            memoRecipient.RecipientId = recipientId;
                            _memoRecipientRepository.Insert(memoRecipient);
                        }
                       
                    });
                }
              
               
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Update(MemoModel model, int currentUserId, FolderType folderType)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetMemoEntity(model.Id);
                //delete previous recipient
                var rec = entity.MemoRecipients.ToList();
                rec.ForEach(itemToDelete =>
                {
                    _memoRecipientRepository.Delete(itemToDelete);
                });

                Mapper.Map(model, entity);
                entity.FolderType = (int) folderType;
                _memoBoxRepository.Update(entity);

                if (folderType == FolderType.Sent)
                {
                    var recipients = GetUniqueRecipient(model.SelectedRecipients);
                    recipients.ForEach(recipientId =>
                    {
                        if (recipientId != currentUserId)
                        {
                            var memoRecipient = new MemoRecipient();
                            memoRecipient.MemoId = entity.Id;
                            memoRecipient.RecipientId = recipientId;
                            _memoRecipientRepository.Insert(memoRecipient);
                        }
                      
                    });
                }              
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public void Delete(int id)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetMemoEntity(id);
                _memoBoxRepository.Delete(entity);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        public MemoDetail GetDetail(int id)
        {
            var entity = GetMemoEntity(id);
            return Mapper.Map<Memo, MemoDetail>(entity);
        }

        public MemoItem GetItem(int id)
        {
            var entity = GetMemoEntity(id);
            return Mapper.Map<Memo, MemoItem>(entity);
        }
        private static IEnumerable<MemoItem> ProcessQuery(IEnumerable<Memo> entities)
        {
            return Mapper.Map<IEnumerable<Memo>, IEnumerable<MemoItem>>(entities).OrderByDescending(x => x.Id);
        }
        public CountModel<MemoItem> GetCount(int page, int count, int currentUserId, string filterExpression = null, string orderByExpression = null)
        {
            var userProfile = _userProfileService.GetUserProfile(currentUserId);
            int totalCount = 0;
            var filterBy = MemoFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);

            if (filterBy.FolderType == (int)FolderType.Sent)
            {
                filterBy.ThroughUnitId = userProfile.UnitId;
                filterBy.UserId = userProfile.AspNetUserId;               
                var entities = _memoBoxRepository.GetMemoPaged(page, count, out totalCount, filterBy, orderBy);

                return new CountModel<MemoItem>()
                {
                    Total = totalCount,
                    Items = ProcessQuery(entities)
                };
            }
            if (filterBy.FolderType == (int)FolderType.InBox)
            {
                var recipientFilterBy = MemoRecipientFilter.Deserilize(filterExpression);
                //filterBy.ThroughUnitId = userProfile.UnitId;
                filterBy.UserId = userProfile.AspNetUserId;
                //var entities = _memoRecipientRepository.GetMemoRecipientPaged(page, count, out totalCount, recipientFilterBy, orderBy);
                var entities = _memoRecipientRepository.Table.Where(x => x.RecipientId == filterBy.UserId);
                return new CountModel<MemoItem>()
                {
                    Total = totalCount,
                    Items = ProcessQuery(entities)
                };
            }
            if (filterBy.FolderType == (int)FolderType.Draft)
            {
                var recipientFilterBy = MemoRecipientFilter.Deserilize(filterExpression);
                // filterBy.ThroughUnitId = userProfile.UnitId;
                filterBy.UserId = userProfile.AspNetUserId;
                //var entities = _memoRecipientRepository.GetMemoRecipientPaged(page, count, recipientFilterBy, orderBy);
                var entities = _memoBoxRepository.Table.Where(x => x.SenderId == filterBy.UserId && x.FolderType == filterBy.FolderType);
                return new CountModel<MemoItem>()
                {
                    Total = totalCount,
                    Items = ProcessQuery(entities)
                };
            }
            return null;
        }
        public IEnumerable<MemoItem> GetQuery(int page, int count, int currentUserId, string filterExpression = null, string orderByExpression = null)
        {
            var userProfile = _userProfileService.GetUserProfile(currentUserId);
            var filterBy = MemoFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);

            if (filterBy.FolderType == (int) FolderType.Sent)
            {
                filterBy.ThroughUnitId = userProfile.UnitId;
                filterBy.UserId = userProfile.AspNetUserId;             
                var entities = _memoBoxRepository.GetMemoPaged(page, count, filterBy, orderBy);
                return ProcessQuery(entities);
            }
            if (filterBy.FolderType == (int)FolderType.InBox)
            {
                var recipientFilterBy = MemoRecipientFilter.Deserilize(filterExpression);
               // filterBy.ThroughUnitId = userProfile.UnitId;
                filterBy.UserId = userProfile.AspNetUserId;
                //var entities = _memoRecipientRepository.GetMemoRecipientPaged(page, count, recipientFilterBy, orderBy);
                var entities = _memoRecipientRepository.Table.Where(x => x.RecipientId == filterBy.UserId);
                return ProcessQuery(entities);
            }
            if (filterBy.FolderType == (int)FolderType.Draft)
            {
                var recipientFilterBy = MemoRecipientFilter.Deserilize(filterExpression);
                // filterBy.ThroughUnitId = userProfile.UnitId;
                filterBy.UserId = userProfile.AspNetUserId;
                //var entities = _memoRecipientRepository.GetMemoRecipientPaged(page, count, recipientFilterBy, orderBy);
                var entities = _memoBoxRepository.Table.Where(x => x.SenderId == filterBy.UserId && x.FolderType == filterBy.FolderType);
                return ProcessQuery(entities);
            }
            return null;
        }
        private static IEnumerable<MemoItem> ProcessQuery(IEnumerable<MemoRecipient> entities)
        {
            return Mapper.Map<IEnumerable<MemoRecipient>, IEnumerable<MemoItem>>(entities).OrderByDescending(x => x.Id);
        }
      
        private Memo GetMemoEntity(int id)
        {
            return _memoBoxRepository.Find(id);

        }
        public MemoModel GetMemo(int userId)
        {
            var entity = GetMemoEntity(userId);           ;
            return Mapper.Map<Memo, MemoModel>(entity);
        }

    }
}
