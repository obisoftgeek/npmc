﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.Utilities;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IAccreditationStatusService
    {
        void Create(AccreditationStatusModel model, int currentUserId);

        void Delete(int id, int currentUserId);
        void Update(AccreditationStatusModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup();

        CountModel<AccreditationStatusItem> GetCount(int page, int count, string filterExpression = null,
            string orderByExpression = null);

        IEnumerable<AccreditationStatusItem> GetQuery(int page, int count, string filterExpression = null,
            string orderByExpression = null);

        AccreditationStatusItem GetDetail(int id);
        AccreditationStatusModel GetAccreditationStatus(int userId);
    }

    public class AccreditationStatusService : IAccreditationStatusService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IAccreditationStatusRepository _departmentRepository;
        private readonly IAuditTrailService _auditTrailService;


        public AccreditationStatusService(IUnitOfWorkAsync unitOfWork,
            IAccreditationStatusRepository departmentRepository, IAuditTrailService auditTrailService)
        {
            _unitOfWork = unitOfWork;
            _departmentRepository = departmentRepository;
            _auditTrailService = auditTrailService;
        }

        public IEnumerable<LookupModel> GetLookup()
        {
            var filterBy = new AccreditationStatusFilter();
            var entities = _departmentRepository.GetAccreditationStatusFilteredQueryable(filterBy);

            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }

        public void Create(AccreditationStatusModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<AccreditationStatusModel, AccreditationStatu>(model);
                entity.DateCreated = DateTime.Now;
                _departmentRepository.Insert(entity);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Accreditation status {0} created", model.Name), currentUserId,AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public void Delete(int id, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetAccreditationStatusEntity(id);
                _departmentRepository.Delete(entity);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Accreditation status {0} deleted", entity.Name), currentUserId,AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public AccreditationStatusItem GetDetail(int id)
        {
            var entity = GetAccreditationStatusEntity(id);
            return Mapper.Map<AccreditationStatu, AccreditationStatusItem>(entity);
        }

        private static IEnumerable<AccreditationStatusItem> ProcessQuery(IEnumerable<AccreditationStatu> entities)
        {
            return Mapper.Map<IEnumerable<AccreditationStatu>, IEnumerable<AccreditationStatusItem>>(entities);
        }

        public CountModel<AccreditationStatusItem> GetCount(int page, int count, string filterExpression = null,
            string orderByExpression = null)
        {
            int totalCount;
            var filterBy = AccreditationStatusFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities =
                _departmentRepository.GetAccreditationStatusPaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<AccreditationStatusItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }

        public IEnumerable<AccreditationStatusItem> GetQuery(int page, int count, string filterExpression = null,
            string orderByExpression = null)
        {
            var filterBy = AccreditationStatusFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _departmentRepository.GetAccreditationStatusPaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(AccreditationStatusModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var userProfile = GetAccreditationStatusEntity(model.Id);
                Mapper.Map(model, userProfile);
                _departmentRepository.Update(userProfile);
                _unitOfWork.Commit();
                _auditTrailService.Log(String.Format("Accreditation status {0} updated", model.Name), currentUserId,AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }

        private AccreditationStatu GetAccreditationStatusEntity(int id)
        {
            return _departmentRepository.Find(id);
        }

        public AccreditationStatusModel GetAccreditationStatus(int userId)
        {
            var entity = GetAccreditationStatusEntity(userId);
            ;
            return Mapper.Map<AccreditationStatu, AccreditationStatusModel>(entity);
        }
    }
}