﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.Utilities;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IInstitutionService
    {

        void Create(InstitutionModel model, int currentUserId);
        
        void Delete(int id,int currentUserId);
        void Update(InstitutionModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup();
        CountModel<InstitutionItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null);
        IEnumerable<InstitutionItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null);        
        InstitutionItem GetDetail(int id);
        InstitutionModel GetInstitution(int userId);
      

    }
    public class InstitutionService : IInstitutionService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IInstitutionRepository _institutionRepository;
        private readonly IAuditTrailService _auditTrailService;

        public InstitutionService(IUnitOfWorkAsync unitOfWork, IInstitutionRepository institutionRepository, IAuditTrailService auditTrailService)
        {
            _unitOfWork = unitOfWork;
            _institutionRepository = institutionRepository;
            _auditTrailService = auditTrailService;
        }
        public IEnumerable<LookupModel> GetLookup()
        {
            var filterBy = new InstitutionFilter {AccreditationStatusId = 1};
            var entities = _institutionRepository.GetInstitutionFilteredQueryable(filterBy);
           
            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }
     
        public void Create(InstitutionModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<InstitutionModel, Institution>(model);
                entity.DateCreated = DateTime.Now;
                entity.CreatedBy = currentUserId;
                _institutionRepository.Insert(entity);
                _unitOfWork.Commit();
                _auditTrailService.Log($"Exam - {model.Name} created", currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetInstitutionEntity(id);
                var entityName = entity.Name;
                _institutionRepository.Delete(entity);
                _unitOfWork.Commit();
                _auditTrailService.Log($"Exam - {entityName} deleted", currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        
        public InstitutionItem GetDetail(int id)
        {
            var entity = GetInstitutionEntity(id);
            return Mapper.Map<Institution, InstitutionItem>(entity);
        }
        private static IEnumerable<InstitutionItem> ProcessQuery(IEnumerable<Institution> entities)
        {
            return Mapper.Map<IEnumerable<Institution>, IEnumerable<InstitutionItem>>(entities);
        }
        public CountModel<InstitutionItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            int totalCount;
            var filterBy = InstitutionFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _institutionRepository.GetInstitutionPaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<InstitutionItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<InstitutionItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            var filterBy = InstitutionFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _institutionRepository.GetInstitutionPaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(InstitutionModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var userProfile = GetInstitutionEntity(model.Id);
                Mapper.Map(model, userProfile);
                _institutionRepository.Update(userProfile);
                _unitOfWork.Commit();
                _auditTrailService.Log($"Exam - {model.Name} updated", currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private Institution GetInstitutionEntity(int id)
        {
            return _institutionRepository.Find(id);

        }
        public InstitutionModel GetInstitution(int userId)
        {
            var entity = GetInstitutionEntity(userId);
            return Mapper.Map<Institution, InstitutionModel>(entity);
        }

    }
}
