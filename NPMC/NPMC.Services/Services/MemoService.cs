﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Web.ModelBinding;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using LinqKit;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IMemoService
    {

        void Create(MemoModel model, int currentUserId);
        
        void Delete(int id);
        void Update(MemoModel model, int currentUserId);
        CountModel<MemoItem> GetCount(int page, int count, int currentUserId, string filterExpression = null, string orderByExpression = null);
        IEnumerable<MemoItem> GetQuery(int page, int count, int currentUserId, string filterExpression = null, string orderByExpression = null);
        MemoItem GetItem(int id);
        MemoDetail GetDetail(int id, int currentUserId);
        MemoModel GetMemo(int userId);
        void CreateMemoThread(MemoThreadModel model, int currentUserId);
        void ForwardMemo(ForwardMemoModel model, int currentUserId);

    }
    public class MemoService : IMemoService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IMemoRepository _memoRepository;
        private readonly IUserProfileService _userProfileService;
        private readonly IMemoRecipientRepository _memoRecipientRepository;
        private readonly IMemoThreadRepository _memoThreadRepository;

        public MemoService(IUnitOfWorkAsync unitOfWork, IMemoRepository memoRepository, IUserProfileService userProfileService,
            IMemoRecipientRepository memoRecipientRepository, IMemoThreadRepository memoThreadRepository)
        {
            _unitOfWork = unitOfWork;
            _memoRepository = memoRepository;
            _userProfileService = userProfileService;
            _memoRecipientRepository = memoRecipientRepository;
            _memoThreadRepository = memoThreadRepository;
        }
        public void CreateMemoThread(MemoThreadModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var fromRecipient = _memoRecipientRepository.Table.FirstOrDefault(
                        x => x.MemoId == model.MemoId && x.RecipientId == currentUserId);
                if (fromRecipient == null)
                    throw new Exception("Invalid memo recipient.");

                var entity = Mapper.Map<MemoThreadModel, MemoThread>(model);
                entity.DateCreated = DateTime.Now;
                entity.RecipientId = fromRecipient.Id;
                _memoThreadRepository.Insert(entity);
                _unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void ForwardMemo(ForwardMemoModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();

                var fromRecipient = _memoRecipientRepository.Table.FirstOrDefault(
                       x => x.MemoId == model.MemoId && x.RecipientId == currentUserId);
                if (fromRecipient == null)
                    throw new Exception("Invalid memo recipient.");

                var memoThread = new MemoThread();
                memoThread.MemoId = model.MemoId;
                memoThread.Comment = model.Comment;
                memoThread.DateCreated = DateTime.Now;
                memoThread.RecipientId = fromRecipient.Id;
                _memoThreadRepository.Insert(memoThread);

                CreateRecipient(model.SelectedRecipients, model.MemoId, currentUserId, fromRecipient.Id);
                _unitOfWork.Commit();
               

            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private void CreateRecipient(List<SelectedRecipientModel> recipientLists,int memoId, int currentUserId, int? fromRecipientId)
        {
            var recipients = GetUniqueRecipient(recipientLists);
            recipients.ForEach(toRecipientId =>
            {
                if (toRecipientId == currentUserId)
                {
                    throw new Exception("The sender cannot be a recipient.");
                }

                    var recipientExist = _memoRecipientRepository.Table.FirstOrDefault(x => x.MemoId == memoId && x.RecipientId == toRecipientId);
                if (recipientExist != null)
                {
                    var existingRecipientName = recipientExist.UserProfile.LastName + " " +
                                              recipientExist.UserProfile.FirstName + " " +
                                              recipientExist.UserProfile.OtherNames;
                    throw new Exception(existingRecipientName + " is already a recipient.");
                }


                if (toRecipientId != currentUserId)
                {
                    var memoRecipient = new MemoRecipient();
                    memoRecipient.MemoId = memoId;
                    memoRecipient.RecipientId = toRecipientId;
                    memoRecipient.ReadStatus = 0;
                    if (fromRecipientId.HasValue)
                    {
                        memoRecipient.IsForwarded = true;
                        memoRecipient.ForwardedBy = fromRecipientId.Value;
                        memoRecipient.DateForwarded = DateTime.Now;
                    }
                   
                    _memoRecipientRepository.Insert(memoRecipient);
                }

            });
        }
        private List<int> GetUniqueRecipient(List<SelectedRecipientModel> recipientLists)
        {
            //removes duplicate recipient
            var recipients = new List<int>();
            recipientLists.ToList().ForEach(rep =>
            {
                if (recipients.Any(c => c == rep.Id))
                {
                    recipients.Remove(rep.Id);
                }
                else
                {
                    recipients.Add(rep.Id);
                }

            });
            return recipients;
        }
        public void Create(MemoModel model, int currentUserId)
        {
            try
            {              
                _unitOfWork.BeginTransaction();
               
                var entity = Mapper.Map<MemoModel, Memo>(model);
                entity.DateCreated = DateTime.Now;
                entity.SenderId = currentUserId;
                entity.MemoStatusId = model.FolderType == (int)MemoFolderType.MemoBox ? (int) MemoStatusEnum.InProgress : (int)MemoStatusEnum.Draft;
                entity.FolderType = (int)model.FolderType;
              
                _memoRepository.Insert(entity);

                if (model.FolderType == (int)MemoFolderType.MemoBox)
                {
                    CreateRecipient(model.SelectedRecipients, entity.Id, currentUserId, null);
                }
                           
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Update(MemoModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetMemoEntity(model.Id);
                //delete previous recipient
                var rec = entity.MemoRecipients.ToList();
                rec.ForEach(itemToDelete =>
                {
                    _memoRecipientRepository.Delete(itemToDelete);
                });

                Mapper.Map(model, entity);
                entity.MemoStatusId = model.FolderType == (int)MemoFolderType.MemoBox ? (int)MemoStatusEnum.InProgress : (int)MemoStatusEnum.Draft;
                entity.FolderType = (int)model.FolderType;
                _memoRepository.Update(entity);

                if (model.FolderType == (int)MemoFolderType.MemoBox)
                {
                    CreateRecipient(model.SelectedRecipients, entity.Id, currentUserId, null);
                }              
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }

        public void Delete(int id)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetMemoEntity(id);
                _memoRepository.Delete(entity);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        public MemoDetail GetDetail(int id, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var recipient = _memoRecipientRepository.Table.FirstOrDefault(x => x.MemoId == id && x.RecipientId == currentUserId);
                if (recipient != null)
                {
                    recipient.ReadStatus = 1;
                    _memoRecipientRepository.Update(recipient);
                    _unitOfWork.Commit();
                }
                var entity = GetMemoEntity(id);
                return Mapper.Map<Memo, MemoDetail>(entity);
              
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
            
        }

        public MemoItem GetItem(int id)
        {
            var entity = GetMemoEntity(id);
            return Mapper.Map<Memo, MemoItem>(entity);
        }
        private static IEnumerable<MemoItem> ProcessQuery(IEnumerable<Memo> entities)
        {
            return Mapper.Map<IEnumerable<Memo>, IEnumerable<MemoItem>>(entities).OrderByDescending(x => x.Id);
        }
        public CountModel<MemoItem> GetCount(int page, int count, int currentUserId, string filterExpression = null, string orderByExpression = null)
        {
            var userProfile = _userProfileService.GetUserProfile(currentUserId);
            int totalCount = 0;
            var filterBy = MemoFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);

            if (filterBy.FolderType == (int)MemoFolderType.MemoBox)
            {
                filterBy.UserId = userProfile.AspNetUserId;
                var sentEntities = _memoRepository.GetMemoPaged(page, count, out totalCount, filterBy, orderBy);
                var sentMemo = ProcessQuery(sentEntities).ToList();
                //return new CountModel<MemoItem>()
                //{
                //    Total = totalCount,
                //    Items = ProcessQuery(sentEntities)
                //};

               // var recipientFilterBy = MemoRecipientFilter.Deserilize(filterExpression);
                filterBy.UserId = userProfile.AspNetUserId;
                var recievedEntities = _memoRecipientRepository.Table.Where(x => x.RecipientId == filterBy.UserId);
                var recievedMemo = ProcessQuery(recievedEntities);
                var returnEntites = sentMemo.Concat(recievedMemo).ToList();


                return new CountModel<MemoItem>()
                {
                    Total = totalCount,
                    Items = returnEntites
                };
            }
          
            if (filterBy.FolderType == (int)MemoFolderType.Draft)
            {
                filterBy.UserId = userProfile.AspNetUserId;
                var entities = _memoRepository.Table.Where(x => x.SenderId == filterBy.UserId && x.FolderType == filterBy.FolderType);
                return new CountModel<MemoItem>()
                {
                    Total = totalCount,
                    Items = ProcessQuery(entities)
                };
            }
            return null;
        }
        public IEnumerable<MemoItem> GetQuery(int page, int count, int currentUserId, string filterExpression = null, string orderByExpression = null)
        {
            var userProfile = _userProfileService.GetUserProfile(currentUserId);
            var filterBy = MemoFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);

            if (filterBy.FolderType == (int) MemoFolderType.MemoBox)
            {

                filterBy.UserId = userProfile.AspNetUserId;
                var sentEntities = _memoRepository.GetMemoPaged(page, count, filterBy, orderBy);
                var sentMemo = ProcessQuery(sentEntities).ToList();
                //return ProcessQuery(entities);

                var recipientFilterBy = MemoRecipientFilter.Deserilize(filterExpression);
                filterBy.UserId = userProfile.AspNetUserId;
                var recievedEntities = _memoRecipientRepository.GetMemoRecipientPaged(page, count, recipientFilterBy, orderBy);
                var recievedMemo = ProcessQuery(recievedEntities);
                return sentMemo.Concat(recievedMemo).ToList();

                //return ProcessQuery(entities);
            }

            if (filterBy.FolderType == (int)MemoFolderType.Draft)
            {
                var recipientFilterBy = MemoRecipientFilter.Deserilize(filterExpression);
              
                filterBy.UserId = userProfile.AspNetUserId;
                //var entities = _memoRecipientRepository.GetMemoRecipientPaged(page, count, recipientFilterBy, orderBy);
                var entities = _memoRepository.Table.Where(x => x.SenderId == filterBy.UserId && x.FolderType == filterBy.FolderType);
                return ProcessQuery(entities);
            }
            return null;
        }
        private static IEnumerable<MemoItem> ProcessQuery(IEnumerable<MemoRecipient> entities)
        {
            return Mapper.Map<IEnumerable<MemoRecipient>, IEnumerable<MemoItem>>(entities).OrderByDescending(x => x.Id);
        }
      
        private Memo GetMemoEntity(int id)
        {
            return _memoRepository.Find(id);

        }
        public MemoModel GetMemo(int userId)
        {
            var entity = GetMemoEntity(userId);           ;
            return Mapper.Map<Memo, MemoModel>(entity);
        }

    }
}
