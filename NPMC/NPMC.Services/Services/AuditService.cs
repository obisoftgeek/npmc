﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.Models;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models.Utilities;
using SilverEdgeProjects.NPMC.Models.ViewModels;
using Library.AutoFac;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IAuditTrailService
    {
        
        IEnumerable<AuditTrailModel> Query(int page, int count,AuditTrailFilter filter, string orderByExpression = null);
        CountModel<AuditTrailModel> GetCount(int page, int count,AuditTrailFilter filter, string orderByExpression = null);
        IEnumerable<TextAndValue> GetAuditActions(int id = 0, long sectionId = 0, string name = "");
        IEnumerable<TextAndValue> GetAuditSections(int id = 0, string name = "");
        void Log(string details, int currentUserId, string auditSection = null, string auditAction = null);
        List<AuditTrailItem> DownloadReport(AuditTrailFilter filter);
    }
  public  class AuditTrailService:IAuditTrailService
    {
      private readonly IAuditTrailRepository _repository;
      private readonly IUserProfileRepository _userProfileRepository;
      public AuditTrailService(IAuditTrailRepository repository, IUserProfileRepository userProfileRepository)
        {
            _repository = repository;
            _userProfileRepository = userProfileRepository;
        }

      public IEnumerable<AuditTrailModel> Query(int page, int count, AuditTrailFilter filter,
          string orderByExpression = null)
      {
          var orderExpression = OrderExpression.Deserilizer(orderByExpression);
          var audits = _repository.GetAuditTrailPaged(page, count, filter, orderExpression).ToList();
          return ProcessItemQuery(audits);
      }

      public CountModel<AuditTrailModel> GetCount(int page, int count, AuditTrailFilter filter,
            string orderByExpression = null)
        {
             
            int totalCount;
            var orderExpression = OrderExpression.Deserilizer(orderByExpression);
            var audits = _repository.GetAuditTrailPaged(page, count, out totalCount, filter, orderExpression).ToList();
            
            return new CountModel<AuditTrailModel>()
            {
                Total = totalCount,
                Items = ProcessItemQuery(audits)
            };
        }

      private static IEnumerable<AuditTrailItem> ProcessItemQuery(IEnumerable<AuditTrail> audits)
        {

            return from audit in audits
                let user = audit.UserProfile
                let action = audit.AuditAction
                let section = action.AuditSection
                select new AuditTrailItem()
                {
                    Id = audit.Id,
                    UserProfileId = user.AspNetUserId, 
                    Timestamp = audit.Timestamp.ToLocalTime(),//new DateTimeOffset(audit.Timestamp, TimeSpan.FromHours(0)).ToLocalTime().DateTime, 
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    AuditActionId = audit.AuditActionId,
                    BrowserName = audit.BrowserName,
                    Details = audit.Details,
                    IsMobile = audit.IsMobile,
                    UserIP = audit.UserIP,
                    AuditAction = action.Name,
                    AuditSection = section.Name,
                    AuditSectionId = action.Id,
                    UserName = user.AspNetUser.Email
                };
        }

         
        public IEnumerable<TextAndValue> GetAuditActions(int id = 0, long sectionId = 0, string name = "")
        {

                var query = _repository.GetRepository<AuditAction>().Table;
                if (id > 0)
                    query = query.Where(x => x.Id == id);
                if (sectionId > 0)
                    query = query.Where(x => x.AuditSectionId == sectionId);
                if (!string.IsNullOrEmpty(name))
                    query = query.Where(x => x.Name.ToLower() == name.ToLower());
                return query.Select(x => new TextAndValue() { Value = x.Id, Name = x.Name });


        }
        public IEnumerable<TextAndValue> GetAuditSections(int id = 0, string name = "")
        {
                var query = _repository.GetRepository<AuditSection>().Table;
                if (id > 0)
                    query = query.Where(x => x.Id == id);
                if (!string.IsNullOrEmpty(name))
                    query = query.Where(x => x.Name.ToLower() == name.ToLower());
                return query.Select(x => new TextAndValue()
                {
                    Name = x.Name,Value = x.Id
                });
        }
        public void Log(string details, int currentUserId, string auditSection = null, string auditAction = null)
        {
            auditAction = auditAction ?? AuditConstants.OtherUserActivitiesAction;
          auditSection = auditSection ?? AuditConstants.OtherUserActivitiesSection;
          if (string.IsNullOrEmpty(details)) return;

          var user = _userProfileRepository.Find(currentUserId);
          
          auditSection = auditSection.Trim();
          auditAction = auditAction.Trim();
          details = details.Trim();

          //  //1. get or insert into Section
          var section =
             _repository.GetRepository<AuditSection>().Table.FirstOrDefault(x => x.Name.ToLower() == auditSection.ToLower());
          if (section == null)
          {
              section = new AuditSection { Name = auditSection };
              _repository.GetRepository<AuditSection>().Insert(section);
          }
          //get or insert into action
          var action =
              _repository.GetRepository<AuditAction>().Table.FirstOrDefault(x => x.AuditSectionId == section.Id && x.Name.ToLower() == auditAction.ToLower());
          if (action == null)
          {
              action = new AuditAction { Name = auditAction, AuditSectionId = section.Id };
              _repository.GetRepository<AuditAction>().Insert(action);
          }
             
          //finally save it
          var trail = new AuditTrail
          {
              Details = details,
              UserProfileId = user.AspNetUserId,
              AuditActionId = action.Id,
              Timestamp = DateTime.Now,
              UserIP = RequestHelper.IpAddress(),
               BrowserName = RequestHelper.BrowserName(),
              IsMobile = RequestHelper.BrowserIsMobile()
          };
             
          _repository.Insert(trail);
        }

        public List<AuditTrailItem> DownloadReport(AuditTrailFilter filter)
      {
          return ProcessItemQuery(_repository.GetAuditTrailFilteredQueryable(filter)).ToList();
      }
    }
}
