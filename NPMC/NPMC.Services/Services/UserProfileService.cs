﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IUserProfileService
    {

        void Create(UserProfileModel model, int currentUserId);        
        void Delete(int id);
        void Update(UserProfileModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup();
        CountModel<UserProfileItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null);
        IEnumerable<UserProfileItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null);
        void ToggleActive(int id);
        void UpdateMustChangePassword(int id,bool value);
        UserProfileItem GetItem(int id);
        UserProfileDetail GetDetail(int id);
        UserProfileModel GetUserProfile(int userId);

        UserProfileModel GetUserProfileByUserName(string username);
        IEnumerable<LookupModel> GetLookupByDept(int departmentId);
    }
    public class UserProfileService : IUserProfileService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly IAspNetUserRepository _aspNetUserRepository;
       
        public UserProfileService(IUnitOfWorkAsync unitOfWork, IUserProfileRepository userProfileRepository, IAspNetUserRepository aspNetUserRepository)
        {
            _unitOfWork = unitOfWork;
            _userProfileRepository = userProfileRepository;
            _aspNetUserRepository = aspNetUserRepository;
           
        }
        public IEnumerable<LookupModel> GetLookupByDept(int departmentId)
        {
            IEnumerable<UserProfile> entities;
            if (departmentId > 0)
                entities = _userProfileRepository.Table.Where(x => x.Unit.DepartmentId == departmentId);
            else
                entities = _userProfileRepository.Table.Where(x => x.Unit.Department.IsActive == true);

            return entities.Select(x => new LookupModel()
            {
                Id = x.AspNetUserId,
                Name = x.FirstName + " " + x.LastName + " " + x.OtherNames
            });
        }
        public void Create(UserProfileModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<UserProfileModel, UserProfile>(model);              
                _userProfileRepository.Insert(entity);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = _aspNetUserRepository.Find(id);
                _aspNetUserRepository.Delete(entity);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public UserProfileDetail GetDetail(int id)
        {
            var entity = GetUserProfileEntity(id);
            return Mapper.Map<UserProfile, UserProfileDetail>(entity);
        }

        public UserProfileItem GetItem(int id)
        {
            var entity = GetUserProfileEntity(id);
            return Mapper.Map<UserProfile, UserProfileItem>(entity);
        }

        public IEnumerable<LookupModel> GetLookup()
        {
            var entities = _userProfileRepository.GetUserProfileFilteredQueryable(new UserProfileFilter()
            {
                IsActive = true
            });

            return entities.Select(x => new LookupModel()
            {
                Id = x.AspNetUserId,
                Name = x.LastName  + " " + x.FirstName
            });
        }
        private static IEnumerable<UserProfileItem> ProcessQuery(IEnumerable<UserProfile> entities)
        {
            return Mapper.Map<IEnumerable<UserProfile>, IEnumerable<UserProfileItem>>(entities);
        }
        public CountModel<UserProfileItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            int totalCount;
            var filterBy = UserProfileFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _userProfileRepository.GetUserProfilePaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<UserProfileItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<UserProfileItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            var filterBy = UserProfileFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _userProfileRepository.GetUserProfilePaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }
       
        public void ToggleActive(int id)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var userProfile = GetUserProfileEntity(id);
                userProfile.IsActive = !userProfile.IsActive;
                _userProfileRepository.Update(userProfile);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void UpdateMustChangePassword(int id,bool value)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var userProfile = GetUserProfileEntity(id);
                userProfile.MustChangePassword = value;
                _userProfileRepository.Update(userProfile);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Update(UserProfileModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var userProfile = GetUserProfileEntity(model.AspNetUserId);
                Mapper.Map(model, userProfile);
                userProfile.AspNetUser.PhoneNumber = model.PhoneNumber;
                _userProfileRepository.Update(userProfile);
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private UserProfile GetUserProfileEntity(int id)
        {
            return _userProfileRepository.Find(id);

        }
        public UserProfileModel GetUserProfile(int userId)
        {
            var entity = GetUserProfileEntity(userId);           ;
            return Mapper.Map<UserProfile, UserProfileModel>(entity);
        }

        public UserProfileModel GetUserProfileByUserName(string username)
        {
            var aspNetUser = _aspNetUserRepository.Find(username);
            if (aspNetUser!=null)
            {
                var entity = GetUserProfileEntity(aspNetUser.Id);
                return Mapper.Map<UserProfile, UserProfileModel>(entity);
            }
            return new UserProfileModel();
        }
    }
}
 
