﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.Models;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.DataAccess.Repositories;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.Utilities;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.BusinessLogic.Services
{
    public interface IFacultyService
    {

        void Create(FacultyModel model, int currentUserId);
        
        void Delete(int id, int currentUserId);
        void Update(FacultyModel model, int currentUserId);
        IEnumerable<LookupModel> GetLookup(FacultyFilter filter=null);
        CountModel<FacultyItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null);
        IEnumerable<FacultyItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null); 
        FacultyItem GetDetail(int id);
        FacultyModel GetById(int id);
      

    }
    public class FacultyService : IFacultyService
    {
        private readonly IUnitOfWorkAsync _unitOfWork;
        private readonly IFacultyRepository _facultyRepository;
        private readonly IAuditTrailService _auditService;

        public FacultyService(IUnitOfWorkAsync unitOfWork, IFacultyRepository facultyRepository, IAuditTrailService auditService)
        {
            _unitOfWork = unitOfWork;
            _facultyRepository = facultyRepository;
            _auditService = auditService;
        }
        public IEnumerable<LookupModel> GetLookup(FacultyFilter filter=null)
        {
            var entities = _facultyRepository.GetFacultyFilteredQueryable(filter);
           
            return entities.Select(x => new LookupModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }
     
        public void Create(FacultyModel model, int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = Mapper.Map<FacultyModel, Faculty>(model);
                entity.DateCreated = DateTime.Now;
                entity.IsActive = true;
                _facultyRepository.Insert(entity);
                _unitOfWork.Commit();
                _auditService.Log(String.Format("Faculty {0} created ",model.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        public void Delete(int id,int currentUserId)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                var entity = GetFacultyEntity(id);
                var facultyName = entity.Name;
                _facultyRepository.Delete(entity);
                _unitOfWork.Commit();
                 
                _auditService.Log(String.Format("Faculty {0} deleted ", facultyName), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
      
        public FacultyItem GetDetail(int id)
        {
            var entity = GetFacultyEntity(id);
            return Mapper.Map<Faculty, FacultyItem>(entity);
        }

         
        private static IEnumerable<FacultyItem> ProcessQuery(IEnumerable<Faculty> entities)
        {
            return entities.Select(c => new FacultyItem()
            {
                Id=c.Id,
                Name=c.Name,
                Code=c.Code,
                PaymentFeeId = c.PaymentFeeId,
                AmountPayable = c.PaymentFee.Amount,
                IsActive=c.IsActive,
                IsApproved=c.IsApproved,
                DateCreated = c.DateCreated,
                SubSpecialtyCount = c.SubSpecialties.Count
            });
        }
        public CountModel<FacultyItem> GetCount(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            int totalCount;
            var filterBy = FacultyFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _facultyRepository.GetFacultyPaged(page, count, out totalCount, filterBy, orderBy);

            return new CountModel<FacultyItem>()
            {
                Total = totalCount,
                Items = ProcessQuery(entities)
            };
        }
        public IEnumerable<FacultyItem> GetQuery(int page, int count, string filterExpression = null, string orderByExpression = null)
        {
            var filterBy = FacultyFilter.Deserilize(filterExpression);
            var orderBy = OrderExpression.Deserilizer(orderByExpression);
            var entities = _facultyRepository.GetFacultyPaged(page, count, filterBy, orderBy);
            return ProcessQuery(entities);
        }

        public void Update(FacultyModel model, int currentUserId)
        {

            try
            {
                _unitOfWork.BeginTransaction();
                var faculty = GetFacultyEntity(model.Id);
                if (faculty == null)
                    throw new Exception("faculty not found!");

                Mapper.Map(model, faculty);
                _facultyRepository.Update(faculty);
                _unitOfWork.Commit();

                _auditService.Log(String.Format("Faculty updated from {0} to {1} ", model.Name,faculty.Name), currentUserId, AuditConstants.SetupSection, AuditConstants.SetupAction);
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw new Exception(ex.Message);
            }
        }
        private Faculty GetFacultyEntity(int id)
        {
            return _facultyRepository.Find(id);
        }
        public FacultyModel GetById(int id)
        {
            var entity = GetFacultyEntity(id);           ;
            return Mapper.Map<Faculty, FacultyModel>(entity);
        }

    }
}
