namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InstitutionFaculty")]
    public partial class InstitutionFaculty
    {
        public int Id { get; set; }

        public int InstitutionId { get; set; }

        public int FacultyId { get; set; }

        public virtual Faculty Faculty { get; set; }

        public virtual Institution Institution { get; set; }
    }
}
