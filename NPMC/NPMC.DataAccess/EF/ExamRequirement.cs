namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ExamRequirement")]
    public partial class ExamRequirement
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        public int? ExamId { get; set; }

        public bool IsActive { get; set; }

        public virtual Exam Exam { get; set; }
    }
}
