namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MemoThread")]
    public partial class MemoThread
    {
        public int Id { get; set; }

        public int MemoId { get; set; }

        public int RecipientId { get; set; }

        [Required]
        public string Comment { get; set; }

        public DateTime DateCreated { get; set; }

        [StringLength(128)]
        public string AttachedFile { get; set; }

        public virtual Memo Memo { get; set; }

        public virtual MemoRecipient MemoRecipient { get; set; }
    }
}
