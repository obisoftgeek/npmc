namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Workshop")]
    public partial class Workshop
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        [Required]
        [StringLength(50)]
        public string Venue { get; set; }

        [Column(TypeName = "date")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime EndDate { get; set; }

        public int PaymentFeeId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedBy { get; set; }

        public bool IsApproved { get; set; }

        public int? ApprovedBy { get; set; }

        public virtual PaymentFee PaymentFee { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual UserProfile UserProfile1 { get; set; }
    }
}
