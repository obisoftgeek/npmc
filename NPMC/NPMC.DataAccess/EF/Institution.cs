namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Institution")]
    public partial class Institution
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Institution()
        {
            InstitutionFaculties = new HashSet<InstitutionFaculty>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Code { get; set; }

        public int ContactPersonId { get; set; }

        public int StateId { get; set; }

        [StringLength(50)]
        public string Address { get; set; }

        [StringLength(50)]
        public string Website { get; set; }

        public int? AccreditationStatusId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LastAccreditationDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? NextDueDate { get; set; }

        public DateTime DateCreated { get; set; }

        public int? YearEstablished { get; set; }

        public int CreatedBy { get; set; }

        public virtual AccreditationStatu AccreditationStatu { get; set; }

        public virtual State State { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual UserProfile UserProfile1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InstitutionFaculty> InstitutionFaculties { get; set; }
    }
}
