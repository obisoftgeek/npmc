namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RolePermission")]
    public partial class RolePermission
    {
        public int Id { get; set; }

        public int RoleId { get; set; }

        public int ControllerId { get; set; }

        public bool? CanView { get; set; }

        public bool? CanCreate { get; set; }

        public bool? CanModify { get; set; }

        public bool? CanApprove { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int? CreatedBy { get; set; }

        public bool IsApproved { get; set; }

        public int ApprovedBy { get; set; }

        public DateTime ApprovedDate { get; set; }
    }
}
