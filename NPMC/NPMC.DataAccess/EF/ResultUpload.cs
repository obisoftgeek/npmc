namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ResultUpload")]
    public partial class ResultUpload
    {
        public int Id { get; set; }

        public int ExamDietId { get; set; }

        public int ExamId { get; set; }

        [Required]
        [StringLength(150)]
        public string Upload { get; set; }

        [Required]
        [StringLength(150)]
        public string ScannedCopy { get; set; }

        public DateTime DateUploaded { get; set; }

        public int? UploadedBy { get; set; }

        public virtual Exam Exam { get; set; }

        public virtual ExamDiet ExamDiet { get; set; }
    }
}
