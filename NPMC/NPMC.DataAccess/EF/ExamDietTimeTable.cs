namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ExamDietTimeTable")]
    public partial class ExamDietTimeTable
    {
        public int Id { get; set; }

        public int ExamId { get; set; }

        public TimeSpan StartTime { get; set; }

        public TimeSpan EndTime { get; set; }

        public int ExamDietId { get; set; }

        public int FacultyId { get; set; }

        public int ExamTypeId { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        public virtual Exam Exam { get; set; }

        public virtual ExamDiet ExamDiet { get; set; }

        public virtual ExamType ExamType { get; set; }

        public virtual Faculty Faculty { get; set; }
    }
}
