namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MemoRecipient")]
    public partial class MemoRecipient
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MemoRecipient()
        {
            MemoRecipient1 = new HashSet<MemoRecipient>();
            MemoThreads = new HashSet<MemoThread>();
        }

        public int Id { get; set; }

        public int MemoId { get; set; }

        public int RecipientId { get; set; }

        public int ReadStatus { get; set; }

        public bool IsForwarded { get; set; }

        public int? ForwardedBy { get; set; }

        public DateTime? DateForwarded { get; set; }

        public virtual Memo Memo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MemoRecipient> MemoRecipient1 { get; set; }

        public virtual MemoRecipient MemoRecipient2 { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MemoThread> MemoThreads { get; set; }
    }
}
