namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Staff")]
    public partial class Staff
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserProfileId { get; set; }

        [StringLength(50)]
        public string StaffId { get; set; }

        public int? DesignationId { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        public virtual Designation Designation { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
