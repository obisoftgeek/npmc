namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ExamStageCenter")]
    public partial class ExamStageCenter
    {
        public int Id { get; set; }

        public int ExamStageId { get; set; }

        public int ExamCenterId { get; set; }

        public virtual ExamCenter ExamCenter { get; set; }

        public virtual ExamStage ExamStage { get; set; }
    }
}
