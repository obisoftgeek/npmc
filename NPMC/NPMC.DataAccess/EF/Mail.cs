namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Mail")]
    public partial class Mail
    {
        public int Id { get; set; }

        [Required]
        [StringLength(150)]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        public string Attachment { get; set; }

        public DateTime DateCreated { get; set; }

        public int StatusId { get; set; }

        public int RecepientId { get; set; }

        [Required]
        [StringLength(150)]
        public string Sender { get; set; }

        [StringLength(150)]
        public string SenderAddress { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
