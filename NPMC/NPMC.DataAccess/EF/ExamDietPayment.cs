namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ExamDietPayment")]
    public partial class ExamDietPayment
    {
        public int Id { get; set; }

        public int DietId { get; set; }

        public int PaymentFeeId { get; set; }

        public int ExamId { get; set; }

        public decimal Amount { get; set; }

        public virtual Exam Exam { get; set; }

        public virtual ExamDiet ExamDiet { get; set; }

        public virtual PaymentFee PaymentFee { get; set; }
    }
}
