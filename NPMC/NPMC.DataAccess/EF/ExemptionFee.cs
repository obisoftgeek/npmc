namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ExemptionFee")]
    public partial class ExemptionFee
    {
        public int Id { get; set; }

        public int ExamId { get; set; }

        public int PaymentFeeId { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        public int CreatedBy { get; set; }

        public virtual Exam Exam { get; set; }

        public virtual PaymentFee PaymentFee { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
