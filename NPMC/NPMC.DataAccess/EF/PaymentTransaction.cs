namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PaymentTransaction")]
    public partial class PaymentTransaction
    {
        public int Id { get; set; }

        public int UserProfileId { get; set; }

        public DateTime PaymentDate { get; set; }

        public int? PaymentType { get; set; }

        public decimal Amount { get; set; }

        public decimal? AmountPaid { get; set; }

        [Required]
        [StringLength(50)]
        public string ReferenceId { get; set; }

        public bool HasPaid { get; set; }

        public int AccountId { get; set; }

        [Required]
        [StringLength(150)]
        public string Narration { get; set; }

        [Required]
        [StringLength(50)]
        public string InvoiceId { get; set; }

        public virtual Account Account { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
