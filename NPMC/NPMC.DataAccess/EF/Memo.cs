namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Memo")]
    public partial class Memo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Memo()
        {
            MemoRecipients = new HashSet<MemoRecipient>();
            MemoThreads = new HashSet<MemoThread>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Subject { get; set; }

        [Required]
        public string Body { get; set; }

        public DateTime DateCreated { get; set; }

        public int SenderId { get; set; }

        public int MemoStatusId { get; set; }

        public int FolderType { get; set; }

        [StringLength(128)]
        public string AttachedFile { get; set; }

        public virtual MemoStatu MemoStatu { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MemoRecipient> MemoRecipients { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MemoThread> MemoThreads { get; set; }
    }
}
