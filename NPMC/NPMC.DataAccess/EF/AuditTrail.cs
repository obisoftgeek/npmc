namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AuditTrail")]
    public partial class AuditTrail
    {
        public long Id { get; set; }

        [StringLength(250)]
        public string UserIP { get; set; }

        public string Details { get; set; }

        public DateTime Timestamp { get; set; }

        public int? AuditActionId { get; set; }

        public bool IsMobile { get; set; }

        [StringLength(250)]
        public string BrowserName { get; set; }

        public int UserProfileId { get; set; }

        public virtual AuditAction AuditAction { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
