namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserRole")]
    public partial class UserRole
    {
        public int Id { get; set; }

        public int UserProfileId { get; set; }

        public int RoleId { get; set; }

        public bool IsActive { get; set; }

        public bool IsApproved { get; set; }

        public int AssignedBy { get; set; }

        public DateTime AssignedDate { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public int? ApprovedBy { get; set; }

        public virtual Role Role { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
