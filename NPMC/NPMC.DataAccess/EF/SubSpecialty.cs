namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SubSpecialty")]
    public partial class SubSpecialty
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public int FacultyId { get; set; }

        public int PaymentFeeId { get; set; }

        public DateTime DateCreated { get; set; }

        public bool IsActive { get; set; }

        public bool IsApproved { get; set; }

        public int? ApprovedBy { get; set; }

        public string Description { get; set; }

        public virtual Faculty Faculty { get; set; }

        public virtual PaymentFee PaymentFee { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
