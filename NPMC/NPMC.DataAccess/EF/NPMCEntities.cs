using Library.Repository.Pattern.EntityFramework;

namespace SilverEdgeProjects.NPMC.DataAccess.EF
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class NPMCEntities : EntityFrameworkDataContext
    {
        public NPMCEntities()
            : base("name=NPMCEntities")
        {
        }

        public virtual DbSet<C__MigrationLog> C__MigrationLog { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<AccreditationStatu> AccreditationStatus { get; set; }
        public virtual DbSet<AccreditationType> AccreditationTypes { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<AuditAction> AuditActions { get; set; }
        public virtual DbSet<AuditSection> AuditSections { get; set; }
        public virtual DbSet<AuditTrail> AuditTrails { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Designation> Designations { get; set; }
        public virtual DbSet<Exam> Exams { get; set; }
        public virtual DbSet<ExamCenter> ExamCenters { get; set; }
        public virtual DbSet<ExamDiet> ExamDiets { get; set; }
        public virtual DbSet<ExamDietPayment> ExamDietPayments { get; set; }
        public virtual DbSet<ExamDietTimeTable> ExamDietTimeTables { get; set; }
        public virtual DbSet<ExamRequirement> ExamRequirements { get; set; }
        public virtual DbSet<ExamStage> ExamStages { get; set; }
        public virtual DbSet<ExamStageCenter> ExamStageCenters { get; set; }
        public virtual DbSet<ExamType> ExamTypes { get; set; }
        public virtual DbSet<ExemptionFee> ExemptionFees { get; set; }
        public virtual DbSet<Faculty> Faculties { get; set; }
        public virtual DbSet<Institution> Institutions { get; set; }
        public virtual DbSet<InstitutionFaculty> InstitutionFaculties { get; set; }
        public virtual DbSet<Mail> Mails { get; set; }
        public virtual DbSet<Memo> Memos { get; set; }
        public virtual DbSet<MemoCategory> MemoCategories { get; set; }
        public virtual DbSet<MemoRecipient> MemoRecipients { get; set; }
        public virtual DbSet<MemoStatu> MemoStatus { get; set; }
        public virtual DbSet<MemoThread> MemoThreads { get; set; }
        public virtual DbSet<PaymentCategory> PaymentCategories { get; set; }
        public virtual DbSet<PaymentFee> PaymentFees { get; set; }
        public virtual DbSet<PaymentTransaction> PaymentTransactions { get; set; }
        public virtual DbSet<ResultCategory> ResultCategories { get; set; }
        public virtual DbSet<ResultUpload> ResultUploads { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<RolePermission> RolePermissions { get; set; }
        public virtual DbSet<Staff> Staffs { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<SubSpecialty> SubSpecialties { get; set; }
        public virtual DbSet<Title> Titles { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<Workshop> Workshops { get; set; }
        public virtual DbSet<Fellow> Fellows { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<C__MigrationLog>()
                .Property(e => e.version)
                .IsUnicode(false);

            modelBuilder.Entity<C__MigrationLog>()
                .Property(e => e.package_version)
                .IsUnicode(false);

            modelBuilder.Entity<C__MigrationLog>()
                .Property(e => e.release_version)
                .IsUnicode(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.PaymentFees)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.PaymentTransactions)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AccreditationStatu>()
                .HasMany(e => e.Institutions)
                .WithOptional(e => e.AccreditationStatu)
                .HasForeignKey(e => e.AccreditationStatusId);

            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasOptional(e => e.UserProfile)
                .WithRequired(e => e.AspNetUser);

            modelBuilder.Entity<AuditSection>()
                .HasMany(e => e.AuditActions)
                .WithRequired(e => e.AuditSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Department>()
                .HasMany(e => e.Units)
                .WithRequired(e => e.Department)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Exam>()
                .HasMany(e => e.ExamDietPayments)
                .WithRequired(e => e.Exam)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Exam>()
                .HasMany(e => e.ExamDietTimeTables)
                .WithRequired(e => e.Exam)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Exam>()
                .HasMany(e => e.ExamStages)
                .WithRequired(e => e.Exam)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Exam>()
                .HasMany(e => e.ExamTypes)
                .WithRequired(e => e.Exam)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Exam>()
                .HasMany(e => e.ExemptionFees)
                .WithRequired(e => e.Exam)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Exam>()
                .HasMany(e => e.ResultUploads)
                .WithRequired(e => e.Exam)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ExamCenter>()
                .HasMany(e => e.ExamStageCenters)
                .WithRequired(e => e.ExamCenter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ExamDiet>()
                .HasMany(e => e.ExamDietPayments)
                .WithRequired(e => e.ExamDiet)
                .HasForeignKey(e => e.DietId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ExamDiet>()
                .HasMany(e => e.ExamDietTimeTables)
                .WithRequired(e => e.ExamDiet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ExamDiet>()
                .HasMany(e => e.ResultUploads)
                .WithRequired(e => e.ExamDiet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ExamType>()
                .HasMany(e => e.ExamDietTimeTables)
                .WithRequired(e => e.ExamType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Faculty>()
                .HasMany(e => e.ExamDietTimeTables)
                .WithRequired(e => e.Faculty)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Faculty>()
                .HasMany(e => e.InstitutionFaculties)
                .WithRequired(e => e.Faculty)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Faculty>()
                .HasMany(e => e.SubSpecialties)
                .WithRequired(e => e.Faculty)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Institution>()
                .HasMany(e => e.InstitutionFaculties)
                .WithRequired(e => e.Institution)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Memo>()
                .HasMany(e => e.MemoThreads)
                .WithRequired(e => e.Memo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MemoRecipient>()
                .HasMany(e => e.MemoRecipient1)
                .WithOptional(e => e.MemoRecipient2)
                .HasForeignKey(e => e.ForwardedBy);

            modelBuilder.Entity<MemoRecipient>()
                .HasMany(e => e.MemoThreads)
                .WithRequired(e => e.MemoRecipient)
                .HasForeignKey(e => e.RecipientId);

            modelBuilder.Entity<MemoStatu>()
                .HasMany(e => e.Memos)
                .WithRequired(e => e.MemoStatu)
                .HasForeignKey(e => e.MemoStatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PaymentCategory>()
                .HasMany(e => e.PaymentFees)
                .WithRequired(e => e.PaymentCategory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PaymentFee>()
                .HasMany(e => e.ExamDietPayments)
                .WithRequired(e => e.PaymentFee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PaymentFee>()
                .HasMany(e => e.ExemptionFees)
                .WithRequired(e => e.PaymentFee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PaymentFee>()
                .HasMany(e => e.Faculties)
                .WithRequired(e => e.PaymentFee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PaymentFee>()
                .HasMany(e => e.SubSpecialties)
                .WithRequired(e => e.PaymentFee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PaymentFee>()
                .HasMany(e => e.Workshops)
                .WithRequired(e => e.PaymentFee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.UserRoles)
                .WithRequired(e => e.Role)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.ExamCenters)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.Institutions)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Unit>()
                .HasMany(e => e.Roles)
                .WithRequired(e => e.Unit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Unit>()
                .HasMany(e => e.UserProfiles)
                .WithRequired(e => e.Unit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.AuditTrails)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserProfileId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Designations)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.ExemptionFees)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Faculties)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.ApprovedBy);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Institutions)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.ContactPersonId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Institutions1)
                .WithRequired(e => e.UserProfile1)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Mails)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.RecepientId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Memos)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.SenderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.MemoRecipients)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.RecipientId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.PaymentCategories)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.PaymentFees)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.AprovedBy);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.PaymentTransactions)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserProfileId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasOptional(e => e.Staff)
                .WithRequired(e => e.UserProfile)
                .WillCascadeOnDelete();

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.SubSpecialties)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.ApprovedBy);

            modelBuilder.Entity<UserProfile>()
                .HasOptional(e => e.Fellow)
                .WithRequired(e => e.UserProfile);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.UserRoles)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.UserProfileId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Workshops)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Workshops1)
                .WithOptional(e => e.UserProfile1)
                .HasForeignKey(e => e.ApprovedBy);
        }
    }
}
