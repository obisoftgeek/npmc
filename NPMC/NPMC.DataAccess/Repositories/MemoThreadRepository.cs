﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IMemoThreadRepository : IRepositoryAsync<MemoThread>
    {
        IEnumerable<MemoThread> GetMemoThreadPaged(int page, int count, out int totalCount,
            MemoThreadFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<MemoThread> GetMemoThreadPaged(int page, int count, MemoThreadFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<MemoThread> GetMemoThreadFilteredQueryable(MemoThreadFilter filter = null);
       
    }

    public class MemoThreadRepository : Repository<MemoThread>, IMemoThreadRepository
    {
        public MemoThreadRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<MemoThread> GetMemoThreadPaged(int page, int count, out int totalCount,
            MemoThreadFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new MemoThreadQueryObject(filter).Expression;
            totalCount = Count(expression);
            return MemoThreadPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<MemoThread> GetMemoThreadPaged(int page, int count, MemoThreadFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new MemoThreadQueryObject(filter).Expression;
            return MemoThreadPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<MemoThread> MemoThreadPaged(int page, int count, Expression<Func<MemoThread, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<MemoThread> GetMemoThreadFilteredQueryable(MemoThreadFilter filter = null)
        {
            var expression = new MemoThreadQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<MemoThread>, IOrderedQueryable<MemoThread>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<MemoThread>, IOrderedQueryable<MemoThread>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class MemoThreadQueryObject : QueryObject<MemoThread>
        {
            public MemoThreadQueryObject(MemoThreadFilter filter)
            {
                if (filter != null)
                {
                   
                }
            }
        }
    }
}
