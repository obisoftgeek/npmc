﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IMemoRepository : IRepositoryAsync<Memo>
    {
        IEnumerable<Memo> GetMemoPaged(int page, int count, out int totalCount,
            MemoFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<Memo> GetMemoPaged(int page, int count, MemoFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<Memo> GetMemoFilteredQueryable(MemoFilter filter = null);
       
    }

    public class MemoRepository : Repository<Memo>, IMemoRepository
    {
        public MemoRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<Memo> GetMemoPaged(int page, int count, out int totalCount,
            MemoFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new MemoQueryObject(filter).Expression;
            totalCount = Count(expression);
            return MemoPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<Memo> GetMemoPaged(int page, int count, MemoFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new MemoQueryObject(filter).Expression;
            return MemoPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<Memo> MemoPaged(int page, int count, Expression<Func<Memo, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<Memo> GetMemoFilteredQueryable(MemoFilter filter = null)
        {
            var expression = new MemoQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<Memo>, IOrderedQueryable<Memo>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<Memo>, IOrderedQueryable<Memo>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class MemoQueryObject : QueryObject<Memo>
        {
            public MemoQueryObject(MemoFilter filter)
            {
                if (filter != null)
                {

                    if (filter.UserId > 0)
                        And(x => x.SenderId == filter.UserId);
                    if (filter.UserId > 0)
                        And(x => x.FolderType == filter.FolderType);
                  

                }
            }
        }
    }
}
