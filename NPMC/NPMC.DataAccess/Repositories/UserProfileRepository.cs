﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IUserProfileRepository : IRepositoryAsync<UserProfile>
    {
        IEnumerable<UserProfile> GetUserProfilePaged(int page, int count, out int totalCount,
            UserProfileFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<UserProfile> GetUserProfilePaged(int page, int count, UserProfileFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<UserProfile> GetUserProfileFilteredQueryable(UserProfileFilter filter = null);

        UserProfile GetUserProfileByUserName(string username);
    }

    public class UserProfileRepository : Repository<UserProfile>, IUserProfileRepository
    {
        public UserProfileRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<UserProfile> GetUserProfilePaged(int page, int count, out int totalCount,
            UserProfileFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new UserProfileQueryObject(filter).Expression;
            totalCount = Count(expression);
            return UserProfilePaged(page, count, expression, orderExpression);
        }
        public IEnumerable<UserProfile> GetUserProfilePaged(int page, int count, UserProfileFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new UserProfileQueryObject(filter).Expression;
            return UserProfilePaged(page, count, expression, orderExpression);
        }

        private IEnumerable<UserProfile> UserProfilePaged(int page, int count, Expression<Func<UserProfile, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<UserProfile> GetUserProfileFilteredQueryable(UserProfileFilter filter = null)
        {
            var expression = new UserProfileQueryObject(filter).Expression;
            return Fetch(expression);
        }

        public UserProfile GetUserProfileByUserName(string username)
        {
            return Table.SingleOrDefault(c => c.AspNetUser.Email.Equals(username));
        }

        public static Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.AspNetUserId);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class UserProfileQueryObject : QueryObject<UserProfile>
        {
            public UserProfileQueryObject(UserProfileFilter filter)
            {
                if (filter != null)
                {
                    if (filter.AspNetUserId > 0)
                     And(x => x.AspNetUserId == filter.AspNetUserId);
                }
            }
        }
    }
}
 
