﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IExamRepository : IRepositoryAsync<Exam>
    {
        IEnumerable<Exam> GetExamPaged(int page, int count, out int totalCount,
            ExamFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<Exam> GetExamPaged(int page, int count, ExamFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<Exam> GetExamFilteredQueryable(ExamFilter filter = null);
       
    }

    public class ExamRepository : Repository<Exam>, IExamRepository
    {
        public ExamRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<Exam> GetExamPaged(int page, int count, out int totalCount,
            ExamFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new ExamQueryObject(filter).Expression;
            totalCount = Count(expression);
            return ExamPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<Exam> GetExamPaged(int page, int count, ExamFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new ExamQueryObject(filter).Expression;
            return ExamPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<Exam> ExamPaged(int page, int count, Expression<Func<Exam, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<Exam> GetExamFilteredQueryable(ExamFilter filter = null)
        {
            var expression = new ExamQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<Exam>, IOrderedQueryable<Exam>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<Exam>, IOrderedQueryable<Exam>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class ExamQueryObject : QueryObject<Exam>
        {
            public ExamQueryObject(ExamFilter filter)
            {
                if (filter != null)
                {
                    if (filter.IsActive)
                    {
                        
                    } 
                }
            }
        }
    }
}
