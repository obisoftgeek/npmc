﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IPaymentCategoryRepository : IRepositoryAsync<PaymentCategory>
    {
        IEnumerable<PaymentCategory> GetPaymentCategoryPaged(int page, int count, out int totalCount,
            PaymentCategoryFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<PaymentCategory> GetPaymentCategoryPaged(int page, int count, PaymentCategoryFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<PaymentCategory> GetPaymentCategoryFilteredQueryable(PaymentCategoryFilter filter = null);
       
    }

    public class PaymentCategoryRepository : Repository<PaymentCategory>, IPaymentCategoryRepository
    {
        public PaymentCategoryRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<PaymentCategory> GetPaymentCategoryPaged(int page, int count, out int totalCount,
            PaymentCategoryFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new PaymentCategoryQueryObject(filter).Expression;
            totalCount = Count(expression);
            return PaymentCategoryPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<PaymentCategory> GetPaymentCategoryPaged(int page, int count, PaymentCategoryFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new PaymentCategoryQueryObject(filter).Expression;
            return PaymentCategoryPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<PaymentCategory> PaymentCategoryPaged(int page, int count, Expression<Func<PaymentCategory, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<PaymentCategory> GetPaymentCategoryFilteredQueryable(PaymentCategoryFilter filter = null)
        {
            var expression = new PaymentCategoryQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<PaymentCategory>, IOrderedQueryable<PaymentCategory>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<PaymentCategory>, IOrderedQueryable<PaymentCategory>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class PaymentCategoryQueryObject : QueryObject<PaymentCategory>
        {
            public PaymentCategoryQueryObject(PaymentCategoryFilter filter)
            {
                if (filter != null)
                {
                     
                    if (!String.IsNullOrEmpty(filter.Name))
                    {
                        And(x => x.Name.Contains(filter.Name));
                    }
                     
                    if (filter.IsActive)
                    {
                        And(x => x.IsActive==filter.IsActive);
                    }
                    if (filter.IsApproved)
                    {
                        And(x => x.IsApproved == filter.IsApproved);
                    }
                    if (filter.DateCreatedFrom.HasValue)
                    {
                        And(x => x.DateCreated>=filter.DateCreatedFrom.Value);
                    }
                    if (filter.DateCreatedTo.HasValue)
                    {
                        And(x => x.DateCreated <= filter.DateCreatedTo.Value);
                    }
                    
                }
            }
        }
    }
}
