﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models.ViewModels;

namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IAuditTrailRepository : IRepositoryAsync<AuditTrail>
    {

        IEnumerable<AuditTrail> GetAuditTrailPaged(int page, int count, out int totalCount,
            AuditTrailFilter filter = null, OrderExpression orderExpression = null);

        IEnumerable<AuditTrail> GetAuditTrailPaged(int page, int count, AuditTrailFilter filter = null, OrderExpression orderExpression = null);

        IEnumerable<AuditTrail> GetAuditTrailFilteredQueryable(AuditTrailFilter filter = null);

    }
    public class AuditTrailRepository : Repository<AuditTrail>, IAuditTrailRepository
    {
        public AuditTrailRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
        public IEnumerable<AuditTrail> GetAuditTrailPaged(int page, int count, out int totalCount,
            AuditTrailFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new AuditTrailQueryObject(filter).Expression;
            totalCount = Count(expression);
            return AuditTrailPaged(page, count, expression, orderExpression);
        }

        public IEnumerable<AuditTrail> GetAuditTrailPaged(int page, int count, AuditTrailFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new AuditTrailQueryObject(filter).Expression;
            return AuditTrailPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<AuditTrail> AuditTrailPaged(int page, int count, Expression<Func<AuditTrail, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<AuditTrail> GetAuditTrailFilteredQueryable(AuditTrailFilter filter = null)
        {
            var expression = new AuditTrailQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<AuditTrail>, IOrderedQueryable<AuditTrail>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<AuditTrail>, IOrderedQueryable<AuditTrail>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderByDescending(a => a.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {
                       
                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class AuditTrailQueryObject : QueryObject<AuditTrail>
        {
            public AuditTrailQueryObject(AuditTrailFilter filter)
            {
                if (filter != null)
                {
                    if (!string.IsNullOrWhiteSpace(filter.UserIP))
                        And(q => q.UserIP.Contains(filter.UserIP));
                    if (filter.AuditActionId > 0)
                        And(q => q.AuditActionId == filter.AuditActionId);
                    if (!string.IsNullOrWhiteSpace(filter.UserName))
                        And(q => q.UserProfile.AspNetUser.Email.Contains(filter.UserName));
                    if (!string.IsNullOrWhiteSpace(filter.FirstName))
                        And(q => q.UserProfile.FirstName.Contains(filter.FirstName));
                    if (!string.IsNullOrWhiteSpace(filter.LastName))
                        And(q => q.UserProfile.LastName.Contains(filter.LastName)); 
                    if (filter.UserProfileId > 0)
                        And(q => q.UserProfileId == filter.UserProfileId);
                    if (filter.DateCreatedFrom.HasValue)
                        And(q => q.Timestamp >= filter.DateCreatedFrom);
                    if (filter.DateCreatedTo.HasValue)
                        And(q => q.Timestamp <= filter.DateCreatedTo);
                }
            }
        }
    }
}
