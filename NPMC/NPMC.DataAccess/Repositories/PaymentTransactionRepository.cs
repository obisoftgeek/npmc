﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IPaymentTransactionRepository : IRepositoryAsync<PaymentTransaction>
    {
        IEnumerable<PaymentTransaction> GetPaymentTransactionPaged(int page, int count, out int totalCount,
            PaymentTransactionFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<PaymentTransaction> GetPaymentTransactionPaged(int page, int count, PaymentTransactionFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<PaymentTransaction> GetPaymentTransactionFilteredQueryable(PaymentTransactionFilter filter = null);
       
    }

    public class PaymentTransactionRepository : Repository<PaymentTransaction>, IPaymentTransactionRepository
    {
        public PaymentTransactionRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<PaymentTransaction> GetPaymentTransactionPaged(int page, int count, out int totalCount,
            PaymentTransactionFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new PaymentTransactionQueryObject(filter).Expression;
            totalCount = Count(expression);
            return PaymentTransactionPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<PaymentTransaction> GetPaymentTransactionPaged(int page, int count, PaymentTransactionFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new PaymentTransactionQueryObject(filter).Expression;
            return PaymentTransactionPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<PaymentTransaction> PaymentTransactionPaged(int page, int count, Expression<Func<PaymentTransaction, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<PaymentTransaction> GetPaymentTransactionFilteredQueryable(PaymentTransactionFilter filter = null)
        {
            var expression = new PaymentTransactionQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<PaymentTransaction>, IOrderedQueryable<PaymentTransaction>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<PaymentTransaction>, IOrderedQueryable<PaymentTransaction>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class PaymentTransactionQueryObject : QueryObject<PaymentTransaction>
        {
            public PaymentTransactionQueryObject(PaymentTransactionFilter filter)
            {
                if (filter != null)
                {
                     
                    if (!String.IsNullOrEmpty(filter.ReferenceId))
                    {
                        And(x => x.ReferenceId.Contains(filter.ReferenceId));
                    }
                    if (filter.PaymentDateFrom.HasValue)
                    {
                        And(x => x.PaymentDate>=filter.PaymentDateFrom.Value);
                    }
                    if (filter.PaymentDateTo.HasValue)
                    {
                        And(x => x.PaymentDate <= filter.PaymentDateTo.Value);
                    }
                    if (filter.AccountId>0)
                    {
                        And(x => x.AccountId==filter.AccountId);
                    }
                    if (filter.UserProfileId>0)
                    {
                        And(x => x.UserProfileId==filter.UserProfileId);
                    }
                    if (filter.PaymentType>0)
                    {
                        And(x => x.PaymentType==filter.PaymentType);
                    }
                    if (filter.HasPaid)
                    {
                        And(x => x.HasPaid==filter.HasPaid);
                    }
                }
            }
        }
    }
}
