﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IAccreditationStatusRepository : IRepositoryAsync<AccreditationStatu>
    {
        IEnumerable<AccreditationStatu> GetAccreditationStatusPaged(int page, int count, out int totalCount,
            AccreditationStatusFilter filter = null, OrderExpression orderExpression = null);

        IEnumerable<AccreditationStatu> GetAccreditationStatusPaged(int page, int count,
            AccreditationStatusFilter filter = null,
            OrderExpression orderExpression = null);

        IEnumerable<AccreditationStatu>
            GetAccreditationStatusFilteredQueryable(AccreditationStatusFilter filter = null);
    }

    public class AccreditationStatusRepository : Repository<AccreditationStatu>, IAccreditationStatusRepository
    {
        public AccreditationStatusRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {
        }

        public IEnumerable<AccreditationStatu> GetAccreditationStatusPaged(int page, int count, out int totalCount,
            AccreditationStatusFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new AccreditationStatusQueryObject(filter).Expression;
            totalCount = Count(expression);
            return AccreditationStatusPaged(page, count, expression, orderExpression);
        }

        public IEnumerable<AccreditationStatu> GetAccreditationStatusPaged(int page, int count,
            AccreditationStatusFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new AccreditationStatusQueryObject(filter).Expression;
            return AccreditationStatusPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<AccreditationStatu> AccreditationStatusPaged(int page, int count,
            Expression<Func<AccreditationStatu, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<AccreditationStatu> GetAccreditationStatusFilteredQueryable(
            AccreditationStatusFilter filter = null)
        {
            var expression = new AccreditationStatusQueryObject(filter).Expression;
            return Fetch(expression);
        }

        public static Func<IQueryable<AccreditationStatu>, IOrderedQueryable<AccreditationStatu>> ProcessOrderFunc(
            OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<AccreditationStatu>, IOrderedQueryable<AccreditationStatu>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {
                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class AccreditationStatusQueryObject : QueryObject<AccreditationStatu>
        {
            public AccreditationStatusQueryObject(AccreditationStatusFilter filter)
            {
                if (filter != null)
                {
                    if (!String.IsNullOrEmpty(filter.Name))
                    {
                        And(x => x.Name.Contains(filter.Name));
                    }
                    if (filter.DateCreatedFrom.HasValue)
                    {
                        And(x => x.DateCreated >= filter.DateCreatedFrom);
                    }
                    if (filter.DateCreatedTo.HasValue)
                    {
                        And(x => x.DateCreated <= filter.DateCreatedTo);
                    }
                }
            }
        }
    }
}