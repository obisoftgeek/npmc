﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface ISubSpecialtyRepository : IRepositoryAsync<SubSpecialty>
    {
        IEnumerable<SubSpecialty> GetSubSpecialtyPaged(int page, int count, out int totalCount,
            SubSpecialtyFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<SubSpecialty> GetSubSpecialtyPaged(int page, int count, SubSpecialtyFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<SubSpecialty> GetSubSpecialtyFilteredQueryable(SubSpecialtyFilter filter = null);
       
    }

    public class SubSpecialtyRepository : Repository<SubSpecialty>, ISubSpecialtyRepository
    {
        public SubSpecialtyRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<SubSpecialty> GetSubSpecialtyPaged(int page, int count, out int totalCount,
            SubSpecialtyFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new SubSpecialtyQueryObject(filter).Expression;
            totalCount = Count(expression);
            return SubSpecialtyPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<SubSpecialty> GetSubSpecialtyPaged(int page, int count, SubSpecialtyFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new SubSpecialtyQueryObject(filter).Expression;
            return SubSpecialtyPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<SubSpecialty> SubSpecialtyPaged(int page, int count, Expression<Func<SubSpecialty, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<SubSpecialty> GetSubSpecialtyFilteredQueryable(SubSpecialtyFilter filter = null)
        {
            var expression = new SubSpecialtyQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<SubSpecialty>, IOrderedQueryable<SubSpecialty>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<SubSpecialty>, IOrderedQueryable<SubSpecialty>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class SubSpecialtyQueryObject : QueryObject<SubSpecialty>
        {
            public SubSpecialtyQueryObject(SubSpecialtyFilter filter)
            {
                if (filter != null)
                {
                     
                    if (!String.IsNullOrEmpty(filter.Name))
                    {
                        And(x => x.Name.Contains(filter.Name));
                    }
                     
                    if (filter.IsActive)
                    {
                        And(x => x.IsActive==filter.IsActive);
                    }
                    if (filter.IsApproved)
                    {
                        And(x => x.IsApproved == filter.IsApproved);
                    }
                    if (filter.DateCreatedFrom.HasValue)
                    {
                        And(x => x.DateCreated>=filter.DateCreatedFrom.Value);
                    }
                    if (filter.DateCreatedTo.HasValue)
                    {
                        And(x => x.DateCreated <= filter.DateCreatedTo.Value);
                    }
                    if (filter.PaymentFeeId>0)
                    {
                        And(x => x.PaymentFeeId==filter.PaymentFeeId);
                    }
                }
            }
        }
    }
}
