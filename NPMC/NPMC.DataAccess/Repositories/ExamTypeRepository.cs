﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IExamTypeRepository : IRepositoryAsync<ExamType>
    {
        IEnumerable<ExamType> GetExamTypePaged(int page, int count, out int totalCount,
            ExamTypeFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<ExamType> GetExamTypePaged(int page, int count, ExamTypeFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<ExamType> GetExamTypeFilteredQueryable(ExamTypeFilter filter = null);
       
    }

    public class ExamTypeRepository : Repository<ExamType>, IExamTypeRepository
    {
        public ExamTypeRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<ExamType> GetExamTypePaged(int page, int count, out int totalCount,
            ExamTypeFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new ExamTypeQueryObject(filter).Expression;
            totalCount = Count(expression);
            return ExamTypePaged(page, count, expression, orderExpression);
        }
        public IEnumerable<ExamType> GetExamTypePaged(int page, int count, ExamTypeFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new ExamTypeQueryObject(filter).Expression;
            return ExamTypePaged(page, count, expression, orderExpression);
        }

        private IEnumerable<ExamType> ExamTypePaged(int page, int count, Expression<Func<ExamType, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<ExamType> GetExamTypeFilteredQueryable(ExamTypeFilter filter = null)
        {
            var expression = new ExamTypeQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<ExamType>, IOrderedQueryable<ExamType>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<ExamType>, IOrderedQueryable<ExamType>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class ExamTypeQueryObject : QueryObject<ExamType>
        {
            public ExamTypeQueryObject(ExamTypeFilter filter)
            {
                if (filter != null)
                {
                    if (filter.IsActive)
                    {
                        And(c => c.IsActive == filter.IsActive);
                    }
                }
            }
        }
    }
}
