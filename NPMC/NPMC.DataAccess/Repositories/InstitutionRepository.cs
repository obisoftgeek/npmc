﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IInstitutionRepository : IRepositoryAsync<Institution>
    {
        IEnumerable<Institution> GetInstitutionPaged(int page, int count, out int totalCount,
            InstitutionFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<Institution> GetInstitutionPaged(int page, int count, InstitutionFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<Institution> GetInstitutionFilteredQueryable(InstitutionFilter filter = null);
       
    }

    public class InstitutionRepository : Repository<Institution>, IInstitutionRepository
    {
        public InstitutionRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<Institution> GetInstitutionPaged(int page, int count, out int totalCount,
            InstitutionFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new InstitutionQueryObject(filter).Expression;
            totalCount = Count(expression);
            return InstitutionPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<Institution> GetInstitutionPaged(int page, int count, InstitutionFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new InstitutionQueryObject(filter).Expression;
            return InstitutionPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<Institution> InstitutionPaged(int page, int count, Expression<Func<Institution, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<Institution> GetInstitutionFilteredQueryable(InstitutionFilter filter = null)
        {
            var expression = new InstitutionQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<Institution>, IOrderedQueryable<Institution>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<Institution>, IOrderedQueryable<Institution>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class InstitutionQueryObject : QueryObject<Institution>
        {
            public InstitutionQueryObject(InstitutionFilter filter)
            {
                if (filter != null)
                {
                    if (!String.IsNullOrEmpty(filter.Name))
                    {
                        And(x => x.Name.Contains(filter.Name));
                    }

                    if (!String.IsNullOrEmpty(filter.Code))
                    {
                        And(x => x.Code.Contains(filter.Code));
                    }

                    if (!String.IsNullOrEmpty(filter.Website))
                    {
                        And(x => x.Website.Contains(filter.Website));
                    }

                    if (filter.LastAccreditationDate.HasValue)
                    {
                        And(x => x.LastAccreditationDate.Value.Equals(filter.LastAccreditationDate));
                    }

                    if (filter.NextDueDate.HasValue)
                    {
                        And(x => x.NextDueDate.Value.Equals(filter.NextDueDate));
                    }

                    if (filter.NextDueDate.HasValue)
                    {
                        And(x => x.NextDueDate.Value.Equals(filter.NextDueDate));
                    }

                    if (filter.YearEstablished.HasValue)
                    {
                        And(x => x.YearEstablished.Value.Equals(filter.YearEstablished));
                    }

                    if (!String.IsNullOrEmpty(filter.Website))
                    {
                        And(x => x.Website.Contains(filter.Website));
                    }

                    if (filter.CreatedDateFrom.HasValue)
                    {
                        And(x => x.DateCreated >= filter.CreatedDateFrom.Value);
                    }
                    if (filter.CreatedDateTo.HasValue)
                    {
                        And(x => x.DateCreated <= filter.CreatedDateTo.Value);
                    }

                }
            }
        }
    }
}
