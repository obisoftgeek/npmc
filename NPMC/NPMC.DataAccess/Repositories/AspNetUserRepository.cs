﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IAspNetUserRepository : IRepositoryAsync<AspNetUser>
    {
        IEnumerable<AspNetUser> GetAspNetUserPaged(int page, int count, out int totalCount,
            AspNetUserFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<AspNetUser> GetAspNetUserPaged(int page, int count, AspNetUserFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<AspNetUser> GetAspNetUserFilteredQueryable(AspNetUserFilter filter = null);
        
    }

    public class AspNetUserRepository : Repository<AspNetUser>, IAspNetUserRepository
    {
        public AspNetUserRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
        public IEnumerable<AspNetUser> GetAspNetUserPaged(int page, int count, out int totalCount,
            AspNetUserFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new AspNetUserQueryObject(filter).Expression;
            totalCount = Count(expression);
            return AspNetUserPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<AspNetUser> GetAspNetUserPaged(int page, int count, AspNetUserFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new AspNetUserQueryObject(filter).Expression;
            return AspNetUserPaged(page, count, expression, orderExpression);
        }
       
        private IEnumerable<AspNetUser> AspNetUserPaged(int page, int count, Expression<Func<AspNetUser, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<AspNetUser> GetAspNetUserFilteredQueryable(AspNetUserFilter filter = null)
        {
            var expression = new AspNetUserQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<AspNetUser>, IOrderedQueryable<AspNetUser>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<AspNetUser>, IOrderedQueryable<AspNetUser>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class AspNetUserQueryObject : QueryObject<AspNetUser>
        {
            public AspNetUserQueryObject(AspNetUserFilter filter)
            {
                if (filter != null)
                {
                    
                }
            }
        }
    }
}
