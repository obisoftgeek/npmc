﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IExamCenterRepository : IRepositoryAsync<ExamCenter>
    {
        IEnumerable<ExamCenter> GetExamCenterPaged(int page, int count, out int totalCount,
            ExamCenterFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<ExamCenter> GetExamCenterPaged(int page, int count, ExamCenterFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<ExamCenter> GetExamCenterFilteredQueryable(ExamCenterFilter filter = null);
       
    }

    public class ExamCenterRepository : Repository<ExamCenter>, IExamCenterRepository
    {
        public ExamCenterRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<ExamCenter> GetExamCenterPaged(int page, int count, out int totalCount,
            ExamCenterFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new ExamCenterQueryObject(filter).Expression;
            totalCount = Count(expression);
            return ExamCenterPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<ExamCenter> GetExamCenterPaged(int page, int count, ExamCenterFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new ExamCenterQueryObject(filter).Expression;
            return ExamCenterPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<ExamCenter> ExamCenterPaged(int page, int count, Expression<Func<ExamCenter, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<ExamCenter> GetExamCenterFilteredQueryable(ExamCenterFilter filter = null)
        {
            var expression = new ExamCenterQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<ExamCenter>, IOrderedQueryable<ExamCenter>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<ExamCenter>, IOrderedQueryable<ExamCenter>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class ExamCenterQueryObject : QueryObject<ExamCenter>
        {
            public ExamCenterQueryObject(ExamCenterFilter filter)
            {
                if (filter != null)
                {
                     
                    if (!String.IsNullOrEmpty(filter.Name))
                    {
                        And(x => x.Name.Contains(filter.Name));
                    }
                    
                    if (filter.IsActive)
                    {
                        And(x => x.IsActive==filter.IsActive);
                    }
                    if (filter.IsApproved)
                    {
                        And(x => x.IsApproved == filter.IsApproved);
                    }
                    if (filter.DateCreatedFrom.HasValue)
                    {
                        And(x => x.DateCreated>=filter.DateCreatedFrom.Value);
                    }
                    if (filter.DateCreatedTo.HasValue)
                    {
                        And(x => x.DateCreated <= filter.DateCreatedTo.Value);
                    }
                     
                }
            }
        }
    }
}
