﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IPaymentFeeRepository : IRepositoryAsync<PaymentFee>
    {
        IEnumerable<PaymentFee> GetPaymentFeePaged(int page, int count, out int totalCount,
            PaymentFeeFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<PaymentFee> GetPaymentFeePaged(int page, int count, PaymentFeeFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<PaymentFee> GetPaymentFeeFilteredQueryable(PaymentFeeFilter filter = null);
       
    }

    public class PaymentFeeRepository : Repository<PaymentFee>, IPaymentFeeRepository
    {
        public PaymentFeeRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<PaymentFee> GetPaymentFeePaged(int page, int count, out int totalCount,
            PaymentFeeFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new PaymentFeeQueryObject(filter).Expression;
            totalCount = Count(expression);
            return PaymentFeePaged(page, count, expression, orderExpression);
        }
        public IEnumerable<PaymentFee> GetPaymentFeePaged(int page, int count, PaymentFeeFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new PaymentFeeQueryObject(filter).Expression;
            return PaymentFeePaged(page, count, expression, orderExpression);
        }

        private IEnumerable<PaymentFee> PaymentFeePaged(int page, int count, Expression<Func<PaymentFee, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<PaymentFee> GetPaymentFeeFilteredQueryable(PaymentFeeFilter filter = null)
        {
            var expression = new PaymentFeeQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<PaymentFee>, IOrderedQueryable<PaymentFee>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<PaymentFee>, IOrderedQueryable<PaymentFee>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class PaymentFeeQueryObject : QueryObject<PaymentFee>
        {
            public PaymentFeeQueryObject(PaymentFeeFilter filter)
            {
                if (filter != null)
                {
                     
                    if (!String.IsNullOrEmpty(filter.Name))
                    {
                        And(x => x.Name.Contains(filter.Name));
                    }
                     
                    if (filter.IsActive)
                    {
                        And(x => x.IsActive==filter.IsActive);
                    }
                    if (filter.IsApproved)
                    {
                        And(x => x.IsApproved == filter.IsApproved);
                    }
                    if (filter.DateCreatedFrom.HasValue)
                    {
                        And(x => x.DateCreated>=filter.DateCreatedFrom.Value);
                    }
                    if (filter.DateCreatedTo.HasValue)
                    {
                        And(x => x.DateCreated <= filter.DateCreatedTo.Value);
                    }
                    if (filter.PaymentCategoryId>0)
                    {
                        And(x => x.PaymentCategoryId==filter.PaymentCategoryId);
                    }
                }
            }
        }
    }
}
