﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IWorkshopRepository : IRepositoryAsync<Workshop>
    {
        IEnumerable<Workshop> GetWorkshopPaged(int page, int count, out int totalCount,
            WorkshopFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<Workshop> GetWorkshopPaged(int page, int count, WorkshopFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<Workshop> GetWorkshopFilteredQueryable(WorkshopFilter filter = null);
       
    }

    public class WorkshopRepository : Repository<Workshop>, IWorkshopRepository
    {
        public WorkshopRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<Workshop> GetWorkshopPaged(int page, int count, out int totalCount,
            WorkshopFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new WorkshopQueryObject(filter).Expression;
            totalCount = Count(expression);
            return WorkshopPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<Workshop> GetWorkshopPaged(int page, int count, WorkshopFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new WorkshopQueryObject(filter).Expression;
            return WorkshopPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<Workshop> WorkshopPaged(int page, int count, Expression<Func<Workshop, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<Workshop> GetWorkshopFilteredQueryable(WorkshopFilter filter = null)
        {
            var expression = new WorkshopQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<Workshop>, IOrderedQueryable<Workshop>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<Workshop>, IOrderedQueryable<Workshop>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class WorkshopQueryObject : QueryObject<Workshop>
        {
            public WorkshopQueryObject(WorkshopFilter filter)
            {
                if (filter != null)
                {
                    if (!String.IsNullOrEmpty(filter.Name))
                    {
                        And(x => x.Name.Contains(filter.Name));
                    }
                    if (!String.IsNullOrEmpty(filter.Venue))
                    {
                        And(x => x.Name.Contains(filter.Venue));
                    }
                    if (!String.IsNullOrEmpty(filter.Description))
                    {
                        And(x => x.Name.Contains(filter.Description));
                    }
                    if (filter.CreatedDateFrom.HasValue)
                    {
                        And(x => x.CreatedDate >= filter.CreatedDateFrom.Value);
                    }
                    if (filter.CreatedDateTo.HasValue)
                    {
                        And(x => x.CreatedDate <= filter.CreatedDateTo.Value);
                    }

                }
            }
        }
    }
}
