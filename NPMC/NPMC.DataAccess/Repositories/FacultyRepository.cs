﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IFacultyRepository : IRepositoryAsync<Faculty>
    {
        IEnumerable<Faculty> GetFacultyPaged(int page, int count, out int totalCount,
            FacultyFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<Faculty> GetFacultyPaged(int page, int count, FacultyFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<Faculty> GetFacultyFilteredQueryable(FacultyFilter filter = null);
       
    }

    public class FacultyRepository : Repository<Faculty>, IFacultyRepository
    {
        public FacultyRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<Faculty> GetFacultyPaged(int page, int count, out int totalCount,
            FacultyFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new FacultyQueryObject(filter).Expression;
            totalCount = Count(expression);
            return FacultyPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<Faculty> GetFacultyPaged(int page, int count, FacultyFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new FacultyQueryObject(filter).Expression;
            return FacultyPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<Faculty> FacultyPaged(int page, int count, Expression<Func<Faculty, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<Faculty> GetFacultyFilteredQueryable(FacultyFilter filter = null)
        {
            var expression = new FacultyQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<Faculty>, IOrderedQueryable<Faculty>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<Faculty>, IOrderedQueryable<Faculty>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class FacultyQueryObject : QueryObject<Faculty>
        {
            public FacultyQueryObject(FacultyFilter filter)
            {
                if (filter != null)
                {
                     
                    if (!String.IsNullOrEmpty(filter.Name))
                    {
                        And(x => x.Name.Contains(filter.Name));
                    }
                    if (!String.IsNullOrEmpty(filter.Code))
                    {
                        And(x => x.Code.Equals(filter.Code));
                    }
                    if (filter.IsActive)
                    {
                        And(x => x.IsActive==filter.IsActive);
                    }
                    if (filter.IsApproved)
                    {
                        And(x => x.IsApproved == filter.IsApproved);
                    }
                    if (filter.DateCreatedFrom.HasValue)
                    {
                        And(x => x.DateCreated>=filter.DateCreatedFrom.Value);
                    }
                    if (filter.DateCreatedTo.HasValue)
                    {
                        And(x => x.DateCreated <= filter.DateCreatedTo.Value);
                    }
                    if (filter.PaymentFeeId>0)
                    {
                        And(x => x.PaymentFeeId==filter.PaymentFeeId);
                    }
                }
            }
        }
    }
}
