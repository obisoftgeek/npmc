﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IMemoRecipientRepository : IRepositoryAsync<MemoRecipient>
    {
        IEnumerable<MemoRecipient> GetMemoRecipientPaged(int page, int count, out int totalCount,
            MemoRecipientFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<MemoRecipient> GetMemoRecipientPaged(int page, int count, MemoRecipientFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<MemoRecipient> GetMemoRecipientFilteredQueryable(MemoRecipientFilter filter = null);
       
    }

    public class MemoRecipientRepository : Repository<MemoRecipient>, IMemoRecipientRepository
    {
        public MemoRecipientRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<MemoRecipient> GetMemoRecipientPaged(int page, int count, out int totalCount,
            MemoRecipientFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new MemoRecipientQueryObject(filter).Expression;
            totalCount = Count(expression);
            return MemoRecipientPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<MemoRecipient> GetMemoRecipientPaged(int page, int count, MemoRecipientFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new MemoRecipientQueryObject(filter).Expression;
            return MemoRecipientPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<MemoRecipient> MemoRecipientPaged(int page, int count, Expression<Func<MemoRecipient, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<MemoRecipient> GetMemoRecipientFilteredQueryable(MemoRecipientFilter filter = null)
        {
            var expression = new MemoRecipientQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<MemoRecipient>, IOrderedQueryable<MemoRecipient>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<MemoRecipient>, IOrderedQueryable<MemoRecipient>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class MemoRecipientQueryObject : QueryObject<MemoRecipient>
        {
            public MemoRecipientQueryObject(MemoRecipientFilter filter)
            {
                if (filter != null)
                {
                    if (filter.UserId > 0)
                    {
                        And(x => x.RecipientId == filter.UserId);
                    }
                   

 


                }
            }
        }
    }
}
