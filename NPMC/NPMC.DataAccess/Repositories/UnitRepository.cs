﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IUnitRepository : IRepositoryAsync<Unit>
    {
        IEnumerable<Unit> GetUnitPaged(int page, int count, out int totalCount,
            UnitFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<Unit> GetUnitPaged(int page, int count, UnitFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<Unit> GetUnitFilteredQueryable(UnitFilter filter = null);
      
    }

    public class UnitRepository : Repository<Unit>, IUnitRepository
    {
        public UnitRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<Unit> GetUnitPaged(int page, int count, out int totalCount,
            UnitFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new UnitQueryObject(filter).Expression;
            totalCount = Count(expression);
            return UnitPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<Unit> GetUnitPaged(int page, int count, UnitFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new UnitQueryObject(filter).Expression;
            return UnitPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<Unit> UnitPaged(int page, int count, Expression<Func<Unit, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<Unit> GetUnitFilteredQueryable(UnitFilter filter = null)
        {
            var expression = new UnitQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<Unit>, IOrderedQueryable<Unit>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<Unit>, IOrderedQueryable<Unit>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class UnitQueryObject : QueryObject<Unit>
        {
            public UnitQueryObject(UnitFilter filter)
            {
                if (filter != null)
                {
                    if (filter.DepartmentId > 0)
                     And(x => x.DepartmentId == filter.DepartmentId);
                }
            }
        }
    }
}

