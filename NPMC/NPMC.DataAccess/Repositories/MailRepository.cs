﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IMailRepository : IRepositoryAsync<Mail>
    {
        IEnumerable<Mail> GetMailPaged(int page, int count, out int totalCount,
            MailFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<Mail> GetMailPaged(int page, int count, MailFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<Mail> GetMailFilteredQueryable(MailFilter filter = null);
       
    }

    public class MailRepository : Repository<Mail>, IMailRepository
    {
        public MailRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<Mail> GetMailPaged(int page, int count, out int totalCount,
            MailFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new MailQueryObject(filter).Expression;
            totalCount = Count(expression);
            return MailPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<Mail> GetMailPaged(int page, int count, MailFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new MailQueryObject(filter).Expression;
            return MailPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<Mail> MailPaged(int page, int count, Expression<Func<Mail, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<Mail> GetMailFilteredQueryable(MailFilter filter = null)
        {
            var expression = new MailQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<Mail>, IOrderedQueryable<Mail>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<Mail>, IOrderedQueryable<Mail>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class MailQueryObject : QueryObject<Mail>
        {
            public MailQueryObject(MailFilter filter)
            {
                if (filter != null)
                {
                     
                    if (!String.IsNullOrEmpty(filter.Title))
                    {
                        And(x => x.Title.Contains(filter.Title));
                    }
                    if (filter.DateCreatedFrom.HasValue)
                    {
                        And(x => x.DateCreated>=filter.DateCreatedFrom.Value);
                    }
                    if (filter.DateCreatedTo.HasValue)
                    {
                        And(x => x.DateCreated <= filter.DateCreatedTo.Value);
                    }
                    if (filter.RecepientId>0)
                    {
                        And(x => x.UserProfile.AspNetUserId==filter.RecepientId);
                    }
                }
            }
        }
    }
}
