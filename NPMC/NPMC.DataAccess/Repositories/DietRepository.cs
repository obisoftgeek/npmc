﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IExamDietRepository : IRepositoryAsync<ExamDiet>
    {
        IEnumerable<ExamDiet> GetExamDietPaged(int page, int count, out int totalCount,
            ExamDietFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<ExamDiet> GetExamDietPaged(int page, int count, ExamDietFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<ExamDiet> GetExamDietFilteredQueryable(ExamDietFilter filter = null);
       
    }

    public class ExamDietRepository : Repository<ExamDiet>, IExamDietRepository
    {
        public ExamDietRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<ExamDiet> GetExamDietPaged(int page, int count, out int totalCount,
            ExamDietFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new ExamDietQueryObject(filter).Expression;
            totalCount = Count(expression);
            return ExamDietPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<ExamDiet> GetExamDietPaged(int page, int count, ExamDietFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new ExamDietQueryObject(filter).Expression;
            return ExamDietPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<ExamDiet> ExamDietPaged(int page, int count, Expression<Func<ExamDiet, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<ExamDiet> GetExamDietFilteredQueryable(ExamDietFilter filter = null)
        {
            var expression = new ExamDietQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<ExamDiet>, IOrderedQueryable<ExamDiet>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<ExamDiet>, IOrderedQueryable<ExamDiet>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class ExamDietQueryObject : QueryObject<ExamDiet>
        {
            public ExamDietQueryObject(ExamDietFilter filter)
            {
                if (filter != null)
                {
                    if (filter.IsActive)
                    {
                        And(x => x.IsActive == filter.IsActive);
                    }
                }
            }
        }
    }
}
