﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IStateRepository : IRepositoryAsync<State>
    {
        IEnumerable<State> GetStatePaged(int page, int count, out int totalCount,
            StateFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<State> GetStatePaged(int page, int count, StateFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<State> GetStateFilteredQueryable(StateFilter filter = null);
        bool NameExist(StateModel state);
    }

    public class StateRepository : Repository<State>, IStateRepository
    {
        public StateRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<State> GetStatePaged(int page, int count, out int totalCount,
            StateFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new StateQueryObject(filter).Expression;
            totalCount = Count(expression);
            return StatePaged(page, count, expression, orderExpression);
        }
        public IEnumerable<State> GetStatePaged(int page, int count, StateFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new StateQueryObject(filter).Expression;
            return StatePaged(page, count, expression, orderExpression);
        }

        private IEnumerable<State> StatePaged(int page, int count, Expression<Func<State, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<State> GetStateFilteredQueryable(StateFilter filter = null)
        {
            var expression = new StateQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<State>, IOrderedQueryable<State>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<State>, IOrderedQueryable<State>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public bool NameExist(StateModel state)
        {
            return Table.Any(x => x.Name.Equals(state.Name));
        }

        public class StateQueryObject : QueryObject<State>
        {
            public StateQueryObject(StateFilter filter)
            {
                if (!string.IsNullOrEmpty(filter.Name))
                {
                    And(x => x.Name.Contains(filter.Name));
                }
            }
        }
    }
}
