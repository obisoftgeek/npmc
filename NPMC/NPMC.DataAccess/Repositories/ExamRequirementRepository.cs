﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IExamRequirementRepository : IRepositoryAsync<ExamRequirement>
    {
        IEnumerable<ExamRequirement> GetExamRequirementPaged(int page, int count, out int totalCount,
            ExamRequirementFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<ExamRequirement> GetExamRequirementPaged(int page, int count, ExamRequirementFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<ExamRequirement> GetExamRequirementFilteredQueryable(ExamRequirementFilter filter = null);
       
    }

    public class ExamRequirementRepository : Repository<ExamRequirement>, IExamRequirementRepository
    {
        public ExamRequirementRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<ExamRequirement> GetExamRequirementPaged(int page, int count, out int totalCount,
            ExamRequirementFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new ExamRequirementQueryObject(filter).Expression;
            totalCount = Count(expression);
            return ExamRequirementPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<ExamRequirement> GetExamRequirementPaged(int page, int count, ExamRequirementFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new ExamRequirementQueryObject(filter).Expression;
            return ExamRequirementPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<ExamRequirement> ExamRequirementPaged(int page, int count, Expression<Func<ExamRequirement, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<ExamRequirement> GetExamRequirementFilteredQueryable(ExamRequirementFilter filter = null)
        {
            var expression = new ExamRequirementQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<ExamRequirement>, IOrderedQueryable<ExamRequirement>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<ExamRequirement>, IOrderedQueryable<ExamRequirement>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class ExamRequirementQueryObject : QueryObject<ExamRequirement>
        {
            public ExamRequirementQueryObject(ExamRequirementFilter filter)
            {
                if (filter != null)
                {
                    if (filter.IsActive)
                    {
                        
                    }
                }
            }
        }
    }
}
