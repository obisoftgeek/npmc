﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IAccountRepository : IRepositoryAsync<Account>
    {
        IEnumerable<Account> GetAccountPaged(int page, int count, out int totalCount,
            AccountFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<Account> GetAccountPaged(int page, int count, AccountFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<Account> GetAccountFilteredQueryable(AccountFilter filter = null);
       
    }

    public class AccountRepository : Repository<Account>, IAccountRepository
    {
        public AccountRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<Account> GetAccountPaged(int page, int count, out int totalCount,
            AccountFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new AccountQueryObject(filter).Expression;
            totalCount = Count(expression);
            return AccountPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<Account> GetAccountPaged(int page, int count, AccountFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new AccountQueryObject(filter).Expression;
            return AccountPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<Account> AccountPaged(int page, int count, Expression<Func<Account, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<Account> GetAccountFilteredQueryable(AccountFilter filter = null)
        {
            var expression = new AccountQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<Account>, IOrderedQueryable<Account>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<Account>, IOrderedQueryable<Account>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class AccountQueryObject : QueryObject<Account>
        {
            public AccountQueryObject(AccountFilter filter)
            {
                if (filter != null)
                {
                     
                    if (!String.IsNullOrEmpty(filter.Name))
                    {
                        And(x => x.Name.Contains(filter.Name));
                    }
                     
                    if (filter.IsActive)
                    {
                        And(x => x.IsActive==filter.IsActive);
                    }
                    if (filter.IsApproved)
                    {
                        And(x => x.IsApproved == filter.IsApproved);
                    }
                    if (filter.DateCreatedFrom.HasValue)
                    {
                        And(x => x.DateCreated>=filter.DateCreatedFrom.Value);
                    }
                    if (filter.DateCreatedTo.HasValue)
                    {
                        And(x => x.DateCreated <= filter.DateCreatedTo.Value);
                    }
                     
                }
            }
        }
    }
}
