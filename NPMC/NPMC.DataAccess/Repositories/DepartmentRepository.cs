﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Library.Models;
using Library.Repository.Pattern.DataContext;
using Library.Repository.Pattern.EntityFramework;
using Library.Repository.Pattern.QueryObject;
using Library.Repository.Pattern.Repositories;
using Library.Repository.Pattern.UnitOfWork;
using SilverEdgeProjects.NPMC.DataAccess.EF;
using SilverEdgeProjects.NPMC.Models;
using SilverEdgeProjects.NPMC.Models.ViewModels;


namespace SilverEdgeProjects.NPMC.DataAccess.Repositories
{
    public interface IDepartmentRepository : IRepositoryAsync<Department>
    {
        IEnumerable<Department> GetDepartmentPaged(int page, int count, out int totalCount,
            DepartmentFilter filter = null, OrderExpression orderExpression = null);
        IEnumerable<Department> GetDepartmentPaged(int page, int count, DepartmentFilter filter = null,
            OrderExpression orderExpression = null);
        IEnumerable<Department> GetDepartmentFilteredQueryable(DepartmentFilter filter = null);
       
    }

    public class DepartmentRepository : Repository<Department>, IDepartmentRepository
    {
        public DepartmentRepository(IDataContextAsync context, IUnitOfWorkAsync unitOfWork)
            : base(context, unitOfWork)
        {

        }
     
        public IEnumerable<Department> GetDepartmentPaged(int page, int count, out int totalCount,
            DepartmentFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new DepartmentQueryObject(filter).Expression;
            totalCount = Count(expression);
            return DepartmentPaged(page, count, expression, orderExpression);
        }
        public IEnumerable<Department> GetDepartmentPaged(int page, int count, DepartmentFilter filter = null, OrderExpression orderExpression = null)
        {
            var expression = new DepartmentQueryObject(filter).Expression;
            return DepartmentPaged(page, count, expression, orderExpression);
        }

        private IEnumerable<Department> DepartmentPaged(int page, int count, Expression<Func<Department, bool>> expression,
            OrderExpression orderExpression)
        {
            var order = ProcessOrderFunc(orderExpression);
            return Fetch(expression, order, page, count);
        }

        public IEnumerable<Department> GetDepartmentFilteredQueryable(DepartmentFilter filter = null)
        {
            var expression = new DepartmentQueryObject(filter).Expression;
            return Fetch(expression);
        }
        public static Func<IQueryable<Department>, IOrderedQueryable<Department>> ProcessOrderFunc(OrderExpression orderDeserilizer = null)
        {
            Func<IQueryable<Department>, IOrderedQueryable<Department>> orderFuction = (queryable) =>
            {
                var orderQueryable = queryable.OrderBy(x => x.Id);
                if (orderDeserilizer != null)
                {
                    switch (orderDeserilizer.Column)
                    {

                    }
                }
                return orderQueryable;
            };
            return orderFuction;
        }

        public class DepartmentQueryObject : QueryObject<Department>
        {
            public DepartmentQueryObject(DepartmentFilter filter)
            {
                if (filter != null)
                {
                    And(x => x.IsActive == filter.IsActive);
                }
            }
        }
    }
}
